module LandPlayEdit {
    export module i18n {
        export const es_ES = {
            mainMenu: {
                tabs: {
                    assets: "Recursos",
                    setup: "Configuración",
                    layout: "Edición",
                    preview: "Vista previa"
                }
            },
            button: {
                update: "Guardar",
                delete: "Borrar"
            },
            validator: {
                required: "Campo obligatorio",
                number: {
                    expected: "Se esperaba un número",
                    required: "Valor numérico obligatorio"
                },
                alpha_numeric: {
                    expected: "Formato incorrecto, sólo letras y números",
                    required: "Campo obligatorio, sólo letras y números"
                },
                identifier: {
                    expected: "Se esperaba un alfanumérico que no empieze por número",
                    required: "Alfanumérico requerido que no empieze por número"
                },
                gtzero: {
                    expected: "Se esperaba un número mayor que zero",
                    required: "Número obligatorio, mayor que zero"
                },
                selection: {
                    expected: "Valor esperado de selección (p.e.: 0-3,4,8,10-12)",
                    required: "Valor obligatorio de selección (p.e.: 0-3,4,8,10-12)"
                },
                color: {
                    expected: "Valor de color esperado (ej. #000000)",
                    required: "Valor de color esperado (ej. #000000)"
                },
                dimension: {
                    expected: "Número (eventualmente seguido de %) esperado",
                    required: "Número (eventualmente seguido de %) requerido"
                },
                option: {
                    eqzero: "El selector debe tener al menos una opción"
                },
                boolean: {
                    required: "Valor esperado 'true' o 'false'"
                }
            },
            option: {
                dimension: {
                    pixels: "Píxeles",
                    percentage: "Porcentage"
                },
                boolean: {
                    yes: "Sí",
                    no: "No"
                },
                confirm: {
                    yes: "Aceptar",
                    no: "Cancelar"
                },
                outbound: {
                    cycle: "Ciclo",
                    rebound: "Rebote"
                },
                pickable: {
                    true: "Al hacer clic",
                    false: "Al inicio"
                },
                async: {
                    true: "Al hacer clic en un botón",
                    false: "En cada evento"
                },
                mode: {
                    Arcade: {
                        label: "Arcade"
                    },
                    Catch: {
                        label: "Atrapar"
                    },
                    DragDrop: {
                        label: "Arrastrar y soltar"
                    },
                    Paint: {
                        label: "Pintar"
                    },
                    Puzzle: {
                        label: "Puzzle"
                    },
                    Simon: {
                        label: "Simon"
                    },
                    Type: {
                        label: "Teclear"
                    },
                    WordSearch: {
                        label: "Sopa de letras"
                    }
                },
                cause: {
                    right: {
                        label: "Respuesta correcta"
                    },
                    wrong: {
                        label: "Respuesta incorrecta"
                    },
                    tryout: {
                        label: "Reintentos superados"
                    },
                    complete: {
                        label: "Actividad completa"
                    },
                    hint: {
                        label: "Pista recibida"
                    },
                    timeout: {
                        label: "Tiempo finalizado"
                    }
                },
                effect: {
                    visible: {
                        label: "Cambiar visibilidad"
                    },
                    back: {
                        label: "Volver a su posición"
                    },
                    runaway: {
                        label: "Huir"
                    },
                    playsound: {
                        label: "Reproducir sonido"
                    },
                    lock: {
                        label: "Inmobilizar"
                    },
                    addpoints: {
                        label: "Sumar puntos"
                    },
                    destroy: {
                        label: "Destruir"
                    },
                    pause: {
                        label: "Pausar"
                    },
                    solve: {
                        label: "Solucionar"
                    },
                    validate: {
                        label: "Validar"
                    }
                },
                case: {
                    uppercase: "Mayúsculas",
                    lowercase: "Minúsculas",
                    capitalize: "Nombre propio"
                }
            },
            editorForm: {
                assets: {
                    origin: {
                        title: "Cargar archivo", 
                        help: "Este apartado te permite cargar recursos como imágenes o sonidos que te ayudarán a ilustrar la actividad",
                        upload: {
                            label: "OPCIÓN 1: Desde tu dispositivo",
                            help: "Selecciona un archivo explorando tu dispositivo. Puedes seleccionar más de uno para formar capas de un mismo recurso, pero deben estar numerados correlativamente."
                        },
                        url: {
                            label: "OPCIÓN 2: Desde internet",
                            help: "Busca imágenes con tu buscador preferido y pega el enlace en este cuadro de texto"
                        }
                    },
                    file: {
                        label: "Elige archivo:"
                    },
                    url: {
                        label: "Pegar enlace"
                    },
                    key: {
                        label: "identificador"
                    },
                    upload: {
                        label: "Cargar",
                        value: "CARGAR"
                    },
                    images: {
                        header: "Imágenes"
                    },
                    sounds: {
                        header: "Sonidos"
                    }
                },
                setup: {
                    mode: {
                        label: "Modalidad"
                    },
                    width: {
                        label: "Ancho"
                    },
                    height: {
                        label: "Alto"
                    }
                },
                layout: {
                    background: {
                        label: "Color de fondo"
                    },
                    loadLayout: {
                        label: "Recargar edición"
                    },
                    togglePanels: {
                        label: "Abrir/Cerrar paneles"
                    },
                    sidePanels: {
                        header: "Paneles laterales"
                    },
                    toolbox: {
                        header: "Caja de herramientas",
                        setup: {
                            header: "Configuración"
                        },
                        sprites: {
                            header: "Iconos"
                        },
                        behaviour: {
                            header: "Comportamiento"
                        },
                        Edit: {
                            tooltip: "Principales aspectos gráficos del icono"
                        },
                        Position: {
                            tooltip: "Arrastra un icono para esblecer su posición"
                        },
                        Text: {
                            tooltip: "Elementos textuales dentro de un icono"
                        },
                        Button: {
                            tooltip: "Convertir un icono en un botón"
                        },
                        locate: {
                            tooltip: "Destacar todos los iconos en la escena"
                        },
                        Grid: {
                            tooltip: "Distribuir el icono o sus fotogramas en formato parrilla"
                        },
                        Shape: {
                            tooltip: "Dibujar rectángulos o círculos en un icono"
                        },
                        retry: {
                            tooltip: "Sistema de reintentos"
                        },
                        counter: {
                            tooltip: "Contadores y barras de progreso"
                        },
                        events: {
                            tooltip: "Eventos"
                        },
                        frames: {
                            tooltip: "Recortar la imagen en fotogramas"
                        },
                        Depth: {
                            tooltip: "Cambiar la profunditdad de los iconos"
                        },
                        validation: {
                            tooltip: "Validación"
                        },
                        Anchor: {
                            tooltip: "Punto de anclaje del icono"
                        },
                        Embed: {
                            tooltip: "Arrastra un icono sobre otro para incrustarlo"
                        },
                        save: {
                            tooltip: "Descargar el archivo de configuración de la actividad"
                        },
                        clear: {
                            tooltip: "Diseña una nueva actividad"
                        }
                    }
                }
            },/*/editForm*/
            reveal: {
                generic: {
                    clear: "¿Seguro que quieres empezar el diseño de la actividad de cero?\nPor favor, asegúrate que antes guardaste su fichero de configuración."
                },
                text: {
                    empty: "Todavía no hay ningún texto",
                    delete: "¿Seguro que quieres borrar este texto?",
                    list: {
                        header: "Textos incrustados"
                    },
                    new: {
                        label: "Nuevo texto"
                    },
                    key: {
                        label: "Identificador"
                    },
                    label: {
                        label: "Etiqueta"
                    },
                    x: {
                        label: "X"
                    },
                    y: {
                        label: "Y"
                    },
                    style: {
                        label: "Estilo",
                        value: "Valor",
                        applied: "Estilos aplicados",
                        delete: "¿Estas seguro que quieres borrar los estilos seleccionados?"
                    }
                },
                event: {
                    empty: "Todavía no hay ningún evento",
                    delete: "¿Seguro que quieres borrar este evento?",
                    list: {
                        header: "Eventos existentes"
                    },
                    new: {
                        label: "Nuevo evento"
                    },
                    cause: {
                        label: "Causa",
                        help: "Puedes seleccionar más de una causa con la tecla Ctrl"
                    },
                    effect: {
                        label: "Efecto"
                    },
                    sprite: {
                        label: "Icono / Sonido",
                        all: "Todos los elemento móviles",
                        this: "Este icono"
                    },
                    value: {
                        label: "Valor"
                    }
                },
                shape: {
                    empty: "Todavía no hay ninguna forma",
                    delete: "¿Seguro que quieres borrar esta forma?",
                    list: {
                        header: "Formas existentes"
                    },
                    new: {
                        label: "Nueva forma"
                    },
                    type: {
                        label: "Tipo"
                    },
                    color: {
                        label: "Color"
                    },
                    radius: {
                        label: "Radio"
                    },
                    width: {
                        label: "Ancho"
                    },
                    height: {
                        label: "Alto"
                    },
                    line: {
                        width: {
                            label: "Ancho de línea"
                        },
                        color: {
                            label: "Color de línea"
                        }
                    }
                },
                sprite: {
                    delete: "¿Seguro que quieres borrar este icono?",
                    embed: "¿Seguro que quieres incrustar {{child}} en {{parent}}?",
                    id: {
                        label: "Nombre"
                    },
                    visible: {
                        label: "Visible"
                    },
                    width: {
                        label: "Ancho"
                    },
                    height: {
                        label: "Alto"
                    },
                    x: {
                        label: "X"
                    },
                    marginRight: {
                        label: "Margen derecho"
                    },
                    y: {
                        label: "Y"
                    },
                    marginBottom: {
                        label: "Margen inferior"
                    },
                    tint: {
                        label: "Pintura"
                    },
                    angle: {
                        label: "Ángulo"
                    },
                    frame: {
                        label: "Fotograma"
                    },
                    layers: {
                        from: {
                            label: "Número de capa inicial"
                        },
                        to: {
                            label: "Número de capa final"
                        }
                    },
                    children: {
                        label: "Gestionar hijos"
                    }
                },
                frame: {
                    nav: {
                        previous: "Fotograma anterior",
                        next: "Fotograma siguiente"
                    }
                },
                button: {
                    type: {
                        label: "Tipo de botón"
                    },
                    value: {
                        label: "Valor"
                    }
                },
                retry: {
                    delete: "¿Seguro que quieres borrar los reintentos?",
                    retries: {
                        label: "Reintentos",
                        help: "Total de errores permitidos",
                        info: "Los fallos decrementarán este contador. Se lanzará el evento 'tryout' cuando llegue a 0"
                    },
                    points: {
                        legend: "Evolución de puntos:"
                    },
                    from: {
                        label: "Desde"
                    },
                    to: {
                        label: "Hasta"
                    },
                    step: {
                        label: "Saltos"
                    },
                    validator: {
                        from: "Debe acompañar el 'hasta'",
                        to: "Debe acompañar el 'desde'",
                        toeqfrom: "'desde' y 'hasta' deben ser diferentes"
                    }
                },
                counter: {
                    delete: "¿Seguro que quieres borrar el comportamiento de los contadores?",
                    live: {
                        text: {
                            label: "Reintentos restantes",
                            help: "Texto que muestra actualizado los reintentos restantes"
                        }
                    },
                    points: {
                        text: {
                            label: "Puntos",
                            help: "Texto que muestra actualizado los puntos conseguidos en la actividad"
                        }
                    },
                    time: {
                        legend: "Tiempo:",
                        text: {
                            label: "Texto",
                            help: "Texto que muestra actualizado el tiempo restante"
                        },
                        secs: {
                            label: "Segundos"
                        }
                    },
                    progress: {
                        sprite: {
                            label: "Barra de progreso",
                            help: "Icono que mostrará el progreso de la actividad (si su modalidad lo contempla)"
                        }
                    }
                },
                grid: {
                    delete: "¿Seguro que quieres borrar esta parrilla?",
                    xoffset: {
                        label: "Desplazamiento X",
                        help: "Separación horizontal entre celdas"
                    },
                    yoffset: {
                        label: "Desplazamiento Y",
                        help: "Separación vertical entre celdas"
                    },
                    cols: {
                        label: "Columnas",
                        help: "Las filas vendran determinadas por el número total de celdas"
                    },
                    length: {
                        label: "Longitud"
                    },
                    selection: {
                        label: "Selección"
                    }
                },
                piece: {
                    empty: "Todavía no hay ninguna pieza",
                    delete: "¿Seguro que quieres borrar esta pieza?",
                    list: {
                        header: "Piezas existentes"
                    },
                    new: {
                        label: "Nueva pieza"
                    },
                    sprite: {
                        label: "Icono"
                    },
                    outbound: {
                        label: "Al salir de escena",
                        help: "Qué hacer cuando el icono toca el borde la escena"
                    },
                    movex: {
                        label: "Movimiento horizontal"
                    },
                    movey: {
                        label: "Movimiento vertical"
                    }
                },
                target: {
                    empty: "Todavía no hay ningún objetivo",
                    delete: "¿Seguro que quieres borrar el objetivo?",
                    list: {
                        header: "Objetivos existentes"
                    },
                    new: {
                        label: "Nuevo objetivo"
                    },
                    events: {
                        label: "Ver eventos del objetivo"
                    },
                    sprite: {
                        label: "Icono"
                    },
                    sound: {
                        label: "Sonido"
                    }
                },
                weapon: {
                    sprite: {
                        label: "Icono",
                        help: "La herramienta completa para pintar"
                    },
                    pickable: {
                        label: "Weapon usage"
                    },
                    brushpoint: {
                        label: "Icono de la punta del pincel",
                        help: "La parte del pincel que se moja en la pintura"
                    },
                    toolcase: {
                        label: "Caja para el Pincel",
                        help: "La caja para guardar el pincel"
                    }
                },
                children: {
                    empty: "Este Icono no tiene descendencia. Puedes añadir hijos arrastrándolos hacia él.",
                    delete: "¿Estás seguro que quieres deshacer la relación? El Icono hijo quedará disponible en la escena como elemento libre.",
                    list: {
                        header: "Hijos del Icono"
                    }
                },
                container: {
                    empty: "Todavía no se ha creado ningun contenedor",
                    delete: "¿Seguro que quieres borrar este contenedor?",
                    list: {
                        header: "Contenedores existentes"
                    },
                    new: {
                        label: "Nuevo contenedor"
                    },
                    sprite: {
                        label: "Icono",
                        help: "Icono(s) donde soltar las respuestas. Crear un contenedor con un icono vacío para aglutinar las respuestas incorrectas.",
                        empty: "{Sin icono}"
                    },
                    random: {
                        label: "Aleatorio",
                        help: "Intercambiar aleatoriamente las posiciones iniciales de las respuestas"
                    },
                    position: {
                        help: "Posición de la respuesta seleccionada relativa al contenedor"
                    },
                    x: {
                        label: "Izquierda"
                    },
                    y: {
                        label: "Superior"
                    },
                    offset: {
                        help: "Posición de la respuesta seleccionada relativa a la respuesta anterior (si se permite mas de una)"
                    },
                    xoffset: {
                        label: "Separación horizontal "
                    },
                    yoffset: {
                        label: "Separación vertical"
                    },
                    values: {
                        label: "Valores textuales",
                        help: "Textos separados por filas para las respuestas esperadas en este contenedor. Se incorporarán a los textos etiquetados como 'textTag' en las respuestas.\nCuando se seleccione una parrilla como icono deben tener el mismo orden que los elementos de la parrilla definidos.\nSi el número de filas es mayor que los elementos de la parrilla se incorporarán al azar."
                    },
                    extraValues: {
                        label: "Valores textuales adicionales",
                        help: "Textos separados por filas para las respuestas que introducen valores incorrectos"
                    },
                    answer: {
                        available: {
                            label: "Respuestas disponibles",
                            help: "Selecciona respuestas disponibles y pásalas al selector de la derecha"
                        },
                        selected: {
                            label: "Respuestas seleccionadas",
                            help: "Respuestas ya seleccionadas para este contenedor"
                        }
                    }
                },
                board: {
                    delete: "¿Seguro que quieres borrar el tablero del puzzle?",
                    table: {
                        label: "Tablero",
                        help: "Tablero donde soltar las piezas (al principio fuera del tablero). Si se deja vacío las piezas deben estar en su orden correcto y al soltar una sobre otra se intercambiarán las posiciones.",
                        random: {
                            label: "Aleatorio",
                            help: "Intercambiar las posiciones iniciales de las piezas"
                        }
                    },
                    available: {
                        label: "Iconos disponibles",
                        help: "Selecciona uno o más iconos y llévalos al selector de la derecha. Serán tomados como piezas del puzzle"
                    },
                    selected: {
                        label: "Iconos seleccionados",
                        help: "Piezas del puzzle. El orden es importante ya que define su validez sobre el tablero"
                    }
                },
                depth: {
                    empty: "Todavía no hay ningún icono",
                    list: {
                        header: "Orden de los iconos"
                    },
                    reload: {
                        label: "Recargar disposición"
                    }
                },
                validation: {
                    delete: "¿Seguro que quieres borra la validación?",
                    async: {
                        legend: "Iniciar validación:"
                    },
                    marks: {
                        legend: "Marcas de validación:"
                    },
                    right: {
                        key: {
                            label: "Recurso gráfico correcto",
                            help: "Las respuestas correctas se marcarán con esta imagen"
                        }
                    },
                    wrong: {
                        key: {
                            label: "Recurso gráfico incorrecto",
                            help: "Las respuestas incorrectas se marcarán con esta imagen"
                        }
                    },
                    top: {
                        label: "Posición superior",
                        help: "Posición superior de la marca (in)correcta, relativa a la respuesta"
                    },
                    left: {
                        label: "Posición izquierda",
                        help: "Posición izquierda de la marca (in)correcta, relativa a la respuesta"
                    },
                    width: {
                        label: "Ancho",
                        help: "Ancho (pixels o %) de la marca (in)correcta"
                    },
                    height: {
                        label: "Alto",
                        help: "Alto (pixels o %) de la marca (in)correcta"
                    }
                },
                color: {
                    empty: "Todavía no hi hay ningún color creado",
                    delete: "¿Seguro que quieres borrar este color?",
                    list: {
                        header: "Colores existentes"
                    },
                    new: {
                        label: "Nuevo color"
                    },
                    sprite: {
                        label: "Icono"
                    },
                    color: {
                        label: "Color"
                    }
                },
                canvas: {
                    empty: "Todavía no hay ningún lienzo creado",
                    delete: "¿Seguro que quieres borrar este lienzo?",
                    list: {
                        header: "Lienzos existentes"
                    },
                    new: {
                        label: "Nuevo lienzo"
                    },
                    sprite: {
                        label: "Icono"
                    },
                    color: {
                        label: "Icono de color"
                    }
                },
                brush: {
                    sprite: {
                        label: "Icono"
                    },
                    pickable: {
                        label: "Uso del pincel"
                    }
                },
                simon: {
                    transition: {
                        label: "Transición",
                        help: "Tiempo (en milisegundos) entre una pastilla y la siguiente"
                    },
                    maxseq: {
                        label: "Longitud de secuencia",
                        help: "Longitud de la secuencia aleatoria más larga"
                    },
                    talk: {
                        label: "Estado",
                        help: "Icono que cambia su estado de 'hablar' a 'escuchar'. Debe tener 3 fotogramas para representar el 'botón de inicio', 'reproduciendo' y 'escuchando'"
                    }
                },
                pad: {
                    delete: "¿Seguro que quieres borrar esta pastilla?",
                    sprite: {
                        label: "Icono",
                        help: "Icono con 2 fotogramas para destacar la que está sonando."
                    },
                    sound: {
                        label: "Sonido",
                        help: "Sonido que se reproduce mientras se destaca el icono"
                    },
                    selected: {
                        label: "Pastillas seleccionadas",
                        help: "Las pastillas que se alternarán en secuencias al azar"
                    }
                },
                stage: {
                    speed: {
                        label: "Velocidad",
                        help: "Velocidad relativa. Variable que multiplica la velocidad de todos los objetos en movimiento."
                    },
                    craftspeed: {
                        label: "Velocidad del jugador",
                        help: "Velocidad relativa del jugador"
                    },
                    runawaySpeed: {
                        label: "Velocidad de huída",
                        help: "Velocidad relativa de todos los objetos en movimiento una vez la pantalla se soluciona."
                    },
                    debug: {
                        label: "Depuración",
                        help: "Muestra el cuerpo efectivo de colisión"
                    },
                    craft: {
                        label: "Aspecto del jugador",
                        help: "Icono que el usuario manejará con el teclado"
                    },
                    floor2d: {
                        label: "Suelo 2D",
                        help: "El jugador se moverá a lo largo del eje Y (teclas arriba/abajo del teclado). Sin esta opción la tecla 'arriba' será para saltar"
                    },
                    ygravity: {
                        label: "Gravedad (Y)",
                        help: "Fuerza de gravedad que afecta al jugador"
                    },
                    movex: {
                        label: "Movimiento X",
                        help: "Pixels Horizontales que se moverá (adelante/atrás)"
                    },
                    movey: {
                        label: "Movimiento Y",
                        help: "Pixels Verticales que se moverá (arriba/abajo). Sólo si está establecido el suelo 2D"
                    },
                    "y-scale": {
                        label: "Y-Scale",
                        help: "Según el jugador se mueve a lo largo del eje Y cambia su dimensión N milesimas partes del porcentage de su tamaño, para dar sensación de profundidad. Solo cuando hay suelo 2D"
                    },
                    tolerance: {
                        label: "Tolerancia",
                        help: "Hace que, en el momento de la colisión, el jugador parezca N pixels más pequeño."
                    },
                    "y-jump": {
                        label: "Salto (Y)",
                        help: "Altura del salto en pixels"
                    },
                    "jump-touch": {
                        label: "Empuje de salto",
                        help: "Sólo cuando el jugador toque el suelo"
                    },
                    ybounce: {
                        label: "Rebote (Y)",
                        help: "Valor entre 0 y 100% (0 sin rebote, 100 rebote infinito)"
                    },
                    animations: {
                        legend: "Animaciones del jugador"
                    },
                    move: {
                        label: "Mover",
                        help: "Se repetirá cuando el jugador esté ocioso"
                    },
                    jump: {
                        label: "Saltar",
                        help: "Se producirá cuando esté saltando"
                    },
                    forward: {
                        label: "Adelante",
                        help: "Se repetirá cuando vaya hacia adelante"
                    },
                    up: {
                        label: "Arriba",
                        help: "Se repetirá cuando vaya hacia arriba"
                    },
                    down: {
                        label: "Abajo",
                        help: "Se repetirá cuando vaya hacia abajo"
                    },
                    back: {
                        label: "Atrás",
                        help: "Se repetirá cuando vaya hacia atrás"
                    }
                },
                asteroid: {
                    empty: "Ningún asteroide creado todavía",
                    delete: "¿Seguro que quieres borrar este asteroide?",
                    list: {
                        header: "Asteroides creados"
                    },
                    new: {
                        label: "Nuevo Asteroide"
                    },
                    events: {
                        label: "Gestionar events"
                    },
                    attach: {
                        label: "Recurso gráfico"
                    },
                    multiplicity: {
                        label: "Multiplicidad",
                        help: "El número de copias que se lanzarán durante la pantalla."
                    },
                    cycle: {
                        label: "Ciclo",
                        help: "PENDIENTE DE REVISAR. EL asteroide será reciclado para lanzarlo de nuevo."
                    },
                    movex: {
                        label: "Movimiento-X",
                        help: "Rango de velocidades aleatorio en el que se moveran horizontalmente las instancias de este asteroide"
                    },
                    movey: {
                        label: "Movimiento-Y",
                        help: "Rango de velocidades aleatorio en el que se moveran verticalmente las instancias de este asteroide"
                    },
                    tics: {
                        label: "Tics",
                        help: "Rango de segundos de espera aleatorio en el que se lanzarán las instancias de este asteroide"
                    },
                    xdif: {
                        label: "Posición X",
                        help: "Rango de posicionces horizontales aleatorio en el que se situarán, inicialmente, las instancias de este asteroide"
                    },
                    ydif: {
                        label: "Posición Y",
                        help: "Rango de posicionces verticales aleatorio en el que se situarán, inicialmente, las instancias de este asteroide"
                    },
                    ygravity: {
                        label: "Gravedad Y",
                        help: "Nivel de fuerza de gravedad que afecta al asteroide"
                    },
                    basedepth: {
                        label: "Profundidad de la base",
                        help: "Cuanto mayor es este valor más probabilidad de que el jugador toque la pieza al pasar por detrás. Sólo aplicable cuando hay suelo 2D."
                    },
                    tolerance: {
                        label: "Tolerancia",
                        help: "Hacer el asteroide N pixels más estrecho para reducir la probabilidad de colisión"
                    }
                },
                landscape: {
                    empty: "Ningún paisaje creado todavía",
                    delete: "¿Seguro que quieres borrar este paisaje?",
                    list: {
                        header: "Paisajes creados"
                    },
                    new: {
                        label: "Nuevo paisaje"
                    },
                    sprite: {
                        label: "Icono"
                    },
                    moveX: {
                        label: "Movimiento X",
                        help: "Velocidad horizontal para los paisajes"
                    },
                    foreground: {
                        label: "Paisaje frontal",
                        help: "Posicionado al frente desde el punto de vista del usuario. La acción transcurrirá 'detrás' suyo"
                    }
                },
                word: {
                    empty: "Ninguna palabra creada todavía",
                    delete: "¿Seguro que quieres borrar esta palabra?",
                    list: {
                        header: "Palabras creadas"
                    },
                    new: {
                        label: "Palabra nueva"
                    },
                    id: {
                        label: "Id",
                        help: "Identificador de la palabra. Útil cuando hay más de una palabra en la actividad"
                    },
                    key: {
                        label: "Imagen",
                        help: "Recurso gráfico como fondo de la palabra (o de cada letra)"
                    },
                    sprite: {
                        label: "Icono",
                        help: "Icono como fondo de la palabra (o de cada letra)"
                    },
                    x: {
                        label: "Posición X",
                        help: "Posición horizontal inicial"
                    },
                    y: {
                        label: "Posición Y",
                        help: "Posición vertical inicial"
                    },
                    text: {
                        label: "Palabra",
                        help: "Texto a resolver"
                    },
                    opentext: {
                        label: "Texto abierto",
                        help: "Texto no específico y/o no validable",
                        row: "No se espera ningún texto específic"
                    },
                    split: {
                        legend: "Separar la palabra en letras",
                        help: "Deja ambos vacíos si quieres que la palabra aparezca entera en un sólo campo de texto",
                        offsetx: {
                            label: "Separación horizontal",
                            help: "Separación horizontal entre los contenedores de letras"
                        },
                        offsety: {
                            label: "Separación vertical",
                            help: "Separación vertical entre los contenedores de letras"
                        }
                    },
                    offset: {
                        legend: "Posición del texto"
                    },
                    offsetx: {
                        label: "Distancia superior",
                        help: "Distancia superior respecto a su contenedor"
                    },
                    offsety: {
                        label: "Distancia izquierda",
                        help: "Distancia a la izquierda respecto a su contenedor"
                    }
                },
                tiles: {
                    case: {
                        label: "Mayúsculas/minúsculas",
                        help: "El formato en el que aparecen las letras en las celdas"
                    },
                    width: {
                        label: "Ancho",
                        help: "Ancho de las celdas de la sopa de letras"
                    },
                    height: {
                        label: "Alto",
                        help: "Alto de las celdas de la sopa de letras"
                    },
                    cols: {
                        label: "Columnas",
                        help: "Número de columnas de la sopa de letras"
                    },
                    rows: {
                        label: "Filas",
                        help: "Número de filas de la sopa de letras"
                    },
                    color: {
                        label: "Color de celda",
                        help: "Añadir un color a los colores posibles de fondo de celdas",
                        
                        right: {
                            label: "Color de palabra"
                        }
                    },
                    colors: {
                        label: "Colores de celda",
                        help: "Colores posibles de fondo de celdas",
                        
                        right: {
                            label: "Colores de palabra",
                            help: "Colores de fondo que se iran alternado conforme vayan descubriendo las palabras"
                        },
                        
                        current: {
                            label: "Palabra actual",
                            help: "Colores de fondo de la palabra que el usuario está desvelando"
                        }
                        
                    }
                },
                hiddenword:{
                    text: {
                        label: "Texto"
                    },
                    sprite: {
                        label: "Icono",
                        help: "Icono que representa la palabra escondida. Si incorpora un texto con el identificador 'textTag' mostrará el texto en cuestión. Se marcarán si se han establecido los iconos de validación"
                    },
                    x: {
                        label: "Columna",
                        help: "Columna en la que empieza la palabra"
                    },
                    y: {
                        label: "Fila",
                        help: "Fila en la que empieza la palabra"
                    },
                    orientation: {
                        label: "Orientación"
                    },
                    preview: {
                        label: "Vista previa"
                    },
                    check: {
                        label: "Disposición correcta"
                    }
                }
            }, /*/reveal*/
            plugin: {
                loaded: "Fichero de plantilla cargado '{{file}}'",
                constraint: {
                    header: "Por favor completa los siguientes requisitos:",
                    catch: {
                        target: "Debes definir uno o más objetivos.",
                        weapon: "You must define a weapon.",
                        piece: "Debes definir una o más piezas.",
                        runaway: "Para definir un evento de escape tienes que proporcionar un valor numérico a 'Velocidad de escape' en la pestaña de 'Configuración'"
                    },
                    paint: {
                        canvas: "Debes definir uno o más lienzos.",
                        brush: "Debes definir un pincel.",
                        color: "Debes definir uno o más botes de pintura.",
                        noColorSprites: "Antes de definir ningún lienzo debes definir uno o más botes de pintura."
                    },
                    validation: {
                        required: "Necesitas definir la validación para ejecutar esta actividad",
                        noButton: "El modo de validación asíncrona requiere un botón de validar"
                    },
                    sprite: {
                        notFound: "Icono '{{sprite}}', declared as '{{type}}', does not exist"
                    },
                    dragdrop: {
                        container: "Debes definir uno o más contenedores.",
                        nosprite: "Al menos un contenedor debe tener un icono definido.",
                        event: {
                            zero: "No se ha seleccionado ninguna respuesta para editar sus eventos. Por favor, selecciona (sólo) una.",
                            gtone: "Se ha seleccionado más de una respuesta para editar sus eventos. Por favor, selecciona sólo una."
                        }
                    },
                    puzzle: {
                        piece: "Debes definir dos o más piezas para un puzzle."
                    },
                    simon: {
                        pad: {
                            nosprite: "Por favor selecciona un icono",
                            nosound: "Por favor selecciona un sonido",
                            notenough: "Por favor selecciona al menos 2 pastillas para jugar al Simon",
                            noframes: "La pastilla '{{sprite}}' debe tener al menos 2 fotogramas"
                        },
                        noprogress: "Por favor selecciona una barra de progreso",
                        talk: {
                            noframes: "El icono de estado '{{sprite}}' debe tener al menos 3 fotogramas"
                        }
                    },
                    arcade: {
                        craft: "Por favor define un icono para el jugador",
                        piece: {
                            empty: "Por favor define al menos un asteroide",
                            noevents: "Por favor define eventos para el asteroide '{{asteroid}}'"
                        }
                    },
                    type: {
                        word: "Por favor define al menos una palabra"
                    },
                    wordsearch: {
                        hiddenword: "Por favor define al menos una palabra oculta",
                        notextsprite: "El icono '{{sprite}}' debe tener un texto con la clave 'textTag'"
                    }
                    
                },
                toolbox: {
                    catch: {
                        piece: {
                            label: "Definir iconos como posibles respuestas"
                        },
                        target: {
                            label: "Definir iconos como respuestas correctas"
                        },
                        weapon: {
                            label: "Definir un icono como la herramienta",
                        }
                    },
                    paint: {
                        brush: {
                            label: "Definir un icono como pincel"
                        },
                        color: {
                            label: "Definir iconos como botes de pintura"
                        },
                        canvas: {
                            label: "Definir iconos como lienzos",
                        }
                    },
                    dragdrop: {
                        container: {
                            label: "Definir iconos como contenedores donde soltar las respuestas"
                        }
                    },
                    puzzle: {
                        board: {
                            label: "Definir iconos como elementos de un puzzle"
                        }
                    },
                    simon: {
                        pad: {
                            label: "Definir las pastillas del simon"
                        }
                    },
                    arcade: {
                        stage: {
                            label: "Definir las bases de la pantalla"
                        },
                        asteroid: {
                            label: "Definir enemigos y objetivos"
                        },
                        edge: {
                            label: "Definir las barreras de la pantalla"
                        },
                        landscape: {
                            label: "Definir elementos de entorno. No se interactua directamente con ellos."
                        }
                    },
                    type: {
                        word: {
                            label: "Definir las palabras a solucionar"
                        }
                    },
                    wordsearch: {
                        tiles: {
                            label: "Definir las dimensiones de la sopa de letras"
                        },
                        hiddenword: {
                            label: "Definir las palabras ocultas"
                        },
                        decorate: {
                            label: "Definir colores adicionales"
                        }
                    }
                },
                field: {
                    setup: {
                        speed: {
                            label: "Velocidad",
                            help: "Velocidad relativa para todas las piezas"
                        },
                        runawaySpeed: {
                            label: "Velocidad de escape",
                            help: "Velocidad relativa para la huída de las piezas"
                        }
                    }
                }
            }, /*plugin*/
            constraint: {
                asset: {
                    none: "No hay ningún recurso gráfico definido. Por favor visita la pestaña '{{asset}}'",
                    keyExists: "El identificador '{{key}}' ya existe como '{{subtype}}', ¿sobrescribir?",
                    novalid: "Por favor selecciona un fichero válido {{sample}}. Estás subiendo un {{type}}",
                    layers: {
                        unmatch: "Los ficheros seleccionados no tienen el mismo patrón (ej.: cualquiera_<<numero>>.png)",
                        nonconsecutive: "Los ficheros seleccionados no tienen sufijos numéricos correlativos.",
                        justimage: "La selección múltiple de ficheros solo es posible con imágenes."
                    }
                }
            }, /*constraint*/
            alert: {
                upload: {
                    failed: "La subida de archivos ha fallado, por favor intentar de nuevo.",
                    novalid: "Por favor selecciona un fichero válido."
                },
                download: {
                    failed: "La descarga del archivos ha fallado, por favor intentar con otro enlace.",
                    write: "Aunque el fichero se ha podido descargar, ha habido algun error al guardarlo.",
                    nourl: "Por favor proporciona una ubicación de fichero."
                },
                sprite: {
                    exists: "El identificador de icono '{{id}}' ya existe, por favor elije otro"
                },
                preview: {
                    nomode: "No se ha seleccionado ninguna modalidad"
                }
            }, /*alert*/
            warning: {
                color: {
                    empty: "No se ha seleccionado ningún color"
                }
            },
            success: {
                upload: "¡El fichero se ha subido satisfactoriamente!",
                anchor: "El anclage ha cambiado satisfactoriamente"
            }
        }
    }
}