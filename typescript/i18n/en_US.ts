module LandPlayEdit {
    export module i18n {
        export const en_US = {
            mainMenu: {
                tabs: {
                    assets: "Assets",
                    setup: "Setup",
                    layout: "Edition",
                    preview: "Preview"
                }
            },
            button: {
                update: "Update",
                delete: "Delete"
            },
            validator: {
                required: "Value required",
                number: {
                    expected: "Number value expected",
                    required: "Number value required"
                },
                alpha_numeric: {
                    expected: "Wrong format, just letters and numbers expected",
                    required: "Name value required, just letters and numbers"
                },
                identifier: {
                    expected: "Alpha-numeric expected that should not start with numbers",
                    required: "Alpha-numeric required that should not start with numbers"
                },
                gtzero: {
                    expected: "Number expected, greater than zero",
                    required: "Number required, greater than zero"
                },
                selection: {
                    expected: "Expected value (e.g.: 0-3,4,8,10-12)",
                    required: "Required value (e.g.: 0-3,4,8,10-12)"
                },
                color: {
                    expected: "Color expected (e.g. #000000)",
                    required: "Color required (e.g. #000000)"
                },
                dimension: {
                    expected: "Number (eventually followed by %) expected",
                    required: "Number (eventually followed by %) required"
                },
                option: {
                    eqzero: "Select must have at least one option"
                },
                boolean: {
                    required: "'true' or 'false' value expected"
                }
            },
            option: {
                dimension: {
                    pixels: "Pixels",
                    percentage: "Pencentage"
                },
                boolean: {
                    yes: "Yes",
                    no: "No"
                },
                confirm: {
                    yes: "Accept",
                    no: "Cancel"
                },
                outbound: {
                    cycle: "Cycle",
                    rebound: "Rebound"
                },
                pickable: {
                    true: "On Click",
                    false: "On Start"
                },
                async: {
                    true: "On click a button",
                    false: "On each Event"
                },
                mode: {
                    Arcade: {
                        label: "Arcade"
                    },
                    Catch: {
                        label: "Catch"
                    },
                    DragDrop: {
                        label: "Drag & Drop"
                    },
                    Paint: {
                        label: "Paint"
                    },
                    Puzzle: {
                        label: "Puzzle"
                    },
                    Simon: {
                        label: "Simon"
                    },
                    Type: {
                        label: "Type"
                    },
                    WordSearch: {
                        label: "Word Search"
                    }
                },
                cause: {
                    right: {
                        label: "Right answer"
                    },
                    wrong: {
                        label: "Wrong answer"
                    },
                    tryout: {
                        label: "Tryout"
                    },
                    complete: {
                        label: "Completed activity"
                    },
                    hint: {
                        label: "Received Hint"
                    },
                    timeout: {
                        label: "Timeout"
                    }
                },
                effect: {
                    visible: {
                        label: "Change visibility"
                    },
                    back: {
                        label: "Back to its position"
                    },
                    runaway: {
                        label: "Runaway"
                    },
                    playsound: {
                        label: "Play a sound"
                    },
                    lock: {
                        label: "Lock"
                    },
                    addpoints: {
                        label: "Add points"
                    },
                    destroy: {
                        label: "Destroy"
                    },
                    pause: {
                        label: "Pause"
                    },
                    solve: {
                        label: "Solve"
                    },
                    validate: {
                        label: "Validate"
                    }
                },
                case: {
                    uppercase: "Uppercase",
                    lowercase: "Lowercase",
                    capitalize: "Capitalize"
                }
            },
            editorForm: {
                assets: {
                    origin: {
                        title: "Load file",
                        help: "This stage allows you to load assets as images or sound files that help you illustrate the activity",
                        upload: {
                            label: "OPTION 1: from your device",
                            help: "Select a file exploring your device. You can select more than one to load layers for the same image asset, but their names must contain a correlative number as a suffix."
                        },
                        url: {
                            label: "OPTION 2: link from internet",
                            help: "Search images with your prefered searcher and paste the link in this text field"
                        }
                    },
                    file: {
                        label: "Choose file:"
                    },
                    url: {
                        label: "Paste link"
                    },
                    key: {
                        label: "key"
                    },
                    upload: {
                        label: "Load",
                        value: "LOAD"
                    },
                    images: {
                        header: "Images"
                    },
                    sounds: {
                        header: "Sounds"
                    }
                },
                setup: {
                    mode: {
                        label: "Mode"
                    },
                    width: {
                        label: "Width"
                    },
                    height: {
                        label: "Height"
                    }
                },
                layout: {
                    background: {
                        label: "Background color"
                    },
                    loadLayout: {
                        label: "Reload Editor"
                    },
                    togglePanels: {
                        label: "Toggle Panels"
                    },
                    sidePanels: {
                        header: "Side Panels"
                    },
                    toolbox: {
                        header: "Toolbox",
                        setup: {
                            header: "Setup"
                        },
                        sprites: {
                            header: "Sprites"
                        },
                        behaviour: {
                            header: "Behaviour"
                        },
                        Edit: {
                            tooltip: "Main graphic aspects of the sprite"
                        },
                        Position: {
                            tooltip: "Drag an sprite to stablish its position"
                        },
                        Text: {
                            tooltip: "Text elements in a sprite"
                        },
                        Button: {
                            tooltip: "Convert a sprite in a button"
                        },
                        locate: {
                            tooltip: "Highlight all the sprites in the scene"
                        },
                        Grid: {
                            tooltip: "Repeat the sprite (frames) in grid"
                        },
                        Shape: {
                            tooltip: "Draw squares or circles in a sprite"
                        },
                        retry: {
                            tooltip: "Retry system"
                        },
                        counter: {
                            tooltip: "Counters and progress bars"
                        },
                        events: {
                            tooltip: "Events"
                        },
                        frames: {
                            tooltip: "Dynamic cut of the frames"
                        },
                        Depth: {
                            tooltip: "Swap Sprite Depths"
                        },
                        validation: {
                            tooltip: "Validation"
                        },
                        Anchor: {
                            tooltip: "Anchor point of the Sprite"
                        },
                        Embed: {
                            tooltip: "Drag an sprite over another to embed it"
                        },
                        save: {
                            tooltip: "Download configuration file"
                        },
                        clear: {
                            tooltip: "Start a new activity design"
                        }
                    }
                }
            },/*/editForm*/
            reveal: {
                generic: {
                    clear: "Are you sure you want to clear this activity?\nPlease, be sure you previously saved this configuration file."
                },
                text: {
                    empty: "No texts yet embeded",
                    delete: "Are you sure you want to delete this Text?",
                    list: {
                        header: "Embeded texts"
                    },
                    new: {
                        label: "New Text"
                    },
                    key: {
                        label: "Key"
                    },
                    label: {
                        label: "Label"
                    },
                    x: {
                        label: "X"
                    },
                    y: {
                        label: "Y"
                    },
                    style: {
                        label: "Style",
                        value: "Value",
                        applied: "Applied Styles",
                        delete: "Are you sure you want to delete selected styles?"
                    }
                },
                event: {
                    empty: "No events yet embeded",
                    delete: "Are you sure you want to delete this event?",
                    list: {
                        header: "Existing events"
                    },
                    new: {
                        label: "New Event"
                    },
                    cause: {
                        label: "Cause",
                        help: "You can select more than one causes using Ctrl key"
                    },
                    effect: {
                        label: "Effect"
                    },
                    sprite: {
                        label: "Sprite / Sound",
                        all: "All sprites",
                        this: "This sprite"
                    },
                    value: {
                        label: "Value"
                    }
                },
                shape: {
                    empty: "No shapes yet embeded",
                    delete: "Are you sure you want to delete this shape?",
                    list: {
                        header: "Existing shapes"
                    },
                    new: {
                        label: "New Shape"
                    },
                    type: {
                        label: "Type"
                    },
                    color: {
                        label: "Color"
                    },
                    radius: {
                        label: "Radius"
                    },
                    width: {
                        label: "Width"
                    },
                    height: {
                        label: "Height"
                    },
                    line: {
                        width: {
                            label: "Line Width"
                        },
                        color: {
                            label: "Line Color"
                        }
                    }
                },
                sprite: {
                    delete: "Do you really want to delete this sprite?",
                    button: "Sprite deletion will delete this button: {{button}} ",
                    embed: "Do you really want to embed '{{child}}' into '{{parent}}'?",
                    id: {
                        label: "Name"
                    },
                    visible: {
                        label: "Visible"
                    },
                    width: {
                        label: "Width"
                    },
                    height: {
                        label: "Height"
                    },
                    x: {
                        label: "X"
                    },
                    marginRight: {
                        label: "Margin Right"
                    },
                    y: {
                        label: "Y"
                    },
                    marginBottom: {
                        label: "Margin Bottom"
                    },
                    tint: {
                        label: "Tint"
                    },
                    angle: {
                        label: "Angle"
                    },
                    frame: {
                        label: "Frame"
                    },
                    layers: {
                        from: {
                            label: "From Layer Number"
                        },
                        to: {
                            label: "To Layer Number"
                        }
                    },
                    children: {
                        label: "Manage children"
                    }
                },
                frame: {
                    nav: {
                        previous: "Previous Frame",
                        next: "Next Frame"
                    }
                },
                button: {
                    type: {
                        label: "Button Type"
                    },
                    value: {
                        label: "Value"
                    }
                },
                retry: {
                    delete: "Are you sure you want to delete retry behaviour?",
                    retries: {
                        label: "Retries",
                        help: "Maximum number of failures allowed",
                        info: "Failure events will decrease this counter. Tryout event is launched once it arrives to 0."
                    },
                    points: {
                        legend: "Points evolution:"
                    },
                    from: {
                        label: "From"
                    },
                    to: {
                        label: "To"
                    },
                    step: {
                        label: "Step"
                    },
                    validator: {
                        from: "From must be set, since To is",
                        to: "To must be set, since From is",
                        toeqfrom: "From and To must be different"
                    }
                },
                counter: {
                    delete: "Are you sure you want to delete counter behaviour?",
                    live: {
                        text: {
                            label: "Retries left",
                            help: "Text container that will show remaining failures"
                        }
                    },
                    points: {
                        text: {
                            label: "Points",
                            help: "Text container that will show points"
                        }
                    },
                    time: {
                        legend: "Timer:",
                        text: {
                            label: "Time text",
                            help: "Text container that will remaining time"
                        },
                        secs: {
                            label: "Seconds"
                        }
                    },
                    progress: {
                        sprite: {
                            label: "Progress Bar",
                            help: "Sprite that will be used as progress bar (where available)"
                        }
                    }
                },
                grid: {
                    delete: "Are you sure you want to delete this sprite grid?",
                    xoffset: {
                        label: "X offset",
                        help: "Horizontal separation between cells"
                    },
                    yoffset: {
                        label: "Y offset",
                        help: "Vertical separation between cells"
                    },
                    cols: {
                        label: "Columns",
                        help: "Rows will be determined by the total number of cells"
                    },
                    length: {
                        label: "Length"
                    },
                    selection: {
                        label: "Selection"
                    }
                },
                piece: {
                    empty: "No pieces yet created",
                    delete: "Are you sure you want to delete this piece?",
                    list: {
                        header: "Existing pieces"
                    },
                    new: {
                        label: "New Piece"
                    },
                    sprite: {
                        label: "Sprite"
                    },
                    outbound: {
                        label: "Outbound behaviour",
                        help: "What to do when sprite hits the edge of the stage"
                    },
                    movex: {
                        label: "Horizontal Move"
                    },
                    movey: {
                        label: "Vertical Move"
                    }
                },
                target: {
                    empty: "No targets yet created",
                    delete: "Are you sure you want to delete this target?",
                    list: {
                        header: "Existing targets"
                    },
                    new: {
                        label: "New Target"
                    },
                    events: {
                        label: "Manage target events"
                    },
                    sprite: {
                        label: "Sprite"
                    },
                    sound: {
                        label: "Sound"
                    }
                },
                weapon: {
                    sprite: {
                        label: "Sprite"
                    },
                    pickable: {
                        label: "Weapon usage"
                    }
                },
                depth: {
                    empty: "No Sprites yet defined",
                    list: {
                        header: "Sprites Order"
                    },
                    reload: {
                        label: "Reload Layout"
                    }
                },
                validation: {
                    delete: "Are you sure you want to delete validation?",
                    async: {
                        legend: "Validation Launcher:"
                    },
                    marks: {
                        legend: "Validation Marks:"
                    },
                    right: {
                        key: {
                            label: "Right image asset",
                            help: "Right responses will be marked with this image"
                        }
                    },
                    wrong: {
                        key: {
                            label: "Wrong image asset",
                            help: "Wrong responses will be marked with this image"
                        }
                    },
                    top: {
                        label: "Top position",
                        help: "Relative top position from the response for the right/wrong mark"
                    },
                    left: {
                        label: "Left position",
                        help: "Relative left position from the response for the right/wrong mark"
                    },
                    width: {
                        label: "Width",
                        help: "Width (pixels or %) of the right/wrong mark"
                    },
                    height: {
                        label: "Height",
                        help: "Height (pixels or %) of the right/wrong mark"
                    }
                },
                color: {
                    empty: "No colors yet created",
                    delete: "Are you sure you want to delete this color?",
                    list: {
                        header: "Existing colors"
                    },
                    new: {
                        label: "New Color"
                    },
                    sprite: {
                        label: "Sprite"
                    },
                    color: {
                        label: "Color"
                    }
                },
                canvas: {
                    empty: "No canvas yet created",
                    delete: "Are you sure you want to delete this canvas?",
                    list: {
                        header: "Existing canvases"
                    },
                    new: {
                        label: "New Canvas"
                    },
                    sprite: {
                        label: "Sprite"
                    },
                    color: {
                        label: "Color Sprite"
                    }
                },
                brush: {
                    sprite: {
                        label: "Brush Sprite",
                        help: "The Whole Painting Tool"
                    },
                    pickable: {
                        label: "Brush usage"
                    },
                    brushpoint: {
                        label: "Brush point Sprite",
                        help: "The Paintint tool part that gets tinted"
                    },
                    toolcase: {
                        label: "Brush tool case Sprite",
                        help: "The case to save the paintint tool"
                    }
                },
                children: {
                    empty: "This Sprite has no children. You can add existing sprites by dragging them over it.",
                    delete: "Are you sure you want to detatch this sprite as a child? It will be available in the scene by itself.",
                    list: {
                        header: "Sprite Children"
                    }
                },
                container: {
                    empty: "No containers yet created",
                    delete: "Are you sure you want to delete this container?",
                    list: {
                        header: "Existing containers"
                    },
                    new: {
                        label: "New Container"
                    },
                    sprite: {
                        label: "Sprite",
                        help: "Sprite(s) to drop the answers in. Create a container with an empty one if you want to bring incorrect answers together.",
                        empty: "{No sprite}"
                    },
                    random: {
                        label: "Random",
                        help: "Swap answer positions randomly"
                    },
                    position: {
                        help: "Container-relative position of the droped answer"
                    },
                    x: {
                        label: "Left"
                    },
                    y: {
                        label: "Top"
                    },
                    offset: {
                        help: "Previous-answer-relative position of the droped answer (if more than one)"
                    },
                    xoffset: {
                        label: "Horizontal separation"
                    },
                    yoffset: {
                        label: "Vertical separation"
                    },
                    values: {
                        label: "Text Values",
                        help: "Line-separated textual values for answers expected in this container. They will be embeded in 'textTag' texts inside the answers.\nWhen a grid is selected as sprite they must have the same order as grid selected as containers.\nIf the number of rows is grater than elements of the grid they will be picked random."
                    },
                    extraValues: {
                        label: "Additional Text Values",
                        help: "Line-separated textual values for wrong additional answers, not expected in this container"
                    },
                    answer: {
                        available: {
                            label: "Available Answers",
                            help: "Select available answers and bring them to the right (side) selector"
                        },
                        selected: {
                            label: "Selected Answers",
                            help: "Already selected answers for this container"
                        }
                    }
                },
                board: {
                    delete: "Are you sure you want to delete the puzzle board?",
                    table: {
                        label: "Board",
                        help: "Board to drop the pieces (at the begining out of the board) over. If empty, pieces must be in their right place and swap their positions on drop.",
                        random: {
                            label: "Random",
                            help: "Swap initial pieces positions"
                        }
                    },
                    available: {
                        label: "Available Sprites",
                        help: "Select one or more sprites and bring them to the right (side) selector. They will be taken as puzzle pieces"
                    },
                    selected: {
                        label: "Selected Sprites",
                        help: "Pieces of the puzzle. Order matters to define its validity."
                    }
                },
                simon: {
                    transition: {
                        label: "Transition",
                        help: "Time (milliseconds) between one color/sound and the next"
                    },
                    maxseq: {
                        label: "Sequence length",
                        help: "Length of the last sequence"
                    },
                    talk: {
                        label: "Talk",
                        help: "Sprite that will toggle from listening to talking state. It must have 3 frames to represent 'start button', 'playing' and 'listening'"
                    }
                },
                pad: {
                    delete: "Do you really want to delete this pad?",
                    sprite: {
                        label: "Sprite",
                        help: "Sprite with 2 frames to highlight the one that is playing."
                    },
                    sound: {
                        label: "Sound",
                        help: "Sound that will play while pad shows."
                    },
                    selected: {
                        label: "Selected pads",
                        help: "Pads that will be randomized along a sequence"
                    }
                },
                stage: {
                    speed: {
                        label: "Speed",
                        help: "Relative speed. Variable to multiply by all the moving items."
                    },
                    craftspeed: {
                        label: "Craft Speed",
                        help: "Relative speed of the craft"
                    },
                    runawaySpeed: {
                        label: "Runaway Speed",
                        help: "Relative speed of all the moving objects once stage is cleared"
                    },
                    debug: {
                        label: "Debug",
                        help: "Shows collission edges"
                    },
                    craft: {
                        label: "Player Character",
                        help: "Sprite that the player will control with the keyboard"
                    },
                    floor2d: {
                        label: "2D floor",
                        help: "Player will move along y-axis (keyboard up/down arrows). Otherwise up key will be for jump"
                    },
                    ygravity: {
                        label: "Y Gravity",
                        help: "Gravity force that affects to the player."
                    },
                    movex: {
                        label: "X-Move",
                        help: "Horizontal pixels to move (forward/backward)"
                    },
                    movey: {
                        label: "Y-Move",
                        help: "Vertical pixels to move (up/down)"
                    },
                    "y-scale": {
                        label: "Y-Scale",
                        help: "As craft moves along y axis changes its scale (n/1000), to give it depth sense. Just when 2D floor"
                    },
                    tolerance: {
                        label: "Tolerance",
                        help: "Make the player N pixels thiner when collission"
                    },
                    "y-jump": {
                        label: "Y-Jump",
                        help: "Height of the jump in pixels"
                    },
                    "jump-touch": {
                        label: "Jump Touch",
                        help: "If yes, only when craft touches the ground"
                    },
                    ybounce: {
                        label: "Y Bounce",
                        help: "Value between 0 and 100% (0 no rebound, 100 infinite rebound)"
                    },
                    animations: {
                        legend: "Player Animations"
                    },
                    move: {
                        label: "Move",
                        help: "Player animation that will repeat when iddle"
                    },
                    jump: {
                        label: "Jump",
                        help: "Player animation that will play (just once) while jumping"
                    },
                    forward: {
                        label: "Forward",
                        help: "Player animation that will repeat when going forward"
                    },
                    up: {
                        label: "Up",
                        help: "Player animation that will repeat when going up"
                    },
                    down: {
                        label: "Down",
                        help: "Player animation that will repeat when going down"
                    },
                    back: {
                        label: "Backward",
                        help: "Player animation that will repeat when going backward"
                    }
                },
                edge: {
                    empty: "No edges yet embeded",
                    delete: "Are you sure you want to delete this edge?",
                    list: {
                        header: "Existing edges"
                    },
                    new: {
                        label: "New Edge"
                    },
                    sprite: {
                        label: "Sprite"
                    },
                    tolerance: {
                        label: "Tolerance",
                        help: "NOT USED YET"
                    }
                },
                asteroid: {
                    empty: "No asteroids yet created",
                    delete: "Are you sure you want to delete this asteroid?",
                    list: {
                        header: "Existing asteroids"
                    },
                    new: {
                        label: "New Asteroid"
                    },
                    events: {
                        label: "Manage events"
                    },
                    attach: {
                        label: "Image Asset"
                    },
                    multiplicity: {
                        label: "Multiplicity",
                        help: "The number of copies that will be launched during the stage."
                    },
                    cycle: {
                        label: "Cycle",
                        help: "TO REVIEW. Asteroid will be recycled to be launched again."
                    },
                    movex: {
                        label: "Move-X",
                        help: "Different random horizontal speed for the instances of this asteroid"
                    },
                    movey: {
                        label: "Move-Y",
                        help: "Different random vertical speed for the instances of this asteroid"
                    },
                    tics: {
                        label: "Tics",
                        help: "Different random seconds to wait to launch the new instance of this asteroid"
                    },
                    xdif: {
                        label: "X-position",
                        help: "Different random position in the horizontal axis for the instances of this asteroid"
                    },
                    ydif: {
                        label: "Y-position",
                        help: "Different random position in the vertical axis for the instances of this asteroid"
                    },
                    ygravity: {
                        label: "Y-gravity",
                        help: "Gravity that affects the asteroid"
                    },
                    basedepth: {
                        label: "Base Depth",
                        help: "The higher is the base depth the most probability the player touches the piece craft tries to pass behind it. Only when 2D floor available."
                    },
                    tolerance: {
                        label: "Tolerance",
                        help: "Make the asteroid N pixels thiner to decrease collission probability"
                    }
                },
                landscape: {
                    empty: "No landscapes yet created",
                    delete: "Are you sure you want to delete this landscape?",
                    list: {
                        header: "Existing landscapes"
                    },
                    new: {
                        label: "New Landscape"
                    },
                    sprite: {
                        label: "Sprite"
                    },
                    moveX: {
                        label: "X-move",
                        help: "Horizontal speed of the landscape item(s)"
                    },
                    foreground: {
                        label: "Foreground",
                        help: "Positioned just in from of the user point of view. Action will occur 'behind' them"
                    }
                },
                word: {
                    empty: "No words yet created",
                    delete: "Are you sure you want to delete this word?",
                    list: {
                        header: "Existing words"
                    },
                    new: {
                        label: "New Word"
                    },
                    id: {
                        label: "Id",
                        help: "Identifier of the word. Useful when there's more than one word in the activity"
                    },
                    key: {
                        label: "Image Asset",
                        help: "Graphic resource to put as the word (letter, when splitted) background"
                    },
                    sprite: {
                        label: "Sprite",
                        help: "Sprite to put as the word (letter, when splitted) background"
                    },
                    x: {
                        label: "X-position",
                        help: "Initial horizontal position"
                    },
                    y: {
                        label: "Y-position",
                        help: "Initial vertical position"
                    },
                    text: {
                        label: "Word",
                        help: "Text to be solved"
                    },
                    opentext: {
                        label: "Open text",
                        help: "No specific text expected o not validable",
                        row: "No specific text expected"
                    },
                    split: {
                        legend: "Split word in letters",
                        help: "Leave both empty if you want just one input text for the whole word",
                        offsetx: {
                            label: "X offset",
                            help: "Horizontal separation beetwen the word letter containers"
                        },
                        offsety: {
                            label: "Y offset",
                            help: "Vertical separation beetwen the word letter containers"
                        }
                    },
                    offset: {
                        legend: "Text position"
                    },
                    offsetx: {
                        label: "Text X offset",
                        help: "Top distance from its container"
                    },
                    offsety: {
                        label: "Text Y offset",
                        help: "Left distance from its container"
                    }
                },
                tiles: {
                    case: {
                        label: "Case",
                        help: "Case for the letters in the cells"
                    },
                    width: {
                        label: "Width",
                        help: "Width of the table cells"
                    },
                    height: {
                        label: "Height",
                        help: "Width of the table cells"
                    },
                    cols: {
                        label: "Columns",
                        help: "Columns of te table"
                    },
                    rows: {
                        label: "Rows",
                        help: "Rows of the table"
                    },
                    color: {
                        label: "Cell Color",
                        help: "Add a color for the cells background",
                        
                        right: {
                            label: "Word Color"
                        }
                    },
                    colors: {
                        label: "Cell Colors",
                        help: "Colors for the cells background",
                        
                        right: {
                            label: "Word Colores",
                            help: "Successive background colors for the unveiled words"
                        },
                        
                        current: {
                            label: "Current Word",
                            help: "Backgroud color for the word that is being unveiled"
                        }
                    }
                },
                hiddenword:{
                    text: {
                        label: "Text"
                    },
                    sprite: {
                        label: "Sprite",
                        help: "Sprite to represent the hidden word. If it embeds a text with 'textTag' key it will show the text. It will be marked when resolved if validation marks ar defined"
                    },
                    x: {
                        label: "Column",
                        help: "Column where to start the word"
                    },
                    y: {
                        label: "Row",
                        help: "Row where to start de word"
                    },
                    orientation: {
                        label: "Direction"
                    },
                    preview: {
                        label: "Preview"
                    },
                    check: {
                        label: "Right layout of the word"
                    }
                }
            }, /*/reveal*/
            plugin: {
                loaded: "Template file successfuly loaded '{{file}}'",
                constraint: {
                    header: "Please complete the following required elements:",
                    catch: {
                        target: "You must define one or more targets.",
                        weapon: "You must define a weapon.",
                        piece: "You must define one or more pieces.",
                        runaway: "To define a runaway event you must give a number value to 'Runaway Speed' at 'Setup' tab"
                    },
                    paint: {
                        canvas: "You must define one or more canvases.",
                        brush: "You must define a paintbrush.",
                        color: "You must define one or more color tins.",
                        noColorSprites: "Before defining any canvas you should define one or more colors"
                    },
                    validation: {
                        required: "You need to define validation to run this activity",
                        noButton: "Asynchronous validation mode requires a validate button"
                    },
                    sprite: {
                        notFound: "Sprite '{{sprite}}', declared as '{{type}}', does not exist"
                    },
                    dragdrop: {
                        container: "You must define one or more containers.",
                        nosprite: "At least one container must have an sprite defined.",
                        event: {
                            zero: "No answer selected to set events. Please, pick (just) one.",
                            gtone: "More than one answer selected to set events. Please, pick just one."
                        }
                    },
                    puzzle: {
                        piece: "You must define two or more pieces for a puzzle."
                    },
                    simon: {
                        pad: {
                            nosprite: "Please select one sprite",
                            nosound: "Please select one sound",
                            notenough: "Please select at least two pads to play Simon",
                            noframes: "Pad '{{sprite}}' must have at least 2 frames"
                        },
                        noprogress: "Please set a progress bar",
                        talk: {
                            noframes: "Talk '{{sprite}}' must have at least 3 frames"
                        }
                    },
                    arcade: {
                        craft: "Please define a player",
                        piece: {
                            empty: "Please define at least an asteroid",
                            noevents: "Please define events for the '{{asteroid}}' asteroids"
                        }
                    },
                    type: {
                        word: "Please define a word at least"
                    },
                    wordsearch: {
                        hiddenword: "Please define a hidden word at least",
                        notextsprite: "Sprite '{{sprite}}' must have a text with the 'textTag' key"
                    }
                },
                toolbox: {
                    catch: {
                        piece: {
                            label: "Set sprites as possible answers"
                        },
                        target: {
                            label: "Set sprites as right answers"
                        },
                        weapon: {
                            label: "Define an sprite as the weapon",
                        }
                    },
                    paint: {
                        brush: {
                            label: "Set an sprite as the paintbrush"
                        },
                        color: {
                            label: "Set sprites as color tins"
                        },
                        canvas: {
                            label: "Set sprites as canvases",
                        }
                    },
                    dragdrop: {
                        container: {
                            label: "Set sprites as containers to drop the answers in"
                        }
                    },
                    puzzle: {
                        board: {
                            label: "Set sprites as elements of a puzzle"
                        }
                    },
                    simon: {
                        pad: {
                            label: "Set Simon pads"
                        }
                    },
                    arcade: {
                        stage: {
                            label: "Set Stage Basis"
                        },
                        asteroid: {
                            label: "Set enemies and targets"
                        },
                        edge: {
                            label: "Set stage edges"
                        },
                        landscape: {
                            label: "Set environment items. No direct interaction with them."
                        }
                    },
                    type: {
                        word: {
                            label: "Set Words to solve"
                        }
                    },
                    wordsearch: {
                        tiles: {
                            label: "Set wordsearch dimensions"
                        },
                        hiddenword: {
                            label: "Set the hidden words"
                        },
                        decorate: {
                            label: "Set additional colors"
                        }
                    }
                },
                field: {
                    setup: {
                        speed: {
                            label: "Speed",
                            help: "Relative speed for all the pieces"
                        },
                        runawaySpeed: {
                            label: "Runaway speed",
                            help: "Relative speed for the pieces to runaway"
                        }
                    },

                }
            }, /*plugin*/
            constraint: {
                asset: {
                    none: "No assets defined yet, please visit '{{asset}}' tab",
                    keyExists: "key '{{key}}' already exists as '{{subtype}}', overwrite?",
                    novalid: "Please select a valid file {{sample}}. It's a {{type}}",
                    layers: {
                        unmatch: "Uploaded files don't have the same pattern (e.g.: whatever_<<number>>.png)",
                        nonconsecutive: "Uploaded files have non-consecutive suffixes.",
                        justimage: "Multiple files just allowed to image assets."
                    }
                }
            }, /*constraint*/
            alert: {
                upload: {
                    failed: "File upload failed, please try again.",
                    novalid: "Please select a valid file to upload."
                },
                download: {
                    failed: "File download failed, please try another link.",
                    write: "File downloaded successfuly, but there has been an error saving it",
                    nourl: "Please provide a file location"
                },
                sprite: {
                    exists: "Sprite identifier '{{id}}' already exists, please choose another one"
                },
                preview: {
                    nomode: "No mode selected"
                }
            }, /*alert*/
            warning: {
                color: {
                    empty: "No color selected"
                }
            },
            success: {
                upload: "File has uploaded successfully!",
                anchor: "Anchor succesfully changed"
            }
        }
    }
}