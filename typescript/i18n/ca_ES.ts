module LandPlayEdit {
    export module i18n {
        export const ca_ES = {
            mainMenu: {
                tabs: {
                    assets: "Recursos",
                    setup: "Configuració",
                    layout: "Edició",
                    preview: "Vista previa"
                }
            },
            button: {
                update: "Guardar",
                delete: "Esborrar"
            },
            validator: {
                required: "Camp obligatori",
                number: {
                    expected: "S'esperaba un número",
                    required: "Valor numéric obligatori"
                },
                alpha_numeric: {
                    expected: "Format incorrecte, nomes lletres i números",
                    required: "Camp obligatori, nomes lletres i números"
                },
                identifier: {
                    expected: "S'esperava un alfa-numèric que no comenci per número",
                    required: "Alfa-numéric requerit que no comenci per número"
                },
                gtzero: {
                    expected: "S'esperaba un número més gran que zero",
                    required: "Número obligatori, més gran que zero"
                },
                selection: {
                    expected: "Valor esperat de selecció (p.e.: 0-3,4,8,10-12)",
                    required: "Valor obligatori de selecció (p.e.: 0-3,4,8,10-12)"
                },
                color: {
                    expected: "Valor de color esperat (ex. #000000)",
                    required: "Valor de color obligatori (ex. #000000)"
                },
                dimension: {
                    expected: "Número (eventualment seguit de %) esperat",
                    required: "Número (eventualment seguit de %) obligatori"
                },
                option: {
                    eqzero: "El selector ha de tenir al menys una opció"
                },
                boolean: {
                    required: "Valor esperat 'true' o 'false'"
                }
            },
            option: {
                dimension: {
                    pixels: "Píxels",
                    percentage: "Percentatge"
                },
                boolean: {
                    yes: "Sí",
                    no: "No"
                },
                confirm: {
                    yes: "Acceptar",
                    no: "Cancel·lar"
                },
                outbound: {
                    cycle: "Cicle",
                    rebound: "Rebot"
                },
                pickable: {
                    true: "Al fer clic",
                    false: "Al començar"
                },
                async: {
                    true: "Al fer clic en un botó",
                    false: "En cada event"
                },
                mode: {
                    Arcade: {
                        label: "Plataformes"
                    },
                    Catch: {
                        label: "Atrapar"
                    },
                    DragDrop: {
                        label: "Arrossegar"
                    },
                    Paint: {
                        label: "Pintar"
                    },
                    Puzzle: {
                        label: "Puzzle"
                    },
                    Simon: {
                        label: "Simon"
                    },
                    Type: {
                        label: "Teclejar"
                    },
                    WordSearch: {
                        label: "Sopa de lletres"
                    }
                },
                cause: {
                    right: {
                        label: "Resposta correcta"
                    },
                    wrong: {
                        label: "Resposta incorrecta"
                    },
                    tryout: {
                        label: "Reintents superats"
                    },
                    complete: {
                        label: "Activitat completa"
                    },
                    hint: {
                        label: "Pista rebuda"
                    },
                    timeout: {
                        label: "Temps finalitzat"
                    }
                },
                effect: {
                    visible: {
                        label: "Canviar visibilitat"
                    },
                    back: {
                        label: "Tornar a la seva posició"
                    },
                    runaway: {
                        label: "Fugir"
                    },
                    playsound: {
                        label: "Reproduïr so"
                    },
                    lock: {
                        label: "Inmovilitzar"
                    },
                    addpoints: {
                        label: "Sumar punts"
                    },
                    destroy: {
                        label: "Destruïr"
                    },
                    pause: {
                        label: "Pausar"
                    },
                    solve: {
                        label: "Solucionar"
                    },
                    validate: {
                        label: "Validar"
                    }
                },
                case: {
                    uppercase: "Majúscules",
                    lowercase: "Minúscules",
                    capitalize: "Nom propi"
                }
            },
            editorForm: {
                assets: {
                    origin: {
                        title: "Carregar fitxer",
                        help: "Aquest apartat et permet carregar recursos com imatges o sons que t'ajudaran a il·lustrar l'activitat",
                        upload: {
                            label: "OPCIÓ 1: Des del teu dispositiu",
                            help: "Selecciona un fitxer explorant el teu dispositiu. Pots seleccionar més d'un per formar capes de un mismo recurso imagen, però han d'estar numerats correlativament."
                        },
                        url: {
                            label: "OPCIÓ 2: Des de Internet",
                            help: "Cerca imatges amb el teu cercador preferit i enganxa l'enllaç al quadre de text"
                        }
                    },
                    file: {
                        label: "Tria un fitxer:"
                    },
                    url: {
                        label: "Enganxar enllaç"
                    },
                    key: {
                        label: "identificador"
                    },
                    upload: {
                        label: "Carregar",
                        value: "CARREGAR"
                    },
                    images: {
                        header: "Imatges"
                    },
                    sounds: {
                        header: "Sons"
                    }
                },
                setup: {
                    mode: {
                        label: "Modalitat"
                    },
                    width: {
                        label: "Ample"
                    },
                    height: {
                        label: "Alt"
                    }
                },
                layout: {
                    background: {
                        label: "Color de fons"
                    },
                    loadLayout: {
                        label: "Recarregar edició"
                    },
                    togglePanels: {
                        label: "Obrir/tancar panells"
                    },
                    sidePanels: {
                        header: "Panells laterals"
                    },
                    toolbox: {
                        header: "Caixa d'eines",
                        setup: {
                            header: "Configuració"
                        },
                        sprites: {
                            header: "Icones"
                        },
                        behaviour: {
                            header: "Comportamient"
                        },
                        Edit: {
                            tooltip: "Principals aspectes gràficos de la icona"
                        },
                        Position: {
                            tooltip: "Arrossega una icona per establir la seva posició"
                        },
                        Text: {
                            tooltip: "Elements textuals en una icona"
                        },
                        Button: {
                            tooltip: "Convertir una icona en un botó"
                        },
                        locate: {
                            tooltip: "Destacar totes les icones de l'escena"
                        },
                        Grid: {
                            tooltip: "Distribuïr la icona o els seus fotogrames en format graella"
                        },
                        Shape: {
                            tooltip: "Dibuixar rectangles o cercles en una icona"
                        },
                        retry: {
                            tooltip: "Sistema de reintents"
                        },
                        counter: {
                            tooltip: "Contadors i barres de progrès"
                        },
                        events: {
                            tooltip: "Events"
                        },
                        frames: {
                            tooltip: "Retallar la imatge en fotogrames"
                        },
                        Depth: {
                            tooltip: "Canviar la profunditat de les icones"
                        },
                        validation: {
                            tooltip: "Validació"
                        },
                        Anchor: {
                            tooltip: "Punt d'anclatge de la icona"
                        },
                        Embed: {
                            tooltip: "Arrossega una icona a sobre d'un altre per incrustar-lo"
                        },
                        save: {
                            tooltip: "Descarregar el fitxer de configuració de l'activitat"
                        },
                        clear: {
                            tooltip: "Disenyaa una nova activitat"
                        }
                    }
                }
            },/*/editForm*/
            reveal: {
                generic: {
                    clear: "Seguro que vols començar el diseny de l'actividad de zero?\nSi us plau, asegura't que abans has guardat el seu arxiu de configuració."
                },
                text: {
                    empty: "Encara no hi ha cap text",
                    delete: "Segur que vols esborrar aquest text?",
                    list: {
                        header: "Texts incrustats"
                    },
                    new: {
                        label: "Nou text"
                    },
                    key: {
                        label: "Identificador"
                    },
                    label: {
                        label: "Etiqueta"
                    },
                    x: {
                        label: "X"
                    },
                    y: {
                        label: "Y"
                    },
                    style: {
                        label: "Estil",
                        value: "Valor",
                        applied: "Estils aplicats",
                        delete: "Segur que vols esborrar els estils seleccionats?"
                    }
                },
                event: {
                    empty: "Encara no hi ha cap event",
                    delete: "Segur que vols esborrar aquest event?",
                    list: {
                        header: "Events existents"
                    },
                    new: {
                        label: "Nou event"
                    },
                    cause: {
                        label: "Causa",
                        help: "Pots sel·leccionar més d'una causa amb la tecla Ctrl"
                    },
                    effect: {
                        label: "Efecte"
                    },
                    sprite: {
                        label: "Icona / So",
                        all: "Tots els elemento mòbils",
                        this: "Aquesta icona"
                    },
                    value: {
                        label: "Valor"
                    }
                },
                shape: {
                    empty: "Encara no hi ha cap forma",
                    delete: "Segur que vols esborrar aquesta forma?",
                    list: {
                        header: "Formes existents"
                    },
                    new: {
                        label: "Nova forma"
                    },
                    type: {
                        label: "Tipus"
                    },
                    color: {
                        label: "Color"
                    },
                    radius: {
                        label: "Radio"
                    },
                    width: {
                        label: "Ample"
                    },
                    height: {
                        label: "Alt"
                    },
                    line: {
                        width: {
                            label: "Ample de línia"
                        },
                        color: {
                            label: "Color de línia"
                        }
                    }
                },
                sprite: {
                    delete: "Seguro que vols esborrar aquesta icona?",
                    embed: "Seguro que vols incrustar {{child}} en {{parent}}?",
                    id: {
                        label: "Nom"
                    },
                    visible: {
                        label: "Visible"
                    },
                    width: {
                        label: "Ample"
                    },
                    height: {
                        label: "Alt"
                    },
                    x: {
                        label: "X"
                    },
                    marginRight: {
                        label: "Marge dret"
                    },
                    y: {
                        label: "Y"
                    },
                    marginBottom: {
                        label: "Marge inferior"
                    },
                    tint: {
                        label: "Pintura"
                    },
                    angle: {
                        label: "Angle"
                    },
                    frame: {
                        label: "Fotograma"
                    },
                    layers: {
                        from: {
                            label: "Número de capa inicial"
                        },
                        to: {
                            label: "Número de capa final"
                        }
                    },
                    children: {
                        label: "Gestionar fills"
                    }
                },
                frame: {
                    nav: {
                        previous: "Fotograma anterior",
                        next: "Fotograma següent"
                    }
                },
                button: {
                    type: {
                        label: "Tipus de botó"
                    },
                    value: {
                        label: "Valor"
                    }
                },
                retry: {
                    delete: "Seguro que vols esborrar els reintents?",
                    retries: {
                        label: "Reintents",
                        help: "Total d'errors permesos",
                        info: "Les errades decrementaran aquest comptador. Es llençarà l'evento 'tryout' quan arribi a 0"
                    },
                    points: {
                        legend: "Evolució de punts:"
                    },
                    from: {
                        label: "Des de"
                    },
                    to: {
                        label: "Fins a"
                    },
                    step: {
                        label: "Salts"
                    },
                    validator: {
                        from: "Ha d'acompañar el 'fins a'",
                        to: "Ha d'acompañar el 'des de'",
                        toeqfrom: "'des de' i 'fins a' han de ser diferents"
                    }
                },
                counter: {
                    delete: "Segur que vols esborrar el comportament dels comptadors?",
                    live: {
                        text: {
                            label: "Reintents restants",
                            help: "Text que mostra els reintents restants actualitzat"
                        }
                    },
                    points: {
                        text: {
                            label: "Punts",
                            help: "Text que mostra els punts aconseguits en l'activitat"
                        }
                    },
                    time: {
                        legend: "Temps:",
                        text: {
                            label: "Text",
                            help: "Text que mostra el temps restant actualitzat"
                        },
                        secs: {
                            label: "Segons"
                        }
                    },
                    progress: {
                        sprite: {
                            label: "Barra de progrès",
                            help: "Icona que mostrarà el progrès de l'activitat (si la seva modalitat ho contempla)"
                        }
                    }
                },
                grid: {
                    delete: "Segur que vols esborrar aquesta graella?",
                    xoffset: {
                        label: "Desplaçament X",
                        help: "Separació horitzontal entre cel·les"
                    },
                    yoffset: {
                        label: "Desplaçament Y",
                        help: "Separación vertical entre cel·les"
                    },
                    cols: {
                        label: "Columnes",
                        help: "Les files vindran determinades pel número total de cel·les"
                    },
                    length: {
                        label: "Longitut"
                    },
                    selection: {
                        label: "Selecció"
                    }
                },
                piece: {
                    empty: "Encara no hi ha cap peça",
                    delete: "Seguro que vols esborrar aquesta peça?",
                    list: {
                        header: "Peces existents"
                    },
                    new: {
                        label: "Nova peça"
                    },
                    sprite: {
                        label: "Icona"
                    },
                    outbound: {
                        label: "Al sortir d'escena",
                        help: "Què fer quan la icona toqui el cantó de l'escena"
                    },
                    movex: {
                        label: "Movimient horitzontal"
                    },
                    movey: {
                        label: "Movimient vertical"
                    }
                },
                target: {
                    empty: "Encara no hi ha cap objectiu",
                    delete: "Seguro que vols esborrar l'objectiu?",
                    list: {
                        header: "Objectius existents"
                    },
                    new: {
                        label: "Nou objetiu"
                    },
                    events: {
                        label: "Veure events de l'objectiu"
                    },
                    sprite: {
                        label: "Icona"
                    },
                    sound: {
                        label: "So"
                    }
                },
                weapon: {
                    sprite: {
                        label: "Icona"
                    },
                    pickable: {
                        label: "Weapon usage"
                    },
                    brushpoint: {
                        label: "Icona de la punta del pinzell",
                        help: "La part del pinzell que es mulla en la pintura"
                    },
                    toolcase: {
                        label: "Caixa per al Pinzell",
                        help: "La caixa per guardar el pinzell"
                    }
                },
                children: {
                    empty: "Aquesta Icona no te descendència. Pots afegir fills arrossegant-los cap a ell.",
                    delete: "Seguro que vols desfer la relació? La Icona fill quedarà disponible en la escena com a element lliure.",
                    list: {
                        header: "Fills de la Icona"
                    }
                },
                container: {
                    empty: "Encara no hi ha cap contenidor",
                    delete: "Segur que vols esborrar aquest contenidor?",
                    list: {
                        header: "Contenidors existents"
                    },
                    new: {
                        label: "Nou contenidor"
                    },
                    sprite: {
                        label: "Icona",
                        help: "Icona(s) on deixar anar les respostes. Crear un contenidor amb una icona buit per aglutinar les respostes incorrectes.",
                        empty: "{Sense icona}"
                    },
                    random: {
                        label: "Aleatori",
                        help: "Intercanviar aleatoriament les posicions inicials de les respostes"
                    },
                    position: {
                        help: "Posició de la resposta seleccionada relativa al contenidor"
                    },
                    x: {
                        label: "Esquerra"
                    },
                    y: {
                        label: "Superior"
                    },
                    offset: {
                        help: "Posició de la resposta seleccionada relativa a la resposta anterior (si se'n permet més d'una)"
                    },
                    xoffset: {
                        label: "Separació horitzontal "
                    },
                    yoffset: {
                        label: "Separació vertical"
                    },
                    values: {
                        label: "Valors textuals",
                        help: "Textos separats per filas per a les respostas esperades en aquest contenidor. S'incorporaran als textos etiquetats com a 'textTag' en les respostes.\nQuan es seleccioni una graella com a icona han de tenir el mateix ordre que els elements de la graella definits.\nSi el nombre de files és mayor que els elements de la graella s'incorporaran a l'atzar."
                    },
                    extraValues: {
                        label: "Valors textuals adicionals",
                        help: "Textos separats prr files per a les respostes que introdueixen valors incorrectes"
                    },
                    answer: {
                        available: {
                            label: "Respostes disponibles",
                            help: "Selecciona respostes disponibles i pasa-les al selector de la dreta"
                        },
                        selected: {
                            label: "Respostes seleccionades",
                            help: "Respostes ja seleccionades per aquest contenidor"
                        }
                    }
                },
                board: {
                    delete: "Seguro que vols esborrar el tauler del puzzle?",
                    table: {
                        label: "Tauler",
                        help: "Tauler on deixar anar les peces (al principi fora del tauler). Si es deixa buit les peces han d'estar en el seu ordre correcte i al deixar anar una sobre l'altra intercanviaran les seves posicions.",
                        random: {
                            label: "Aleatori",
                            help: "Intercanviar les posicions inicials de les peces"
                        }
                    },
                    available: {
                        label: "Icones disponibles",
                        help: "Selecciona una o més icones i porta'ls al selector de la dreta. Esdevindran les peces del puzzle"
                    },
                    selected: {
                        label: "Icones seleccionades",
                        help: "Peces del puzzle. L'ordre és important ja que defineix la seva validesa sobre el tauler."
                    }
                },
                depth: {
                    empty: "Encara no hi ha cap icona",
                    list: {
                        header: "Ordre de les icones"
                    },
                    reload: {
                        label: "Recarregar disposició"
                    }
                },
                validation: {
                    delete: "Segur que vols esborrar la validació?",
                    async: {
                        legend: "Engegar validació:"
                    },
                    marks: {
                        legend: "Marques de validació:"
                    },
                    right: {
                        key: {
                            label: "Recurs gràfic correcte",
                            help: "Les respostes correctes es marcaran amb aquesta imatge"
                        }
                    },
                    wrong: {
                        key: {
                            label: "Recurs gràfic incorrecte",
                            help: "Les respostes incorrectes es marcaran amb aquesta imatge"
                        }
                    },
                    top: {
                        label: "Posició superior",
                        help: "Posició superior de la marca (in)correcta, relativa a la resposta"
                    },
                    left: {
                        label: "Posició esquerra",
                        help: "Posició esquerra de la marca (in)correcta, relativa a la resposta"
                    },
                    width: {
                        label: "Amplada",
                        help: "Amplada (pixels o %) de la marca (in)correcta"
                    },
                    height: {
                        label: "Alçada",
                        help: "Alçada (pixels o %) de la marca (in)correcta"
                    }
                },
                color: {
                    empty: "Encara no hi ha cap color creat",
                    delete: "Segur que vols esborrar aquest color?",
                    list: {
                        header: "Colors existents"
                    },
                    new: {
                        label: "Nou color"
                    },
                    sprite: {
                        label: "Icona"
                    },
                    color: {
                        label: "Color"
                    }
                },
                canvas: {
                    empty: "Encara no hi ha cap llenç creat",
                    delete: "Segur que vols esborrar aquest llenç?",
                    list: {
                        header: "Llenços existents"
                    },
                    new: {
                        label: "Nou llenç"
                    },
                    sprite: {
                        label: "Icona"
                    },
                    color: {
                        label: "Icona del Color"
                    }
                },
                brush: {
                    sprite: {
                        label: "Icona"
                    },
                    pickable: {
                        label: "Us del pinzell"
                    }
                },
                simon: {
                    transition: {
                        label: "Transició",
                        help: "Temps (en milisegons) entre una pastilla i la seguent"
                    },
                    maxseq: {
                        label: "Longitut de seqüència",
                        help: "Longitud de la seqüència aleatoria més llarga"
                    },
                    talk: {
                        label: "Estat",
                        help: "Icona que canvia el seu estat de 'parlar' a 'escoltar'. Ha de tenir 3 fotogrames per representar el 'botó d'inici', 'reproduïnt' i 'escoltant'"
                    }
                },
                pad: {
                    delete: "Segur que vols esborrar aquesta pastilla?",
                    sprite: {
                        label: "Icona",
                        help: "Icona amb 2 fotogrames per destacar la que està sonant."
                    },
                    sound: {
                        label: "Sonido",
                        help: "So que es reprodueix mentre es destaca la icona"
                    },
                    selected: {
                        label: "Pastilles seleccionades",
                        help: "Les pastilles que s'alternaran en seqüencies a l'atzar"
                    }
                },
                stage: {
                    speed: {
                        label: "Velocitat",
                        help: "Velocitat relativa. Variable que multiplica la velocitat de tots els objectes en moviment."
                    },
                    craftspeed: {
                        label: "Velocitat del jugador",
                        help: "Velocitat relativa del jugador"
                    },
                    runawaySpeed: {
                        label: "Velocitat de fugida",
                        help: "Velocidad relativa de tots els objectes en moviment un cop la pantalla es soluciona."
                    },
                    debug: {
                        label: "Depuració",
                        help: "Mostra el cos efectiu de col·lisió"
                    },
                    craft: {
                        label: "Aspecte del jugador",
                        help: "Icona que l'usuario mourà amb el teclat"
                    },
                    floor2d: {
                        label: "Terra 2D",
                        help: "El jugador es mourà al llarg de l'eix Y (tecles amunt/avall del teclat). Si no, la tecla 'amunt' serà per saltar"
                    },
                    ygravity: {
                        label: "Gravetat (Y)",
                        help: "Força de gravetat que afecta al jugador"
                    },
                    movex: {
                        label: "Moviment X",
                        help: "Pixels Horitzontals que es mourà (endavant/enrera)"
                    },
                    movey: {
                        label: "Moviment Y",
                        help: "Pixels Verticals que es mourà (amunt/avall). Només si s'ha marcat el terra 2D"
                    },
                    "y-scale": {
                        label: "Y-Scale",
                        help: "Segons el jugador es mou al llarg de l'eix Y canvia la seva dimensió N mil·lèssimes parts del percentatge de la seva mida, per donar sensació de profunditat. Només si hi ha terra 2D"
                    },
                    tolerance: {
                        label: "Tolerància",
                        help: "Fa que, en el moment de la col·lisió, el jugador sembli N pixels més petit."
                    },
                    "y-jump": {
                        label: "Salt (Y)",
                        help: "Alçada del salt en píxels"
                    },
                    "jump-touch": {
                        label: "Empenta de salt",
                        help: "Nomes quan el jugador toqui el terra"
                    },
                    ybounce: {
                        label: "Rebot (Y)",
                        help: "Valor entre 0 y 100% (0 sense rebot, 100 rebot infinit)"
                    },
                    animations: {
                        legend: "Animacions del jugador"
                    },
                    move: {
                        label: "Moure",
                        help: "Es repetirà quan el jugador estigui ociòs"
                    },
                    jump: {
                        label: "Saltar",
                        help: "Es produïrà quan estigui saltant"
                    },
                    forward: {
                        label: "Endavant",
                        help: "Es repetirà quan vagi cap endavant"
                    },
                    up: {
                        label: "Amunt",
                        help: "Es repetirà quan vagi cap amunt"
                    },
                    down: {
                        label: "Avall",
                        help: "Es repetirà quan vagi cap avall"
                    },
                    back: {
                        label: "Enrera",
                        help: "Es repetirà quan vagi cap enrera"
                    }
                },
                asteroid: {
                    empty: "Encara no hi ha cap asteroid creat",
                    delete: "Segur que vols esborrar aquest asteroid?",
                    list: {
                        header: "Asteroids creats"
                    },
                    new: {
                        label: "Nou Asteroid"
                    },
                    events: {
                        label: "Gestionar eventos"
                    },
                    attach: {
                        label: "Recurso gráfico"
                    },
                    multiplicity: {
                        label: "Multiplicitat",
                        help: "El número de còpies que es llançaran durant la pantalla."
                    },
                    cycle: {
                        label: "Cicle",
                        help: "PENDENT DE REVISAR. L'asteroid serà reciclat per llançar-lo de nou."
                    },
                    movex: {
                        label: "Moviment-X",
                        help: "Rang de velocitats aleatori en el que es mouran horitzontalment les instancies d'aquest asteroid"
                    },
                    movey: {
                        label: "Moviment-Y",
                        help: "Rang de velocitats aleatori en el que es mouran verticalment les instancies d'aquest asteroid"
                    },
                    tics: {
                        label: "Tics",
                        help: "Rang de segons d'espera aleatori en el que es llençaran les instancies d'aquest asteroid"
                    },
                    xdif: {
                        label: "Posició X",
                        help: "Rang de posicions horitzontals aleatori en el que es situaran, inicialment, les instancies d'aquest asteroid"
                    },
                    ydif: {
                        label: "Posició Y",
                        help: "Rang de posicions verticals aleatori en el que es situaran, inicialment, les instancies d'aquest asteroid"
                    },
                    ygravity: {
                        label: "Gravetat Y",
                        help: "Nivell de força de gravetat que afecta a l'asteroid"
                    },
                    basedepth: {
                        label: "Profunditat de la base",
                        help: "Quan més gran sigui aquest valor més probabilitat de que el jugador toqui la peça al pasar per detras. Nomes aplicable quan hi ha terra 2D."
                    },
                    tolerance: {
                        label: "Tolerància",
                        help: "Fer l'asteroid N pixels més estret per reduïr la probabilitat de col·lisió"
                    }
                },
                landscape: {
                    empty: "Encara no hi ha cap paisatge creat",
                    delete: "Segur que vols esborrar aquest paisatge?",
                    list: {
                        header: "Paisatges creats"
                    },
                    new: {
                        label: "Nou paisatge"
                    },
                    sprite: {
                        label: "Icona"
                    },
                    moveX: {
                        label: "Moviment X",
                        help: "Velocitat horitzontal per als paisatges"
                    },
                    foreground: {
                        label: "Paisatge frontal",
                        help: "Posicionat en front des del punt de vista de l'usuari. La acció transcurrirà 'al darrera' seu"
                    }
                },
                word: {
                    empty: "Encara no hi ha cap paraula creada",
                    delete: "Seguro que vols esborrar aquesta paraula?",
                    list: {
                        header: "Paraules creades"
                    },
                    new: {
                        label: "Paraula nova"
                    },
                    id: {
                        label: "Id",
                        help: "Identificador de la paraula. Útil quan hi ha més d'una paraula en l'activitat"
                    },
                    key: {
                        label: "Imatge",
                        help: "Recurs gràfic com a fons de la paraula (o de cada lletra)"
                    },
                    sprite: {
                        label: "Icona",
                        help: "Icona com a fons de la paraula (o de cada lletra)"
                    },
                    x: {
                        label: "Posició X",
                        help: "Posició horitzontal inicial"
                    },
                    y: {
                        label: "Posició Y",
                        help: "Posició vertical inicial"
                    },
                    text: {
                        label: "Paraula",
                        help: "Texte a resoldre"
                    },
                    opentext: {
                        label: "Texte obert",
                        help: "Texte no específic i/o no validable",
                        row: "No s'espera cap texte específic"
                    },
                    split: {
                        legend: "Separar la paraula en lletres",
                        help: "Deixa ambdós buits si vols que la paraula apareixi sencera en un sol camp de texte",
                        offsetx: {
                            label: "Separació horitzontal",
                            help: "Separació horitzontal entre els contenidors de lletres"
                        },
                        offsety: {
                            label: "Separació vertical",
                            help: "Separació vertical entre els contenidors de lletres"
                        }
                    },
                    offset: {
                        legend: "Posició del texte"
                    },
                    offsetx: {
                        label: "Distancia superior",
                        help: "Distancia superior respecte al seu contenidor"
                    },
                    offsety: {
                        label: "Distancia esquerra",
                        help: "Distancia a l'esquerra respecte al seu contenidor"
                    }
                },
                tiles: {
                    case: {
                        label: "Majúscules/minúscules",
                        help: "El format en el que apareixen les lletres en les cel·les"
                    },
                    width: {
                        label: "Ample",
                        help: "Ample de les cel·les de la sopa de lletres"
                    },
                    height: {
                        label: "Alçada",
                        help: "Alçada de les cel·les de la sopa de lletres"
                    },
                    cols: {
                        label: "Columnes",
                        help: "Número de columnes de la sopa de lletres"
                    },
                    rows: {
                        label: "Files",
                        help: "Número de files de la sopa de lletres"
                    },
                    color: {
                        label: "Color de cel·la",
                        
                        right: {
                            label: "Color de paraula"
                        }
                    },
                    colors: {
                        label: "Colors de cel·la",
                        help: "Colors possibles del fons de cel·les",
                        
                        right: {
                            label: "Colors de paraula",
                            help: "Colors de fons que s'aniran alternant conforme vagin descubrint les paraules"
                        },
                        
                        current: {
                            label: "Paraula actual",
                            help: "Colors de fons de la paraula que l'usuari està desvetllant"
                        }
                    }
                },
                hiddenword: {
                    text: {
                        label: "Text"
                    },
                    sprite: {
                        label: "Icona",
                        help: "Icona que representa la paraula amagada. Si incorpora un text amb l'identificador 'textTag' mostrarà el text en questió. Es marcarà quan es resolgui si s'han establert les marques de validació"
                    },
                    x: {
                        label: "Columna",
                        help: "Columna on comença la paraula"
                    },
                    y: {
                        label: "Fila",
                        help: "Fila on comença la paraula"
                    },
                    orientation: {
                        label: "Orientació"
                    },
                    preview: {
                        label: "Vista prèvia"
                    },
                    check: {
                        label: "Disposició correcta"
                    }
                }
            }, /*/reveal*/
            plugin: {
                loaded: "Fitxer de plantilla carregat '{{file}}'",
                constraint: {
                    header: "Si us plau, completa els següents requeriments:",
                    catch: {
                        target: "Has de definir un o més objectius.",
                        weapon: "You must define a weapon.",
                        piece: "Has de definir una o més peces.",
                        runaway: "Per definir un event de fugida (runaway) has de proporcionar un valor numèric a 'Velocitat de fugida' a la pestanya de 'Configuració'"
                    },
                    paint: {
                        canvas: "Has de definir un o més llenços.",
                        brush: "Has de definir un pinzell.",
                        color: "Has de definir un o més pots de pintura.",
                        noColorSprites: "Abans de definir cap llenç has de definir un o més pots de pintura."
                    },
                    validation: {
                        required: "Necesites definir la validació per executar aquesta activitat",
                        noButton: "El mode de validació asíncrona requereix un botó de validar"
                    },
                    sprite: {
                        notFound: "Icona '{{sprite}}', declared as '{{type}}', does not exist"
                    },
                    dragdrop: {
                        container: "Has de definir un o més contenidors.",
                        nosprite: "Al menys un contenedor ha de tenir una icona definit.",
                        event: {
                            zero: "No s'ha seleccionat cap resposta per editar els seus events. Si us plau, selecciona'n (nomes) una.",
                            gtone: "S'ha seleccionat més d'una resposta per editar els seus events. Si us plau, selecciona'n nomes una."
                        }
                    },
                    puzzle: {
                        piece: "Has de definir dues o més peces per a un puzzle."
                    },
                    simon: {
                        pad: {
                            nosprite: "Si us plau selecciona una icona",
                            nosound: "Si us plau selecciona un so",
                            notenough: "Si us plau selecciona al menys dus pastille per jugar al Simon",
                            noframes: "La pastilla '{{sprite}}' ha de tenir al menys 2 fotogrames"
                        },
                        noprogress: "Si us plau defineix una barra de progrés",
                        talk: {
                            noframes: "La icona d'estat '{{sprite}}' ha de tenir al menys 3 fotogrames"
                        }
                    },
                    arcade: {
                        craft: "Si us plau defineix un jugador",
                        piece: {
                            empty: "Si us plau defineix al menys un asteroide",
                            noevents: "Si us plau defineix events per als asteroides '{{asteroid}}'"
                        }
                    },
                    type: {
                        word: "Si us plau defineix al menys una paraula"
                    },
                    wordsearch: {
                        hiddenword: "Si us plau defineix al menys una paraula oculta",
                        notextsprite: "L'icona '{{sprite}}' ha de tenir un text amb la clau 'textTag'"
                    }
                },
                toolbox: {
                    catch: {
                        piece: {
                            label: "Definir icones com a possibles respostes"
                        },
                        target: {
                            label: "Definir icones com a respostes correctes"
                        },
                        weapon: {
                            label: "Definir una icona com a eina",
                        }
                    },
                    paint: {
                        brush: {
                            label: "Definir una icona com a pinzell"
                        },
                        color: {
                            label: "Definir icones com a pots de pintura"
                        },
                        canvas: {
                            label: "Definir icones com a llenços",
                        }
                    },
                    dragdrop: {
                        container: {
                            label: "Definir icones com a contenidors on deixar anar les respostes"
                        }
                    },
                    puzzle: {
                        board: {
                            label: "Definir icones com a elements d'un puzzle"
                        }
                    },
                    simon: {
                        pad: {
                            label: "Definir les pastilles del Simon"
                        }
                    },
                    arcade: {
                        stage: {
                            label: "Definir les bases de la pantalla"
                        },
                        asteroid: {
                            label: "Definir enemics i objectius"
                        },
                        edge: {
                            label: "Definir les barreres de la pantalla"
                        },
                        landscape: {
                            label: "Definir elements d'entorn. No s'interactua directament amb ells."
                        }
                    },
                    type: {
                        word: {
                            label: "Definir paraules a resoldre"
                        }
                    },
                    wordsearch: {
                        tiles: {
                            label: "Definir les dimensions de la sopa de lletres"
                        },
                        hiddenword: {
                            label: "Definir les paraules ocultes"
                        },
                        decorate: {
                            label: "Definir colors adicionals"
                        }
                    }
                },
                field: {
                    setup: {
                        speed: {
                            label: "Velocitat",
                            help: "Velocitat relativa per a totes les peces"
                        },
                        runawaySpeed: {
                            label: "Velocitat de fugida",
                            help: "Velocitat relativa per a la fugida de les peces"
                        }
                    },

                }
            }, /*plugin*/
            constraint: {
                asset: {
                    none: "No hi ha cap recurs gràfic definit. Si us plau visita la pestanya '{{asset}}'",
                    keyExists: "L'identificador '{{key}}' ja existeix com a '{{subtype}}', sobrescriure?",
                    novalid: "Si us plau, selecciona un arxiu vàlid {{sample}}. Estas pujant un {{type}}",
                    layers: {
                        unmatch: "Els arxius seleccionats no tenen el mateix patró (ex.: qualsevol_<<numero>>.png)",
                        nonconsecutive: "Els arxius seleccionats no tenen sufixos numèrics correlatius.",
                        justimage: "La selecció múltiple d'arxius només és possible amb imatges."
                    }
                }
            }, /*constraint*/
            alert: {
                upload: {
                    failed: "La pujada d'arxius ha fallat, si us plau intentar de nou.",
                    novalid: "Si us plau selecciona un arxiu vàlid."
                },
                download: {
                    failed: "La descàrrega d'arxiu ha fallat, si us plau intenta pujar un altre enllaç",
                    write: "L'arxiu s'ha pogut descarregar, però hi ha hagut algun error al guardar-lo",
                    nourl: "Si us plau proporciona una ubicació d'arxiu"
                },
                sprite: {
                    exists: "L'identificador de icona '{{id}}' ja existeix, si us plau tria'n un altre"
                },
                preview: {
                    nomode: "No s'ha seleccionat cap modalitat"
                }
            }, /*alert*/
            warning: {
                color: {
                    empty: "No s'ha seleccionat cap color"
                }
            },
            success: {
                upload: "L'arxiu s'ha pujat satisfactoriament!",
                anchor: "L'anclatge ha canviat satisfactoriament"
            }
        }
    }
}