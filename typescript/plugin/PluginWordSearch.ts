/// <reference path='Plugin.ts' />

module LandPlayEdit {
    export class PluginWordSearch extends LandPlayEdit.Plugin {

        constructor() {
            super( {
                toolBox: {
                    behaviour: [
                        {
                            key: "tiles",
                            label: "plugin.toolbox.wordsearch.tiles.label",
                            icon: "fas fa-border-none"
                        },
                        {
                            key: "hiddenword",
                            label: "plugin.toolbox.wordsearch.hiddenword.label",
                            icon: "fas fa-text-height"
                        }
                    ]
                }
            } );
        }

        getTemplateName(): string {
            return "wordsearch.html";
        }

        behaviour( type ) {
            switch ( type ) {
                case "tiles":
                    this.editTiles();
                    break;

                case "hiddenword":
                    this.openWords();
                    break;

                default:
                    LandPlayEdit.warning( "Please develop pending behaviour '" + type + "'" );
                    break;
            }
        }

        editRecord( type: string, pos: number ) {
            switch ( type ) {
                case "hiddenword":
                    this.editWord( pos );
                    break;

                default:
                    break;
            }
        }

        deleteRecord( type: string, pos: number ) {
            switch ( type ) {
                case "hiddenword":
                    this.deleteWord( pos );
                    break;

                default:
                    break;
            }
        }

        /**
         * Preview additional graphic elements
         * Launched from editor when Stage is ready
         * Override in each plugin
         ******* */
        preview() {

        }

        private hasWord( pos: string ) {
            if ( LandPlayEdit.editor.json.config.words != null ) {
                return this.hasItem( LandPlayEdit.editor.json.config.words.word, pos );
            } else {
                return false;
            }
        }

        editTiles() {
            var classRef = this;
            this.modal( "tiles.html", function() {

                $( "#tilesForm" ).submit( function( event ) {
                    event.preventDefault();
                } );

                $( '#tilesForm' ).on( "formvalid.zf.abide", function( ev, frm ) {
                    classRef.updateTiles();
                } );

                $( ".addColor" ).click( function() {
                    var from = $( this ).attr( "data-from" );
                    var target = $( this ).attr( "data-target" );
                    var color = $( "#" + from ).val();
                    if ( color == "" ) {
                        LandPlayEdit.warning( i18next.t( "warning.color.empty" ) );
                    } else {
                        var option = $( "<option>" );
                        option.html( color );
                        $( option ).attr( "value", color );
                        $( option ).attr( "style", "background-color:" + color );

                        $( "[name='" + target + "']" ).append( option );
                    }

                } );

                $( ".delColor" ).click( function() {
                    var target = $( this ).attr( "data-target" );
                    $( "[name='" + target + "'] option:selected" ).remove();
                } );

                $( "#tilesColor, #rightColor, #currentColor" ).colorpicker( { history: false } );
                $( "#tilesColor" ).colorpicker( "val", "#FFFFFF" );
                $( "#rightColor" ).colorpicker( "val", "#00FF00" );


                $( "#tilesForm" ).show();
                $( "#tilesForm" ).foundation( "resetForm" );
                $( "[name='tiles.@case'][value='uppercase']" ).prop( "checked", true );
                if ( LandPlayEdit.editor.json.config.setup.tiles != null ) {
                    var tiles = LandPlayEdit.editor.json.config.setup.tiles;
                    $( "[name*='tiles.']" ).each( function() {
                        var name = $( this ).attr( "name" );
                        if ( name == "tiles.@case" ) {
                            $( "[name='" + name + "'][value='" + tiles["@case"] + "']" ).prop( "checked", true );
                        } else {
                            $( this ).val( LandPlayEdit.editor.parse( { tiles: tiles }, name ) );
                        }
                    } );
                    $( "#currentColor" ).colorpicker( "val", tiles.colors["@current"] );
                    if ( tiles["@colors"] != null ) {
                        classRef.loadColors( tiles["@colors"], "tiles.@colors" );
                    }
                    if ( tiles.colors != null && tiles.colors["@right"] != null ) {
                        classRef.loadColors( tiles.colors["@right"], "tiles.colors.@right" );
                    }
                }
            } );
        }

        updateTiles() {
            var container = {};
            $( "[name*='tiles.']" ).each( function() {
                var field = {
                    name: $( this ).attr( "name" ),
                    value: $( this ).val()
                }
                var ignoreEmpty = true;
                container = LandPlayEdit.editor.walk( container, field, ignoreEmpty );

            } );
            container.tiles["@case"] = $( "[name='tiles.@case']:checked" ).val();

            var colors = [];
            $( "[name='tiles.@colors'] option" ).each( function() {
                colors.push( $( this ).val() );
            } );
            container.tiles["@colors"] = colors.join( "," );

            colors = [];
            $( "[name='tiles.colors.@right'] option" ).each( function() {
                colors.push( $( this ).val() );
            } );
            container.tiles.colors["@right"] = colors.join( "," );

            LandPlayEdit.editor.json.config.setup.tiles = container.tiles;
            LandPlayEdit.saveData();
            this.preview();
            $( "#pluginEditor" ).foundation( "close" );
        }

        private loadColors( csvcolors: string, fieldname: string ) {
            var colors = csvcolors.split( "," );
            for ( var i = 0; i < colors.length; i++ ) {
                var option = $( "<option>" );
                $( option ).attr( "value", colors[i] );
                $( option ).html( colors[i] );
                $( option ).attr( "style", "background-color:" + colors[i] );
                $( "[name='" + fieldname + "']" ).append( option );
            }
        }

        private loadRemainingSpritesForWords( value?: string ) {
            var collection = this.loadRemainingSpritesForWhatever( value );
            this.doLoadRemainingSprites( collection, "word.@sprite", value );
        }

        private loadRemainingSpritesForWhatever( value?: string ) {
            var collection: Array<object> = new Array<object>();

            if ( LandPlayEdit.editor.json.config.words != null && LandPlayEdit.editor.json.config.words.word != null ) {
                var words = LandPlayEdit.editor.json.config.words.word;
                if ( collection == null ) {
                    collection = new Array<object>();
                }
                collection = collection.concat( words )
            }
            return collection;
        }

        openWords( pos?: number ) {
            var classRef = this;
            this.modal( "hiddenword.html", function( params?: object ) {
                var wordForm = new Foundation.Abide( $( "#wordForm" ), {} );
                Foundation.reInit( 'abide' );

                $( "#createWord" ).click( function() {
                    classRef.createWord();
                } );
                $( "#wordForm" ).submit( function( event ) {
                    event.preventDefault();
                } );

                $( '#wordForm' ).on( "formvalid.zf.abide", function( ev, frm ) {
                    classRef.updateWord();
                } );

                $( "[name*='word.'], [name='transient.opentext']" ).change( function() {
                    classRef.validateWord( $( this ).attr( "name" ) );
                } );

                $( ".orientation" ).click( function() {
                    $( ".orientation" ).each( function() {
                        $( this ).removeClass( "selected" );
                    } );
                    $( this ).addClass( "selected" );
                    var geo = $( this ).attr( "data-geo" );
                    $( "[name='word.@xoffset']" ).val( 0 );
                    $( "[name='word.@yoffset']" ).val( 0 );
                    if ( geo.indexOf( "n" ) > -1 ) {
                        $( "[name='word.@yoffset']" ).val( -1 );
                    } else if ( geo.indexOf( "s" ) > -1 ) {
                        $( "[name='word.@yoffset']" ).val( 1 );
                    }
                    if ( geo.indexOf( "w" ) > -1 ) {
                        $( "[name='word.@xoffset']" ).val( -1 );
                    } else if ( geo.indexOf( "e" ) > -1 ) {
                        $( "[name='word.@xoffset']" ).val( 1 );
                    }
                    classRef.validateWord( "word.@xoffset" );
                } );

                $( "#autoCheckWord" ).click( function() {
                    classRef.validateWord( "word.@xoffset" );
                } );

                var tiles = LandPlayEdit.editor.json.config.setup.tiles;
                if ( tiles != null ) {
                    var rows = tiles["@rows"];
                    var cols = tiles["@cols"];
                    if ( isNaN( rows ) ) {
                        rows = LandPlay.WordSearch.DEFAULT_ROWS;
                    }
                    if ( isNaN( cols ) ) {
                        cols = LandPlay.WordSearch.DEFAULT_COLS;
                    }
                    for ( var row = 0; row < rows; row++ ) {
                        var tr = $( "<tr>" );
                        for ( var col = 0; col < cols; col++ ) {
                            var td = $( "<td>" );
                            $( td ).attr( "data-row", row );
                            $( td ).attr( "data-col", col );
                            $( td ).html( "&nbsp;" );
                            $( tr ).append( td );
                        }
                        $( "#wordsearchPreview" ).append( tr );
                    }
                    $( "[name='word.@x']" ).attr( "max", cols - 1 );
                    $( "[name='word.@y']" ).attr( "max", rows - 1 );
                }

                var hasWords = false;

                if ( LandPlayEdit.editor.json.config.words != null && LandPlayEdit.editor.json.config.words.word != null ) {
                    for ( var i = 0; i < LandPlayEdit.editor.json.config.words.word.length; i++ ) {
                        hasWords = true;
                        var word = LandPlayEdit.editor.json.config.words.word[i];
                        var text = "'" + word["@text"] + "'";
                        LandPlayEdit.listRecord( "#hiddenword-list", i, text );
                    }

                }
                if ( !hasWords ) {
                    var textDiv = $( '#templates > #tpl_word_no_item > div:first-child' ).clone( true );
                    $( "#hiddenword-list" ).append( textDiv );
                } else {
                    LandPlayEdit.activateList( "hiddenword" );
                }

                $( "#wordForm" ).hide();

                if ( params != null && params.pos != null ) {
                    $( "#wordForm" ).show();
                    classRef.editWord( params.pos );
                }

            }, { pos: pos } );
        }

        createWord() {
            $( "#wordForm" ).show();
            $( "#wordForm" ).foundation( "resetForm" );
            $( "[name='word_pos']" ).val( "" );
            $( "[name='word.@x']" ).val( 0 );
            $( "[name='word.@y']" ).val( 0 );
            $( "[name='word.@xoffset']" ).val( 1 );
            $( "[name='word.@yoffset']" ).val( 0 );
            $( "[name='transient.opentext'][value='false']" ).prop( "checked", true );
            LandPlayEdit.reloadAssetsSelect( LandPlay.AssetType.Image, "[name='word.@key']", null, true );
            this.loadRemainingSpritesForWords();
        }

        editWord( pos: number ) {
            this.clearWords();
            if ( LandPlayEdit.editor.json.config.words != null && LandPlayEdit.editor.json.config.words.word != null ) {

                if ( this.hasItem( LandPlayEdit.editor.json.config.words.word, pos ) ) {
                    this.createWord();
                    var word = LandPlayEdit.editor.json.config.words.word[pos];

                    $( "[name*='word.']" ).each( function() {
                        var name = $( this ).attr( "name" );
                        $( this ).val( LandPlayEdit.editor.parse( { word: word }, name ) );
                    } );

                    $( "[data-geo]" ).removeClass( "selected" );
                    var geo = "";
                    if ( word["@yoffset"] < 0 ) {
                        geo = "n";
                    } else if ( word["@yoffset"] > 0 ) {
                        geo = "s";
                    }
                    if ( word["@xoffset"] < 0 ) {
                        geo += "w";
                    } else if ( word["@xoffset"] > 0 ) {
                        geo += "e";
                    }
                    $( "[data-geo='" + geo + "']" ).addClass( "selected" );

                    word.editing = true;
                    this.renderWord( word );

                    this.loadRemainingSpritesForWords( word["@sprite"] );

                    $( "[name='word_pos']" ).val( pos );
                }
            }
        }

        updateWord() {
            var container = {};
            $( "[name*='word.']" ).each( function() {
                var field = {
                    name: $( this ).attr( "name" ),
                    value: $( this ).val()
                }
                var ignoreEmpty = true;
                container = LandPlayEdit.editor.walk( container, field, ignoreEmpty );

            } );

            var pos = parseInt( $( "[name='word_pos']" ).val() );
            if ( LandPlayEdit.editor.json.config.words == null ) {
                LandPlayEdit.editor.json.config.words = { word: [] };
            }
            if ( LandPlayEdit.editor.json.config.words.word == null ) {
                LandPlayEdit.editor.json.config.words.word = [];
            }
            this.updateRecord( LandPlayEdit.editor.json.config.words.word, pos, container.word );
            LandPlayEdit.saveData();
            this.preview();
            $( "#pluginEditor" ).foundation( "close" );
            this.openWords();
        }

        deleteWord( pos: number ) {
            var classRef = this;
            LandPlayEdit.confirm( i18next.t( "reveal.word.delete" ), function() {
                if ( LandPlayEdit.editor.json.config.words != null && LandPlayEdit.editor.json.config.words.word != null ) {
                    if ( classRef.hasItem( LandPlayEdit.editor.json.config.words.word, pos ) ) {
                        LandPlayEdit.editor.json.config.words.word.splice( pos, 1 );
                        LandPlayEdit.saveData();
                        classRef.preview();
                        $( "#pluginEditor" ).foundation( "close" );
                        classRef.openWords();
                    }
                }
            } );
        }

        private clearWords() {
            $( "[data-row]" ).html( "&nbsp;" );
            if ( LandPlayEdit.editor.json.config.words != null && LandPlayEdit.editor.json.config.words.word != null ) {
                for ( var i = 0; i < LandPlayEdit.editor.json.config.words.word.length; i++ ) {
                    var word = LandPlayEdit.editor.json.config.words.word[i];
                    word.editing = false;
                    this.renderWord( word );
                }
            }
            $( "[data-row].edit" ).removeClass( "edit" );
            $( "[data-row].error" ).removeClass( "error" );
        }

        private renderWord( params: object ) {
            var col = parseInt( params["@x"] );
            var row = parseInt( params["@y"] );
            var word = params["@text"];
            word = word.replace(/ /g, "");
            var xo = parseInt( params["@xoffset"] );
            var yo = parseInt( params["@yoffset"] );
            var letters = word.split( "" );
            var editing = params.editing;
            var ok = true;
            for ( var letter of letters ) {
                var cellSel = "[data-row=" + row + "][data-col=" + col + "]";
                if ( $( cellSel ).length == 1 ) {
                    if ( editing ) {
                        $( cellSel ).addClass( "edit" );
                        var current = $( cellSel ).html();
                        if ( current != "&nbsp;" && current != letter ) {
                            $( cellSel ).addClass( "error" );
                            /*ok = false;*/ /*too much restrictive*/
                        }
                    }
                    $( cellSel ).html( letter );
                } else {
                    if ( editing ) {
                        /* the filled cells so far are marked as mistakes */
                        $( "[data-row].edit" ).addClass( "error" );
                        $( "[data-row].edit" ).removeClass( "edit" );
                    }
                    return false;
                }
                col += xo;
                row += yo;
            }
            return ok;
        }

        validateWord( name ) {
            this.clearWords();

            var check = this.renderWord( {
                "@text": $( "[name='word.@text']" ).val(),
                "@x": $( "[name='word.@x']" ).val(),
                "@y": $( "[name='word.@y']" ).val(),
                "@xoffset": $( "[name='word.@xoffset']" ).val(),
                "@yoffset": $( "[name='word.@yoffset']" ).val(),
                editing: true
            } );

            $( "#autoCheckWord" ).prop( "checked", check );
            if ( check ) {
                $( "[for=autoCheckWord]" ).removeClass( "is-invalid-label" );
            }

            var selector = "[name='" + name + "']";
            var value = $( selector ).val();
            switch ( name ) {
                case "transient.opentext":
                    value = $( selector + ":checked" ).val();
                    var required = true;
                    if ( value == "true" ) {
                        $( "[name='word.@text']" ).val( "" );
                        $( "[name='word.split.@offsetx']" ).val( "" );
                        $( "[name='word.split.@offsety']" ).val( "" );
                        required = false;
                    }

                    LandPlayEdit.toggleField( "word.@text", required );
                    LandPlayEdit.toggleField( "word.split.@offsetx", required );
                    LandPlayEdit.toggleField( "word.split.@offsety", required );
                    break;

                case "word.@sprite":
                    var required = ( value == "" );
                    $( "[name='word.@key']" ).val( "" );
                    LandPlayEdit.toggleField( "word.@key", required );
                    break;

                case "word.@key":
                    var required = ( value == "" );
                    $( "[name='word.@sprite']" ).val( "" );
                    LandPlayEdit.toggleField( "word.@sprite", required );
                    break;

                default:
                    break;
            }
        }

        validate() {
            this.resetConstraints();
            if ( LandPlayEdit.editor.json.config.words == null || LandPlayEdit.editor.json.config.words.word == null
                || LandPlayEdit.editor.json.config.words.word.length == 0 ) {
                this.addConstraint( new LandPlayEdit.Constraint( "behaviour", "hiddenword", i18next.t( "plugin.constraint.wordsearch.hiddenword" ) ) );
            } else {
                this.checkSprites( LandPlayEdit.editor.json.config.words.word, "[hidden word]" );
            }

            return super.validate();
        }

    }
}