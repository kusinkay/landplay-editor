/// <reference path='Plugin.ts' />

module LandPlayEdit {
    export class PluginDragDrop extends LandPlayEdit.Plugin {

        constructor() {
            super( {
                toolBox: {
                    behaviour: [
                        {
                            key: "container",
                            label: "plugin.toolbox.dragdrop.container.label",
                            icon: "fas fa-inbox"
                        }
                    ]
                }
            } );
        }

        getTemplateName(): string {
            return "dragdrop.html";
        }

        behaviour( type ) {
            switch ( type ) {
                case "container":
                    this.openContainers();
                    break;
                default:
                    LandPlayEdit.warning( "Please develop pending behaviour '" + type + "'" );
                    break;
            }
        }

        editRecord( type: string, pos: number ) {
            switch ( type ) {
                case "container":
                    this.editContainer( pos );
                    break;
                default:
                    break;
            }
        }

        deleteRecord( type: string, pos: number ) {
            switch ( type ) {
                case "container":
                    this.deleteContainer( pos );
                    break;

                default:
                    break;
            }
        }

        private hasContainer( pos: string ) {
            return this.hasItem( LandPlayEdit.editor.json.config.container, pos );
        }

        private hasAnswer( pos: string, answers: Array<object> ) {
            return this.hasItem( answers, pos );
        }

        private loadRemainingSpritesForContainers( value?: string ) {
            var collection: Array<object> = this.getUsedSpritesInContainers();
            $( "[name='container.@answer'] option" ).each( function() {
                collection.push( { "@sprite": $( this ).val() } );
            } );
            this.doLoadRemainingSprites( collection, "container.@sprite", value, false, true );
        }

        private loadRemainingSpritesForAnswers( value?: string ) {
            var collection: Array<object> = this.getUsedSpritesInContainers( false );
            collection.push( { "@sprite": $( "[name='container.@sprite']" ).val() } );
            this.doLoadRemainingSprites( collection, "transient.container.@answer", value, false, true );
        }

        private getUsedSpritesInContainers( answers?: boolean ) {
            var collection: Array<object> = new Array<object>();

            if ( LandPlayEdit.editor.json.config.container != null ) {
                var containers = LandPlayEdit.editor.json.config.container;
                if ( answers ) {
                    for ( var i = 0; i < containers.length; i++ ) {
                        if ( containers[i].answer != null ) {
                            collection = collection.concat( containers[i].answer );
                        }
                    }
                }
                collection = collection.concat( containers );
            }
            return collection;
        }

        openEvents( pos: string, subPos: string ) {
            pos = parseInt( pos );
            if ( this.hasContainer( pos ) ) {
                var container = LandPlayEdit.editor.json.config.container[pos];
                if ( this.hasAnswer( subPos, container.answer ) ) {
                    var answer = container.answer[subPos];
                    $( "#pluginEditor" ).foundation( "close" );
                    var excludedCauses: Array<string> = Object.keys( LandPlay.LandEvent.Cause );
                    excludedCauses.splice( excludedCauses.indexOf( "CAUSE_RIGHT" ), 1 );
                    var addAll = true;
                    var addThis = true;
                    return LandPlayEdit.openEventsDo( answer.event, pos, excludedCauses, null, subPos, addAll, addThis );
                }
            }
            LandPlayEdit.alert( "No way to find the answer to attatch events to" );
        }


        getEventsCollection( container: number, subcontainer: number ) {
            if ( this.hasContainer( container ) ) {
                var container = LandPlayEdit.editor.json.config.container[container];
                if ( this.hasAnswer( subcontainer, container.answer ) ) {
                    var answer = container.answer[subcontainer];
                    if ( answer.event == null ) {
                        answer.event = [];
                    }
                    return answer.event;
                }
            }
            return null;
        }

        openContainers() {
            var classRef = this;
            this.modal( "container.html", function() {
                var containerForm = new Foundation.Abide( $( "#containerForm" ), {} );
                Foundation.reInit( 'abide' );

                $( "#createContainer" ).click( function() {
                    classRef.createContainer();
                } );
                $( "#containerForm" ).submit( function( event ) {
                    event.preventDefault();
                } );

                $( '#containerForm' ).on( "formvalid.zf.abide", function( ev, frm ) {
                    classRef.updateContainer();
                } );

                $( "#eventsOpen" ).click( function() {
                    var pos = $( "[name=container_pos]" ).val();
                    var howMany = $( '[name="container.@answer"] option:selected' ).length;
                    if ( howMany == 0 ) {
                        LandPlayEdit.alert( i18next.t( "plugin.constraint.dragdrop.event.zero" ) );
                    } else if ( howMany > 1 ) {
                        LandPlayEdit.alert( i18next.t( "plugin.constraint.dragdrop.event.gtone" ) );
                    } else {
                        var subPos = $( '[name="container.@answer"]' )[0].selectedIndex;
                        classRef.openEvents( pos, subPos );
                    }
                } );

                $( "[name*='container.']" ).change( function() {
                    classRef.validateContainer( $( this ).attr( "name" ) );
                } );

                $( "#addContainerAnswer" ).click( function() {
                    $( "[name='transient.container.@answer'] option:selected" ).each( function() {
                        var value = $( this ).val();
                        if ( value != "" ) {
                            if ( $( "[name='container.@answer'] option[value='" + value + "']" ).length == 0 ) {
                                var option = $( "<option>" );
                                $( option ).attr( "value", value );
                                $( option ).html( value );
                                $( "[name='container.@answer']" ).append( option );
                            }

                        }
                    } );
                    classRef.validateContainer( "container.@answer" );
                } );

                $( "#delContainerAnswer" ).click( function() {
                    $( "[name='container.@answer'] option:selected" ).each( function() {
                        var value = $( this ).attr( "value" );
                        $( this ).remove();
                        if ( $( "[name='transient.container.@answer'] option[value='" + value + "']" ).length == 0 ) {
                            /* now it is available */
                            var option = $( "<option>" );
                            $( option ).attr( "value", value );
                            $( option ).html( value );
                            $( "[name='transient.container.@answer']" ).append( option );
                        }

                    } );
                    classRef.validateContainer( "container.@answer" );
                } );


                var hasContainers = false;

                if ( LandPlayEdit.editor.json.config.container != null ) {
                    for ( var i = 0; i < LandPlayEdit.editor.json.config.container.length; i++ ) {
                        hasContainers = true;
                        var container = LandPlayEdit.editor.json.config.container[i];
                        var label = container["@sprite"];
                        if ( label == null ) {
                            label = i18next.t( "reveal.container.sprite.empty" );
                        }
                        LandPlayEdit.listRecord( "#container-list", i, label );
                    }

                }
                if ( !hasContainers ) {
                    var textDiv = $( '#templates > #tpl_container_no_item > div:first-child' ).clone( true );
                    $( "#container-list" ).append( textDiv );
                } else {
                    LandPlayEdit.activateList( "container" );
                }

                $( "#containerForm" ).hide();

            } );
        }

        hasContainer( pos ) {
            return LandPlayEdit.editor.json.config.container != null && !isNaN( pos ) && pos >= 0 && pos < LandPlayEdit.editor.json.config.container.length;
        }

        createContainer() {
            $( "#containerForm" ).show();
            $( "[name='container.@answer']" ).html( "" );
            $( "#containerForm" ).foundation( "resetForm" );
            $( "[name='container_pos']" ).val( "" );
            this.loadRemainingSpritesForContainers();
            this.loadRemainingSpritesForAnswers();
        }

        editContainer( pos: number ) {
            if ( this.hasContainer( pos ) ) {
                this.createContainer();
                var container = LandPlayEdit.editor.json.config.container[pos];
                $( "[name*='container.@']" ).each( function() {
                    var attr = $( this ).attr( "name" ).replace( "container.@", "" );
                    if ( ( attr == "values" || attr == "extra-values" ) && container["@" + attr] != null ) {
                        container["@" + attr] = container["@" + attr].split( "|" ).join( "\r\n" );
                    }
                    $( this ).val( container["@" + attr] );
                    if ( attr == "answer" && container.answer != null ) {
                        for ( var i = 0; i < container.answer.length; i++ ) {
                            var value = container.answer[i]["@sprite"];
                            var option = $( "<option>" );
                            $( option ).attr( "value", value );
                            $( option ).html( value );
                            $( this ).append( option );
                        }
                    }
                } );
                /** avoid to repeat sprites */
                this.loadRemainingSpritesForContainers( container["@sprite"] );
                $( "[name='container_pos']" ).val( pos );
            }
        }

        updateContainer() {
            var container = {};
            $( "[name*='container.@']" ).each( function() {
                var attr = $( this ).attr( "name" ).replace( "container.@", "" );
                container["@" + attr] = $( this ).val();

                switch ( attr ) {
                    case "answer":
                        $( "option", $( this ) ).each( function() {
                            if ( container.answer == null ) {
                                container.answer = [];
                            }
                            container.answer.push( { "@sprite": $( this ).val() } );
                        } );
                        break;
                    case "values":
                    case "extra-values":
                        container["@" + attr] = container["@" + attr].replace( /(?:\r\n|\r|\n)/gm, '|' )
                        break;

                    default:
                        break;
                }

                if ( container["@" + attr] == "" ) {
                    delete container["@" + attr];
                }
            } );
            if ( LandPlayEdit.editor.json.config.container == null ) {
                LandPlayEdit.editor.json.config.container = [];
            }
            var pos = parseInt( $( "[name='container_pos']" ).val() );
            var replaced = this.updateRecord( LandPlayEdit.editor.json.config.container, pos, container );
            if ( replaced != null ) {
                if ( replaced.answer != null && container.answer != null ) {
                    var found = false;
                    for ( var i = 0; i < container.answer.length; i++ ) {
                        for ( var j = 0; j < replaced.answer.length; j++ ) {
                            if ( container.answer[i]["@sprite"] == replaced.answer[j]["@sprite"] ) {
                                if ( replaced.answer[j].event != null ) {
                                    /* recover its events and save */
                                    container.answer[i].event = replaced.answer[j].event;
                                    this.updateRecord( LandPlayEdit.editor.json.config.container, pos, container );
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if ( found ) break;
                    }
                }
            }

            LandPlayEdit.saveData();
            $( "#pluginEditor" ).foundation( "close" );
            this.openContainers();
        }

        deleteContainer( pos: number ) {
            var classRef = this;
            LandPlayEdit.confirm( i18next.t( "reveal.container.delete" ), function() {
                if ( LandPlayEdit.editor.json.config.container != null ) {
                    if ( !isNaN( pos ) && pos >= 0 && pos < LandPlayEdit.editor.json.config.container.length ) {
                        LandPlayEdit.editor.json.config.container.splice( pos, 1 );
                        LandPlayEdit.saveData();
                        $( "#pluginEditor" ).foundation( "close" );
                        classRef.openContainers();
                    }
                }
            } );
        }

        validateContainer( name ) {
            var selector = "[name='" + name + "']";
            var value = $( selector ).val();
            if ( name == "container.@sprite" ) {
                this.loadRemainingSpritesForAnswers();
            } else if ( name == "container.@answer" ) {
                this.loadRemainingSpritesForContainers( $( "[name='container.@sprite']" ).val() );
            }
        }

        validate() {
            this.resetConstraints();
            if ( LandPlayEdit.editor.json.config.container == null
                || LandPlayEdit.editor.json.config.container.length == 0 ) {
                this.addConstraint( new LandPlayEdit.Constraint( "behaviour", "container", i18next.t( "plugin.constraint.dragdrop.container" ) ) );
            } else {

                var hasSprite = false;
                for ( var i = 0; i < LandPlayEdit.editor.json.config.container.length; i++ ) {
                    var container = LandPlayEdit.editor.json.config.container[i];
                    if ( container["@sprite"] != null && container["@sprite"] != "" ) {
                        hasSprite = true;
                        break;
                    }
                }
                if ( !hasSprite ) {
                    this.addConstraint( new LandPlayEdit.Constraint( "behaviour", "container", i18next.t( "plugin.constraint.dragdrop.nosprite" ) ) );
                }

                this.checkSprites( LandPlayEdit.editor.json.config.container, "[container]" );
            }
            return super.validate();
        }

    }
}