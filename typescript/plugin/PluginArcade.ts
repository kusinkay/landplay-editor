/// <reference path='Plugin.ts' />

module LandPlayEdit {
    export class PluginArcade extends LandPlayEdit.Plugin {

        private brushSelects = ["brush.@sprite", "brush.@brushpoint", "brush.@toolcase"];

        constructor() {
            super( {
                toolBox: {
                    behaviour: [
                        {
                            key: "stage",
                            label: "plugin.toolbox.arcade.stage.label",
                            icon: "fas fa-rocket"
                        },
                        {
                            key: "edge",
                            label: "plugin.toolbox.arcade.edge.label",
                            icon: "fas fa-vector-square"
                        },
                        {
                            key: "asteroid",
                            label: "plugin.toolbox.arcade.asteroid.label",
                            icon: "fas fa-meteor"
                        },
                        {
                            key: "landscape",
                            label: "plugin.toolbox.arcade.landscape.label",
                            icon: "fas fa-tree"
                        }
                    ]
                },
                effects: [{ value: "tint" }]
            } );
        }

        getTemplateName(): string {
            return "arcade.html";
        }

        behaviour( type ) {
            switch ( type ) {
                case "edge":
                    this.openEdges();
                    break;

                case "landscape":
                    this.openLandscapes();
                    break;

                case "asteroid":
                    this.openAsteroids();
                    break;

                case "stage":
                    this.editStage();
                    break;

                default:
                    LandPlayEdit.warning( "Please develop pending behaviour '" + type + "'" );
                    break;
            }
        }

        editRecord( type: string, pos: number ) {
            switch ( type ) {
                case "edge":
                    this.editEdge( pos );
                    break;

                case "asteroid":
                    this.editAsteroid( pos );
                    break;

                case "landscape":
                    this.editLandscape( pos );
                    break;

                default:
                    break;
            }
        }

        deleteRecord( type: string, pos: number ) {
            switch ( type ) {
                case "edge":
                    this.deleteEdge( pos );
                    break;

                case "canvas":
                    this.deleteCanvas( pos );
                    break;

                case "landscape":
                    this.deleteLandscape( pos );
                    break;

                default:
                    break;
            }
        }

        private loadRemainingSpritesForCraft( value?: string ) {
            var collection = this.loadRemainingSpritesForWhatever( value );
            this.doLoadRemainingSprites( collection, "craft.@sprite", value );
        }

        private loadRemainingSpritesForEdges( value?: string ) {
            var collection = this.loadRemainingSpritesForWhatever( value );
            this.doLoadRemainingSprites( collection, "edge.@sprite", value );
        }

        private loadRemainingSpritesForLandscapes( value?: string ) {
            var collection = this.loadRemainingSpritesForWhatever( value );
            this.doLoadRemainingSprites( collection, "landscape.@sprite", value, true );
        }
        private loadRemainingSpritesForWhatever( value?: string ) {
            var collection: Array<object> = this.getUsedSpritesInLandscapes();

            if ( LandPlayEdit.editor.json.config.edge != null ) {
                var edges = LandPlayEdit.editor.json.config.edge;
                if ( collection == null ) {
                    collection = new Array<object>();
                }
                collection = collection.concat( edges )
            }
            if ( LandPlayEdit.editor.json.config.landscape != null ) {
                var landscapes = LandPlayEdit.editor.json.config.landscape;
                if ( collection == null ) {
                    collection = new Array<object>();
                }
                collection = collection.concat( landscapes )
            }
            collection.push( LandPlayEdit.editor.json.config.craft )
            return collection;
        }

        private getUsedSpritesInLandscapes() {
            var collection: Array<object> = new Array<object>();
            if ( LandPlayEdit.editor.json.config.landscape != null ) {
                collection.push( LandPlayEdit.editor.json.config.landscape );
            }

            return collection;
        }

        private loadEdgeSprites( value?: string ) {
            var edges: Array<string> = new Array<string>();
            if ( LandPlayEdit.editor.json.config.edge != null ) {
                for ( var i = 0; i < LandPlayEdit.editor.json.config.edge.length; i++ ) {
                    var edge: object = LandPlayEdit.editor.json.config.edge[i];
                    edges.push( edge["@sprite"] );
                }
                if ( LandPlayEdit.lgEditor.sprites != null ) {
                    var excluded: Array<object> = new Array<object>();
                    var keys: Array<string> = Object.keys( LandPlayEdit.lgEditor.sprites );
                    keys.sort();
                    for ( var i = 0; i < keys.length; i++ ) {
                        if ( edges.indexOf( keys[i] ) < 0 ) {
                            excluded.push( { "@sprite": keys[i] } );
                        }
                    }
                    this.doLoadRemainingSprites( excluded, "canvas.@edge", value );
                }
            } else {
                LandPlayEdit.alert( i180next.t( "plugin.constraint.paint.noEdgeSprites" ) );
            }


        }

        validateEvent( name ) {
            var selector = "[name='" + name + "']";
            var extended = false;
            switch ( name ) {
                case "event.@type":
                    var type = $( selector ).val();
                    switch ( type ) {
                        case LandPlay.PaintEvents.TYPE_TINT:
                            LandPlayEdit.toggleField( "event.@sprite", true );
                            LandPlayEdit.toggleField( "event.@value", true, "#([a-fA-F0-9]{3})([a-fA-F0-9]{3})?", i18next.t( "validator.color.required" ) );
                            var force = true;
                            LandPlayEdit.reloadSpriteSelect( $( "[name='event.@sprite']" ), null, force, extended );
                            break;
                        default:
                            console.warn( "Effect '" + type + "' has not dependent selectors actions" );
                    }
                    break;
            }
        }

        validateStage( name ) {
            var selector = "[name='" + name + "']";
            var value = $( selector ).val();
            for ( var i = 0; i < this.brushSelects.length; i++ ) {
                var otherSelector = this.brushSelects[i];
                otherSelector = "[name='" + otherSelector + "']";
                if ( otherSelector != selector ) {
                    var force = true;
                    var except = [{ "@sprite": value }];
                    var excluded = LandPlayEdit.editor.json.config.edge.concat( LandPlayEdit.editor.json.config.canvas );
                    excluded = excluded.concat( except );
                    classRef.doLoadRemainingSprites( excluded, otherSelector, $( otherSelector ).val() );
                }
            }
        }

        private hasEdge( pos: string ) {
            return this.hasItem( LandPlayEdit.editor.json.config.edge, pos );
        }

        private hasCanvas( pos: string ) {
            return this.hasItem( LandPlayEdit.editor.json.config.canvas, pos );
        }

        private hasItem( collection: Array<object>, pos: string ) {
            pos = parseInt( pos );
            if ( collection != null ) {
                if ( !isNaN( pos ) && pos >= 0 && pos < collection.length ) {
                    return true;
                }
            }
            return false;
        }

        openAsteroids() {
            var classRef = this;
            this.modal( "asteroid.html", function() {
                var pieceForm = new Foundation.Abide( $( "#asteroidForm" ), {} );
                Foundation.reInit( 'abide' );

                $( "#createAsteroid" ).click( function() {
                    classRef.createAsteroid();
                } );
                $( "#asteroidForm" ).submit( function( event ) {
                    event.preventDefault();
                } );

                $( '#asteroidForm' ).on( "formvalid.zf.abide", function( ev, frm ) {
                    classRef.updateAsteroid();
                } );

                $( '[data-slider]' ).on( "moved.zf.slider", function( ev, frm ) {
                    var id = $( this ).attr( "id" );
                    var values = [];
                    $( "[type='hidden']", this ).each( function() {
                        values.push( $( this ).val() );
                    } );
                    $( "#" + id + "_range" ).html( values.join( " / " ) );

                    $( "[name='piece.@" + id + "']" ).val( values.join( "," ) );
                } );
                $( "#eventsOpen" ).click( function() {
                    var pos = $( "[name=asteroid_pos]" ).val();
                    classRef.openEvents( pos );
                } );

                $( "[data-slide-listen]" ).change( function() {
                    var id = $( this ).attr( "id" );
                    $( "#" + id + "_label" ).html( $( this ).val() );
                } );

                var hasAsteroids = false;

                if ( LandPlayEdit.editor.json.config.piece != null ) {
                    for ( var i = 0; i < LandPlayEdit.editor.json.config.piece.length; i++ ) {
                        hasAsteroids = true;
                        var piece = LandPlayEdit.editor.json.config.piece[i];
                        LandPlayEdit.listRecord( "#asteroid-list", i, piece["@attach"] );
                    }

                }
                if ( !hasAsteroids ) {
                    var textDiv = $( '#templates > #tpl_asteroid_no_item > div:first-child' ).clone( true );
                    $( "#asteroid-list" ).append( textDiv );
                } else {
                    LandPlayEdit.activateList( "asteroid" );
                }

                $( "#asteroidForm" ).hide();

            } );
        }

        createAsteroid() {
            $( "#asteroidForm" ).show();
            $( "#asteroidForm" ).foundation( "resetForm" );
            $( "#eventsOpen" ).attr( "disabled", "disabled" );
            $( "[name='asteroid_pos']" ).val( "" );
            LandPlayEdit.reloadAssetsSelect( LandPlay.AssetType.Image, "[name='piece.@attach']", null, true );
        }

        editAsteroid( pos: number ) {
            if ( LandPlayEdit.editor.json.config.piece != null ) {
                if ( !isNaN( pos ) && pos >= 0 && pos < LandPlayEdit.editor.json.config.piece.length ) {
                    this.createAsteroid();
                    var piece = LandPlayEdit.editor.json.config.piece[pos];
                    $( "[name*='piece.@']" ).each( function() {
                        var attr = $( this ).attr( "name" ).replace( "piece.@", "" );
                        var value = piece["@" + attr];
                        /**
                         * Sliders*/
                        if ( attr == "moveX" || attr == "moveY" || attr == "tics" || attr == "xdif" || attr == "ydif" ) {
                            var parts = value.split( "," );
                            if ( parts.length == 1 ) {
                                parts = [parts, parts];
                            }
                            for ( var i = 0; i < parts.length; i++ ) {
                                $( "#" + attr + " input[type=hidden]:eq(" + i + ")" ).val( parts[i] );
                                $( "#" + attr + " input[type=hidden]:eq(" + i + ")" ).trigger( "change" );
                            }

                        }
                        /*sliders*/
                        $( this ).val( piece["@" + attr] );
                    } );

                    $( "#eventsOpen" ).removeAttr( "disabled" );
                    $( "[name='asteroid_pos']" ).val( pos );
                }
            }
        }

        updateAsteroid() {
            var piece = {};
            $( "[name*='piece.@']" ).each( function() {
                var attr = $( this ).attr( "name" ).replace( "piece.@", "" );
                piece["@" + attr] = $( this ).val();

                if ( piece["@" + attr] == "" || piece["@" + attr] == null ) {
                    delete piece["@" + attr];
                }
            } );
            if ( LandPlayEdit.editor.json.config.piece == null ) {
                LandPlayEdit.editor.json.config.piece = [];
            }
            var pos = parseInt( $( "[name='asteroid_pos']" ).val() );
            this.updateRecord( LandPlayEdit.editor.json.config.piece, pos, piece );
            LandPlayEdit.saveData();
            $( "#pluginEditor" ).foundation( "close" );
            this.openAsteroids();
        }

        deleteAsteroid( pos: number ) {
            var classRef = this;
            LandPlayEdit.confirm( i18next.t( "reveal.asteroid.delete" ), function() {
                if ( LandPlayEdit.editor.json.config.piece != null ) {
                    if ( !isNaN( pos ) && pos >= 0 && pos < LandPlayEdit.editor.json.config.piece.length ) {
                        LandPlayEdit.editor.json.config.piece.splice( pos, 1 );
                        LandPlayEdit.saveData();
                        $( "#pluginEditor" ).foundation( "close" );
                        classRef.openAsteroids();
                    }
                }
            } );
        }

        openEvents( pos: string ) {
            pos = parseInt( pos );
            if ( this.hasPiece( pos ) ) {
                var piece = LandPlayEdit.editor.json.config.piece[pos];
                $( "#pluginEditor" ).foundation( "close" );
                var excludedCauses: Array<string> = Object.keys( LandPlay.LandEvent.Cause );
                excludedCauses.splice( excludedCauses.indexOf( "CAUSE_RIGHT" ), 1 );
                excludedCauses.splice( excludedCauses.indexOf( "CAUSE_WRONG" ), 1 );
                var addAll = true;
                var addThis = true;
                var subPos = null;
                return LandPlayEdit.openEventsDo( piece.event, pos, excludedCauses, null, subPos, addAll, addThis );
            }
            LandPlayEdit.alert( "No way to find the piece (asteroid) to attatch events to" );
        }

        getEventsCollection( container: number ) {
            if ( this.hasPiece( container ) ) {
                var piece = LandPlayEdit.editor.json.config.piece[container];
                if ( piece.event == null ) {
                    piece.event = [];
                }
                return piece.event;
            }
            return null;
        }

        private hasPiece( pos: string ) {
            pos = parseInt( pos );
            if ( LandPlayEdit.editor.json.config.piece != null ) {
                if ( !isNaN( pos ) && pos >= 0 && pos < LandPlayEdit.editor.json.config.piece.length ) {
                    return true;
                }
            }
            return false;
        }

        openEdges() {
            var classRef = this;
            this.modal( "edge.html", function() {
                var edgeForm = new Foundation.Abide( $( "#edgeForm" ), {} );
                Foundation.reInit( 'abide' );

                $( "#createEdge" ).click( function() {
                    classRef.createEdge();
                } );
                $( "#edgeForm" ).submit( function( event ) {
                    event.preventDefault();
                } );

                $( '#edgeForm' ).on( "formvalid.zf.abide", function( ev, frm ) {
                    classRef.updateEdge();
                } );

                var hasEdges = false;

                if ( LandPlayEdit.editor.json.config.edge != null ) {
                    for ( var i = 0; i < LandPlayEdit.editor.json.config.edge.length; i++ ) {
                        hasEdges = true;
                        var edge = LandPlayEdit.editor.json.config.edge[i];
                        LandPlayEdit.listRecord( "#edge-list", i, edge["@sprite"] );
                    }

                }
                if ( !hasEdges ) {
                    var textDiv = $( '#templates > #tpl_edge_no_item > div:first-child' ).clone( true );
                    $( "#edge-list" ).append( textDiv );
                } else {
                    LandPlayEdit.activateList( "edge" );
                }

                $( "#edgeForm" ).hide();

            } );
        }

        createEdge() {
            $( "#edgeForm" ).show();
            $( "#edgeForm" ).foundation( "resetForm" );
            $( "[name='edge_pos']" ).val( "" );
            this.loadRemainingSpritesForEdges();
        }

        editEdge( pos: number ) {
            if ( LandPlayEdit.editor.json.config.edge != null ) {
                if ( !isNaN( pos ) && pos >= 0 && pos < LandPlayEdit.editor.json.config.edge.length ) {
                    this.createEdge();
                    var edge = LandPlayEdit.editor.json.config.edge[pos];
                    $( "[name*='edge.@']" ).each( function() {
                        var attr = $( this ).attr( "name" ).replace( "edge.@", "" );
                        $( this ).val( edge["@" + attr] );
                    } );
                    /** avoid to repeat sprites */
                    this.loadRemainingSpritesForEdges( edge["@sprite"] );
                    $( "[name='edge_pos']" ).val( pos );
                }
            }
        }

        updateEdge() {
            var edge = {};
            $( "[name*='edge.@']" ).each( function() {
                var attr = $( this ).attr( "name" ).replace( "edge.@", "" );
                edge["@" + attr] = $( this ).val();

                if ( edge["@" + attr] == "" ) {
                    delete edge["@" + attr];
                }
            } );
            if ( LandPlayEdit.editor.json.config.edge == null ) {
                LandPlayEdit.editor.json.config.edge = [];
            }
            var pos = parseInt( $( "[name='edge_pos']" ).val() );
            if ( !isNaN( pos ) ) {
                if ( LandPlayEdit.editor.json.config.edge != null ) {
                    if ( pos >= 0 && pos < LandPlayEdit.editor.json.config.edge.length ) {
                        LandPlayEdit.editor.json.config.edge.splice( pos, 1 );
                    }
                }
            }
            LandPlayEdit.editor.json.config.edge.push( edge );
            LandPlayEdit.saveData();
            $( "#pluginEditor" ).foundation( "close" );
            this.openEdges();
        }

        deleteEdge( pos: number ) {
            var classRef = this;
            LandPlayEdit.confirm( i18next.t( "reveal.edge.delete" ), function() {
                if ( LandPlayEdit.editor.json.config.edge != null ) {
                    if ( !isNaN( pos ) && pos >= 0 && pos < LandPlayEdit.editor.json.config.edge.length ) {
                        LandPlayEdit.editor.json.config.edge.splice( pos, 1 );
                        LandPlayEdit.saveData();
                        $( "#pluginEditor" ).foundation( "close" );
                        classRef.openEdges();
                    }
                }
            } );
        }

        openLandscapes() {
            var classRef = this;
            this.modal( "landscape.html", function() {
                var landscapeForm = new Foundation.Abide( $( "#landscapeForm" ), {} );
                Foundation.reInit( 'abide' );

                $( "#createLandscape" ).click( function() {
                    classRef.createLandscape();
                } );
                $( "#landscapeForm" ).submit( function( event ) {
                    event.preventDefault();
                } );

                $( '#landscapeForm' ).on( "formvalid.zf.abide", function( ev, frm ) {
                    classRef.updateLandscape();
                } );

                var hasLandscapes = false;

                if ( LandPlayEdit.editor.json.config.landscape != null ) {
                    for ( var i = 0; i < LandPlayEdit.editor.json.config.landscape.length; i++ ) {
                        hasLandscapes = true;
                        var landscape = LandPlayEdit.editor.json.config.landscape[i];
                        LandPlayEdit.listRecord( "#landscape-list", i, landscape["@sprite"] );
                    }

                }
                if ( !hasLandscapes ) {
                    var textDiv = $( '#templates > #tpl_landscape_no_item > div:first-child' ).clone( true );
                    $( "#landscape-list" ).append( textDiv );
                } else {
                    LandPlayEdit.activateList( "landscape" );
                }

                $( "#landscapeForm" ).hide();

            } );
        }

        createLandscape() {
            $( "#landscapeForm" ).show();
            $( "#landscapeForm" ).foundation( "resetForm" );
            $( "[name='landscape_pos']" ).val( "" );
            this.loadRemainingSpritesForLandscapes();
        }

        editLandscape( pos: number ) {
            if ( LandPlayEdit.editor.json.config.landscape != null ) {
                if ( !isNaN( pos ) && pos >= 0 && pos < LandPlayEdit.editor.json.config.landscape.length ) {
                    this.createLandscape();
                    var landscape = LandPlayEdit.editor.json.config.landscape[pos];
                    $( "[name*='landscape.@']" ).each( function() {
                        var attr = $( this ).attr( "name" ).replace( "landscape.@", "" );
                        $( this ).val( landscape["@" + attr] );
                    } );
                    /** avoid to repeat sprites */
                    this.loadRemainingSpritesForLandscapes( landscape["@sprite"] );
                    $( "[name='landscape_pos']" ).val( pos );
                }
            }
        }

        updateLandscape() {
            var landscape = {};
            $( "[name*='landscape.@']" ).each( function() {
                var attr = $( this ).attr( "name" ).replace( "landscape.@", "" );
                landscape["@" + attr] = $( this ).val();

                if ( landscape["@" + attr] == "" || landscape["@" + attr] == null ) {
                    delete landscape["@" + attr];
                }
            } );
            if ( LandPlayEdit.editor.json.config.landscape == null ) {
                LandPlayEdit.editor.json.config.landscape = [];
            }
            var pos = parseInt( $( "[name='landscape_pos']" ).val() );
            if ( !isNaN( pos ) ) {
                if ( LandPlayEdit.editor.json.config.landscape != null ) {
                    if ( pos >= 0 && pos < LandPlayEdit.editor.json.config.landscape.length ) {
                        LandPlayEdit.editor.json.config.landscape.splice( pos, 1 );
                    }
                }
            }
            LandPlayEdit.editor.json.config.landscape.push( landscape );
            LandPlayEdit.saveData();
            $( "#pluginEditor" ).foundation( "close" );
            this.openLandscapes();
        }

        deleteLandscape( pos: number ) {
            var classRef = this;
            LandPlayEdit.confirm( i18next.t( "reveal.landscape.delete" ), function() {
                if ( LandPlayEdit.editor.json.config.landscape != null ) {
                    if ( !isNaN( pos ) && pos >= 0 && pos < LandPlayEdit.editor.json.config.landscape.length ) {
                        LandPlayEdit.editor.json.config.landscape.splice( pos, 1 );
                        LandPlayEdit.saveData();
                        $( "#pluginEditor" ).foundation( "close" );
                        classRef.openLandscapes();
                    }
                }
            } );
        }

        editStage() {
            var classRef = this;
            this.modal( "stage.html", function() {
                var stageForm = new Foundation.Abide( $( "#stageForm" ), {} );
                Foundation.reInit( 'abide' );

                $( "#stageForm" ).submit( function( event ) {
                    event.preventDefault();
                } );

                $( '#stageForm' ).on( "formvalid.zf.abide", function( ev, frm ) {
                    classRef.updateStage();
                } );

                $( "[name*='stage.']" ).change( function() {
                    classRef.validateStage( $( this ).attr( "name" ) );
                } );
                var sprite = null;
                if ( LandPlayEdit.editor.json.config.craft != null ) {
                    var craft = LandPlayEdit.editor.json.config.craft;
                    $( "[name*='craft.@']" ).each( function() {
                        var name = $( this ).attr( "name" );
                        var attr = name.replace( "craft.@", "" );
                        var value = craft["@" + attr];
                        if ( attr == "y-scale" ) {
                            value = parseFloat( value ) * 1000;
                        }
                        if ( attr == "jump-touch" ) {
                            $( "[name='" + name + "'][value='" + value + "']" ).prop( "checked", true );
                        } else {
                            $( this ).val( value );
                        }
                        if ( attr == "ybounce" ) {
                            $( this ).trigger( "change" );
                        }
                    } );
                    sprite = craft["@sprite"];
                }
                $( "[name*='stage.setup.@']" ).each( function() {
                    var name = $( this ).attr( "name" );
                    var attr = name.replace( "stage.setup.@", "" );
                    var value = LandPlayEdit.editor.json.config.setup["@" + attr];
                    if ( attr == "floor2d" ) {
                        $( "[name='" + name + "'][value='" + value + "']" ).prop( "checked", true );
                    } else {
                        $( this ).val( LandPlayEdit.editor.json.config.setup["@" + attr] );
                    }

                } );
                classRef.loadRemainingSpritesForCraft( sprite );
            } );
        }

        updateStage() {
            var craft = {};
            $( "[name*='craft.@']" ).each( function() {
                var value = $( this ).val();
                var name = $( this ).attr( "name" );
                var attr = name.replace( "craft.@", "" );
                if ( attr == "y-scale" ) {
                    value = parseInt( value ) / 1000;
                }

                if ( attr == "jump-touch" ) {
                    craft["@" + attr] = $( "[name='" + name + "']:checked" ).val();
                } else {
                    craft["@" + attr] = value;
                }

                if ( craft["@" + attr] == null ) {
                    delete craft["@" + attr];
                }
            } );
            LandPlayEdit.editor.json.config.craft = craft;

            var setup = LandPlayEdit.editor.json.config.setup;
            $( "[name*='stage.setup.@']" ).each( function() {
                var name = $( this ).attr( "name" );
                var attr = $( this ).attr( "name" ).replace( "stage.setup.@", "" );
                if ( attr == "floor2d" ) {
                    setup["@" + attr] = $( "[name='" + name + "']:checked" ).val();
                } else {
                    setup["@" + attr] = $( this ).val();
                }

                if ( setup["@" + attr] == null ) {
                    delete setup["@" + attr];
                }
            } );


            LandPlayEdit.saveData();
            $( "#pluginEditor" ).foundation( "close" );
        }

        validate() {
            this.resetConstraints();
            if ( LandPlayEdit.editor.json.config.craft == null ) {
                this.addConstraint( new LandPlayEdit.Constraint( "behaviour", "stage", i18next.t( "plugin.constraint.arcade.craft" ) ) );
            } else {
                this.checkSprites( [LandPlayEdit.editor.json.config.craft], i18next.t( "reveal.stage.craft.label" ) );
            }
            if ( LandPlayEdit.editor.json.config.piece == null || LandPlayEdit.editor.json.config.piece.length == 0 ) {
                this.addConstraint( new LandPlayEdit.Constraint( "behaviour", "asteroid", i18next.t( "plugin.constraint.arcade.piece.empty" ) ) );
            } else {
                this.checkSprites( LandPlayEdit.editor.json.config.piece, "[asteroid]" );
                for ( var i = 0; i < LandPlayEdit.editor.json.config.piece.length; i++ ) {
                    var piece = LandPlayEdit.editor.json.config.piece[i];
                    if ( piece.event == null || piece.event.length == 0 ) {
                        this.addConstraint( new LandPlayEdit.Constraint( "behaviour", "asteroid", i18next.t( "plugin.constraint.arcade.piece.noevents", { asteroid: piece["@attach"] } ) ) );
                    }
                }
            }
            if ( LandPlayEdit.editor.json.config.landscape != null ) {
                this.checkSprites( LandPlayEdit.editor.json.config.landscape, "[landscape]" );
            }
            if ( LandPlayEdit.editor.json.config.edge != null ) {
                this.checkSprites( LandPlayEdit.editor.json.config.edge, "[edge]" );
            }

            return super.validate();
        }

    }
}