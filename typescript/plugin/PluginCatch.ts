/// <reference path='Plugin.ts' />

module LandPlayEdit {
    export class PluginCatch extends LandPlayEdit.Plugin {

        constructor() {
            super( {
                toolBox: {
                    behaviour: [
                        {
                            key: "piece",
                            label: "plugin.toolbox.catch.piece.label",
                            icon: "fas fa-puzzle-piece"
                        },
                        {
                            key: "target",
                            label: "plugin.toolbox.catch.target.label",
                            icon: "fas fa-bullseye"
                        },
                        {
                            key: "weapon",
                            label: "plugin.toolbox.catch.weapon.label",
                            icon: "fas fa-crosshairs"
                        }
                    ]
                },
                effects: [{ value: "freeze" }],
                fields: {
                    tabs: {
                        setup: {
                            0: [
                                {
                                    label: i18next.t( "plugin.field.setup.speed.label" ),
                                    name: "config.setup.@speed",
                                    type: "input",
                                    required: false,
                                    validation: i18next.t( "validator.number.expected" ),
                                    pattern: "number",
                                    help: i18next.t( "plugin.field.setup.speed.help" ),
                                    value: LandPlayEdit.editor.json.config.setup["@speed"]
                                },
                                {
                                    label: i18next.t( "plugin.field.setup.runawaySpeed.label" ),
                                    name: "config.setup.@runawaySpeed",
                                    type: "input",
                                    required: false,
                                    validation: i18next.t( "validator.number.expected" ),
                                    pattern: "number",
                                    help: i18next.t( "plugin.field.setup.runawaySpeed.help" ),
                                    value: LandPlayEdit.editor.json.config.setup["@runawaySpeed"]
                                }
                            ]
                        },
                    }
                }
            } );
        }

        getTemplateName(): string {
            return "catch.html";
        }

        behaviour( type ) {
            switch ( type ) {
                case "target":
                    this.openTargets();
                    break;

                case "piece":
                    this.openPieces();
                    break;

                case "weapon":
                    this.editWeapon();
                    break;

                default:
                    LandPlayEdit.warning( "Please develop pending behaviour '" + type + "'" );
                    break;
            }
        }

        editRecord( type: string, pos: number ) {
            switch ( type ) {
                case "target":
                    this.editTarget( pos );
                    break;

                case "piece":
                    this.editPiece( pos );
                    break;

                default:
                    break;
            }
        }

        deleteRecord( type: string, pos: number ) {
            switch ( type ) {
                case "target":
                    this.deleteTarget( pos );
                    break;

                case "piece":
                    this.deletePiece( pos );
                    break;

                default:
                    break;
            }
        }

        private loadRemainingSprites( value?: string ) {
            var collection = null;
            if ( LandPlayEdit.editor.json.config.targets != null ) {
                collection = LandPlayEdit.editor.json.config.targets.target;
            }
            this.doLoadRemainingSprites( collection, "target.@sprite", value );
        }

        openEvents( pos: string ) {
            pos = parseInt( pos );
            if ( this.hasTarget( pos ) ) {
                var target = LandPlayEdit.editor.json.config.targets.target[pos];
                $( "#pluginEditor" ).foundation( "close" );
                var excludedCauses: Array<string> = Object.keys( LandPlay.LandEvent.Cause );
                excludedCauses.splice( excludedCauses.indexOf( "CAUSE_RIGHT" ), 1 );
                var addAll = true;
                var addThis = true;
                var subPos = null;
                return LandPlayEdit.openEventsDo( target.event, pos, excludedCauses, null, subPos, addAll, addThis );
            }
            LandPlayEdit.alert( "No way to find the target to attatch events to" );
        }

        getEventsCollection( container: number ) {
            if ( this.hasTarget( container ) ) {
                var target = LandPlayEdit.editor.json.config.targets.target[container];
                if ( target.event == null ) {
                    target.event = [];
                }
                return target.event;
            }
            return null;
        }

        validateEvent( name ) {
            var selector = "[name='" + name + "']";
            var extended = true;
            switch ( name ) {
                case "event.@type":
                    var type = $( selector ).val();
                    switch ( type ) {
                        case LandPlay.CatchEvents.TYPE_FREEZE:
                            LandPlayEdit.toggleField( "event.@sprite", true );
                            LandPlayEdit.toggleField( "event.@value", false );
                            var force = true;
                            LandPlayEdit.reloadSpriteSelect( $( "[name='event.@sprite']" ), null, force, extended );
                            break;
                        default:
                            console.warn( "Effect '" + type + "' has not dependent selectors actions" );
                    }
                    break;
            }
        }

        private hasTarget( pos: string ) {
            pos = parseInt( pos );
            if ( LandPlayEdit.editor.json.config.targets != null && LandPlayEdit.editor.json.config.targets.target != null ) {
                if ( !isNaN( pos ) && pos >= 0 && pos < LandPlayEdit.editor.json.config.targets.target.length ) {
                    return true;
                }
            }
            return false;
        }

        openTargets() {
            var classRef = this;
            this.modal( "target.html", function() {
                var targetForm = new Foundation.Abide( $( "#targetForm" ), {} );
                Foundation.reInit( 'abide' );

                $( "#createTarget" ).click( function() {
                    classRef.createTarget();
                } );
                $( "#targetForm" ).submit( function( event ) {
                    event.preventDefault();
                } );
                $( '#targetForm' ).on( "formvalid.zf.abide", function( ev, frm ) {
                    classRef.updateTarget();
                } );
                $( "#eventsOpen" ).click( function() {
                    var pos = $( "[name=target_pos]" ).val();
                    classRef.openEvents( pos );
                } );

                var hasTargets = false;

                if ( LandPlayEdit.editor.json.config.targets != null && LandPlayEdit.editor.json.config.targets.target != null ) {
                    for ( var i = 0; i < LandPlayEdit.editor.json.config.targets.target.length; i++ ) {
                        hasTargets = true;
                        var target = LandPlayEdit.editor.json.config.targets.target[i];
                        LandPlayEdit.listRecord( "#target-list", i, target["@sprite"] );
                    }

                }
                if ( !hasTargets ) {
                    var textDiv = $( '#templates > #tpl_target_no_item > div:first-child' ).clone( true );
                    $( "#target-list" ).append( textDiv );
                } else {
                    LandPlayEdit.activateList( "target" );
                }

                $( "#targetForm" ).hide();

            } );
        }

        createTarget() {
            $( "#targetForm" ).show();
            $( "#targetForm" ).foundation( "resetForm" );
            $( "[name='target_pos']" ).val( "" );
            $( "#eventsOpen" ).attr( "disabled", "disabled" );
            this.loadRemainingSprites();
            LandPlayEdit.reloadSoundSelect( "[name='target.@sound']" );

        }

        editTarget( pos: number ) {
            if ( LandPlayEdit.editor.json.config.targets != null && LandPlayEdit.editor.json.config.targets.target != null ) {
                if ( !isNaN( pos ) && pos >= 0 && pos < LandPlayEdit.editor.json.config.targets.target.length ) {
                    this.createTarget();
                    var target = LandPlayEdit.editor.json.config.targets.target[pos];
                    $( "[name*='target.@']" ).each( function() {
                        var attr = $( this ).attr( "name" ).replace( "target.@", "" );
                        $( this ).val( target["@" + attr] );
                    } );
                    /** avoid to repeat sprites */
                    this.loadRemainingSprites( target["@sprite"] );
                    $( "[name='target_pos']" ).val( pos );
                    $( "#eventsOpen" ).removeAttr( "disabled" );
                }
            }
        }

        deleteTarget( pos: number ) {
            var classRef = this;
            LandPlayEdit.confirm( i18next.t( "reveal.target.delete" ), function() {
                if ( LandPlayEdit.editor.json.config.targets != null && LandPlayEdit.editor.json.config.targets.target != null ) {
                    if ( !isNaN( pos ) && pos >= 0 && pos < LandPlayEdit.editor.json.config.targets.target.length ) {
                        LandPlayEdit.editor.json.config.targets.target.splice( pos, 1 );
                        LandPlayEdit.saveData();
                        $( "#pluginEditor" ).foundation( "close" );
                        classRef.openTargets();
                    }
                }
            } );
        }

        updateTarget() {
            var target = {};
            $( "[name*='target.@']" ).each( function() {
                var attr = $( this ).attr( "name" ).replace( "target.@", "" );
                target["@" + attr] = $( this ).val();
            } );
            if ( LandPlayEdit.editor.json.config.targets == null ) {
                LandPlayEdit.editor.json.config.targets = {};
            }
            if ( LandPlayEdit.editor.json.config.targets.target == null ) {
                LandPlayEdit.editor.json.config.targets.target = [];
            }
            var pos = parseInt( $( "[name='target_pos']" ).val() );
            if ( LandPlayEdit.editor.json.config.targets != null ) {
                this.updateRecord( LandPlayEdit.editor.json.config.targets.target, pos, target );
            }
            LandPlayEdit.saveData();
            $( "#pluginEditor" ).foundation( "close" );
            this.openTargets();
        }

        openPieces() {
            var classRef = this;
            this.modal( "piece.html", function() {
                var pieceForm = new Foundation.Abide( $( "#pieceForm" ), {} );
                Foundation.reInit( 'abide' );

                $( "#createPiece" ).click( function() {
                    classRef.createPiece();
                } );
                $( "#pieceForm" ).submit( function( event ) {
                    event.preventDefault();
                } );

                $( '#pieceForm' ).on( "formvalid.zf.abide", function( ev, frm ) {
                    classRef.updatePiece();
                } );

                var hasPieces = false;

                if ( LandPlayEdit.editor.json.config.piece != null ) {
                    for ( var i = 0; i < LandPlayEdit.editor.json.config.piece.length; i++ ) {
                        hasPieces = true;
                        var piece = LandPlayEdit.editor.json.config.piece[i];
                        LandPlayEdit.listRecord( "#piece-list", i, piece["@sprite"] );
                    }

                }
                if ( !hasPieces ) {
                    var textDiv = $( '#templates > #tpl_piece_no_item > div:first-child' ).clone( true );
                    $( "#piece-list" ).append( textDiv );
                } else {
                    LandPlayEdit.activateList( "piece" );
                }

                $( "#pieceForm" ).hide();

            } );
        }

        createPiece() {
            $( "#pieceForm" ).show();
            $( "#pieceForm" ).foundation( "resetForm" );
            $( "[name='piece_pos']" ).val( "" );
            this.doLoadRemainingSprites( LandPlayEdit.editor.json.config.piece, "piece.@sprite", null );
        }

        editPiece( pos: number ) {
            if ( LandPlayEdit.editor.json.config.piece != null ) {
                if ( !isNaN( pos ) && pos >= 0 && pos < LandPlayEdit.editor.json.config.piece.length ) {
                    this.createPiece();
                    var piece = LandPlayEdit.editor.json.config.piece[pos];
                    $( "[name*='piece.@']" ).each( function() {
                        var attr = $( this ).attr( "name" ).replace( "piece.@", "" );
                        $( this ).val( piece["@" + attr] );
                    } );
                    /** avoid to repeat sprites */
                    this.doLoadRemainingSprites( LandPlayEdit.editor.json.config.piece, "piece.@sprite", piece["@sprite"] );
                    $( "[name='piece_pos']" ).val( pos );
                }
            }
        }

        updatePiece() {
            var piece = {};
            $( "[name*='piece.@']" ).each( function() {
                var attr = $( this ).attr( "name" ).replace( "piece.@", "" );
                piece["@" + attr] = $( this ).val();

                if ( piece["@" + attr] == "" ) {
                    delete piece["@" + attr];
                }
            } );
            if ( LandPlayEdit.editor.json.config.piece == null ) {
                LandPlayEdit.editor.json.config.piece = [];
            }
            var pos = parseInt( $( "[name='piece_pos']" ).val() );
            if ( !isNaN( pos ) ) {
                if ( LandPlayEdit.editor.json.config.piece != null ) {
                    if ( pos >= 0 && pos < LandPlayEdit.editor.json.config.piece.length ) {
                        LandPlayEdit.editor.json.config.piece.splice( pos, 1 );
                    }
                }
            }
            LandPlayEdit.editor.json.config.piece.push( piece );
            LandPlayEdit.saveData();
            $( "#pluginEditor" ).foundation( "close" );
            this.openPieces();
        }

        deletePiece( pos: number ) {
            var classRef = this;
            LandPlayEdit.confirm( i18next.t( "reveal.piece.delete" ), function() {
                if ( LandPlayEdit.editor.json.config.piece != null ) {
                    if ( !isNaN( pos ) && pos >= 0 && pos < LandPlayEdit.editor.json.config.piece.length ) {
                        LandPlayEdit.editor.json.config.piece.splice( pos, 1 );
                        LandPlayEdit.saveData();
                        $( "#pluginEditor" ).foundation( "close" );
                        classRef.openPieces();
                    }
                }
            } );
        }

        editWeapon() {
            var classRef = this;
            this.modal( "weapon.html", function() {
                var targetForm = new Foundation.Abide( $( "#weaponForm" ), {} );
                Foundation.reInit( 'abide' );

                $( "#weaponForm" ).submit( function( event ) {
                    event.preventDefault();
                } );

                $( '#weaponForm' ).on( "formvalid.zf.abide", function( ev, frm ) {
                    classRef.updateWeapon();
                } );

                var sprite = null;

                if ( LandPlayEdit.editor.json.config.weapon != null ) {
                    var weapon = LandPlayEdit.editor.json.config.weapon;
                    $( "[name*='weapon.@']" ).each( function() {
                        var attr = $( this ).attr( "name" ).replace( "weapon.@", "" );
                        if ( attr == "pickable" ) {
                            $( "[name='weapon.@pickable'][value='" + weapon["@" + attr] + "']" ).prop( "checked", true );
                        } else {
                            $( this ).val( weapon["@" + attr] );
                        }
                    } );
                    sprite = weapon["@sprite"];
                }

                classRef.doLoadRemainingSprites( LandPlayEdit.editor.json.config.piece, "weapon.@sprite", sprite );

            } );
        }

        updateWeapon() {
            var weapon = {};
            $( "[name*='weapon.@']" ).each( function() {
                var attr = $( this ).attr( "name" ).replace( "weapon.@", "" );
                if ( attr == "pickable" ) {
                    weapon["@" + attr] = $( "[name='weapon.@pickable']:checked" ).val();
                } else {
                    weapon["@" + attr] = $( this ).val();
                }
            } );
            LandPlayEdit.editor.json.config.weapon = weapon;
            LandPlayEdit.saveData();
            $( "#pluginEditor" ).foundation( "close" );
        }

        validate() {
            this.resetConstraints();
            if ( LandPlayEdit.editor.json.config.weapon == null ) {
                this.addConstraint( new LandPlayEdit.Constraint( "behaviour", "weapon", i18next.t( "plugin.constraint.catch.weapon" ) ) );
            }
            if ( LandPlayEdit.editor.json.config.targets == null
                || LandPlayEdit.editor.json.config.targets.target == null
                || LandPlayEdit.editor.json.config.targets.target.length == 0 ) {
                this.addConstraint( new LandPlayEdit.Constraint( "behaviour", "target", i18next.t( "plugin.constraint.catch.target" ) ) );
            } else {
                this.checkSprites( LandPlayEdit.editor.json.config.targets.target, "[target]" );
            }
            if ( LandPlayEdit.editor.json.config.piece == null
                || LandPlayEdit.editor.json.config.piece.length == 0 ) {
                this.addConstraint( new LandPlayEdit.Constraint( "behaviour", "piece", i18next.t( "plugin.constraint.catch.piece" ) ) );
            } else {
                this.checkSprites( LandPlayEdit.editor.json.config.piece, "[piece]" );
            }

            if ( LandPlayEdit.editor.json.config.setup.event != null ) {
                for ( var i = 0; i < LandPlayEdit.editor.json.config.setup.event.length; i++ ) {
                    if ( LandPlayEdit.editor.json.config.setup.event[i]["@type"] == "runaway" ) {
                        if ( LandPlayEdit.editor.json.config.setup["@runawaySpeed"] == null ) {
                            var c: LandPlayEdit.Constraint = new LandPlayEdit.Constraint();
                            c.text = i18next.t( "plugin.constraint.catch.runaway" );
                            this.addConstraint( c );
                        }
                    }
                }
            }

            return super.validate();
        }

    }
}