/// <reference path='Plugin.ts' />

module LandPlayEdit {
    export class PluginPuzzle extends LandPlayEdit.Plugin {

        constructor() {
            super( {
                toolBox: {
                    behaviour: [
                        {
                            key: "board",
                            label: "plugin.toolbox.puzzle.board.label",
                            icon: "fas fa-chess-board"
                        }
                    ]
                }
            } );
        }

        getTemplateName(): string {
            return "puzzle.html";
        }

        behaviour( type ) {
            switch ( type ) {
                case "board":
                    this.editBoard();
                    break;
                default:
                    LandPlayEdit.warning( "Please develop pending behaviour '" + type + "'" );
                    break;
            }
        }

        private loadRemainingSpritesForTable( value?: string ) {
            var collection: Array<object> = this.getUsedSpritesInBoard();
            $( "[name='board.@piece'] option" ).each( function() {
                collection.push( { "@sprite": $( this ).val() } );
            } );
            this.doLoadRemainingSprites( collection, "table.@sprite", value );
        }

        private loadRemainingSpritesForPieces( value?: string ) {
            var collection: Array<object> = this.getUsedSpritesInBoard( false );
            collection.push( { "@sprite": $( "[name='container.@sprite']" ).val() } );
            this.doLoadRemainingSprites( collection, "transient.board.@piece", value );
        }

        private getUsedSpritesInBoard( answers?: boolean ) {
            var collection: Array<object> = new Array<object>();

            if ( LandPlayEdit.editor.json.config.piece != null ) {
                var pieces = LandPlayEdit.editor.json.config.piece;
                if ( answers ) {
                    for ( var i = 0; i < pieces.length; i++ ) {
                        if ( pieces[i].answer != null ) {
                            collection = collection.concat( pieces[i].answer );
                        }
                    }
                }
                collection = collection.concat( pieces );
            }
            return collection;
        }


        createBoard() {
            $( "#boardForm" ).show();
            $( "[name='board.@piece']" ).html( "" );
            $( "[name='table.@random'][value='true']" ).prop( "checked", true );
            $( "#boardForm" ).foundation( "resetForm" );
            this.loadRemainingSpritesForTable();
            this.loadRemainingSpritesForPieces();
        }

        editBoard() {
            var classRef = this;
            this.modal( "board.html", function() {

                var boardForm = new Foundation.Abide( $( "#boardForm" ), {} );
                Foundation.reInit( 'abide' );

                $( "#boardForm" ).submit( function( event ) {
                    event.preventDefault();
                } );

                $( '#boardForm' ).on( "formvalid.zf.abide", function( ev, frm ) {
                    classRef.updateBoard();
                } );

                $( "#addBoardPiece" ).click( function() {
                    $( "[name='transient.board.@piece'] option:selected" ).each( function() {
                        var value = $( this ).val();
                        if ( value != "" ) {
                            if ( $( "[name='board.@piece'] option[value='" + value + "']" ).length == 0 ) {
                                var option = $( "<option>" );
                                $( option ).attr( "value", value );
                                $( option ).html( value );
                                $( "[name='board.@piece']" ).append( option );
                            }

                        }
                    } );
                    classRef.validateBoard( "board.@piece" );
                } );

                $( "#delBoardPiece" ).click( function() {
                    $( "[name='board.@piece'] option:selected" ).each( function() {
                        var value = $( this ).attr( "value" );
                        $( this ).remove();
                        if ( $( "[name='transient.board.@piece'] option[value='" + value + "']" ).length == 0 ) {
                            /* now it is available */
                            var option = $( "<option>" );
                            $( option ).attr( "value", value );
                            $( option ).html( value );
                            $( "[name='transient.board.@piece']" ).append( option );
                        }

                    } );
                    classRef.validateBoard( "board.@piece" );
                } );

                classRef.createBoard();
                if ( LandPlayEdit.editor.json.config.table != null) {
                    $( "[name='table.@random'][value='" + LandPlayEdit.editor.json.config.table["@random"] + "']" ).prop( "checked", true );
                }
                
                var pieces = LandPlayEdit.editor.json.config.piece;
                if ( pieces != null ) {
                    for ( var i = 0; i < pieces.length; i++ ) {
                        var value = pieces[i]["@sprite"];
                        var option = $( "<option>" );
                        $( option ).attr( "value", value );
                        $( option ).html( value );
                        $( "[name='board.@piece']" ).append( option );
                    }
                }
                /** avoid to repeat sprites */
                if ( LandPlayEdit.editor.json.config.table != null ) {
                    classRef.loadRemainingSpritesForTable( LandPlayEdit.editor.json.config.table["@sprite"] );
                }
            } );


        }

        updateBoard() {
            var tableSel = "[name='table.@sprite']";
            if ( $( tableSel ).val() != "" && $( tableSel ).val() != null ) {
                if ( LandPlayEdit.editor.json.config.table == null ) {
                    LandPlayEdit.editor.json.config.table = {};
                }
                LandPlayEdit.editor.json.config.table = { "@sprite": $( "[name='table.@sprite']" ).val(), "@random": $( "[name='table.@random']:checked" ).val() };
                
            }
            LandPlayEdit.editor.json.config.piece = [];
            $( "[name='board.@piece'] option" ).each( function() {
                LandPlayEdit.editor.json.config.piece.push( { "@sprite": $( this ).val() } );
            } );


            LandPlayEdit.saveData();
            $( "#pluginEditor" ).foundation( "close" );
        }

        deleteBoard() {
            var classRef = this;
            LandPlayEdit.confirm( i18next.t( "reveal.board.delete" ), function() {
                delete LandPlayEdit.editor.json.config.piece;
                delete LandPlayEdit.editor.json.config.table;
            } );
        }

        validateBoard( name ) {
            var selector = "[name='" + name + "']";
            var value = $( selector ).val();

        }

        validate() {
            this.resetConstraints();
            if ( LandPlayEdit.editor.json.config.piece == null
                || LandPlayEdit.editor.json.config.piece.length < 2 ) {
                this.addConstraint( new LandPlayEdit.Constraint( "behaviour", "board", i18next.t( "plugin.constraint.puzzle.piece" ) ) );
            } else {
                this.checkSprites( LandPlayEdit.editor.json.config.piece, "[piece]" );
            }
            return super.validate();
        }

    }
}