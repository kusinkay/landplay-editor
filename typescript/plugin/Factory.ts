/// <reference path="../landplay.d.ts" />

module LandPlayEdit {
    export module PluginFactory {
        export function newInstance( mode: LandPlay.Mode ): LandPlayEdit.Plugin {
            switch ( mode ) {
                case LandPlay.Mode.Catch:
                    return new LandPlayEdit.PluginCatch();
                    break;
                case LandPlay.Mode.Paint:
                    return new LandPlayEdit.PluginPaint();
                    break;
                case LandPlay.Mode.DragDrop:
                    return new LandPlayEdit.PluginDragDrop();
                    break;
                case LandPlay.Mode.Puzzle:
                    return new LandPlayEdit.PluginPuzzle();
                    break;
                case LandPlay.Mode.Simon:
                    return new LandPlayEdit.PluginSimon();
                    break;
                case LandPlay.Mode.Arcade:
                    return new LandPlayEdit.PluginArcade();
                    break;
                case LandPlay.Mode.Type:
                    return new LandPlayEdit.PluginType();
                    break;
                case LandPlay.Mode.WordSearch:
                    return new LandPlayEdit.PluginWordSearch();
                    break;
                default:
                    break;
            }
            return mode;
        }
    }

}