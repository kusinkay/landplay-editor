/// <reference path='Plugin.ts' />

module LandPlayEdit {
    export class PluginPaint extends LandPlayEdit.Plugin {

        private brushSelects = ["brush.@sprite", "brush.@brushpoint", "brush.@toolcase"];

        constructor() {
            super( {
                toolBox: {
                    behaviour: [
                        {
                            key: "brush",
                            label: "plugin.toolbox.paint.brush.label",
                            icon: "fas fa-paint-brush"
                        },
                        {
                            key: "color",
                            label: "plugin.toolbox.paint.color.label",
                            icon: "fas fa-palette"
                        },
                        {
                            key: "canvas",
                            label: "plugin.toolbox.paint.canvas.label",
                            icon: "far fa-image"
                        }
                    ]
                },
                effects: [{ value: "tint" }]
            } );
        }

        getTemplateName(): string {
            return "paint.html";
        }

        behaviour( type ) {
            switch ( type ) {
                case "color":
                    this.openColors();
                    break;

                case "canvas":
                    this.openCanvas();
                    break;

                case "brush":
                    this.editBrush();
                    break;

                default:
                    LandPlayEdit.warning( "Please develop pending behaviour '" + type + "'" );
                    break;
            }
        }

        editRecord( type: string, pos: number ) {
            switch ( type ) {
                case "color":
                    this.editColor( pos );
                    break;

                case "canvas":
                    this.editCanvas( pos );
                    break;

                default:
                    break;
            }
        }

        deleteRecord( type: string, pos: number ) {
            switch ( type ) {
                case "color":
                    this.deleteColor( pos );
                    break;

                case "canvas":
                    this.deleteCanvas( pos );
                    break;

                default:
                    break;
            }
        }

        private loadRemainingSpritesForColors( value?: string ) {
            var collection = this.loadRemainingSpritesForWhatever( value );
            this.doLoadRemainingSprites( collection, "color.@sprite", value );
        }

        private loadRemainingSpritesForCanvas( value?: string ) {
            var collection = this.loadRemainingSpritesForWhatever( value );
            this.doLoadRemainingSprites( collection, "canvas.@sprite", value, true );
        }
        private loadRemainingSpritesForWhatever( value?: string ) {
            var collection: Array<object> = this.getUsedSpritesInCanvas();

            if ( LandPlayEdit.editor.json.config.color != null ) {
                var colors = LandPlayEdit.editor.json.config.color;
                if ( collection == null ) {
                    collection = new Array<object>();
                }
                collection = collection.concat( colors )
            }
            return collection;
        }

        private getUsedSpritesInCanvas() {
            var collection: Array<object> = new Array<object>();
            if ( LandPlayEdit.editor.json.config.canvas != null ) {
                for ( var i = 0; i < LandPlayEdit.editor.json.config.canvas.length; i++ ) {
                    var canvas = LandPlayEdit.editor.json.config.canvas[i];
                    if ( canvas.layers != null ) {
                        for ( var j = canvas.layers["@from"]; j <= canvas.layers["@to"]; j++ ) {
                            collection.push( { "@sprite": canvas["@sprite"] + j } );
                        }
                    }
                    collection.push( canvas );
                }
            }
            return collection;
        }

        private loadColorSprites( value?: string ) {
            var colors: Array<string> = new Array<string>();
            if ( LandPlayEdit.editor.json.config.color != null ) {
                for ( var i = 0; i < LandPlayEdit.editor.json.config.color.length; i++ ) {
                    var color: object = LandPlayEdit.editor.json.config.color[i];
                    colors.push( color["@sprite"] );
                }
                if ( LandPlayEdit.lgEditor.sprites != null ) {
                    var excluded: Array<object> = new Array<object>();
                    var keys: Array<string> = Object.keys( LandPlayEdit.lgEditor.sprites );
                    keys.sort();
                    for ( var i = 0; i < keys.length; i++ ) {
                        if ( colors.indexOf( keys[i] ) < 0 ) {
                            excluded.push( { "@sprite": keys[i] } );
                        }
                    }
                    this.doLoadRemainingSprites( excluded, "canvas.@color", value );
                }
            } else {
                LandPlayEdit.alert( i180next.t( "plugin.constraint.paint.noColorSprites" ) );
            }


        }

        validateEvent( name ) {
            var selector = "[name='" + name + "']";
            var extended = false;
            switch ( name ) {
                case "event.@type":
                    var type = $( selector ).val();
                    switch ( type ) {
                        case LandPlay.PaintEvents.TYPE_TINT:
                            LandPlayEdit.toggleField( "event.@sprite", true );
                            LandPlayEdit.toggleField( "event.@value", true, "#([a-fA-F0-9]{3})([a-fA-F0-9]{3})?", i18next.t( "validator.color.required" ) );
                            var force = true;
                            LandPlayEdit.reloadSpriteSelect( $( "[name='event.@sprite']" ), null, force, extended );
                            break;
                        default:
                            console.warn( "Effect '" + type + "' has not dependent selectors actions" );
                    }
                    break;
            }
        }

        validateBrush( name ) {
            var selector = "[name='" + name + "']";
            var value = $( selector ).val();
            for ( var i = 0; i < this.brushSelects.length; i++ ) {
                var otherSelector = this.brushSelects[i];
                if ( otherSelector != name ) {
                    var force = true;
                    var except = [{ "@sprite": value }];
                    var excluded = LandPlayEdit.editor.json.config.color.concat( LandPlayEdit.editor.json.config.canvas );
                    excluded = excluded.concat( except );
                    this.doLoadRemainingSprites( excluded, otherSelector, $( "[name='" + otherSelector + "']" ).val() );
                }
            }
        }

        private hasColor( pos: string ) {
            return this.hasItem( LandPlayEdit.editor.json.config.color, pos );
        }

        private hasCanvas( pos: string ) {
            return this.hasItem( LandPlayEdit.editor.json.config.canvas, pos );
        }

        private hasItem( collection: Array<object>, pos: string ) {
            pos = parseInt( pos );
            if ( collection != null ) {
                if ( !isNaN( pos ) && pos >= 0 && pos < collection.length ) {
                    return true;
                }
            }
            return false;
        }

        openCanvas() {
            var classRef = this;
            this.modal( "canvas.html", function() {
                var canvasForm = new Foundation.Abide( $( "#canvasForm" ), {} );
                Foundation.reInit( 'abide' );

                $( "#createCanvas" ).click( function() {
                    classRef.createCanvas();
                } );
                $( "#canvasForm" ).submit( function( event ) {
                    event.preventDefault();
                } );
                $( '#canvasForm' ).on( "formvalid.zf.abide", function( ev, frm ) {
                    classRef.updateCanvas();
                } );

                var hasCanvas = false;

                if ( LandPlayEdit.editor.json.config.canvas != null ) {
                    for ( var i = 0; i < LandPlayEdit.editor.json.config.canvas.length; i++ ) {
                        hasCanvas = true;
                        var canvas = LandPlayEdit.editor.json.config.canvas[i];
                        var label = canvas["@sprite"];
                        if ( canvas.layers != null ) {
                            label += " (" + canvas.layers["@from"] + ", " + canvas.layers["@to"] + ")"
                        }
                        LandPlayEdit.listRecord( "#canvas-list", i, label + " - " + canvas["@color"] );
                    }

                }
                if ( !hasCanvas ) {
                    var textDiv = $( '#templates > #tpl_canvas_no_item > div:first-child' ).clone( true );
                    $( "#canvas-list" ).append( textDiv );
                } else {
                    LandPlayEdit.activateList( "canvas" );
                }

                $( "#canvasForm" ).hide();

            } );
        }

        createCanvas() {
            $( "#canvasForm" ).show();
            $( "#canvasForm" ).foundation( "resetForm" );
            $( "[name='canvas_pos']" ).val( "" );
            this.loadRemainingSpritesForCanvas();
            this.loadColorSprites();
        }

        editCanvas( pos: number ) {
            if ( this.hasCanvas( pos ) ) {
                this.createCanvas();
                var canvas = LandPlayEdit.editor.json.config.canvas[pos];
                $( "[name*='canvas.@']" ).each( function() {
                    var attr = $( this ).attr( "name" ).replace( "canvas.@", "" );
                    $( this ).val( canvas["@" + attr] );
                } );
                /** avoid to repeat sprites */
                var key = canvas["@sprite"];
                if ( canvas.layers != null ) {
                    key += "|" + canvas.layers["@from"] + "|" + canvas.layers["@to"];
                }
                this.loadRemainingSpritesForCanvas( key );
                this.loadColorSprites( canvas["@color"] );
                $( "[name='canvas_pos']" ).val( pos );
            }
        }

        deleteCanvas( pos: number ) {
            var classRef = this;
            LandPlayEdit.confirm( i18next.t( "reveal.canvas.delete" ), function() {
                if ( classRef.hasCanvas( pos ) ) {
                    LandPlayEdit.editor.json.config.canvas.splice( pos, 1 );
                    LandPlayEdit.saveData();
                    $( "#pluginEditor" ).foundation( "close" );
                    classRef.openCanvas();
                }
            } );
        }

        updateCanvas() {
            var canvas = {};
            $( "[name*='canvas.@']" ).each( function() {
                var attr = $( this ).attr( "name" ).replace( "canvas.@", "" );
                canvas["@" + attr] = $( this ).val();
                if ( canvas["@" + attr] == null || canvas["@" + attr] == "" ) {
                    delete canvas["@" + attr];
                }
            } );
            if ( canvas["@sprite"].indexOf( "|" ) > -1 ) {
                var parts = canvas["@sprite"].split( "|" );
                if ( parts.length != 3 ) {
                    console.error( "3 |-separated string expected" );
                } else {
                    canvas["@sprite"] = parts[0];
                    canvas.layers = { "@from": parts[1], "@to": parts[2] };
                    /* clean previously selected individual layers */
                    if ( LandPlayEdit.editor.json.config.canvas != null ) {
                        for ( var i = 0; i < LandPlayEdit.editor.json.config.canvas.length; i++ ) {
                            var current = LandPlayEdit.editor.json.config.canvas[i];
                            var suffix = current["@sprite"].replace( parts[0], "" );
                            suffix = parseInt( suffix );
                            if ( !isNaN( suffix ) ) {
                                LandPlayEdit.editor.json.config.canvas.splice( i, 1 );
                                i--;
                            }
                        }
                    }
                }
            }
            if ( LandPlayEdit.editor.json.config.canvas == null ) {
                LandPlayEdit.editor.json.config.canvas = [];
            }
            var pos = parseInt( $( "[name='canvas_pos']" ).val() );
            if ( this.hasCanvas( pos ) ) {
                LandPlayEdit.editor.json.config.canvas.splice( pos, 1 );
            }
            LandPlayEdit.editor.json.config.canvas.push( canvas );
            LandPlayEdit.saveData();
            $( "#pluginEditor" ).foundation( "close" );
            this.openCanvas();
        }

        openColors() {
            var classRef = this;
            this.modal( "color.html", function() {
                var colorForm = new Foundation.Abide( $( "#colorForm" ), {} );
                Foundation.reInit( 'abide' );

                $( "#createColor" ).click( function() {
                    classRef.createColor();
                } );
                $( "#colorForm" ).submit( function( event ) {
                    event.preventDefault();
                } );

                $( '#colorForm' ).on( "formvalid.zf.abide", function( ev, frm ) {
                    classRef.updateColor();
                } );

                $( "#tinColor" ).colorpicker( { history: false } );
                $( "#tinColor" ).colorpicker( "val", "#FFFFFF" );

                var hasColors = false;

                if ( LandPlayEdit.editor.json.config.color != null ) {
                    for ( var i = 0; i < LandPlayEdit.editor.json.config.color.length; i++ ) {
                        hasColors = true;
                        var color = LandPlayEdit.editor.json.config.color[i];
                        LandPlayEdit.listRecord( "#color-list", i, color["@sprite"] + ": &nbsp;<span style=\"background-color:" + color["@color"] + "\">" + color["@color"] + "</span>" );
                    }

                }
                if ( !hasColors ) {
                    var textDiv = $( '#templates > #tpl_color_no_item > div:first-child' ).clone( true );
                    $( "#color-list" ).append( textDiv );
                } else {
                    LandPlayEdit.activateList( "color" );
                }

                $( "#colorForm" ).hide();

            } );
        }

        createColor() {
            $( "#colorForm" ).show();
            $( "#colorForm" ).foundation( "resetForm" );
            $( "[name='color_pos']" ).val( "" );
            this.loadRemainingSpritesForColors();
        }

        editColor( pos: number ) {
            if ( LandPlayEdit.editor.json.config.color != null ) {
                if ( !isNaN( pos ) && pos >= 0 && pos < LandPlayEdit.editor.json.config.color.length ) {
                    this.createColor();
                    var color = LandPlayEdit.editor.json.config.color[pos];
                    $( "[name*='color.@']" ).each( function() {
                        var attr = $( this ).attr( "name" ).replace( "color.@", "" );
                        $( this ).val( color["@" + attr] );
                    } );
                    $( "#tinColor" ).colorpicker( "val", $( "#tinColor" ).val() );
                    /** avoid to repeat sprites */
                    this.loadRemainingSpritesForColors( color["@sprite"] );
                    $( "[name='color_pos']" ).val( pos );
                }
            }
        }

        updateColor() {
            var color = {};
            $( "[name*='color.@']" ).each( function() {
                var attr = $( this ).attr( "name" ).replace( "color.@", "" );
                color["@" + attr] = $( this ).val();

                if ( color["@" + attr] == "" ) {
                    delete color["@" + attr];
                }
            } );
            if ( LandPlayEdit.editor.json.config.color == null ) {
                LandPlayEdit.editor.json.config.color = [];
            }
            var pos = parseInt( $( "[name='color_pos']" ).val() );
            if ( !isNaN( pos ) ) {
                if ( LandPlayEdit.editor.json.config.color != null ) {
                    if ( pos >= 0 && pos < LandPlayEdit.editor.json.config.color.length ) {
                        LandPlayEdit.editor.json.config.color.splice( pos, 1 );
                    }
                }
            }
            LandPlayEdit.editor.json.config.color.push( color );
            LandPlayEdit.saveData();
            $( "#pluginEditor" ).foundation( "close" );
            this.openColors();
        }

        deleteColor( pos: number ) {
            var classRef = this;
            LandPlayEdit.confirm( i18next.t( "reveal.color.delete" ), function() {
                if ( LandPlayEdit.editor.json.config.color != null ) {
                    if ( !isNaN( pos ) && pos >= 0 && pos < LandPlayEdit.editor.json.config.color.length ) {
                        LandPlayEdit.editor.json.config.color.splice( pos, 1 );
                        LandPlayEdit.saveData();
                        $( "#pluginEditor" ).foundation( "close" );
                        classRef.openColors();
                    }
                }
            } );
        }

        editBrush() {
            var classRef = this;
            this.modal( "brush.html", function() {
                var brushForm = new Foundation.Abide( $( "#brushForm" ), {} );
                Foundation.reInit( 'abide' );

                $( "#brushForm" ).submit( function( event ) {
                    event.preventDefault();
                } );

                $( '#brushForm' ).on( "formvalid.zf.abide", function( ev, frm ) {
                    classRef.updateBrush();
                } );

                $( "[name*='brush.']" ).change( function() {
                    classRef.validateBrush( $( this ).attr( "name" ) );
                } );

                var sprite = null;
                var point = null;
                var toolcase = null;
                if ( LandPlayEdit.editor.json.config.brush != null ) {
                    var brush = LandPlayEdit.editor.json.config.brush;
                    $( "[name*='brush.@']" ).each( function() {
                        var attr = $( this ).attr( "name" ).replace( "brush.@", "" );
                        if ( attr == "pickable" ) {
                            $( "[name='brush.@pickable'][value='" + brush["@" + attr] + "']" ).prop( "checked", true );
                        } else {
                            $( this ).val( brush["@" + attr] );
                        }
                    } );
                    sprite = brush["@sprite"];
                    point = brush["@brushpoint"];
                    toolcase = brush["@toolcase"];
                }
                var excluded = LandPlayEdit.editor.json.config.color.concat( LandPlayEdit.editor.json.config.canvas );
                classRef.doLoadRemainingSprites( excluded, "brush.@sprite", sprite );
                classRef.doLoadRemainingSprites( excluded, "brush.@brushpoint", point );
                classRef.doLoadRemainingSprites( excluded, "brush.@toolcase", toolcase );
            } );
        }

        updateBrush() {
            var brush = {};
            $( "[name*='brush.@']" ).each( function() {
                var attr = $( this ).attr( "name" ).replace( "brush.@", "" );
                if ( attr == "pickable" ) {
                    brush["@" + attr] = $( "[name='brush.@pickable']:checked" ).val();
                } else {
                    brush["@" + attr] = $( this ).val();
                }
                if ( brush["@" + attr] == null || brush["@" + attr] == "" ) {
                    delete brush["@" + attr];
                }
            } );
            LandPlayEdit.editor.json.config.brush = brush;
            LandPlayEdit.saveData();
            $( "#pluginEditor" ).foundation( "close" );
        }

        validate() {
            this.resetConstraints();
            if ( LandPlayEdit.editor.json.config.brush == null ) {
                this.addConstraint( new LandPlayEdit.Constraint( "behaviour", "brush", i18next.t( "plugin.constraint.paint.brush" ) ) );
            } else {
                this.checkSprites( [LandPlayEdit.editor.json.config.brush], i18next.t( "reveal.brush.sprite.label" ) );
                this.checkSprites( [LandPlayEdit.editor.json.config.brush], i18next.t( "reveal.brush.brushpoint.label" ), "@brushpoint" );
                this.checkSprites( [LandPlayEdit.editor.json.config.brush], i18next.t( "reveal.brush.toolcase.label" ), "@toolcase" );
            }
            if ( LandPlayEdit.editor.json.config.validation == null ) {
                this.addConstraint( new LandPlayEdit.Constraint( "behaviour", "validation", i18next.t( "plugin.constraint.validation.required" ) ) );
            }
            if ( LandPlayEdit.editor.json.config.color == null
                || LandPlayEdit.editor.json.config.color.length == 0 ) {
                this.addConstraint( new LandPlayEdit.Constraint( "behaviour", "color", i18next.t( "plugin.constraint.paint.color" ) ) );
            } else {
                this.checkSprites( LandPlayEdit.editor.json.config.color, "[color]" );
            }
            if ( LandPlayEdit.editor.json.config.canvas == null
                || LandPlayEdit.editor.json.config.canvas.length == 0 ) {
                this.addConstraint( new LandPlayEdit.Constraint( "behaviour", "canvas", i18next.t( "plugin.constraint.paint.canvas" ) ) );
            } else {
                this.checkSprites( LandPlayEdit.editor.json.config.canvas, "[canvas]" );
            }
            return super.validate();
        }

    }
}