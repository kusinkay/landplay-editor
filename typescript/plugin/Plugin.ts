/// <reference path='../LandPlayEdit.ts' />

module LandPlayEdit {
    export class Plugin {
        protected toolBox = {
            behaviour: []
        }

        protected effects = [];

        protected fields = {
            tabs: {
                setup: [],
                layout: []
            }
        };

        static TplPath = "templates/plugins/";

        private constraints: Array<LandPlayEdit.Constraint> = new Array<LandPlayEdit.Constraint>();

        constructor( params: object ) {
            this.toolBox = params.toolBox;
            this.effects = params.effects;
            this.fields = params.fields;
            this.completeToolBox();
            this.customizeForms();
            this.loadTemplate();
        }

        completeToolBox() {
            for ( var i = 0; i < this.toolBox.behaviour.length; i++ ) {
                var tool = this.toolBox.behaviour[i];
                tool.action = "behaviour";
                LandPlayEdit.addTool( tool, "behaviour" );
            }
            $( "[data-tooltip-plugin]" ).tooltip( {
                classes: {
                    "ui-tooltip": "highlight"
                }
            } );
            LandPlayEdit.reInitFoundation();
        }

        private customizeForms() {
            if ( this.fields != null && this.fields.tabs != null ) {
                for ( var tab of Object.keys( this.fields.tabs ) ) {
                    for ( var index of Object.keys( this.fields.tabs[tab] ) ) {
                        var row = this.fields.tabs[tab][index];
                        var grid = $( "<div>" )
                            .addClass( "grid-x" );
                        for ( var i = 0; i < row.length; i++ ) {
                            var record = row[i];
                            var width = Math.round( 12 / row.length )
                            var cell = $( "<div>" )
                                .addClass( "cell" )
                                .addClass( "small-" + width );
                            var label = $( "<label>" )
                                .html( record.label );
                            var field = $( "<input>" )
                                .attr( "type", "text" )
                                .attr( "name", record.name )
                                .attr( "pattern", record.pattern );
                            if ( record.required ) {
                                field.attr( "required", "required" );
                            }
                            if ( record.value != null ) {
                                field.val( record.value );
                            }
                            $( cell ).append( label );
                            $( cell ).append( field );
                            if ( record.validation != null ) {
                                var val = $( "<span>" )
                                    .addClass( "form-error" )
                                    .html( record.validation );
                                $( cell ).append( val );
                            }
                            if ( record.help != null ) {
                                var help = $( "<p>" )
                                    .addClass( "help-text" )
                                    .html( record.help );
                                $( cell ).append( help );
                            }
                            $( grid ).append( cell );
                        }
                        $( "#" + tab ).append( grid );
                    }
                }
            }
            Foundation.reInit( 'abide' );
        }

        private doLoadRemainingSprites( collection: Array<object>, formField: string, value: string, layers?: boolean, grids?: boolean ) {
            var excluded = [];
            if ( collection != null ) {
                for ( var i = 0; i < collection.length; i++ ) {
                    var item = collection[i];
                    excluded.push( item["@sprite"] );
                }
            }
            LandPlayEdit.reloadSpriteSelect( "[name='" + formField + "']", value, true, false, excluded, layers, grids );
        }

        /** Loads the plugin HTML templates to the HTML templates section of the editor page*/
        loadTemplate() {
            classRef = this;
            var xmlFile = Plugin.TplPath + this.getTemplateName();
            $.ajax(
                {
                    type: "GET",
                    url: xmlFile,
                    dataType: "xml",
                    success: function( html ) {
                        var xmlDoc = $.parseHTML( html );
                        $( "#templates >", html ).each( function() {
                            $( "#templates" ).append( $( this ) );
                        } );
                        $( "#reveals >", html ).each( function() {
                            $( "#reveals" ).append( $( this ) );
                        } );
                        var reveal = new Foundation.Reveal( $( "#targetEditor" ), {} );
                        LandPlayEdit.success( i18next.t( "plugin.loaded", { file: xmlFile } ) );
                    }
                }
            );
        }

        abstract getTemplateName(): string;

        abstract behaviour( type );

        /**
         * Preview additional graphic elements
         * Launched from editor when Stage is ready
         * Override in each plugin
         ******* */
        preview() {

        }

        validate() {
            if ( LandPlayEdit.editor.json.config.validation != null ) {
                if ( LandPlayEdit.editor.json.config.validation["@async"] == "true" ) {
                    var validate = false;
                    for ( var i = 0; i < LandPlayEdit.editor.json.config.button.length; i++ ) {
                        var button = LandPlayEdit.editor.json.config.button[i];
                        if ( button["@validate"] != null ) {
                            validate = true;
                            break;
                        }
                    }
                    if ( !validate ) {
                        this.addConstraint( new Constraint( "edit-action", "Button", i18next.t( "plugin.constraint.validation.noButton" ) ) );
                    }
                }
            }
            this.checkSprites( LandPlayEdit.editor.json.config.button, "[button]" );
            return !this.hasConstraints();
        }

        abstract getEventsCollection( container: number, subcontainer: number );

        abstract validateEvent( name );

        abstract editRecord( type: string, pos: number );

        abstract deleteRecord( type: string, pos: number );
        
        updateRecord( collection: Array<object>, pos: number, record: object ): object {
            var result = null;
            var events = null;
            if ( !isNaN( pos ) ) {
                if ( collection != null ) {
                    if ( pos >= 0 && pos < collection.length ) {
                        var deleted = collection.splice( pos, 1 );
                        result = deleted[0];
                        events = result.event;
                    }
                }
            }
            if ( events != null ) {
                record.event = events;
            }
            collection.push( record );
            return result;
        }

        modal( template: string, callback, params?: object ) {
            $.ajax( Plugin.TplPath + "reveals/" + template )
                .done( function( resp ) {
                    $( "#pluginEditor" ).html( resp ).foundation( 'open' );
                    callback( params );
                    LandPlayEdit.localize( "#pluginEditor" );
                } );
        }

        appendEffects( selector: string ) {
            if ( this.effects != null ) {
                for ( var i = 0; i < this.effects.length; i++ ) {
                    var effect = this.effects[i];
                    var option = $( "<option>" );
                    option.attr( "value", effect.value );
                    option.html( effect.value );
                    $( selector ).append( option );
                }
            }
        }

        protected hasItem( collection: Array<object>, pos: string ) {
            pos = parseInt( pos );
            if ( collection != null ) {
                if ( !isNaN( pos ) && pos >= 0 && pos < collection.length ) {
                    return true;
                }
            }
            return false;
        }

        protected nextId( collection: Array<object>, field: string ) {
            if ( collection == null || collection.length == 0 ) {
                return 1;
            } else {
                var temp = collection.slice();
                temp.sort( function( a, b ) {
                    return a[field] - b[field];
                } );
                return parseInt( temp[temp.length - 1][field] ) + 1;
            }
        }

        protected checkSprites( collection: Array<object>, type: string, attrName?: string ) {
            if ( attrName == null ) {
                attrName = "@sprite";
            }
            for ( var i = 0; i < collection.length; i++ ) {
                var item = collection[i];
                var id: string = item[attrName];
                if ( id != null && id != "" && LandPlayEdit.editor.getSprite( id ) == null ) {
                    var pivot = id.replace( /(_?(\d+))$/g, "" ); /* replace the trailing _<<number>> by "" */
                    var firstInGrid = id.replace( "_*", "" );
                    if ( ( LandPlayEdit.editor.getSprite( pivot ) == null ) && ( LandPlayEdit.editor.getSprite( firstInGrid ) == null ) ) {
                        var c: LandPlayEdit.Constraint = new Constraint();
                        c.text = i18next.t( "plugin.constraint.sprite.notFound", { sprite: item[attrName] + " (" + pivot + ", " + firstInGrid + ")", type: type } );
                        this.addConstraint( c );
                    }
                }
            }
        }

        protected checkFrames( sprite: string, length: number ): boolean {
            var sprite: object = LandPlayEdit.editor.getSprite( sprite );
            var asset = LandPlayEdit.editor.getAsset( sprite["@key"], LandPlay.AssetType.Image );

            if ( asset != null && asset["@frameMax"] != null && !isNaN( parseInt( asset["@frameMax"] ) ) && parseInt( asset["@frameMax"] ) >= length ) {
                return true;
            }
            return false;
        }

        addConstraint( constraint: LandPlayEdit.Constraint ) {
            this.constraints.push( constraint );
        }

        hasConstraints() {
            return ( this.constraints.length > 0 );
        }

        showConstraints() {
            var text: string = i18next.t( "plugin.constraint.header" ) + "<br>";
            for ( var i = 0; i < this.constraints.length; i++ ) {
                var c: LandPlayEdit.Constraint = this.constraints[i];
                text += " - " + c.text + "<br>";
            }

            var callout = $( "#tpl_message > div:first" ).clone( true );
            $( callout ).addClass( "alert" );
            $( "span:first", callout ).html( text );
            $( "#message-container" ).append( callout );
        }

        resetConstraints() {
            this.constraints = new Array<LandPlayEdit.Constraint>();
        }
    }

    export class Constraint {
        public text: string;
        public toolBoxKey: string;
        public toolBoxSection: string;

        constructor( toolBoxSection: string, toolBoxKey: string, text: string ) {
            this.text = text;
            this.toolBoxKey = toolBoxKey;
            this.toolBoxSection = toolBoxSection;
        }
    }
}