/// <reference path='Plugin.ts' />

module LandPlayEdit {
    export class PluginSimon extends LandPlayEdit.Plugin {

        private padAttrSep = "|";

        constructor() {
            super( {
                toolBox: {
                    behaviour: [
                        {
                            key: "pad",
                            label: "plugin.toolbox.simon.pad.label",
                            icon: "fas fa-drum-steelpan"
                        }
                    ]
                }
            } );
        }

        getTemplateName(): string {
            return "simon.html";
        }

        behaviour( type ) {
            switch ( type ) {
                case "pad":
                    this.editSimon();
                    break;
                default:
                    LandPlayEdit.warning( "Please develop pending behaviour '" + type + "'" );
                    break;
            }
        }

        private loadRemainingSpritesForTalk( value?: string ) {
            var collection: Array<object> = this.getUsedSpritesInSimon();
            $( "[name='board.@piece'] option" ).each( function() {
                collection.push( { "@sprite": $( this ).val() } );
            } );
            this.doLoadRemainingSprites( collection, "simon.setup.@talk", value );
        }

        private loadRemainingSpritesForPads( value?: string ) {
            var collection: Array<object> = this.getUsedSpritesInSimon();
            this.doLoadRemainingSprites( collection, "simon.pad.@sprite", value );
        }

        private getUsedSpritesInSimon() {
            var collection: Array<object> = new Array<object>();

            if ( LandPlayEdit.editor.json.config.pad != null ) {
                collection = collection.concat( LandPlayEdit.editor.json.config.pad );
            }
            return collection;
        }


        createSimon() {
            $( "#simonForm" ).show();
            $( "[name='simon.@piece']" ).html( "" );
            $( "[name='table.@random'][value='true']" ).prop( "checked", true );
            $( "#simonForm" ).foundation( "resetForm" );
            this.loadRemainingSpritesForTalk();
            this.loadRemainingSpritesForPads()
            LandPlayEdit.reloadSoundSelect( "#padSound", null, true );
        }

        editSimon() {
            var classRef = this;
            this.modal( "pad.html", function() {

                var simonForm = new Foundation.Abide( $( "#simonForm" ), {} );
                Foundation.reInit( 'abide' );

                $( "#simonForm" ).submit( function( event ) {
                    event.preventDefault();
                } );

                $( '#simonForm' ).on( "formvalid.zf.abide", function( ev, frm ) {
                    classRef.updateSimon();
                } );

                $( "#addPad" ).click( function() {
                    var sprite = $( "#padSprite" ).val();
                    var sound = $( "#padSound" ).val();
                    if ( sprite == "" || sprite == null ) {
                        LandPlayEdit.alert( i18next.t( "plugin.constraint.simon.pad.nosprite" ) );
                    } else {
                        if ( sound == "" || sound == null ) {
                            LandPlayEdit.alert( i18next.t( "plugin.constraint.simon.pad.nosound" ) );
                        } else {
                            var id = sprite + classRef.padAttrSep + sound;
                            if ( $( "#padSelected option[value='" + id + "']" ).length == 0
                                && $( "#padSelected option[value*='" + sprite + classRef.padAttrSep + "']" ).length == 0 ) {
                                var option = $( "<option>" );
                                option.attr( "value", id );
                                option.html( sprite + " - " + sound );
                                $( "#padSelected" ).append( option );
                            }
                        }
                    }
                    classRef.validateSimon( "simon.pad.@selected" );
                } );

                $( "#delPad" ).click( function() {
                    $( "#padSelected option:selected" ).remove();
                    classRef.validateSimon( "simon.pad.@selected" );
                } );

                classRef.createSimon();

                if ( LandPlayEdit.editor.json.config.setup != null ) {
                    $( "[name='simon.setup.@talk']" ).val( LandPlayEdit.editor.json.config.setup["@talk"] );
                    classRef.loadRemainingSpritesForTalk( LandPlayEdit.editor.json.config.setup["@talk"] );
                    $( "[name='simon.setup.@transition']" ).val( LandPlayEdit.editor.json.config.setup["@transition"] );
                    $( "[name='simon.setup.@maxseq']" ).val( LandPlayEdit.editor.json.config.setup["@maxseq"] );
                }

                var pads = LandPlayEdit.editor.json.config.pad;
                if ( pads != null ) {
                    for ( var i = 0; i < pads.length; i++ ) {
                        var sprite = pads[i]["@sprite"];
                        var sound = pads[i]["@sound"];
                        var id = sprite + classRef.padAttrSep + sound
                        var option = $( "<option>" );
                        $( option ).attr( "value", id );
                        $( option ).html( sprite + " - " + sound );
                        $( "#padSelected" ).append( option );
                    }
                }

            } );


        }

        updateSimon() {
            var classRef = this;
            LandPlayEdit.editor.json.config.setup["@talk"] = $( "[name='simon.setup.@talk']" ).val();
            LandPlayEdit.editor.json.config.setup["@transition"] = $( "[name='simon.setup.@transition']" ).val();
            LandPlayEdit.editor.json.config.setup["@maxseq"] = $( "[name='simon.setup.@maxseq']" ).val();

            LandPlayEdit.editor.json.config.pad = [];
            $( "#padSelected option" ).each( function() {
                var parts = $( this ).val().split( classRef.padAttrSep );
                LandPlayEdit.editor.json.config.pad.push( { "@sprite": parts[0], "@sound": parts[1] } );
            } );


            LandPlayEdit.saveData();
            $( "#pluginEditor" ).foundation( "close" );
        }

        validateSimon( name ) {
            var selector = "[name='" + name + "']";
            var value = $( selector ).val();

        }

        validate() {
            this.resetConstraints();
            if ( LandPlayEdit.editor.json.config.pad == null
                || LandPlayEdit.editor.json.config.pad.length < 2 ) {
                this.addConstraint( new LandPlayEdit.Constraint( "behaviour", "pad", i18next.t( "plugin.constraint.simon.pad.notenough" ) ) );
            } else {
                this.checkSprites( LandPlayEdit.editor.json.config.pad, "[pad]" );

                for ( var i = 0; i < LandPlayEdit.editor.json.config.pad.length; i++ ) {
                    var pad = LandPlayEdit.editor.json.config.pad[i];
                    if ( !this.checkFrames( pad["@sprite"], 2 ) ) {
                        this.addConstraint( new LandPlayEdit.Constraint( "behaviour", "pad", i18next.t( "plugin.constraint.simon.pad.noframes", { "sprite": pad["@sprite"] } ) ) );
                    }
                }

                if ( LandPlayEdit.editor.json.config.setup["@talk"] != null ) {
                    if ( !this.checkFrames( LandPlayEdit.editor.json.config.setup["@talk"], 3 ) ) {
                        this.addConstraint( new LandPlayEdit.Constraint( "behaviour", "pad", i18next.t( "plugin.constraint.simon.talk.noframes", { "sprite": LandPlayEdit.editor.json.config.setup["@talk"] } ) ) );
                    }
                }
            }
            if ( LandPlayEdit.editor.json.config.setup.counter == null
                || LandPlayEdit.editor.json.config.setup.counter.progress == null
                || LandPlayEdit.editor.json.config.setup.counter.progress["@sprite"] == null ) {
                this.addConstraint( new LandPlayEdit.Constraint( "behaviour", "pad", i18next.t( "plugin.constraint.simon.noprogress" ) ) );
            }

            return super.validate();
        }

    }
}