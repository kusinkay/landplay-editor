/// <reference path='Plugin.ts' />

module LandPlayEdit {
    export class PluginType extends LandPlayEdit.Plugin {

        private FREE_TEXT = ( Math.E + "" + Math.E + "" + Math.E ).split( "." ).join( "" );

        wordsGroup: Phaser.Group = null;

        constructor() {
            super( {
                toolBox: {
                    behaviour: [
                        {
                            key: "word",
                            label: "plugin.toolbox.type.word.label",
                            icon: "fas fa-i-cursor"
                        }
                    ]
                }
            } );
        }

        getTemplateName(): string {
            return "type.html";
        }

        behaviour( type ) {
            switch ( type ) {
                case "word":
                    this.openWords();
                    break;

                default:
                    LandPlayEdit.warning( "Please develop pending behaviour '" + type + "'" );
                    break;
            }
        }

        editRecord( type: string, pos: number ) {
            switch ( type ) {
                case "word":
                    this.editWord( pos );
                    break;

                default:
                    break;
            }
        }

        deleteRecord( type: string, pos: number ) {
            switch ( type ) {
                case "word":
                    this.deleteWord( pos );
                    break;

                default:
                    break;
            }
        }

        /**
         * Preview additional graphic elements
         * Launched from editor when Stage is ready
         * Override in each plugin
         ******* */
        preview() {
            if ( LandPlayEdit.editor.json.config.word != null ) {
                var classRef = this;

                if ( this.wordsGroup != null ) {
                    /* reset */
                    this.wordsGroup.destroy( true );
                }

                this.wordsGroup = LandPlayEdit.lgEditor.getGame().add.group( LandPlayEdit.lgEditor.getGame().world );
                for ( var i = 0; i < LandPlayEdit.editor.json.config.word.length; i++ ) {
                    var word = LandPlayEdit.editor.json.config.word[i];
                    var group: Phaser.Group = LandPlayEdit.lgEditor.getGame().add.group( this.wordsGroup, "word_" + word["@x"] );
                    var x = parseInt( word["@x"] );
                    var y = parseInt( word["@y"] );
                    var style = null;
                    if ( word["@style"] != null && word["@style"] != "" ) {
                        style = JSON.parse( word["@style"] );
                    }

                    if ( word.split != null ) {
                        var offsetx = parseInt( word.split["@offsetx"] );
                        var offsety = parseInt( word.split["@offsety"] );
                        var chars = word["@text"].split( "" );
                        for ( var j = 0; j < chars.length; j++ ) {
                            var sprite = LandPlayEdit.lgEditor.getGame().add.sprite( x, y, word["@key"] );
                            var text = LandPlayEdit.lgEditor.getGame().add.text( word["@offsetx"], word["@offsety"], chars[j], style );
                            sprite.addChild( text );
                            group.addChild( sprite );

                            classRef.initWordPreview( sprite );

                            x += offsetx;
                            y += offsety;
                        }
                    } else {
                        var sample: Phaser.Sprite = LandPlayEdit.lgEditor.sprites[word["@sprite"]];
                        var key = word["@key"];
                        var x = word["@x"];
                        var y = word["@y"];
                        if ( sample != null ) {
                            key = LandPlayEdit.lgEditor.EmptySpriteKey;
                            x = sample.x;
                            y = sample.y;
                        }
                        var wordText = word["@text"];
                        if ( word["@text"] == this.FREE_TEXT ) {
                            wordText = "[---]"
                        }
                        var sprite = LandPlayEdit.lgEditor.getGame().add.sprite( x, y, key );
                        var text = LandPlayEdit.lgEditor.getGame().add.text( word["@offsetx"], word["@offsety"], wordText, style );
                        sprite.addChild( text );
                        group.addChild( sprite );

                        classRef.initWordPreview( sprite );
                    }

                }
            }
        }

        private initWordPreview( sprite: Phaser.Sprite ) {
            var classRef = this;
            sprite.inputEnabled = true;
            sprite.events.onInputOver.add( function( sprite: Phaser.Sprite ) {
                LandPlayEdit.lgEditor.showBounds( sprite );
            }, sprite );
            sprite.events.onInputOut.add( function( sprite: Phaser.Sprite ) {
                LandPlayEdit.lgEditor.clearBounds( sprite );
            }, sprite );

            sprite.events.onInputUp.add( function( sprite: Phaser.Sprite ) {
                LandPlayEdit.lgEditor.clearBounds( sprite );
                var pos = classRef.wordsGroup.getChildIndex( sprite.parent );
                classRef.openWords( pos );
            }, sprite );
        }

        private loadRemainingSpritesForWords( value?: string ) {
            var collection = this.loadRemainingSpritesForWhatever( value );
            this.doLoadRemainingSprites( collection, "word.@sprite", value );
        }

        private loadRemainingSpritesForWhatever( value?: string ) {
            var collection: Array<object> = this.getUsedSpritesInWords();

            if ( LandPlayEdit.editor.json.config.word != null ) {
                var words = LandPlayEdit.editor.json.config.word;
                if ( collection == null ) {
                    collection = new Array<object>();
                }
                collection = collection.concat( words )
            }
            return collection;
        }

        private getUsedSpritesInWords() {
            var collection: Array<object> = new Array<object>();
            if ( LandPlayEdit.editor.json.config.word != null ) {
                for ( var i = 0; i < LandPlayEdit.editor.json.config.word.length; i++ ) {
                    var word = LandPlayEdit.editor.json.config.word[i];
                    if ( word.layers != null ) {
                        for ( var j = word.layers["@from"]; j <= word.layers["@to"]; j++ ) {
                            collection.push( { "@sprite": word["@sprite"] + j } );
                        }
                    }
                    collection.push( word );
                }
            }
            return collection;
        }

        private loadWordSprites( value?: string ) {
            var words: Array<string> = new Array<string>();
            if ( LandPlayEdit.editor.json.config.word != null ) {
                for ( var i = 0; i < LandPlayEdit.editor.json.config.word.length; i++ ) {
                    var word: object = LandPlayEdit.editor.json.config.word[i];
                    words.push( word["@sprite"] );
                }
                if ( LandPlayEdit.lgEditor.sprites != null ) {
                    var excluded: Array<object> = new Array<object>();
                    var keys: Array<string> = Object.keys( LandPlayEdit.lgEditor.sprites );
                    keys.sort();
                    for ( var i = 0; i < keys.length; i++ ) {
                        if ( words.indexOf( keys[i] ) < 0 ) {
                            excluded.push( { "@sprite": keys[i] } );
                        }
                    }
                    this.doLoadRemainingSprites( excluded, "word.@sprite", value );
                }
            } else {
                LandPlayEdit.alert( i180next.t( "plugin.constraint.paint.noWordSprites" ) );
            }


        }



        private hasWord( pos: string ) {
            return this.hasItem( LandPlayEdit.editor.json.config.word, pos );
        }

        private hasCanvas( pos: string ) {
            return this.hasItem( LandPlayEdit.editor.json.config.canvas, pos );
        }

        openWords( pos?: number ) {
            var classRef = this;
            this.modal( "word.html", function( params?: object ) {
                var wordForm = new Foundation.Abide( $( "#wordForm" ), {} );
                Foundation.reInit( 'abide' );

                $( "#createWord" ).click( function() {
                    classRef.createWord();
                } );
                $( "#wordForm" ).submit( function( event ) {
                    event.preventDefault();
                } );

                $( '#wordForm' ).on( "formvalid.zf.abide", function( ev, frm ) {
                    classRef.updateWord();
                } );

                $( "[name*='word.'], [name='transient.opentext']" ).change( function() {
                    classRef.validateWord( $( this ).attr( "name" ) );
                } );

                var hasWords = false;

                if ( LandPlayEdit.editor.json.config.word != null ) {
                    for ( var i = 0; i < LandPlayEdit.editor.json.config.word.length; i++ ) {
                        hasWords = true;
                        var word = LandPlayEdit.editor.json.config.word[i];
                        var text = "'" + word["@text"] + "'";
                        if ( word["@text"] == classRef.FREE_TEXT ) {
                            text = "[" + i18next.t( "reveal.word.opentext.row" ) + "]";
                        }
                        LandPlayEdit.listRecord( "#word-list", i, word["@id"] + ": " + text );
                    }

                }
                if ( !hasWords ) {
                    var textDiv = $( '#templates > #tpl_word_no_item > div:first-child' ).clone( true );
                    $( "#word-list" ).append( textDiv );
                } else {
                    LandPlayEdit.activateList( "word" );
                }

                $( "#wordForm" ).hide();

                if ( params != null && params.pos != null ) {
                    $( "#wordForm" ).show();
                    classRef.editWord( params.pos );
                }

            }, { pos: pos } );
        }

        createWord() {
            $( "#wordForm" ).show();
            $( "#wordForm" ).foundation( "resetForm" );
            $( "[name='word_pos']" ).val( "" );
            $( "[name='word.@id']" ).val( this.nextId( LandPlayEdit.editor.json.config.word, "@id" ) );
            $( "[name='transient.opentext'][value='false']" ).prop( "checked", true );
            LandPlayEdit.reloadAssetsSelect( LandPlay.AssetType.Image, "[name='word.@key']", null, true );
            this.loadRemainingSpritesForWords();
            LandPlayEdit.loadStyleEditorTemplate( "#pluginEditor .styleEditorPlace", "word.@style" );
            LandPlayEdit.loadDefaultTextStyle();
        }

        editWord( pos: number ) {
            if ( LandPlayEdit.editor.json.config.word != null ) {
                if ( !isNaN( pos ) && pos >= 0 && pos < LandPlayEdit.editor.json.config.word.length ) {
                    this.createWord();
                    var word = LandPlayEdit.editor.json.config.word[pos];

                    $( "[name*='word.']" ).each( function() {
                        var name = $( this ).attr( "name" );
                        $( this ).val( LandPlayEdit.editor.parse( { word: word }, name ) );
                    } );

                    if ( word["@sprite"] != null ) {
                        LandPlayEdit.toggleField( "word.@key", false );
                        this.loadRemainingSpritesForWords( word["@sprite"] );

                    }
                    if ( word["@key"] != null ) {
                        LandPlayEdit.toggleField( "word.@sprite", false );
                    }
                    var freetext = false;
                    if ( word["@text"] == this.FREE_TEXT ) {
                        $( "[name='word.@text']" ).val( "" );
                        freetext = true;
                        $( "[name='transient.opentext'][value='true']" ).prop( "checked", true );
                    } else {
                        LandPlayEdit.toggleField( "word.@text", true );
                        $( "[name='transient.opentext'][value='false']" ).prop( "checked", true );
                    }
                    LandPlayEdit.toggleField( "word.@text", !freetext );
                    LandPlayEdit.toggleField( "word.split.@offsetx", !freetext );
                    LandPlayEdit.toggleField( "word.split.@offsety", !freetext );

                    $( ".textSelectedStyles" ).html( "" );
                    var fields = JSON.parse( word["@style"] );
                    for ( var key of Object.keys( fields ) ) {
                        LandPlayEdit.loadStyleOption( key, fields[key] );
                    }

                    $( "[name='word_pos']" ).val( pos );
                }
            }
        }

        updateWord() {
            var container = {};
            $( "[name*='word.']" ).each( function() {
                var field = {
                    name: $( this ).attr( "name" ),
                    value: $( this ).val()
                }
                var ignoreEmpty = true;
                container = LandPlayEdit.editor.walk( container, field, ignoreEmpty );
            } );

            if ( $( "[name='transient.opentext']:checked" ).val() == "true" ) {
                container.word["@text"] = this.FREE_TEXT;
            }
            container.word["@style"] = LandPlayEdit.parseStyleFromSelect( "word.@style" );

            var pos = parseInt( $( "[name='word_pos']" ).val() );
            if ( !isNaN( pos ) ) {
                if ( LandPlayEdit.editor.json.config.word != null ) {
                    if ( pos >= 0 && pos < LandPlayEdit.editor.json.config.word.length ) {
                        LandPlayEdit.editor.json.config.word.splice( pos, 1 );
                    }
                }
            }
            if ( LandPlayEdit.editor.json.config.word == null ) {
                LandPlayEdit.editor.json.config.word = [];
            }
            if ( Object.keys( container.word.split ).length === 0 ) {
                delete container.word.split;
            }
            LandPlayEdit.editor.json.config.word.push( container.word );
            LandPlayEdit.saveData();
            this.preview();
            $( "#pluginEditor" ).foundation( "close" );
            this.openWords();
        }

        deleteWord( pos: number ) {
            var classRef = this;
            LandPlayEdit.confirm( i18next.t( "reveal.word.delete" ), function() {
                if ( LandPlayEdit.editor.json.config.word != null ) {
                    if ( !isNaN( pos ) && pos >= 0 && pos < LandPlayEdit.editor.json.config.word.length ) {
                        LandPlayEdit.editor.json.config.word.splice( pos, 1 );
                        LandPlayEdit.saveData();
                        classRef.preview();
                        $( "#pluginEditor" ).foundation( "close" );
                        classRef.openWords();
                    }
                }
            } );
        }

        validateWord( name ) {
            var selector = "[name='" + name + "']";
            var value = $( selector ).val();
            switch ( name ) {
                case "transient.opentext":
                    value = $( selector + ":checked" ).val();
                    var required = true;
                    if ( value == "true" ) {
                        $( "[name='word.@text']" ).val( "" );
                        $( "[name='word.split.@offsetx']" ).val( "" );
                        $( "[name='word.split.@offsety']" ).val( "" );
                        required = false;
                    }

                    LandPlayEdit.toggleField( "word.@text", required );
                    LandPlayEdit.toggleField( "word.split.@offsetx", required );
                    LandPlayEdit.toggleField( "word.split.@offsety", required );
                    break;

                case "word.@sprite":
                    var required = ( value == "" );
                    $( "[name='word.@key']" ).val( "" );
                    LandPlayEdit.toggleField( "word.@key", required );
                    break;

                case "word.@key":
                    var required = ( value == "" );
                    $( "[name='word.@sprite']" ).val( "" );
                    LandPlayEdit.toggleField( "word.@sprite", required );
                    break;

                default:
                    break;
            }
        }

        validate() {
            this.resetConstraints();
            if ( LandPlayEdit.editor.json.config.word == null || LandPlayEdit.editor.json.config.word.length == 0 ) {
                this.addConstraint( new LandPlayEdit.Constraint( "behaviour", "word", i18next.t( "plugin.constraint.type.word" ) ) );
            }

            return super.validate();
        }

    }
}