module LandPlay {
    export enum AssetType {
        Image = "image",
        Sound = "sound"
    }

    export const EmptyImageAsset = "__EMPTY__";

    export class Editor {

        /** 
         * @deprecated located directly on json.config.assets
         **/
        private assets = {};

        private sprites = [];

        public json = {
            "config": {
                "assets": {},
                "setup": {
                    "counter": {},
                    "event": []
                },
                "layout": {
                    "sprite": []
                },
                "button": [],
                "retry": {}
            }
        };

        private serverSidePath = './serverside/';

        private frameAtts = ["frameWidth", "frameHeight", "frameMax"];



        constructor( args: object ) {

        }

        getAssets() {
            return this.json.config.assets;
        }

        addAsset( args: object, type: AssetType ) {
            if ( args["@url"] == null ) {
                throw new Error( "asset requires an url attribute" );
            }
            if ( args["@key"] == null ) {
                throw new Error( "asset requires a key attribute" );
            }
            if ( this.json.config.assets[type] == null ) {
                this.json.config.assets[type] = [];
            }
            if ( !args.overwrite && this.getAsset( args["@key"], type ) != null ) {
                if ( typeof args.keyexists == 'function' ) {
                    args.keyexists();
                } else {
                    throw new Error( "asset key '" + args["@key"] + "' already exists" );
                }
            } else {
                this.json.config.assets[type].push( args );
            }
        }

        updateAsset( args: object, type: AssetType ) {
            var pos = this.getAssetPos( args["@key"], type );
            if ( pos > -1 ) {
                this.json.config.assets[type].splice( pos, 1 );
                var rest = this.json.config.assets[type].splice( pos );
                this.json.config.assets[type] = this.json.config.assets[type].concat( [args], rest );
            }
        }

        setFrames( key: string, frames: object ) {
            var asset = this.getAsset( key, AssetType.Image );
            if ( asset != null ) {
                for ( var frameKey of Object.keys( frames ) ) {
                    asset[frameKey] = frames[frameKey];
                }
            }
        }

        getAsset( key: string, type: AssetType ) {
            var pos = this.getAssetPos( key, type );
            if ( pos > -1 ) {
                return this.json.config.assets[type][pos];
            }
        }

        getAssetPos( key: string, type: AssetType ) {
            if ( this.json.config.assets[type] == null ) {
                this.json.config.assets[type] = [];
            }
            for ( var i = 0; i < this.json.config.assets[type].length; i++ ) {
                if ( this.json.config.assets[type][i]["@key"] == key ) {
                    return i;
                }
            }
            return -1;
        }

        getSprite( id: string, sprites?: Array<object>, goDeeper?: boolean ) {
            if ( sprites == null ) {
                sprites = this.json.config.layout.sprite;
            }
            if ( goDeeper == null ) {
                goDeeper = true;
            }
            var index: number = this.getSpriteIndex( id, sprites );
            if ( index > -1 ) {
                return sprites[index];
            } else if ( goDeeper ) {
                /*search for the embeded ones*/
                for ( var i = 0; i < sprites.length; i++ ) {
                    if ( sprites[i].sprite != null && sprites[i].sprite.length > 0 ) {
                        var result = this.getSprite( id, sprites[i].sprite );
                        if ( result != null ) {
                            result._parent = sprites[i]["@id"];
                            return result;
                        }
                    }
                }
            }
            return null;
        }

        getSpriteIndex( id: string, sprites?: Array<object> ) {
            if ( sprites == null ) {
                sprites = this.json.config.layout.sprite;
            }
            if ( sprites != null ) {
                for ( var i = 0; i < sprites.length; i++ ) {
                    if ( sprites[i]["@id"] == id ) {
                        return i;
                    }
                }
            }
            return -1;
        }

        removeSprite( id: string, sprites?: Array<object> ) {
            if ( sprites == null ) {
                sprites = this.json.config.layout.sprite;
            }
            var pos = this.getSpriteIndex( id, sprites );
            if ( pos > -1 ) {
                sprites.splice( pos, 1 );
            } else {
                for ( var i = 0; i < sprites.length; i++ ) {
                    if ( sprites[i].sprite != null ) {
                        this.removeSprite( id, sprites[i].sprite );
                    }
                }
            }
        }

        getSpriteParent( id ) {
            var child = this.getSprite( id );
            if ( child != null && child._parent != null ) {
                return this.getSprite( child._parent );
            }
        }

        addChildren( id: string, children: Array<object> ) {
            var parent = this.getSprite( id );
            if ( parent != null ) {
                if ( parent.sprite == null ) {
                    parent.sprite = [];
                }
                for ( var i = 0; i < children.length; i++ ) {
                    var child = children[i];
                    var pos = this.getSpriteIndex( child["@id"], parent.sprite );
                    if ( pos > -1 ) {
                        parent.sprite.splice( pos, 1 );
                    }
                    child._parent = id;
                    parent.sprite.push( child );
                }
            }
        }

        removeChild( id: string, child: object ) {
            var parent = this.getSprite( id );
            if ( parent != null ) {
                if ( parent.sprite == null ) {
                    parent.sprite = [];
                }
                var pos = this.getSpriteIndex( child["@id"], parent.sprite );
                if ( pos > -1 ) {
                    var removed = parent.sprite.splice( pos, 1 );
                    delete removed[0]._parent;
                    this.json.config.layout.sprite = this.json.config.layout.sprite.concat( removed );
                }
            }
        }

        appendForm( formSelector: string ) {
            var fields = $( formSelector ).serializeArray();
            for ( var i = 0; i < fields.length; i++ ) {
                if ( fields[i].name.indexOf( "config." ) == 0 ) {
                    this.walk( this.json, fields[i], true );
                }
            }
        }

        getSpriteXml( formSelector: string ) {
            return json2xml( this.getSpriteJson( formSelector ), "" );
        }

        getSpriteJson( formSelector: string ) {
            var fields = $( formSelector ).serializeArray();
            var xsprite = { "sprite": {} };
            for ( var i = 0; i < fields.length; i++ ) {
                if ( fields[i].name.indexOf( "sprite." ) == 0 ) {
                    this.walk( xsprite, fields[i] );
                }
            }
            return xsprite;
        }

        addButton( button: object ) {
            if ( this.json.config.button == null ) {
                this.json.config.button = [];
            }
            var sprite = button["@sprite"];
            if ( this.getButton( sprite ) != null ) {
                /* update */
                this.deleteButton( sprite );
            }
            this.json.config.button.push( button );
        }

        getButton( sprite: string, position?: boolean ) {
            if ( this.json.config.button != null ) {
                for ( var i = 0; i < this.json.config.button.length; i++ ) {
                    var cButton = this.json.config.button[i];
                    if ( cButton["@sprite"] == sprite ) {
                        if ( position ) {
                            return i;
                        } else {
                            return cButton;
                        }
                    }
                }
            }
            return ( position ? -1 : null );
        }

        deleteButton( sprite: string ) {
            var pos = this.getButton( sprite, true );
            if ( pos > -1 ) {
                this.json.config.button.splice( pos, 1 );
            }
        }

        json2xml( json ) {
            return json2xml( json, "" );
        }


        /**
         * recursive: current config node appending the suffix
         */
        public walk( node: object, field: object, ignoreEmpty?: boolean ): object {
            var pos = field.name.indexOf( "." );
            if ( pos > -1 ) {
                var current = field.name.substring( 0, pos );
                var rest = field.name.substring( pos + 1 );
                if ( node[current] == null ) {
                    node[current] = {};
                }
                node[current] = this.walk( node[current], { "name": rest, "value": field.value }, ignoreEmpty );
            } else {
                /*is leave: scalar value*/
                node[field.name] = field.value;
                if ( (field.value == "" || field.value == null ) && ignoreEmpty ) {
                    delete node[field.name];
                }
            }
            return node;
        }

        public parse( json: object, fieldName: string ) {
            if ( json == null ) {
                return null;
            }
            var pos = fieldName.indexOf( "." );
            if ( pos > -1 ) {
                var current = fieldName.substring( 0, pos );
                var rest = fieldName.substring( pos + 1 );
                return this.parse( json[current], rest );
            } else {
                /*is leave: scalar value*/
                return json[fieldName];
            }
        }

        toXMLURL() {
            return this.serverSidePath + "xml.php?json=" + JSON.stringify( this.toJSON() );
        }

        toJSON() {
            //standard loader
            if ( this.getAsset( "preloadBar", LandPlay.AssetType.Image ) == null ) {
                this.json.config.assets.image.push( { "@key": "preloadBar", "@url": "img/loader.png" } );
            }
            if ( this.getAsset( LandPlay.EmptyImageAsset, LandPlay.AssetType.Image ) == null ) {
                this.json.config.assets.image.push( { "@key": LandPlay.EmptyImageAsset, "@url": "img/empty.png" } );
            }
            return this.json;
        }

        private addAssetAtts( from: object, to: object, reverse?: boolean ) {
            var add = "@";
            var addRev = "";
            if ( reverse ) {
                add = "";
                addRev = "@";
            }

            for ( var i = 0; i < this.frameAtts.length; i++ ) {
                var attr = this.frameAtts[i];
                if ( from[attr] != null ) {
                    to[add + attr] = from[addRev + attr];
                }
            }
            return to;
        }

        fromJSON( json ) {
            this.json = json;
            /*
            for ( var type of Object.keys( json.config.assets ) ) {
                for ( var i = 0; i < json.config.assets[type].length; i++ ) {
                    var asset = json.config.assets[type][i];
                    var key = asset["@key"];
                    if ( this.assets[type] == null ) {
                        this.assets[type] = {};
                    }
                    var reverse = true;
                    this.assets[type][key] = this.addAssetAtts(asset, {}, reverse);
                }
            }
            */
        }

        toXML( indent?: boolean ) {
            return json2xml( this.toJSON(), ( indent != null && indent ? "  " : "" ) );
        }
    }
}