/// <reference path="../landplay.d.ts" />
module LandPlay {

    export enum EditAction {
        Edit,
        Position,
        Text,
        Frames,
        Button,
        Grid,
        Shape,
        Anchor,
        Embed
    }

    export class EditorMode extends LandGame {

        private action: LandPlay.EditAction = LandPlay.EditAction.Edit;

        private editor: LandPlay.Editor;

        /**
         * Callbacks for sprite
         */
        private sprite_click;
        private sprite_drop;
        private sprite_unnamed;

        private minFrameDim = 15;

        public textFields = {
            "@key": { "attr": "name", "default": "textTag" },
            "@label": { "attr": "text" },
            "@x": { "attr": "x", "default": 0 },
            "@y": { "attr": "y", "default": 0 }
        }

        /**
         * The Sprite whose that is being edited*/
        private targetSprite: Phaser.Sprite;
        private destinationSprite: Phaser.Sprite;
        private bmd: Phaser.BitmapData;
        private frameInfo: object = {};
        private frameCloseSprite: Phaser.Sprite;
        private anchor: object = {};

        public EmptySpriteKey = "__EMPTY__";

        public ungroupedGridActions = [LandPlay.EditAction.Button];

        private groups = [LandPlay.LandGame.GridsContainer, LandPlay.LandGame.LayersContainer];

        /** 
         * We can't operate at all with no visible sprites
         * So we stablish an alpha value to emulate visible=false
         * */
        private GhostNumber = Math.PI * 0.1;

        constructor( xmlFile: string, params?: object ) {
            super();
            this.className = "EditorMode";
            this.init( xmlFile, params );
            this.editor = params.editor;
            this.sprite_click = params.sprite_click;
            this.sprite_drop = params.sprite_drop;
            this.sprite_framed = params.sprite_framed;
            this.sprite_unnamed = params.sprite_unnamed;
            this.stage_load = params.stage_load;
            this.setAction( LandPlay.EditAction.Edit );
        }

        renderSprite( spriteNode: object, spParent: Phaser.Sprite, depth?: number, children?: object ): object {
            var jsonSprite = { sprite: spriteNode };
            var classRef = this;
            var id = jsonSprite.sprite['@id'];
            if ( jsonSprite.sprite['@visible'] == "false" ) {
                jsonSprite.sprite['@alpha'] = classRef.GhostNumber;
            }
            jsonSprite.sprite['@visible'] = true;
            if ( classRef.getKeys( classRef.sprites ).indexOf( id ) > -1 ) {
                this.log( "sprite '" + id + "' already exist, kill to render again", 4 );
                /*but first preserve its children*/
                /** 
                 * text sprites to JSON
                 **/
                /** not anymore, since it comes from JSON
                jsonSprite.sprite.text = [];
                var withText = false;
                for ( var i = 0; i < classRef.sprites[id].children.length; i++ ) {
                    var curText = classRef.sprites[id].getChildAt( i );
                    if ( curText.text != null ) {
                        if ( !withText ) {
                            withText = true;
                            jsonSprite.text = [];
                        }
                        jsonSprite.sprite.text.push( classRef.text2Json( curText ) );
                    }
                }
                */
                classRef.sprites[id].kill();
            }

            if ( children != null ) {
                for ( var attr of Object.keys( children ) ) {
                    jsonSprite.sprite[attr] = children[attr];
                }
            }
            var xmlDoc = $.parseXML( this.editor.json2xml( jsonSprite ) );
            $( xmlDoc ).children( 'sprite' ).each( function() {
                classRef.landstage.renderSprite( classRef.landstage, spParent, $( this ), depth );
            } );

            for ( var key in this.sprites ) {
                this.initClip( this.sprites[key] );
            }
            return jsonSprite;
        }

        /**
         * @override 
         * */
        preRenderSprite( jqXmlnode ) {
            if ( $( jqXmlnode ).attr( "visible" ) == "false" ) {
                $( jqXmlnode ).attr( "visible", "true" );
                $( jqXmlnode ).attr( "alpha", this.GhostNumber );
            }
        }

        newSprite( key, params?: object ) {
            var classRef = this;
            var keyParam = key;
            if ( params != null && params.editFrames ) {
                keyParam = new PIXI.Texture( new PIXI.BaseTexture( params.rawImage ) );

            } else if ( key == classRef.EmptySpriteKey ) {
                keyParam = LandPlay.EmptyImageAsset;
            }
            var asset = this.editor.getAsset( key, AssetType.Image );
            var hasLayers = false;
            if ( asset.layers != null && asset.layers["@from"] != null && asset.layers["@to"] != null ) {
                keyParam = key + asset.layers["@from"];
                hasLayers = true;
            }
            var sprite: Phaser.Sprite = this.game.add.sprite( 0, 0, keyParam );
            if ( hasLayers ) {
                var from = parseInt( asset.layers["@from"] );
                var to = parseInt( asset.layers["@to"] );
                if ( !isNaN( from ) && !isNaN( to ) ) {
                    var layerCont = new LandLayer( classRef, from, to, key );
                    /*sprite._lpLayers = asset.layers;*/
                    layerCont.add( sprite );
                    for ( var i = from + 1; i <= to; i++ ) {
                        var layer = this.game.add.sprite( 0, 0, key + i );
                        layerCont.add( layer );
                    }
                }
            }

            if ( params != null && params.editFrames ) {
                sprite.x = 20;
                sprite.y = 20;

                classRef.frameCloseSprite = this.game.add.sprite( 0, 0 );
                var style = { fill: "black", stroke: "white", strokeThickness: 2 };
                var closeText: Phaser.Text = this.game.add.text( 0, 0, "X", style );
                classRef.frameCloseSprite.addChild( closeText );
                classRef.frameCloseSprite.inputEnabled = true;
                classRef.frameCloseSprite.events.onInputDown.add( function( frameCloseSprite: Phaser.Sprite ) {
                    sprite.destroy( true );
                    frameCloseSprite.destroy( true );
                }, classRef.frameCloseSprite );
                classRef.frameCloseSprite.events.onInputOver.add( function( frameCloseSprite: Phaser.Sprite ) {
                    var style = closeText.style;
                    style.strokeThickness = 4;
                    closeText.setStyle( style );
                }, classRef.frameCloseSprite );
                classRef.frameCloseSprite.events.onInputOut.add( function( frameCloseSprite: Phaser.Sprite ) {
                    var style = closeText.style;
                    style.strokeThickness = 2;
                    closeText.setStyle( style );
                }, classRef.frameCloseSprite );


                sprite.key = key;
                classRef.action = LandPlay.EditAction.Frames;
                classRef.targetSprite = sprite;
                classRef.initLiner( sprite );

                /*TODO fit into de stage bounds if necessary*/
            }

            this.initClip( sprite, params );

            if ( key == classRef.EmptySpriteKey ) {
                this.showBounds( sprite );
            }
        }

        initClip( sprite: Phaser.Sprite, params?: object ) {
            var classRef = this;
            sprite.inputEnabled = true;

            sprite.events.onInputDown.dispose();
            sprite.events.onInputDown.add(
                function( sprite: Phaser.Sprite ) {
                    if ( classRef.action != LandPlay.EditAction.Frames && ( sprite.id == null || sprite.id == "" ) ) {
                        sprite._lpUnnamed = true;
                        if ( typeof classRef.sprite_unnamed == "function" ) {
                            classRef.sprite_unnamed( sprite );
                        }
                    } else {
                        sprite._lpUnnamed = false;
                    }
                }, sprite );
            sprite.events.onInputUp.dispose();
            sprite.events.onInputUp.add(
                function( sprite: Phaser.Sprite ) {
                    if ( classRef.isEditable() ) {
                        if ( typeof classRef.sprite_click == "function" ) {
                            if ( !sprite._lpUnnamed || classRef.action == LandPlay.EditAction.Edit || classRef.action == LandPlay.EditAction.Anchor ) {
                                var clickedSprite = sprite;
                                var extra = null;
                                if ( classRef.ungroupedGridActions.indexOf( classRef.action ) < 0 ) {
                                    clickedSprite = classRef.getPivot( sprite );
                                }
                                if ( classRef.action == LandPlay.EditAction.Anchor ) {
                                    extra = classRef.anchor;
                                }
                                classRef.sprite_click( clickedSprite, extra );
                            }
                        }
                    } else if ( !sprite._lpUnnamed ) {
                        if ( classRef.action == LandPlay.EditAction.Frames
                            && ( classRef.frameInfo["@frameWidth"] >= classRef.minFrameDim || classRef.frameInfo["@frameHeight"] >= classRef.minFrameDim ) ) {
                            if ( typeof classRef.sprite_framed == "function" ) {
                                classRef.sprite_framed( sprite, classRef.frameInfo );
                                classRef.action == LandPlay.EditAction.Edit;
                                classRef.frameCloseSprite.destroy( true );
                            }
                        }
                    }
                    classRef.clearBounds( sprite );
                }, sprite );

            var dragableSprite = this.isDragableSprite( sprite );
            sprite.input.enableDrag( dragableSprite );
            if ( dragableSprite ) {
                this.addDragEvents( sprite );
            }
            sprite.input.dragFromCenter = false;

            /** 
             * No drag till Dragable action is selected 
             * */
            var drag = this.isDragable() && dragableSprite;
            if ( drag ) {
                sprite.input.enableDrag();
            } else {
                sprite.input.disableDrag();
            }

            /** 
             * Let's draw an square to identify what sprite we are up to edit
             **/
            sprite.events.onInputOver.dispose();
            sprite.events.onInputOver.add(
                function( sprite: Phaser.Sprite ) {
                    var group: Phaser.Group = classRef.getGroup( sprite );
                    if ( classRef.ungroupedGridActions.indexOf( classRef.action ) < 0 && group != null ) {
                        group.forEach( function( sprite ) {
                            classRef.showBounds( sprite );
                        }, group );
                    } else {
                        classRef.showBounds( sprite );
                    }
                    if ( classRef.action == LandPlay.EditAction.Anchor ) {
                        classRef.initLiner( sprite );
                        classRef.targetSprite = sprite;
                    }

                }, sprite );

            sprite.events.onInputOut.dispose();
            sprite.events.onInputOut.add(
                function( sprite: Phaser.Sprite ) {
                    var group: Phaser.Group = classRef.getGroup( sprite );
                    if ( classRef.ungroupedGridActions.indexOf( classRef.action ) < 0 && group != null ) {
                        group.forEach( function( sprite ) {
                            classRef.clearBounds( sprite );
                        }, group );
                    } else {
                        classRef.clearBounds( sprite );
                    }

                    if ( classRef.action == LandPlay.EditAction.Anchor ) {
                        classRef.clearAnchor();
                    }
                }, sprite );

        }

        private initLiner( sprite: Phaser.Sprite ) {
            var classRef = this;
            if ( sprite._lpLiner == null ) {
                classRef.bmd = sprite.game.add.bitmapData( sprite.game.width, sprite.game.height );
                var color = 'white';

                classRef.bmd.ctx.beginPath();
                classRef.bmd.ctx.lineWidth = "4";
                classRef.bmd.ctx.strokeStyle = color;
                classRef.bmd.ctx.stroke();

                var liner = classRef.game.add.sprite( 0, 0, classRef.bmd );
                sprite.addChild( liner );
                sprite._lpLiner = liner;
            }
        }

        showBounds( sprite: Phaser.Sprite, colors?: Array<number> ) {
            if ( sprite._lpOverSign == null ) {
                sprite._lpOverSign = sprite.game.add.sprite( 0, 0 );
                {
                    if ( colors == null ) {
                        colors = [0x000000, 0xFFFFFF]
                    }
                    var bounds = sprite.getLocalBounds();
                    var grafics: Phaser.Graphics = sprite.game.add.graphics( 0, 0 );//sprite.x, sprite.y);
                    var border = 1 / sprite.scale.x;
                    grafics.lineStyle( border, colors[0] );
                    grafics.drawRect( 0, 0, bounds.width, bounds.height );

                    grafics.lineStyle( border, colors[1] );
                    grafics.drawRect( border, border, bounds.width - ( border * 2 ), bounds.height - ( border * 2 ) );

                    grafics.endFill();
                    grafics.boundsPadding = 0;
                    grafics.cacheAsBitmap = true;

                    sprite._lpOverSign.addChild( grafics );
                }
                sprite.addChild( sprite._lpOverSign );
            }
        }

        clearBounds( sprite: Phaser.Sprite ) {
            if ( sprite._lpOverSign != null ) {
                sprite._lpOverSign.kill();
                sprite._lpOverSign = null;
            }
        }

        public getGridPivot( sprite: Phaser.Sprite ) {
            return this.getPivot( sprite );
        }

        public getPivot( sprite: Phaser.Sprite, depth?: number ) {
            if ( depth == null ) {
                depth = 0;
            }
            if ( depth < this.groups.length ) {
                var parent: Phaser.Group = this.getGroup( sprite );
                if ( parent != null ) {
                    /*its a group from a given group, give them the first child*/
                    var first = parent.children[0];
                    if ( first.id != null ) {
                        first.id = parent.name.replace( /(.*)(_)$/g, "$1" );
                    } else {
                        /* is a sprite that has never been saved, let the user set the ID */
                    }
                    return first;
                } else {
                    return this.getPivot( sprite, depth + 1 );
                }
            }
            return sprite;
        }

        public getGridGroup( sprite: Phaser.Sprite ) {
            return this.getGroup( sprite );
        }

        public getGroup( sprite: Phaser.Sprite, depth?: number ) {
            if ( depth == null ) {
                depth = 0;
            }
            if ( depth < this.groups.length ) {
                if ( sprite != null ) {
                    var parent: Phaser.Group = sprite.parent;
                    if ( parent != null && parent != undefined &&
                        (
                            (
                                parent.parent != null && parent.parent.name != null && parent.parent.name == this.groups[depth]
                            ) ||
                            (
                                this.isEmbeded(sprite)
                                /* its an embeded sprite */
                            )
                        ) ) {
                        if ( this.isEmbeded(sprite) ) {
                            parent.name = parent.id;
                        }
                        return parent;
                    } else {
                        return this.getGroup( sprite, depth + 1 );
                    }
                }
            }
            return null;
        }
        
        private isEmbeded( sprite: Phaser.Sprite ) {
            return (parent.id != null && Object.keys( this.sprites ).indexOf( parent.id ) > -1);
        }

        public getGridPivotByName( name: string ) {
            return this.getPivotByName( name );
        }

        public getPivotByName( name: string ) {
            var parent = this.getGroupByName( name );
            if ( parent != null ) {
                /*its a group from a grid, give them the first child*/
                var first = parent.children[0];
                first.id = parent.name;
                return first;
            }
            return null;
        }


        public getGridGroupByName( name: string ) {
            return this.getGroupByName( name );
        }

        public getGroupByName( name: string, depth?: number ) {
            if ( depth == null ) {
                depth = 0;
            }
            if ( depth < this.groups.length ) {
                for ( var i = 0; i < this.game.world.children.length; i++ ) {
                    var container = this.game.world.children[i];
                    if ( container.name == this.groups[depth] ) {
                        for ( var j = 0; j < container.children.length; j++ ) {
                            var group = container.children[j];
                            if ( group.name == name ) {
                                return group
                            }
                        }
                    }
                }
                return this.getGroupByName( name, depth + 1 );
            }
        }

        public text2Json( text: Phaser.Text ) {
            var result = {};
            for ( var key of Object.keys( LandPlayEdit.lgEditor.textFields ) ) {
                result[key] = text[LandPlayEdit.lgEditor.textFields[key].attr];
            }
            result["@style"] = JSON.stringify( text.style );
            return result;
        }

        private addDragEvents( sprite: Phaser.Sprite ) {
            var classRef = this;
            sprite.events.onDragStart.dispose();
            sprite.events.onDragStart.add(
                function( sprite: Phaser.Sprite ) {
                    if ( classRef.action == LandPlay.EditAction.Position ) {

                    } else if ( classRef.action == LandPlay.EditAction.Embed ) {
                        classRef.targetSprite = sprite;
                        classRef.showBounds( classRef.targetSprite );
                    }
                }, sprite );

            sprite.events.onDragStop.dispose();
            sprite.events.onDragStop.add(
                function( sprite ) {
                    if ( !sprite._lpUnnamed ) {
                        if ( classRef.action == LandPlay.EditAction.Position
                            || classRef.action == LandPlay.EditAction.Embed ) {
                            if ( typeof classRef.sprite_drop == "function" ) {
                                var extra = null;
                                var pivot: Phaser.Sprite = classRef.getPivot( sprite );
                                pivot.x = sprite.x;
                                pivot.y = sprite.y;
                                if ( classRef.action == LandPlay.EditAction.Embed ) {
                                    extra = { container: classRef.destinationSprite };
                                    classRef.clearBounds( classRef.targetSprite );
                                }
                                classRef.sprite_drop( pivot, extra );
                            }
                        }
                    }
                }, sprite );
        }

        setAction( action: LandPlay.EditAction ) {
            this.action = action;
            var drag = this.isDragable();
            for ( var key in this.sprites ) {
                this.initClip( this.sprites[key] );
                this.sprites[key].input.setDragLock( drag, drag );

            }

        }

        locateAllSprites( on: boolean ) {
            for ( var key in this.sprites ) {
                if ( on ) {
                    this.showBounds( this.sprites[key] );
                } else {
                    this.clearBounds( this.sprites[key] );
                }
            }
        }

        isDragable() {
            return ( this.action == LandPlay.EditAction.Position
                || this.action == LandPlay.EditAction.Embed );
        }

        isDragableSprite( sprite: Phaser.Sprite ) {
            if ( this.action != LandPlay.EditAction.Embed ) {
                return true;
            }
            var jsonSprite = this.editor.getSprite( sprite.id );
            if ( jsonSprite != null ) {
                return ( jsonSprite.layers == null ) && ( jsonSprite.grid == null ) && ( jsonSprite.sprite == null );
            }
            return false;
        }

        isEditable() {
            return ( this.action == LandPlay.EditAction.Edit )
                || ( this.action == LandPlay.EditAction.Text )
                || ( this.action == LandPlay.EditAction.Button )
                || ( this.action == LandPlay.EditAction.Grid )
                || ( this.action == LandPlay.EditAction.Shape )
                || ( this.action == LandPlay.EditAction.Anchor )
                ;
        }

        loadConf() {
            this.log( 'loadConf', 4 );
            super.loadConf();
        }

        onConfigLoad() {
            var classRef = this;

            $( this.xml ).find( 'setup' ).each( function() {


            } );
        }

        update() {
            var classRef = this;
            if ( classRef.action == LandPlay.EditAction.Frames ) {
                this.updateFrames();
            } else if ( classRef.action == LandPlay.EditAction.Anchor ) {
                this.updateAnchor();
            } else if ( classRef.action == LandPlay.EditAction.Embed ) {
                this.updateEmbed();
            }
        }

        doEmbed() {
            if ( this.destinationSprite != null && this.targetSprite != null ) {
                this.destinationSprite.addChild( this.targetSprite );
            }
        }

        private updateEmbed() {
            var classRef = this;
            if ( classRef.targetSprite != null && classRef.targetSprite._lpOverSign != null ) {
                for ( var i = 0; i < classRef.editor.json.config.layout.sprite.length; i++ ) {
                    var jsonSprite = classRef.editor.json.config.layout.sprite[i];
                    if ( jsonSprite.layers == null && jsonSprite.grid == null ) {
                        var currentSprite = this.sprites[jsonSprite["@id"]];
                        if ( jsonSprite["@id"] != classRef.targetSprite.id ) {
                            if ( classRef.targetSprite.overlap( currentSprite ) ) {
                                classRef.showBounds( currentSprite, [0xE0115F, 0xccff33] );
                                classRef.destinationSprite = currentSprite;
                            } else {
                                classRef.clearBounds( currentSprite );
                            }
                        }
                    }
                }
            }
        }

        private updateFrames() {
            var classRef = this;
            if ( classRef.bmd != null ) {
                const lineWidth = 1;
                var width = classRef.targetSprite.width;
                var gridx = classRef.game.input.x;
                while ( width % gridx != 0 && gridx > 0 ) {
                    gridx--;
                }

                var height = classRef.targetSprite.height;
                var gridy = classRef.game.input.y;
                while ( height % gridy != 0 && gridy > 0 ) {
                    gridy--;
                }

                classRef.bmd.clear();
                /*vertical*/
                for ( var posx = gridx; posx > 0 && posx < width; posx += gridx ) {
                    classRef.drawLine( "white", posx, posx, 0, classRef.targetSprite.height, lineWidth );
                    classRef.drawLine( "black", posx + lineWidth, posx + lineWidth, 0, classRef.targetSprite.height, lineWidth );

                }
                /*horizontal*/
                for ( var posy = gridy; posy > 0 && posy < height; posy += gridy ) {
                    classRef.drawLine( "white", 0, classRef.targetSprite.width, posy, posy, lineWidth );
                    classRef.drawLine( "black", 0, classRef.targetSprite.width, posy + lineWidth, posy + lineWidth, lineWidth );
                }

                classRef.frameInfo['@frameWidth'] = gridx;
                classRef.frameInfo['@frameHeight'] = gridy;
                classRef.frameInfo['@frameMax'] = ( width / gridx ) * ( height / gridy );
                classRef.frameInfo['width'] = width;
            }
        }

        private updateAnchor() {
            var classRef = this;
            if ( classRef.targetSprite != null && classRef.targetSprite._lpLiner != null ) {
                const lineWidth = 1 / classRef.targetSprite.scale.x;
                var width = classRef.targetSprite.width / classRef.targetSprite.scale.x;
                var gridx = classRef.targetSprite.input.pointerX() / classRef.targetSprite.scale.x;

                var height = classRef.targetSprite.height / classRef.targetSprite.scale.y;
                var gridy = classRef.targetSprite.input.pointerY() / classRef.targetSprite.scale.y;

                classRef.bmd.clear();
                /*vertical*/
                var posx = gridx;
                classRef.drawLine( "white", posx, posx, 0, height, lineWidth );
                classRef.drawLine( "black", posx + lineWidth, posx + lineWidth, 0, height, lineWidth );
                /*horizontal*/
                var posy = gridy;
                classRef.drawLine( "white", 0, width, posy, posy, lineWidth );
                classRef.drawLine( "black", 0, width, posy + lineWidth, posy + lineWidth, lineWidth );

                if ( gridx > width || gridy > height ) {
                    classRef.clearAnchor();
                } else {
                    classRef.anchor = {
                        x: Math.round( gridx / width * 100 ),
                        y: Math.round( gridy / height * 100 ),
                    }
                }
            }
        }

        private clearAnchor() {
            var classRef = this;
            if ( classRef.targetSprite._lpLiner != null ) {
                classRef.targetSprite._lpLiner.destroy( true );
                classRef.targetSprite._lpLiner = null;
            }
        }

        drawLine( color: string, xb: number, xe: number, yb: number, ye: number, lineWidth: number ) {
            var classRef = this;
            classRef.bmd.ctx.beginPath();
            classRef.bmd.ctx.moveTo( xb, yb );
            classRef.bmd.ctx.lineTo( xe, ye );
            classRef.bmd.ctx.lineWidth = lineWidth;
            classRef.bmd.ctx.strokeStyle = color;
            classRef.bmd.ctx.stroke();
            classRef.bmd.ctx.closePath();
            classRef.bmd.render();
        }

        onStageLoad() {
            var classRef = this;

            $( this.xml ).find( 'piece' ).each( function() {


            } );

            super.onStageLoad();
            if ( this.sprites != null ) {
                for ( var i = 0; i < this.sprites.length; i++ ) {
                    this.initClip( this.sprites[i] );
                }
            }

            if ( typeof this.stage_load == "function" ) {
                this.stage_load();
            }

        }





    }


}


