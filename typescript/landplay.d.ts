/// <reference path="../typescript/phaser.d.ts" />
/// <reference path="../typescript/jquery.d.ts" />
declare module LandPlay {
    class CoreObject {
        /**
         * defined debug level
         * 		0: lowest level, logs at this level always will be shown
         *
         */
        private debug;
        /**
         * to debug variables from console
         */
        debugVar: any;
        /**
         * TextField object where logs will be shown
         * if it's undefined logs will be shown by trace()
         * @see setLogTextField method
         */
        private logTextField;
        /**
         * path of the ASCII file where logs will be written at
         * if it's undefined logs will be shown by trace()
         * @TODO
         */
        private logfile;
        /**
         * Attribute that must be filled with the name of the class that extends CoreObject
         *
         *
         */
        protected className: String;
        /**
         * This array stores the errors to be shown afterwards
         */
        protected errors: Array<string>;
        /**
         * Log text is showed if the corresponding debug level is set
         */
        log(text: any, _debug?: number): void;
        error(text: any): void;
        /**
         * get Phaser.Sprite object ('MovieClip' in ActionScript, here is the reason of the name)
         * from its key
         * @param name: id
         * @return LandSprite
         * //TODO
         */
        getMovie(name: string): void;
        setDebug(debug: number): void;
        colorToInt(tcolor: string): number;
        checkOverlap(a: any, b?: any): boolean;
        parseSelection(selection: string): number[];
        randPos(from: number, to: number, len?: number): string[];
        /**
         * If len is greater than array.length, there could be repeating items
         */
        rand(array: Array<string>, len?: number): string[];
        random(n: number): number;
        randRange(min: number, max: number): number;
        getKeys(obj: any, filter?: any): Array<string>;
    }
}
declare module LandPlay {
    class Scorm {
        private nFindAPITries;
        private API;
        private maxTries;
        private SCORM_TRUE;
        private SCORM_FALSE;
        private SCORM_NO_ERROR;
        private terminateCalled;
        private initialized;
        private startTimeStamp;
        private manifest;
        private id;
        private pageNum;
        private pageMax;
        private passThreshold;
        private pageTitle;
        private files;
        private currentPage;
        private static NS_URI;
        private static NS_XSI_URI;
        private static NS_ADLCP_URI;
        private static NS_ADLSEC_URI;
        private static NS_ADLNAV_URI;
        private static NS_IMSSS_URI;
        constructor();
        loadDOM(node: any): void;
        addFile(file: any): void;
        /**
         * // The this.ScanForAPI() function searches for an object named API_1484_11
             // in the window that is passed into the function.  If the object is
             // found a reference to the object is returned to the calling function.
             // If the instance is found the SCO now has a handle to the LMS
             // provided this.API Instance.  The function searches a maximum number
             // of parents of the current window.  If no object is found the
             // function returns a null reference.  This function also reassigns a
             // value to the win parameter passed in, based on the number of
             // parents.  At the end of the function call, the win variable will be
             // set to the upper most parent in the chain of parents.
         * @param win
         */
        ScanForAPI(win: any): any;
        GetAPI(win: any): void;
        ScormProcessInitialize(): void;
        ScormProcessTerminate(): void;
        ScormProcessGetValue(element: any, checkError: any): any;
        ScormProcessSetValue(element: any, value: any): void;
        doStart(): void;
        RecordTest(score: any): void;
        private RecordTestByContext(score, context);
        ConvertMilliSecondsIntoSCORM2004Time(intTotalMilliseconds: any): string;
    }
}
declare module LandPlay {
    enum Mode {
        Arcade = "Arcade",
        Catch = "Catch",
        DragDrop = "DragDrop",
        Paint = "Paint",
        Puzzle = "Puzzle",
        Simon = "Simon",
        Type = "Type",
        WordSearch = "WordSearch",
    }
    class LandGame extends CoreObject {
        protected version: string;
        protected game: Phaser.Game;
        xml: any;
        assets: any[];
        sounds: Array<Asset>;
        images: Array<Asset>;
        maps: Array<Asset>;
        /**
         * Indica si la asignacion debe ser evaluada de forma sincrona o no
         */
        validate: boolean;
        /**
         * el nombre de imagen que corresponde a cada resultado de la validacion
         */
        rightValidate: string;
        wrongValidate: string;
        /**
         * sprite group where validation icon are stored
         */
        validateGroup: Phaser.Group;
        validateIconXoffset: number;
        validateIconYoffset: number;
        validateIconWidth: string;
        validateIconHeight: string;
        /**
         * Puntos acumulados en la pantalla
         */
        private points;
        /**
         * Puntos por escena, se usa para cuando se quiere sumar y mantener la informacion de
         * las diferentes pantallas.
         * scenePoints[0] = puntos en la escena 1
         */
        private scenePoints;
        /**
         * Booleano que indica si el XML se ha cargado
         */
        xmlLoaded: any;
        private canvasWidth;
        private canvasHeight;
        private DOMcontainer;
        private scorm;
        /**
         * No se usa
         */
        private td;
        /**
         * Eventos definidos en el XML
         */
        protected itemEvents: Array<LandEvent>;
        /**
         * Event causes that only can happen once
         **/
        private oneShotEventCauses;
        /***
         * Already fired event causes
         **/
        private firedEventCauses;
        /**
         * Elecciones consideradas como correctas, estructura que contiene
         * todos los datos correctos. Aqui vendremos a comparar las respuestas del jugador
         * para validar sus respuestas.
         */
        protected assignacions: Array<Array<string>>;
        /**
         * Posibles respuestas que pueden ser elegidas por el jugador
         */
        protected responses: Array<string>;
        /**
         * Respuestas elegidas por el jugador
         */
        eleccions: any;
        /**
         * Etiquetas textuales alternativas de las posibles respuestas
         */
        private responseTags;
        private responseExtraTags;
        /**
         * Número de intentos maximos para hacer el exercicio.
         */
        private retries;
        private from;
        private to;
        private step;
        private left;
        /**
         * Indica que se desea reorganizar las opciones, usado habitualmente
         * para que si se juega dos veces el mismo juego no siempre esten
         * las opciones en la misma posición de la pantalla.
         */
        protected randFlag: boolean;
        /**
         * Array unidimensional de recipientes definidos
         */
        recipients: Array<string>;
        respostes: Array<string>;
        /**
         * Elementos de herramienta (Catch y Paint)
         */
        private tool;
        private toolPoint;
        private toolBag;
        pickable: boolean;
        picked: Boolean;
        /**
         * Objeto movieclip que contiene el contador
         */
        private counter;
        private textCounter;
        private textPoints;
        private textTime;
        private timer;
        private remainingSecs;
        /**
         * sprite id for game progress bar
         */
        private progress;
        private progressMask;
        sprites: Array<LandSprite>;
        texts: Array<Phaser.Text>;
        buttons: Array<LandButton>;
        landstage: LandPlay.LandStage;
        /**
         * Steps that take to consider stage cleared
         */
        steps: number;
        bitMaps: Array<Phaser.BitmapData>;
        protected debugBitMapGroup: Phaser.Group;
        static GridsContainer: string;
        static LayersContainer: string;
        grids: Phaser.Group;
        layers: Phaser.Group;
        init(xmlFile: string, params?: object): void;
        private parseXml(xml);
        updateCounter(): void;
        updateProgress(): void;
        getProgressPerc(): number;
        createAsset(url: string, key: string, frameWidth?: number, frameHeight?: number, frameMax?: number): Asset;
        loadConf(): void;
        addSprite(id: string, sprite: LandSprite): void;
        setEvents(option: any, clear?: boolean, children?: boolean): LandEvent[];
        setAnimations(jqXmlnode: any, sprite: LandSprite): void;
        private setEvent(option, itemEvents);
        addText(id: string, text: Phaser.Text): void;
        setRetries(option: any): void;
        /**
         * Actualizar los atributos de reintento. Usar en caso de fallo.
         */
        retry(): number;
        tryout(): boolean;
        registerOption(option: any): void;
        preRenderSprite(jqXmlnode: any): void;
        getAlternativeTags(option: any): void;
        /**
         * Asignar las etiquetas textuales a las respuestas
         */
        assignTags(): void;
        setRandom(option: any): void;
        setClipEvents(): void;
        initClip(clip: any): void;
        hashResponsesPosition(): void;
        hashMoviesPosition(movies: Array<string>): void;
        private loaded();
        triggerEvents(cause: string, sprite?: string): void;
        triggerRight(sprite?: string): void;
        triggerWrong(sprite?: string): void;
        clearEvents(tipo: string): void;
        hasCause(piece: LandSprite, cause: string): boolean;
        checkValidate(): void;
        solve(): void;
        hint(): void;
        /**
         * Reset items
         */
        reset(): void;
        /**
         * Checks if "movieName" is the name of a right answer within the "recipient"
         */
        goodAnswer(movieName: string, recipient: string): boolean;
        getAsset(key: string): Asset;
        getGame(): Phaser.Game;
        getLayers(): Phaser.Group;
        getGrids(): Phaser.Group;
        getMovie(name: string): LandSprite;
        resetValidate(): void;
        private addCheckIcon(target, status);
        collectValidable(): string[][];
        back(id: string): void;
        visible(id: string, value: boolean): void;
        getPosPointer(): Phaser.Point;
        getNumber(opt: any, name: any): number;
        addpoints(value: number): void;
        lock(id: string): void;
        destroy(id: string): void;
        onConfigLoad(): void;
        onStageLoad(): void;
        overTrasparentPixel(sprite: LandSprite, x?: number, y?: number): boolean;
        debugBitMap(canvas: LandSprite): void;
        /**
         * changes z of the given sprite over the other this.sprites
         */
        bringTop(sprite: string): void;
        update(): void;
        render(): void;
    }
    class LandButton {
        sprite: string;
        tipo: string;
        value: string;
        static Types: {
            TYPE_HREF: {
                value: string;
            };
            TYPE_SOUND: {
                value: string;
            };
            TYPE_VALIDATE: {
                value: string;
            };
            TYPE_RESET: {
                value: string;
            };
            TYPE_HINT: {
                value: string;
            };
        };
        constructor(sprite: string, tipo: string, value: string);
        activate(lg: LandGame): void;
    }
    class LandEvent {
        cause: any;
        evtType: any;
        sprite: string;
        value: any;
        /** @deprecated see Cause */
        static CAUSE_RIGHT: string;
        /** @deprecated see Cause */
        static CAUSE_WRONG: string;
        /** @deprecated see Cause */
        static CAUSE_TRYOUT: string;
        /** @deprecated see Cause */
        static CAUSE_COMPLETE: string;
        /** @deprecated see Cause */
        static CAUSE_HINT: string;
        /** @deprecated see Cause */
        static CAUSE_TIMEOUT: string;
        static Cause: {
            CAUSE_RIGHT: {
                value: string;
            };
            CAUSE_WRONG: {
                value: string;
            };
            CAUSE_TRYOUT: {
                value: string;
            };
            CAUSE_COMPLETE: {
                value: string;
            };
            CAUSE_HINT: {
                value: string;
            };
            CAUSE_TIMEOUT: {
                value: string;
            };
        };
        /** @deprecated see Effect */
        static TYPE_VISIBLE: string;
        /** @deprecated see Effect */
        static TYPE_BACK: string;
        /** @deprecated see Effect */
        static TYPE_RUNAWAY: string;
        /** @deprecated see Effect */
        static TYPE_PLAYSOUND: string;
        /** @deprecated see Effect */
        static TYPE_LOCK: string;
        /** @deprecated see Effect */
        static TYPE_ADDPOINTS: string;
        /** @deprecated see Effect */
        static TYPE_DESTROY: string;
        /** @deprecated see Effect */
        static TYPE_PAUSE: string;
        /** @deprecated see Effect */
        static TYPE_SOLVE: string;
        /** @deprecated see Effect */
        static TYPE_VALIDATE: string;
        static Effect: {
            TYPE_VISIBLE: {
                value: string;
            };
            TYPE_BACK: {
                value: string;
            };
            TYPE_RUNAWAY: {
                value: string;
            };
            TYPE_PLAYSOUND: {
                value: string;
            };
            TYPE_LOCK: {
                value: string;
            };
            TYPE_ADDPOINTS: {
                value: string;
            };
            TYPE_DESTROY: {
                value: string;
            };
            TYPE_PAUSE: {
                value: string;
            };
            TYPE_SOLVE: {
                value: string;
            };
            TYPE_VALIDATE: {
                value: string;
            };
        };
        static ALL_SPRITES: string;
        static THIS_SPRITE: string;
        constructor(cause: any, evtType: any, sprite: any, value: any);
        trigger(): void;
    }
    class LandSprite extends Phaser.Sprite {
        id: string;
        _x: number;
        _y: number;
        _realx: number;
        _realy: number;
        _children: LandSprite[];
        itemEvents: LandEvent[];
        busy: boolean;
        iNum: number;
        idName: string;
        isClonated: boolean;
        tics: number;
        moveX: number;
        moveY: number;
        throwMove: Phaser.Point;
        cycle: boolean;
        gotten: boolean;
        runningAway: boolean;
        ydifmin: number;
        ydifmax: number;
        gravity: Phaser.Point;
        basedepth: number;
        tolerance: number;
        floored: boolean;
        isLandscape: boolean;
        gapchecked: boolean;
        depthIsSet: boolean;
        landscapeMounted: boolean;
        clontics: number;
        clonticscopy: number;
        ticsTmp: number;
        paused: boolean;
        craftEnabled: boolean;
        pieceContainer: LandSprite;
        bullets: Object[];
        constructor(game: any, x: any, y: any, key: any);
        embedChild(child: LandSprite): void;
    }
    class Asset {
        url: string;
        key: string;
        frameWidth: number;
        frameHeight: number;
        frameMax: number;
        constructor(url: string, key: string, frameWidth?: number, frameHeight?: number, frameMax?: number);
        toString(): string;
    }
    class LandLayer extends Phaser.Group {
        fromLayerNumber: number;
        toLayerNumber: number;
        constructor(landgame: LandGame, from: number, to: number, name: any, addToStage?: any, enableBody?: any, physicsBodyType?: any);
        toJsonLayers(): {
            "@from": number;
            "@to": number;
        };
    }
    class LandGrid extends Phaser.Group {
        gridLength: number;
        constructor(landgame: LandGame, length: number, name: any, addToStage?: any, enableBody?: any, physicsBodyType?: any);
    }
}
declare module LandPlay {
    class Tool extends LandGame {
        toolId: string;
        toolCase: string;
        setupTool(): void;
        private setupTool00(node);
        protected setupTool01(node: any): void;
        triggerEvents(cause: any, sprite?: string): void;
        release(key: string): void;
        update(): void;
        getTool(): LandSprite;
    }
}
declare module LandPlay {
    class Catch extends Tool {
        /**
         * Nombre de la instancia de Movie del arma
         * (alias de tool)
         */
        /**
         * Array unidimensional de dos elementos
         * Posiciones x, y donde finaliza el recorrido de las piezas
         * necesario para delimitar su recorrido
         * Delimita el vertice inferior derecho
         */
        private endStage;
        /**
         * Array unidimensional de dos elementos
         * Posiciones x, y donde empieza el recorrido de las piezas
         * necesario para delimitar su recorrido
         * Delimita el vertice superior izquierdo
         */
        private beginStage;
        /**
         * Multiplicador de velocidad a la que se mueven las piezas
         *
         */
        private speed;
        /**
         * Velocidad por la que se multiplica la velocidad del moviemiento
         * de las piezas cuando huyen.
         */
        private runawaySpeed;
        /**
         * Array unidimensional con nombres de instancias de movies
         * Piezas que se pueden cazar
         */
        pieces: Array<Piece>;
        /**
         * Array unidimensional con nombres de instancias de movies
         * Piezas que han sido capturadas
         */
        caughtPieces: any;
        /**
         * Nombre de la inst. de movie que representa el bote donde se guarda el pincel
         * si éste es pickable
         * (alias de toolBag)
         */
        weaponBag: string;
        /**
         * Indica si el weapon es WYSIWYG, si no el mouse se convierte en el weapon
         */
        /**
         * Indica si tenemos agarrado el weapon
         */
        /**
         * Strings que representan los textos de los objetivos que se alternaran
         * @TODO: Madurar para mas de un objetivo
         */
        private targets;
        private targetSounds;
        private targetValues;
        private tagTargetMovie;
        constructor(xmlFile: string, params?: object);
        onConfigLoad(): void;
        update(): void;
        onStageLoad(): void;
        getWeapon(): LandSprite;
        triggerEvents(cause: any, sprite?: string): void;
        runaway(sprite: string, all: boolean): void;
        collectValidable(): string[][];
        freeze(sprite: string, all: boolean): void;
    }
    class CatchEvents extends LandEvent {
        static TYPE_RUNAWAY: string;
        static TYPE_FREEZE: string;
        static TYPE_LOAD: string;
    }
    class Piece extends LandSprite {
        movex: number;
        movey: number;
        outbound: string;
        static CYCLE: string;
        static REBOUND: string;
        static DIE: string;
        runaway(runawaySpeed: number): void;
        toString(): string;
        embedChild(piece: Piece): void;
    }
}
declare module LandPlay {
    class Paint extends Tool {
        /**
         * Array unidimensional que representa los nombres de instancia
         * de las movies de los botes de pintura
         */
        pots: Array<Pot>;
        /**
         * Array unidimensional que representa los nombres de instancia
         * de las movies de los lienzos
         */
        canvases: Array<Canvas>;
        /**
         * Representa el nombre de instancia de la movie del pincel
         * (alias de tool)
         */
        brush: string;
        /**
         * Representa el nombre de instancia de la movie de la punta del pincel
         * (alias de toolPoint)
         */
        brushPoint: string;
        /**
         * Nombre de la inst. de movie que representa el bote donde se guarda el pincel
         * si éste es pickable
         */
        pot: string;
        constructor(xmlFile: string, params?: object);
        onConfigLoad(): void;
        onStageLoad(): void;
        setupTool01(node: any): void;
        getPot(id: string): Pot;
        getCanvas(id: string): Canvas;
        update(): void;
        triggerEvents(cause: any): void;
        validatePaint(canvas: Canvas): boolean;
        collectValidable(): string[][];
    }
    class Paintable extends LandSprite {
        color: string;
    }
    class Canvas extends Paintable {
    }
    class Pot extends Paintable {
        constructor(x: any, y: any, key: any);
    }
    class PaintEvents extends LandEvent {
        static TYPE_TINT: string;
    }
}
declare module LandPlay {
    /**
     * @class DragDrop
     *
     * Realiza los ejercicios en los que, para su resolución, es necesario arrastrar elementos
     *
     *
     * @version 1.0, 18-04-2005
     *		- Permite autoevaluar ejercicios de una sola respuesta de forma automatica
     *		- Varios recipientes
     * @TODO: Evaluar de forma asincrona las respuestas
     *
     * @author Juane Puig <juanepm@tinet.org>
     *
     */
    class DragDrop extends LandGame {
        /**
         * Indica que se esta usando tipos de ejercicios que extienden de él (p.e. Puzzle)
         */
        protected subclass: String;
        /**
         * Array bidimensional (x, y) de los recipientes definidos
         * indica sus cordenadas para centrar la respuesta dentro del recipiente
         */
        recippos: Array<Phaser.Point>;
        /**
         * Array bidimensional (x, y) de ls recipientes definidos
         * indica el desplaçamiento que debe haber entre dos elementos situados en el recipiente
         */
        recipoffset: Array<Phaser.Point>;
        eventsAsGeneric: Boolean;
        /**
         * if is not null pieces must be dropped here
         */
        protected table: LandSprite;
        protected cntAnswers: number;
        protected cntContainers: number;
        /**
         * Constructor
         */
        constructor(xmlFile: string, params?: object);
        /**
         * Aplicamos eventos drag y drop
         */
        protected extendMovieClip(): void;
        /**
         * Registra el recipiente y sus respuestas validas
         * al tiempo que genera un nuevo espacio para permitir responder a esa pregunta
         */
        private addRecip(name, respostes);
        /**
         * Registra la opcion, que puede ser:
         *		- un recipiente o pregunta (registra tambien sus respuestas)
         *		- una pieza de un puzzle ya posicionado
         *		- un tablero de un puzzle sin posicionar (las piezas son los nodos hijos, los registra)
         */
        registerOption(option: any): void;
        onStageLoad(): void;
        hashRecipientsPositions(): void;
        /**
         * No importa la distribucion que se haya hecho hasta ahora
         * se hara en funcion de los grids habilitados
         */
        allocateGrids(): void;
        update(): void;
        /**
         * Inicializa los clips que pueden ser movidos
         *		- Guarda su posicion inicial
         *		- Les aplica las funciones Drag y Drop
         */
        initClip(name: any): void;
        /**
         * Intercambia las posiciones de las dos movies
         * @param m1, m2:nombres de las pelis en formato "movie.innermovie"
         * m1 recuperara la posicio inicial
         */
        swapPositions(m1: string, m2: string): void;
        /**
         * Registrar respuesta posible
         */
        private addAnswer(name);
        /**
         * checks if all answers are right
         */
        validateDragDrop(): boolean;
        validatePuzzle(): void;
    }
}
declare module LandPlay {
    /**
     * @class Puzzle
     * @see DragDrop, LandPlay
     * To resolve this exercise the user must put the pieces in the correct order and position
     * (a puzzle... much easier to play than to explain)
     *
     *
     * @author Juane Puig <juanepm@tinet.org>
     *
     */
    class Puzzle extends DragDrop {
        constructor(xmlFile: string, params?: object);
        /**
         * init "key" sprite to be draggable
         *
         * @precondition Se asume que, inicialmente, cada respuesta en su posición correcta
         * 	de ahí que  por defecto siempre tengamos el randFlag a true en el constructor
         *  para garantizar que se hace la mezcla
         *
         * overrides DragDrop's methode
         */
        initClip(nom: any): void;
        /**
         * Realiza la verificación de todas las posiciones del puzzle, ejecutando los
         * Eventos de acierto cargados, en caso que sea correcto.
         * Si la vlidacion es sincrona sera llamado desde el prototipo "arrossega", definido en
         * DragDrop
         */
        validatePuzzle(): void;
        collectValidable(): string[][];
        onStageLoad(): void;
    }
}
declare module LandPlay {
    /**
     * @class Simon
     * @see LandPlay
     *
     * @version 1.0, 13-11-2005
     *
     * @author Juane Puig <juanepm@tinet.org>
     *
     */
    class Simon extends LandGame {
        /**
         * milisegundos entre mostrar un elemento de la secuencia i el siguiente
         *
         */
        private transition;
        /**
         * unidimensional array with sprite names
         * Pads that can take part of the sequence
         * Alias for responses
         */
        pads: Array<string>;
        padsounds: Array<string>;
        /**
         * unidimensional array with the hashed sequence of the pad names
         */
        sequence: Array<string>;
        /**
         * index of the sequence
         */
        private indSeq;
        /**
         * interval: not used
         */
        private _itv;
        /**
         * Farest point of the sequence
         * it increases as long a game successes
         */
        private farest;
        /**
         * sequence is auto playing
         */
        private playing;
        private talking;
        /**
         * sequence length
         */
        private maxseq;
        /**
         * Clip with frames "stop" and "play"
         * shows that Simon is talking
         */
        private mcTalk;
        /**
         * Constructor
         */
        constructor(xmlFile: string, params?: object);
        /**
         * former MovieClip prototype extention, not used
         */
        private extendMovieClip();
        /**
         * xml nodes parsing for simon game
         *		- setup
         *		- pads
         *		- ...
         */
        registerOption(option: any): void;
        private initTalk();
        getProgressPerc(): number;
        initClip(nom: any): void;
        private setPadsBehaviour();
        /**
         * Restart items
         */
        reset(): void;
        private hashPieces();
        start(): void;
        onStageLoad(): void;
        update(): void;
        private next();
        playSequence(): void;
        private doPlaySequence();
        private clearSequence();
        private addResponse(button);
    }
}
declare module LandPlay {
    /**
     * @class Shot
     *
     * @version 1.0, 23/11/2016
     * @see https://www.phaser.io/tutorials/coding-tips-007
     * @author Juane Puig <juanepm@tinet.org>
     *
     */
    class Bullet extends Phaser.Sprite {
        id: string;
        tracking: Boolean;
        scaleSpeed: number;
        constructor(game: any, key: any);
        fire(x: any, y: any, angle: any, speed: any, gx: any, gy: any): void;
        update(): void;
    }
    class BulletsPack extends Phaser.Group {
        nextFire: number;
        bulletSpeed: number;
        fireRate: number;
        padding: number;
        conf: BulletConfig;
        constructor(game: any, name: any, conf: BulletConfig, padding?: number);
        load(top?: number): void;
        isInfinite(): boolean;
        fire(source: any): void;
    }
    /**
     * For two or more shoot types
     */
    class BulletCombo extends CoreObject {
        key: number;
        text: string;
        static KEYS: {
            'spacebar': number;
            'control': number;
        };
        bulletsPacks: BulletsPack[];
        constructor(key: number);
        add(pack: BulletsPack): void;
        fire(source: any): void;
        reset(): void;
        isInfinite(): boolean;
        getArmor(): number;
    }
    class BulletConfig extends CoreObject {
        key: string;
        rows: BulletConfigRow[];
        remains: number;
        constructor(key: string);
        addRow(x: number, y: number, angle: number, speed: number, gx: number, gy: number): void;
    }
    class BulletConfigRow extends CoreObject {
        x: number;
        y: number;
        angle: number;
        speed: number;
        gx: number;
        gy: number;
        constructor(x: number, y: number, angle: number, speed: number, gx: number, gy: number);
    }
}
declare module LandPlay {
    /**
     * @class Arcade
     * @see LandPlay
     *
     * @version 1.0, 20/05/2016
     *
     * @author Juane Puig <juanepm@tinet.org>
     *
     */
    class Arcade extends LandGame {
        /**
         * sprite id for the craft (player)
         */
        craft: string;
        /**
         * the craft sprite itself
         */
        mcCraft: LandSprite;
        /**
         * sprite id of the edges
         * Thought for a Labyrinth, not used yet
         */
        edges: String;
        /**
         * Pixels of tolerance to soften edges limits
         *
         */
        tolerance: Number;
        /**
         * Two points (x, y)
         * where craft and pieces end its way
         * right-botton pixel
         */
        private endStage;
        /**
         * Two points (x, y)
         * where craft and pieces end its way
         * left-top pixel
         */
        private beginStage;
        /**
         * relative speed for pieces+landscape+craft
         */
        private speed;
        /**
         * relative speed for craft
         */
        private craftSpeed;
        /**
         * speed the pieces+craft go when they runaway
         */
        private runawaySpeed;
        /**
         * pieces in motion
         */
        pieces: Array<Piece>;
        /**
         * groups of clonated pieces
         */
        piecesGroups: Array<Phaser.Group>;
        /**
         * landscape sprites group
         */
        landscapeGroup: Phaser.Group;
        /**
         * landscape foreground sprites group
         */
        landscapeForeGroup: Phaser.Group;
        /**
         * edges grouped to simplify collisions
         */
        edgesGroup: Phaser.Group;
        edgesLayer: Phaser.TilemapLayer;
        /**
         * ids of the pieces that have been captured
         */
        caughtPieces: Array<string>;
        /**
         * standby craft animation
         */
        moveFrame: string;
        /**
         * stop craft animation
         */
        stopFrame: string;
        /**
         * right cursor key animation
         */
        forwardFrame: string;
        /**
         * left cursor key animation
         */
        backFrame: string;
        /**
         * up cursor key animation
         */
        upFrame: string;
        /**
         * down cursor key animation
         */
        downFrame: string;
        /**
         * spacebar key animation
         */
        jumpFrame: string;
        jumpTouch: boolean;
        yJump: number;
        yScale: number;
        backpieces: Array<string>;
        /**
         * remaning stage time
         * 0: Undefined
         */
        time: number;
        private second;
        /**
         * floor has 2 dimensions,
         * up/down key cursor move craft up/down (y-axis) the stage
         */
        private floor2d;
        /**
         * Not used, better use pieces
         */
        private targets;
        private targetSounds;
        private targetValues;
        private tagTargetMovie;
        /**
         * key cursors (left/right/up/down)
         */
        private cursors;
        /**
         * Constructor
         */
        constructor(xmlFile: string, params?: object);
        onStageLoad(): void;
        shootPiece(bullet: Bullet, piece: LandSprite): void;
        touchPiece(craft: LandSprite, piece: LandSprite): void;
        update(): void;
        render(): void;
        private castBullets(piece);
        debugPiece(piece: LandSprite): void;
        private suitToMaze();
        /**
         * craft goes to animation, if it exists
         */
        private moveCraft(frame);
        /**
         * go to animation, if exits
         */
        private animate(sprite, frame, loop?);
        /**
         * kill the piece once is out the world
         */
        private recyclePiece(piece);
        /**
         * Aplicamos prototipos a la MovieClip
         */
        private extendMovieClip();
        private addRecip(name, respostes);
        /**
         *	more xml items
         *		- craft
         *		- piece
         * 		- edge
         * 		- setup
         */
        registerOption(opt: any): void;
        setBullets(opt: any, sprite: LandSprite): void;
        spriteToTilesetJson(shape: LandSprite, maxFrame: number, opt: any): Object;
        initClip(nom: any): void;
        private nestedLaunchPiece(opt, parent?);
        private buildPiece(mc, opt, parent?);
        private launchPiece(mc, parent?);
        /**
         * Record answer, not used
         */
        private addAnswer(name);
        /**
         * no effect, by now
         */
        reset(): void;
        /**
         * Evident, is not it? But not used
         */
        private setShot(opt);
        /**
         * Not used
         */
        private caught();
        triggerEvents(cause: any, pieceId?: any): void;
        runaway(sprite: string, all: boolean): void;
        /**
         * not used
         */
        private evaluate(pieceName, cleared);
        /**
         * not used
         */
        private setTargets(nodes);
        /**
         * not used
         */
        private loadTarget(t);
        /**
         * not used
         */
        private doNextTarget();
        /**
         * not used see runaway
         */
        private doRunaway(eve);
        getCaughtPieces(): string[];
        private hashPieces();
        private clonatePieces(opt, parent?);
        /**
         * not used, better use this.game.paused=true
         */
        pauseAll(): void;
        resumeAll(): void;
        disableKeyboard(): void;
        disableXMove(): void;
        clearAll(onStage: any): void;
        /**
         * not used
         */
        movePiece(piece: Piece): void;
        clearBackgrounds(): void;
    }
}
declare module LandPlay {
    class LandField {
        tabindex: number;
        /**
        Hidden value to validate
         */
        protected _value: any;
        constructor();
    }
    class TextField extends LandField {
        maxlength: number;
        size: number;
        txtcolor: number;
        cursor: string;
        static CURSOR_VERTICAL: string;
        static CURSOR_HORIZONTAL: string;
        private _focus;
        private _cursorOn;
        private _itv;
        text: Phaser.Text;
        private _sprite;
        private _lock;
        constructor(sprite: Phaser.Sprite, value?: string, offsetx?: number, offsety?: number, hide?: boolean);
        validate(): boolean;
        render(): void;
        focus(): void;
        endFocus(): void;
        private realText();
        getSize(): number;
        isFull(): boolean;
        isEmpty(): boolean;
        empty(): void;
        getSprite(): Phaser.Sprite;
        addChar(char: string): void;
        solve(): void;
        deleteChar(): void;
        private clear();
        private hasCursor();
        private toggleCursor(tObj);
    }
}
declare module LandPlay {
    class Type extends LandGame {
        private textFields;
        private tabIndex;
        constructor(xmlFile: string, params?: object);
        onStageLoad(): void;
        private addTextField(text, key, refx, refy, offsetx, offsety, style, sprite);
        registerOption(opt: any): void;
        private tabulate(sense?);
        private focused();
        checkValidate(): void;
        hint(): void;
        solve(): void;
        collectValidable(): string[][];
        update(): void;
        render(): void;
    }
}
declare var dummy: string;
/**
 * @deprecated to LandPlay edition context
 * */
declare module LandPlay {
}
/**
 * @deprecated to LandPlay edition context
 * */
declare module LandPlay {
}
declare module LandPlay {
    /**
     * Thanks: joshmorony for https://www.joshmorony.com/part-1-building-a-word-search-game-in-html5-with-phaser/
     */
    class WordSearch extends LandGame {
        static DEFAULT_WIDTH: number;
        static DEFAULT_HEIGHT: number;
        static DEFAULT_COLS: number;
        static DEFAULT_ROWS: number;
        /**
         * Declare assets that will be used as tiles
         * */
        private tileLetters;
        private case;
        /**
         * What colours will be used for our tiles?
         */
        private tileColors;
        private currentColor;
        private rightColors;
        /**
         * Set the width and height for the tiles
         */
        private tileWidth;
        private tileHeight;
        private gridCols;
        private gridRows;
        private tiles;
        private tileGrid;
        /**
         * Keep a reference to the total grid width and height
         */
        private boardWidth;
        private boardHeight;
        /**
         * We want to keep a buffer on the left and top so that the grid can be centered
         */
        private leftBuffer;
        private topBuffer;
        private guessing;
        private currentWord;
        private currentWordEdges;
        private correctWords;
        /**
         * A buffer for how much of the tile activates a select
         */
        private selectBuffer;
        /**
         * words to spread in the grid
         */
        private words;
        /**
         * 2d letter offset for each word with values -1, 0 or 1
         * forbidden combination {x: 0, y: 0}
         */
        private wordOrientations;
        private wordPositions;
        private rightLetters;
        private rightWords;
        private wordSprites;
        constructor(xmlFile: string, params?: object);
        onStageLoad(): void;
        registerOption(opt: any): void;
        private disposeWords();
        private tryPosition(i);
        private write(word, x, y);
        private getTilesWord(i);
        /**
         * //addapt grid dimensions to the longest word
        */
        private addaptGrid();
        private initTiles();
        private addTile(x, y);
        private createTile(letter, color);
        checkValidate(): void;
        hint(): void;
        solve(): void;
        collectValidable(): string[][];
        getDictionary(): string;
        update(): void;
        private markRightWords();
        private markSelection();
        render(): void;
    }
}
