/// <reference path='phaser.d.ts' />
/// <reference path='jquery.d.ts' />
/// <reference path='landplay.d.ts' />
/// <reference path='edit/EditorMode.ts' />
/// <reference path='edit/LandPlayEditor.ts' />
/// <reference path='plugin/Plugin.ts' />
/// <reference path='plugin/Factory.ts' />
/// <reference path='../js/translations.d.ts' />
/// <reference path='Literals.ts' />

module LandPlayEdit {
    /**
     * Sprite that its beeing edited right now (one at a time)
     */
    export var sprite: Phaser.Sprite = null;

    export var storageKey = "LandPlayConfig";

    /** 
     * Space to temporary save information about the game config 
     * */
    export var editor: LandPlay.Editor = new LandPlay.Editor( {} );

    export var plugin: LandPlayEdit.Plugin;

    export var buttonActionKeys = []

    var messageIndex = 0;
    var messageItvs = [];

    const ShapeTypes = {
        Rectangle: "square",
        Circle: "circle"
    }

    /** 
     * Not all the effects are programatically expected to be expanded amont all the sprites 
     * Just the ones below
     * */
    var allSpritesEffects = [
        LandPlay.LandEvent.Effect.TYPE_LOCK.value,
        LandPlay.LandEvent.Effect.TYPE_BACK.value,
        LandPlay.LandEvent.Effect.TYPE_RUNAWAY.value
    ];

    export var textSelects = [
        "counter.points.@text",
        "counter.life.@text",
        "counter.time.@text"
    ];

    export var validationSelects = [
        "validation.right.@key",
        "validation.wrong.@key"
    ];

    export function Start() {

        i18next.init( {
            lng: 'es', // evtl. use language-detector https://github.com/i18next/i18next-browser-languageDetector
            resources: { // evtl. load via xhr https://github.com/i18next/i18next-xhr-backend
                en: {
                    translation: LandPlayEdit.i18n.en_US
                },
                es: {
                    translation: LandPlayEdit.i18n.es_ES
                },
                ca: {
                    translation: LandPlayEdit.i18n.ca_ES
                }
            }
        }, function( err, t ) {
            LandPlayEdit.localize = locI18next.init( i18next );
            LandPlayEdit.localize( 'body' );
        } );

        var option = $( "<option>" );
        $( "select[name='config.setup.@mode']" ).append( option );
        for ( var mode of Object.keys( LandPlay.Mode ) ) {
            option = $( "<option>" );
            $( option ).attr( "value", mode );
            $( option ).html( i18next.t( "option.mode." + mode + ".label" ) );
            $( "select[name='config.setup.@mode']" ).append( option );
        }

        var currentWork = localStorage.getItem( LandPlayEdit.storageKey );
        if ( currentWork != undefined ) {
            LandPlayEdit.editor.fromJSON( JSON.parse( currentWork ) );
            /**
             * Load other values from saved json
             */
            for ( var key of Object.keys( LandPlayEdit.editor.json.config.setup ) ) {
                var selector = "[name='config.setup." + key + "']";
                var value = LandPlayEdit.editor.json.config.setup[key];
                $( selector ).val( value );
                if ( key == "@mode" && value != "" ) {
                    $( selector ).attr( "disabled", "disabled" );
                }
            }
            for ( var key of Object.keys( LandPlayEdit.editor.json.config.layout ) ) {
                $( "[name='config.layout." + key + "'" ).val( LandPlayEdit.editor.json.config.layout[key] );
            }
            /**
             * Load assets on assets' tab 
             */
            var assets = LandPlayEdit.editor.getAssets();
            for ( var subtype of Object.keys( assets ) ) {
                for ( var i = 0; i < assets[subtype].length; i++ ) {
                    var asset = assets[subtype][i];
                    if ( LandPlayEdit.reservedSprites.indexOf( asset['@key'] ) < 0 ) {
                        LandPlayEdit.addAssetRow( asset, subtype );
                    }
                }
            }

        }

        LandPlayEdit.initPlugin();

        LandPlayEdit.reInitFoundation();

    }

    export function localize() { };

    export function reInitFoundation() {
        Foundation.Abide.defaults.patterns['identifier'] = /^[a-zA-Z]+[a-zA-Z0-9]+$/;
        Foundation.Abide.defaults.validators['not_empty_select'] = LandPlayEdit.notEmptySelectValidator;

        $( document ).foundation();

        $("#fileSelector").on("down.zf.accordion", function( ev, frm ) {
            if ( $("#fileUpload.is-active").length == 1 ) {
                $("[name=url]").val("");
                LandPlayEdit.toggleField("file[]", true);
                LandPlayEdit.toggleField("url", false);
            } else {
                $("[name='file[]']").val("");
                LandPlayEdit.toggleField("file[]", false);
                LandPlayEdit.toggleField("url", true);
            }
        });
        
        $( "#spriteEditor form, #textForm, #buttonForm, #retryForm, #counterForm, #eventForm, #gridForm, #shapeForm, #validationForm" ).submit( function( event ) {
            event.preventDefault();
        } );
        $( '#side_panels' ).on( "closed.zf.offCanvas", function( ev, frm ) {
            $( "#floating-toggler" ).animate( {
                left: "-30px"
            }, 800 );
        } );
        $( '#side_panels' ).on( "opened.zf.offCanvas", function( ev, frm ) {
            $( "#floating-toggler" ).animate( {
                left: "-300px"
            }, 800 );
        } );
        $( '#spriteEditor form' ).on( "formvalid.zf.abide", function( ev, frm ) {
            LandPlayEdit.updateForms();
        } );
        $( '#textEditor form' ).on( "formvalid.zf.abide", function( ev, frm ) {
            LandPlayEdit.updateText();
        } );
        $( '#buttonEditor form' ).on( "formvalid.zf.abide", function( ev, frm ) {
            LandPlayEdit.updateButton();
        } );
        $( '#retryEditor form' ).on( "formvalid.zf.abide", function( ev, frm ) {
            LandPlayEdit.updateRetry();
        } );
        $( '#counterEditor form' ).on( "formvalid.zf.abide", function( ev, frm ) {
            LandPlayEdit.updateCounter();
        } );
        $( '#eventEditor form' ).on( "formvalid.zf.abide", function( ev, frm ) {
            LandPlayEdit.updateEvent();
        } );
        $( '#gridEditor form' ).on( "formvalid.zf.abide", function( ev, frm ) {
            LandPlayEdit.updateGrid();
        } );
        $( '#shapeEditor form' ).on( "formvalid.zf.abide", function( ev, frm ) {
            LandPlayEdit.updateShape();
        } );
        $( '#validationEditor form' ).on( "formvalid.zf.abide", function( ev, frm ) {
            LandPlayEdit.updateValidation();
        } );

        $( '[data-asset-type]' ).unbind( "click" );
        $( '[data-asset-type]' ).click( function( event ) {
            event.preventDefault();
            var content = "<img src='" + $( this ).attr( "href" ) + "'>";
            if ( $( this ).attr( "data-asset-type" ) == "layers" ) {
                $( "#framesOrbit .orbit-container" ).html( "" );
                $( "#framesOrbit .orbit-bullets" ).html( "" );
                for ( var i = $( this ).attr( "data-from-layer" ); i < $( this ).attr( "data-to-layer" ); i++ ) {
                    var orbitCont = $( "#tpl_orbit_slide > li:first" ).clone( true );
                    var img = $( "<img>" );
                    $( img ).attr( "src", $( this ).attr( "href" ).replace( "(?)", i ) );
                    $( '.orbit-frame', orbitCont ).append( img );
                    $( "#framesOrbit .orbit-container" ).append( orbitCont );

                    var orbitBullet = $( "#tpl_orbit_bullet > button:first" ).clone( true );
                    $( 'button', orbitBullet ).attr( "data-slide", i );
                    $( "#framesOrbit .orbit-bullets" ).append( orbitBullet );

                }
                $( "#frameEditor" ).foundation( 'open' );
                var elem = new Foundation.Orbit( $( '#framesOrbit' ), {
                    autoPlay: false
                } );
            } else {
                if ( $( this ).attr( "data-asset-type" ) == "sound" ) {
                    content = " <audio controls autoplay><source src='" + $( this ).attr( "href" ) + "'></audio> "
                }
                $( "#assetViewer .content" ).html( content );
                $( "#assetViewer" ).foundation( 'open' );
            }
        } );

        $( "a[data-toggleable]" ).unbind( "click" );
        $( "a[data-toggleable]" ).click( function( event ) {
            event.preventDefault();
            var on = $( this ).hasClass( "ui-state-default" );
            switch ( $( this ).attr( "data-toggleable" ) ) {
                case "locate":
                    LandPlayEdit.lgEditor.locateAllSprites( on );
                    break;
            }
            if ( !on ) {
                $( this ).removeClass( "ui-state-focus" );
                $( this ).addClass( "ui-state-default" );
            } else {
                $( this ).addClass( "ui-state-focus" );
                $( this ).removeClass( "ui-state-default" );
            }
        } );

        $( "[data-behaviour]" ).unbind( "click" );
        $( "[data-behaviour]" ).click( function( event ) {
            event.preventDefault();
            LandPlayEdit.behaviour( $( this ).attr( "data-behaviour" ) );
        } );

    }

    /** 
    * Interactive phaser game/canvas to graphically deal with sprites 
    * */
    export var lgEditor: LandPlay.EditorMode;

    /** 
     * fields that match between sprite form and sprite object 
     * */
    export var spriteFields = [
        "id", "key", "width", "height", "x", "y", "angle", "tint", "margin-right", "margin-bottom", "visible", "frame"
    ];
    /**
     * inverse from textFields, loaded at Start
     */
    export var textAttributes = {};

    export var textStyleAttributes = null;

    export var reservedSprites = ["preloadBar"];

    export function initPlugin() {
        if ( $( 'select[name="config.setup.@mode"]' ).val() != "" ) {
            LandPlayEdit.plugin = LandPlayEdit.PluginFactory.newInstance( $( 'select[name="config.setup.@mode"]' ).val() );

        }
    }

    export function editSprite() {
        var sprite = LandPlayEdit.spriteToJson( LandPlayEdit.sprite );
        if ( sprite != undefined ) {
            for ( var i = 0; i < LandPlayEdit.spriteFields.length; i++ ) {
                var attribute = LandPlayEdit.spriteFields[i];
                var field = "";
                field = "@" + attribute;
                var value = sprite[field];
                if ( attribute == "margin-bottom" ) {
                    var checked = attribute;
                    if ( isNaN( sprite[field] ) ) {
                        checked = 'y';
                    }
                    $( "input[name='transient.sprite.ypos'][value='" + checked + "']" ).prop( "checked", true );
                    LandPlayEdit.changeSpritePosition( "transient.sprite.ypos", checked );
                } else if ( attribute == "margin-right" ) {
                    var checked = attribute;
                    if ( isNaN( sprite[field] ) ) {
                        checked = 'x';
                    }
                    $( "input[name='transient.sprite.xpos'][value='" + checked + "']" ).prop( "checked", true );;
                    LandPlayEdit.changeSpritePosition( "transient.sprite.xpos", checked );
                }
                $( "[name='sprite." + field + "']" ).val( value );

            }
            var maxFrame = 0;
            var asset = LandPlayEdit.editor.getAsset( sprite["@key"], LandPlay.AssetType.Image );
            if ( asset != null ) {
                maxFrame = parseInt( asset["@frameMax"] );
                if ( !isNaN( maxFrame ) ) {
                    maxFrame--;
                }
            }
            var elem = new Foundation.Slider( $( "#sld_sprite_frame" ), {
                "end": maxFrame,
                "initial-start": sprite["@frame"]
            } );
            $( "[name='sprite.@frame']" ).val( sprite["@frame"] );
            $( "[name='sprite.@frame']" ).trigger( "change" );

            if ( sprite.layers != null ) {
                $( "[name='sprite.layers.@from']" ).removeAttr( "disabled" );
                $( "[name='sprite.layers.@from']" ).val( sprite.layers["@from"] );
                $( "[name='sprite.layers.@to']" ).removeAttr( "disabled" );
                $( "[name='sprite.layers.@to']" ).val( sprite.layers["@to"] );

                var re = /(\w+)_(\d+)$/
                var key = $( "[name='sprite.@key']" ).val().replace( re, '$1_' );
                $( "[name='sprite.@key']" ).val( key );

            } else {
                $( "[name='sprite.layers.@from']" ).attr( "disabled", "disabled" );
                $( "[name='sprite.layers.@from']" ).val( "" );
                $( "[name='sprite.layers.@to']" ).attr( "disabled", "disabled" );
                $( "[name='sprite.layers.@to']" ).val( "" );
            }

        } else {
            console.error( "No sprite JSON found for the sprite '" + LandPlayEdit.sprite.id + "'" )
        }
    }

    export function changeSpritePosition( name: string, value: string ) {
        var dim = "x";
        var ref = "right";
        if ( name.indexOf( "ypos" ) > -1 ) {
            dim = "y";
            ref = "bottom";
        }
        if ( value.indexOf( "margin-" ) == 0 ) {
            $( "[name='sprite.@" + dim + "']" ).removeAttr( "required" );
            $( "[name='sprite.@" + dim + "']" ).attr( "disabled", "disabled" );

            $( "[name='sprite.@margin-" + ref + "']" ).removeAttr( "disabled" );
            $( "[name='sprite.@margin-" + ref + "']" ).attr( "required", "required" );

        } else {
            $( "[name='sprite.@" + dim + "']" ).removeAttr( "disabled" );
            $( "[name='sprite.@" + dim + "']" ).attr( "required", "required" );

            $( "[name='sprite.@margin-" + ref + "']" ).removeAttr( "required" );
            $( "[name='sprite.@margin-" + ref + "']" ).attr( "disabled", "disabled" );
        }
    }

    export function sprite_unnamed( sprite: Phaser.Sprite ) {
        alert( "Sprite has no name defined. Please edit it and fill the 'name' field" );
    }

    export function sprite_click( sprite: Phaser.Sprite, info?: object ) {
        LandPlayEdit.sprite = sprite;
        if ( LandPlayEdit.lgEditor.action == LandPlay.EditAction.Text ) {
            $( '#text-list' ).html( "" );
            /* find embeded texts*/
            var texts = [];
            var textFound = false;
            var jsonSprite = LandPlayEdit.editor.getSprite( LandPlayEdit.sprite.id );
            if ( jsonSprite != null ) {
                if ( jsonSprite.text != null ) {
                    for ( var i = 0; i < jsonSprite.text.length; i++ ) {
                        if ( !textFound ) {
                            textFound = true;
                        }
                        LandPlayEdit.listRecord( "#text-list", i, jsonSprite.text[i]["@label"] );
                    }
                    if ( !textFound ) {
                        var textDiv = $( '#templates > #tpl_text_no_item > div:first-child' ).clone( true );
                        $( "#text-list" ).append( textDiv );
                    } else {
                        LandPlayEdit.activateList( "text" );
                    }
                }

                $( '#textForm' ).foundation( 'resetForm' );
                $( '#textForm' ).hide();
                $( '#textEditor' ).foundation( 'open' );

            } else {
                console.error( "Sprite JSON object not found" );
            }

        } else if ( LandPlayEdit.lgEditor.action == LandPlay.EditAction.Shape ) {
            $( '#shape-list' ).html( "" );
            var jsonSprite = LandPlayEdit.editor.getSprite( LandPlayEdit.sprite.id );
            var shapeFound = false;
            if ( jsonSprite != null && jsonSprite.shape != null ) {
                for ( var i = 0; i < jsonSprite.shape.length; i++ ) {
                    if ( !shapeFound ) {
                        shapeFound = true;
                    }
                    LandPlayEdit.listRecord( "#shape-list", i, jsonSprite.shape[i]["@type"] );
                }
            }
            if ( !shapeFound ) {
                var textDiv = $( '#templates > #tpl_shape_no_item > div:first-child' ).clone( true );
                $( "#shape-list" ).append( textDiv );
            } else {
                LandPlayEdit.activateList( "shape" );
            }

            $( '#shapeForm' ).foundation( 'resetForm' );
            $( '#shapeForm' ).hide();
            $( '#shapeEditor' ).foundation( 'open' );
        } else if ( LandPlayEdit.lgEditor.action == LandPlay.EditAction.Button ) {
            LandPlayEdit.editButton();
        } else if ( LandPlayEdit.lgEditor.action == LandPlay.EditAction.Grid ) {
            LandPlayEdit.editGrid();
        } else if ( LandPlayEdit.lgEditor.action == LandPlay.EditAction.Anchor ) {
            var jsonSprite = LandPlayEdit.editor.getSprite( LandPlayEdit.sprite.id );
            if ( info != null && info.x != null && info.y != null ) {
                jsonSprite["@anchorx"] = info.x + "%";
                jsonSprite["@anchory"] = info.y + "%";
                if ( jsonSprite.sprite != null ) {
                    for ( var i = 0; i < jsonSprite.sprite.length; i++ ) {
                        var jsonChild = jsonSprite.sprite[i];
                        jsonChild["@anchorx"] = jsonSprite["@anchorx"];
                        jsonChild["@anchory"] = jsonSprite["@anchory"];
                        var child: Phaser.Sprite = LandPlayEdit.lgEditor.sprites[jsonChild["@id"]];
                        if ( child != null ) {
                            child.anchor.set( info.x / 100, info.y / 100 );
                        }
                    }
                }
                LandPlayEdit.sprite.anchor.set( info.x / 100, info.y / 100 );
                LandPlayEdit.saveData();
                LandPlayEdit.success( i18next.t( "success.anchor" ) );
            }
        } else {
            $( '#spriteEditor' ).foundation( 'open' );
            if ( LandPlayEdit.sprite.id == undefined || LandPlayEdit.sprite.id == "" ) {
                $( "input[name='sprite.@id']" ).removeAttr( "readonly" );
            } else {
                $( "input[name='sprite.@id']" ).attr( "readonly", "readonly" );
            }
            LandPlayEdit.editSprite();
        }
    }

    export function sprite_drop( sprite, info?: object ) {
        LandPlayEdit.sprite = sprite;
        if ( LandPlayEdit.sprite.id != undefined && LandPlayEdit.sprite.id != "" ) {
            if ( LandPlayEdit.lgEditor.action == LandPlay.EditAction.Embed ) {
                if ( info.container != null ) {
                    var jsonSprite = LandPlayEdit.editor.getSprite( LandPlayEdit.sprite.id );
                    var jsonContainer = LandPlayEdit.editor.getSprite( info.container.id );
                    if ( jsonSprite != null && jsonContainer != null ) {
                        LandPlayEdit.confirm( i18next.t( "reveal.sprite.embed", { child: LandPlayEdit.sprite.id, parent: info.container.id } ), function() {
                            LandPlayEdit.lgEditor.doEmbed();
                            if ( jsonContainer.sprite == null ) {
                                jsonContainer.sprite = [];
                            }
                            LandPlayEdit.sprite.position.set( 0, 0 );
                            jsonSprite["@x"] = 0;
                            jsonSprite["@y"] = 0;
                            jsonContainer.sprite.push( jsonSprite );
                            LandPlayEdit.editor.removeSprite( LandPlayEdit.sprite.id );
                            LandPlayEdit.saveData();
                        }, function() {
                            LandPlayEdit.sprite.position.set( jsonSprite["@x"], jsonSprite["@y"] );
                        } );
                    }
                }
            } else {
                LandPlayEdit.resetSprite( LandPlayEdit.sprite );
            }
        }
    }

    export function listRecord( destSelector: string, i: number, label: string ) {
        var textDiv = $( '#templates > #tpl_record_row > div:first-child' ).clone( true );
        $( 'div.record_label', textDiv ).html( label );
        $( 'a.edit', textDiv ).attr( "data-pos", i );
        $( 'a.delete', textDiv ).attr( "data-pos", i );
        $( destSelector ).append( textDiv );
    }

    export function listOrder( destSelector: string, i: number, label: string, top: boolean, bottom: boolean ) {
        var textDiv = $( '#templates > #tpl_order_row > div:first-child' ).clone( true );
        $( 'div.record_label', textDiv ).html( label );
        $( 'a.up', textDiv ).attr( "data-pos", i );
        if ( top ) {
            $( 'a.up', textDiv ).remove();
        }
        $( 'a.down', textDiv ).attr( "data-pos", i );
        if ( bottom ) {
            $( 'a.down', textDiv ).remove();
        }
        $( destSelector ).append( textDiv );
    }


    export function activateList( prefix: string ) {
        $( "#" + prefix + "-list a.edit" ).click( function( event ) {
            event.preventDefault();
            LandPlayEdit.editRecord( prefix, $( this ).attr( 'data-pos' ) );
        } );

        $( "#" + prefix + "-list a.delete" ).click( function( event ) {
            event.preventDefault();
            LandPlayEdit.deleteRecord( prefix, $( this ).attr( 'data-pos' ) );
        } );

        $( "#" + prefix + "-list a.up" ).click( function( event ) {
            event.preventDefault();
            LandPlayEdit.upRecord( prefix, $( this ).attr( 'data-pos' ) );
        } );

        $( "#" + prefix + "-list a.down" ).click( function( event ) {
            event.preventDefault();
            LandPlayEdit.downRecord( prefix, $( this ).attr( 'data-pos' ) );
        } );
    }

    export function editRecord( type: string, pos: number ) {
        switch ( type ) {
            case "text":
                LandPlayEdit.editText( pos );
                break;
            case "event":
                LandPlayEdit.editEvent( pos );
                break;
            case "shape":
                LandPlayEdit.editShape( pos );
                break;
            case "children":
                LandPlayEdit.editChildren( pos );
                break;
            default:
                if ( LandPlayEdit.plugin != null ) {
                    LandPlayEdit.plugin.editRecord( type, pos );
                } else {
                    console.error( "Please, define edit method for type '" + type + "'" );
                }

        }
    }

    export function deleteRecord( type: string, pos: number ) {
        switch ( type ) {
            case "text":
                LandPlayEdit.deleteText( pos );
                break;
            case "event":
                LandPlayEdit.deleteEvent( pos );
                break;
            case "shape":
                LandPlayEdit.deleteShape( pos );
                break;
            case "children":
                LandPlayEdit.deleteChildren( pos );
                break;
            default:
                if ( LandPlayEdit.plugin != null ) {
                    LandPlayEdit.plugin.deleteRecord( type, pos );
                } else {
                    console.error( "Please, define delete method for type '" + type + "'" );
                }
        }
    }

    export function upRecord( type: string, pos: number ) {
        switch ( type ) {
            case "depth":
                pos = parseInt( pos );
                if ( !isNaN( pos ) ) {
                    LandPlayEdit.changeDepth( pos, 1 );
                }
                break;
            default:
                if ( LandPlayEdit.plugin != null ) {
                    LandPlayEdit.plugin.upRecord( type, pos );
                } else {
                    console.error( "Please, define up method for type '" + type + "'" );
                }
        }
    }

    export function downRecord( type: string, pos: number ) {
        switch ( type ) {
            case "depth":
                pos = parseInt( pos );
                if ( !isNaN( pos ) ) {
                    LandPlayEdit.changeDepth( pos, -1 );
                }
                break;
            default:
                if ( LandPlayEdit.plugin != null ) {
                    LandPlayEdit.plugin.downRecord( type, pos );
                } else {
                    console.error( "Please, define up method for type '" + type + "'" );
                }
        }
    }

    export function changeDepth( pos: number, offset: number ) {
        var moved = LandPlayEdit.editor.json.config.layout.sprite.splice( pos, 1 );
        var next = LandPlayEdit.editor.json.config.layout.sprite.splice( pos + offset );
        LandPlayEdit.editor.json.config.layout.sprite = LandPlayEdit.editor.json.config.layout.sprite.concat( moved, next );
        LandPlayEdit.saveData();
        $( "#depthEditor" ).foundation( "close" );
        LandPlayEdit.openDepth();
    }

    export function saveJson() {
        var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent( JSON.stringify( LandPlayEdit.editor.json ) );
        var dlAnchorElem = document.getElementById( 'downloadAnchorElem' );
        dlAnchorElem.setAttribute( "href", dataStr );
        dlAnchorElem.setAttribute( "download", "config.json" );
        dlAnchorElem.click();
    }

    export function clearActivity() {
        LandPlayEdit.confirm( i18next.t( "reveal.generic.clear" ), function() {
            localStorage.removeItem( LandPlayEdit.storageKey );
            document.location = document.location;
        } );
    }

    export function resetSprite( sprite: Phaser.Sprite ) {
        var oSprite: object = LandPlayEdit.editor.getSprite( sprite.id );
        destroyGroups( sprite );
        var jsonSprite = LandPlayEdit.spriteToJson( sprite, oSprite );
        if ( jsonSprite.layers != null ) {
            jsonSprite["@key"] = jsonSprite["@key"].replace( /(.*)(\d)$/g, "$1" );
        }
        LandPlayEdit.renderSprite( jsonSprite );
    }

    function destroyGroups( sprite: any, deleting?: boolean ) {
        var spriteId = ( sprite["@id"] != null ? sprite["@id"] : sprite.id );
        var oSprite: object = LandPlayEdit.editor.getSprite( spriteId );
        if ( sprite["@id"] != null ) {
            /* it's a JSON presentation */
            sprite = LandPlayEdit.lgEditor.getPivotByName( spriteId );
        }

        if ( sprite != null ) {
            if ( oSprite.grid != null || oSprite.layers != null ) {
                /* avoid repeating grid or layers*/
                var group: Phaser.Group = LandPlayEdit.getGroup( LandPlayEdit.lgEditor.getPivot( sprite ) );
                if ( group != null ) {
                    if ( deleting ) {
                        for ( var i = 0; i < group.children.length; i++ ) {
                            var id = group.children[i].id;
                            if ( i == 0 ) {
                                switch ( group.parent.name ) {
                                    case LandPlay.LandGame.GridsContainer:
                                        id += "_0"; /* pivot renamed to root name */
                                        break;
                                    case LandPlay.LandGame.LayersContainer:
                                        id += "1"; /* pivot renamed to root name */
                                        break;
                                    default:
                                        break;
                                }
                            }
                            delete LandPlayEdit.lgEditor.sprites[id];
                        }
                    }
                    group.destroy( true );
                } else {
                    /* maybe this is the first time */
                }

                /* render that comes next will recreate the group */
            }
        }
    }

    export function sprite_framed( sprite: Phaser.Sprite, frames: object ) {
        LandPlayEdit.editor.setFrames( sprite.key, frames );
        LandPlayEdit.editFrames( LandPlayEdit.editor.getAsset( sprite.key, LandPlay.AssetType.Image ) );
        LandPlayEdit.saveData();
        LandPlayEdit.lgEditor.setAction( LandPlay.EditAction.Edit );
        sprite.destroy( true );
    }

    export function stage_load() {
        $( "#side_panels" ).show();
        if ( LandPlayEdit.plugin != null ) {
            LandPlayEdit.plugin.preview();
        }
    }

    export function loadLayout() {
        $( "#side_panels" ).hide();
        try {
            LandPlayEdit.editor.appendForm( "#editorForm" );
            if ( LandPlayEdit.editor.json.config.assets == null || LandPlayEdit.editor.json.config.assets[LandPlay.AssetType.Image] == null ) {
                LandPlayEdit.alert( i18next.t( "constraint.asset.none", { "asset": i18next.t( "mainMenu.tabs.assets" ) } ) );
            } else {
                $( '#content' ).html( "" );
                LandPlayEdit.lgEditor = new LandPlay.EditorMode( LandPlayEdit.editor.toXML(), {
                    "editor": LandPlayEdit.editor,
                    "sprite_click": LandPlayEdit.sprite_click,
                    "sprite_drop": LandPlayEdit.sprite_drop,
                    "sprite_framed": LandPlayEdit.sprite_framed,
                    "sprite_unnamed": LandPlayEdit.sprite_unnamed,
                    "stage_load": LandPlayEdit.stage_load
                } );

                $( "#available_assets .assets_table" ).html( "" );
                var assets: Array<object> = LandPlayEdit.editor.getAssets();
                assets.image.sort( function( a, b ) {
                    if ( a["@key"] > b["@key"] ) {
                        return 1;
                    } else if ( a["@key"] < b["@key"] ) {
                        return -1;
                    } else {
                        return 0;
                    }
                } )
                for ( var i = 0; ( i < assets.image.length ); i++ ) {
                    var asset = assets.image[i];
                    if ( LandPlayEdit.reservedSprites.indexOf( asset["@key"] ) < 0 ) {
                        showImageAsset( asset );
                    }
                }

                LandPlayEdit.saveData();
            }
        } catch ( e ) {
            console.error( e );
        }
    }

    export function loadPreview() {
        $( '#player' ).html( "" );
        if ( LandPlayEdit.plugin != null ) {
            /** TODO check what it takes to define this kind of game */
            if ( LandPlayEdit.plugin.validate() ) {
                var game = eval( "new LandPlay." + $( 'select[name="config.setup.@mode"]' ).val() + "(LandPlayEdit.editor.toXML(), {DOMcontainer: 'player'})" );
                $( "#player" ).focus();
            } else {
                LandPlayEdit.plugin.showConstraints();
            }
        } else {
            LandPlayEdit.alert( i18next.t( "alert.preview.nomode" ) );
        }
    }

    function showImageAsset( asset: object ) {
        var label = asset["@key"];
        var assetLi = $( '#templates #tpl_available_asset_row li:first-child' ).clone( true );
        if ( label == LandPlay.EmptyImageAsset ) {
            label = "Empty";
            $( 'a.frames', assetLi ).hide();
        }
        $( 'a.asset', assetLi ).html( label );
        $( 'a.asset', assetLi ).attr( "href", asset["@url"] );
        $( 'a.asset', assetLi ).attr( "data-key", asset["@key"] );

        /* frames */
        $( 'a.frames', assetLi ).attr( "data-key", asset["@key"] );

        $( "#available_assets .assets_table" ).append( assetLi );
    }

    export function addAssetRow( asset, subtype ) {
        var assetLi = $( '#templates #tpl_asset_row li:first-child' ).clone();
        $( 'a', assetLi ).html( asset["@key"] );
        $( 'a', assetLi ).attr( "href", asset["@url"] );
        $( 'a', assetLi ).attr( "data-asset-type", subtype );
        if ( asset.layers != null ) {
            $( 'a', assetLi ).attr( "data-asset-type", "layers" );
            var from = parseInt( asset.layers["@from"] );
            var to = parseInt( asset.layers["@to"] );
            $( 'a', assetLi ).attr( "data-from-layer", from );
            $( 'a', assetLi ).attr( "data-to-layer", to );

            if ( !isNaN( from ) && !isNaN( to ) ) {
                assetLi.append( ": (" + from + " - " + to + ")" );
                /*
                for ( var i = from; i < to; i++ ) {
                    var layer = $( "<a>" );
                    layer.attr( "href", asset["@url"].replace( "(?)", i ) );
                    layer.attr( "target", "_blank" );
                    layer.html( i );
                    assetLi.append( layer );
                    assetLi.append( " - " );
                }
                */
            }
        }
        $( "#" + subtype + "_assets .assets_table" ).append( assetLi );
    }

    export function scaleSprite( updated ) {

        if ( $( "input[name='transient.sprite.scaled']:checked" ).length > 0 ) {
            var rate = LandPlayEdit.sprite.height / LandPlayEdit.sprite.width;
            var name = $( updated ).attr( "name" );
            var other;
            switch ( name ) {
                case "sprite.@width":
                    other = "sprite.@height";
                    break;
                case "sprite.height":
                    other = "sprite.@width";
                    rate = 1 / rate;
                    break;
            }
            var value = Math.round( $( updated ).val() * rate );
            if ( isNaN( value ) ) {
                /*is a %, copy*/
                value = $( updated ).val();
            }
            $( "input[name='" + other + "']" ).val( value );
        }
    }

    export function updateForms() {
        if ( LandPlayEdit.sprite.id == undefined || LandPlayEdit.sprite.id == "" ) {
            /* it's the first used to create a sprite */
            var id = $( "[name='sprite.@id']" ).val();
            if ( LandPlayEdit.editor.getSprite( id ) != null ) {
                $( '#spriteEditor' ).foundation( 'close' );
                LandPlayEdit.alert( i18next.t( "alert.sprite.exists", { id: id } ) );
                return false;
            }
        }
        LandPlayEdit.updateSprite();
        $( '#spriteEditor' ).foundation( 'close' );
    }

    export function createText() {
        $( "[name='text_pos']" ).val( "" );
        LandPlayEdit.loadStyleEditorTemplate( "#textEditor .styleEditorPlace" );
        LandPlayEdit.loadDefaultTextStyle();
        $( "#textForm" ).show();
        $( '#textForm' ).foundation( 'resetForm' );
    }
    /** 
     * To sprite JSON, as a new text node 
     * */
    export function updateText() {
        var text: object = {};
        var jsonSprite: object = LandPlayEdit.editor.getSprite( LandPlayEdit.sprite.id );
        if ( jsonSprite.text == null ) {
            jsonSprite.text = [];
        }
        for ( var key of Object.keys( LandPlayEdit.lgEditor.textFields ) ) {
            var defval = LandPlayEdit.lgEditor.textFields[key].default;
            var value = $( "[name='sprite.text." + key + "']" ).val();
            if ( value == "" && defval != null ) {
                value = defval;
            }
            text[key] = value;
        }
        text["@style"] = LandPlayEdit.parseStyleFromSelect( "sprite.text.@style" );
        if ( $( "[name='text_pos']" ).val() == "" ) {

        } else {
            var pos = parseInt( $( "[name='text_pos']" ).val() );
            jsonSprite.text.splice( pos, 1 );
        }
        jsonSprite.text.push( text );
        $( '#textEditor' ).foundation( 'close' );
        LandPlayEdit.resetSprite( LandPlayEdit.sprite );
    }

    export function parseStyleFromSelect( fieldName: string ) {
        var sample: Phaser.Text = LandPlayEdit.lgEditor.getGame().add.text( 0, 0, "" );
        var styles: Array<string> = new Array<string>();
        $( "[name='" + fieldName + "'] option" ).each( function() {
            var pair = $( this ).val();
            var parts = pair.split( ":" );
            var value = parts[1];
            if ( typeof sample.style[parts[0]] == "number" ) {
                value = parseInt( parts[1] );
            } else if ( typeof sample.style[parts[0]] == "boolean" ) {
                value = parts[1];
            } else {
                value = '\"' + value + '\"'
            }
            styles.push( '\"' + parts[0] + '\"' + ":" + value );
        } );
        sample.destroy();
        return "{" + styles.join( ", " ) + "}";
    }

    function saveText( text: Phaser.Text ) {
        for ( var key of Object.keys( LandPlayEdit.lgEditor.textFields ) ) {
            var defval = LandPlayEdit.lgEditor.textFields[key].default;
            var value = $( "[name='sprite.text." + key + "']" ).val();
            if ( value == "" && defval != null ) {
                value = defval;
            }
            text[LandPlayEdit.lgEditor.textFields[key].attr] = value;
        }
        /**styles */
        var style = {};
        $( ".textSelectedStyles option" ).each( function() {
            var pair = $( this ).val();
            var parts = pair.split( ":" );
            var value = parts[1];
            if ( typeof text.style[parts[0]] == "number" ) {
                value = parseInt( parts[1] );
            }
            style[parts[0]] = value;
        } );
        text.setStyle( style );
    }

    function loadText( text: object ) {
        for ( var key of Object.keys( LandPlayEdit.lgEditor.textFields ) ) {
            $( "[name='sprite.text." + key + "']" ).val( text[key] );
        }
        $( ".textSelectedStyles" ).html( "" );
        var fields = JSON.parse( text["@style"] );
        for ( var key of Object.keys( fields ) ) {
            loadStyleOption( key, fields[key] );
        }
    }

    export function editText( pos: number ) {
        var jsonSprite = LandPlayEdit.editor.getSprite( LandPlayEdit.sprite.id );
        LandPlayEdit.loadStyleEditorTemplate( "#textEditor .styleEditorPlace" );
        if ( jsonSprite != null && jsonSprite.text != null && jsonSprite.text.length > pos ) {
            var text: object = jsonSprite.text[pos];
            $( "[name='text_pos']" ).val( pos );
            loadText( text );
            $( '#textForm' ).show();
        }
    }

    export function deleteText( pos ) {
        LandPlayEdit.confirm( i18next.t( "reveal.text.delete" ), function() {
            var jsonSprite = LandPlayEdit.editor.getSprite( LandPlayEdit.sprite.id );
            var i = parseInt( pos );
            if ( !isNaN( i ) ) {
                if ( jsonSprite.text != null && jsonSprite.text.length > i ) {
                    jsonSprite.text.splice( pos, 1 );
                }
            } else {
                console.warn( "Given position is not a number" );
            }
            $( "#textEditor" ).foundation( "close" );
            LandPlayEdit.resetSprite( LandPlayEdit.sprite );
            LandPlayEdit.saveData();
        } );
    }

    export function launchFrameEditor( key: string ) {
        var asset = LandPlayEdit.editor.getAsset( key, LandPlay.AssetType.Image );
        if ( asset != null ) {
            var rawImage: HTMLImageElement = new Image();
            rawImage.name = key;
            rawImage.src = asset["@url"];
            LandPlayEdit.lgEditor.newSprite( key, {
                editFrames: true,
                rawImage: rawImage
            } );
        } else {
            console.error( "Asset not found: '" + key + "" );
        }
    }

    export function editFrames( asset: object ) {
        $( "#framesOrbit .orbit-container" ).html( "" );
        $( "#framesOrbit .orbit-bullets" ).html( "" );
        var row = 0;
        for ( var i = 0; i < asset["@frameMax"]; i++ ) {
            if ( i > 0 && ( asset["@frameWidth"] * i ) % asset.width == 0 ) {
                row++;
            }
            var orbitCont = $( "#tpl_orbit_slide > li:first" ).clone( true );
            var style = "background-image: url(" + asset["@url"] + ");";
            style += "background-position-x: " + ( ( asset["@frameWidth"] * i ) % asset.width ) + "px;";
            style += "background-position-y: " + ( -asset["@frameHeight"] * row ) + "px;";
            style += "width: " + asset["@frameWidth"] + "px;";
            style += "height: " + asset["@frameHeight"] + "px;";
            style += "margin: auto;";
            $( '.orbit-frame', orbitCont ).attr( "style", style );
            $( "#framesOrbit .orbit-container" ).append( orbitCont );


            var orbitBullet = $( "#tpl_orbit_bullet > button:first" ).clone( true );
            $( 'button', orbitBullet ).attr( "data-slide", i );
            $( "#framesOrbit .orbit-bullets" ).append( orbitBullet );

        }
        $( '#frameEditor' ).foundation( 'open' );
        var elem = new Foundation.Orbit( $( '#framesOrbit' ), {
            autoPlay: false
        } );
    }

    export function editButton() {
        if ( $( "#button_type_select option" ).length == 0 ) {
            var option = $( "<option>" );
            $( "#button_type_select" ).append( option );
            for ( var key of Object.keys( LandPlay.LandButton.Types ) ) {
                var option = $( "<option>" );
                var value = LandPlay.LandButton.Types[key].value;
                $( option ).attr( "value", value );
                $( option ).html( value );
                $( "#button_type_select" ).append( option );
                LandPlayEdit.buttonActionKeys.push( value );
            }
        }
        var button = LandPlayEdit.editor.getButton( LandPlayEdit.sprite.id );
        if ( button != null ) {
            /* this sprite already had a button */
            for ( var key of Object.keys( button ) ) {
                var actionAttr = key.replace( "@", "" );
                if ( LandPlayEdit.buttonActionKeys.indexOf( actionAttr ) > -1 ) {
                    $( "#button_type_select" ).val( actionAttr );
                    $( "#button_type_select" ).trigger( "change" );
                }
                $( "[name='button." + key + "']" ).val( button[key] )
            }
        } else {
            LandPlayEdit.resetButtonForm();
        }
        $( '#buttonForm' ).foundation( 'resetForm' );
        $( "[name='button.@sprite']" ).val( LandPlayEdit.sprite.id );
        $( '#buttonEditor' ).foundation( 'open' );
    }

    export function editRetry() {
        for ( var key of Object.keys( LandPlayEdit.editor.json.config.retry ) ) {
            $( "[name='retry." + key + "']" ).val( LandPlayEdit.editor.json.config.retry[key] );
        }
        $( "#retryEditor" ).foundation( "open" );
    }

    export function updateRetry() {
        var retry = {};
        $( "[name*='retry.@']" ).each( function() {
            var attr = $( this ).attr( "name" ).replace( "retry.@", "" );
            var value = parseInt( $( this ).val() );
            if ( !isNaN( value ) ) {
                retry["@" + attr] = value;
            }
        } );
        LandPlayEdit.editor.json.config.retry = retry;
        $( "#retryEditor" ).foundation( "close" );
        LandPlayEdit.saveData();
    }

    export function deleteRetry() {
        LandPlayEdit.confirm( i18next.t( "reveal.retry.delete" ), function() {
            LandPlayEdit.editor.json.config.retry = {};
            $( "#retryForm" ).foundation( "resetForm" );
            $( "#retryEditor" ).foundation( "close" );
            LandPlayEdit.saveData();
        } );
    }

    export function validateRetry() {
        var retries = parseInt( $( "[name='retry.@retries']" ).val() );
        var from = parseInt( $( "[name='retry.@from']" ).val() );
        var to = parseInt( $( "[name='retry.@to']" ).val() );
        var step = parseInt( $( "[name='retry.@step']" ).val() );

        if ( !isNaN( retries ) ) {
            if ( !isNaN( from ) ) {
                if ( isNaN( to ) ) {
                    customFieldError( "retry_to", i18next.t( "reveal.retry.validator.to" ) );
                }
            }
            if ( !isNaN( to ) ) {
                if ( isNaN( from ) ) {
                    customFieldError( "retry_from", i18next.t( "reveal.retry.validator.from" ) );
                }
            }
            if ( !isNaN( from ) && !isNaN( from ) ) {
                if ( from == to ) {
                    customFieldError( "retry_from", i18next.t( "reveal.retry.validator.toeqfrom" ) );
                    customFieldError( "retry_to", i18next.t( "reveal.retry.validator.toeqfrom" ) );
                } else {
                    if ( isNaN( step ) ) {
                        $( "[name='retry.@step']" ).val( "1" );
                    }
                }
            }
            if ( !isNaN( step ) ) {
                if ( step < 0 ) {
                    $( "[name='retry.@step']" ).val( "" + Math.abs( step ) );
                }
            }
        }
    }

    export function editCounter() {
        for ( var i = 0; i < LandPlayEdit.textSelects.length; i++ ) {
            var selector = "[name='" + LandPlayEdit.textSelects[i] + "']";
            LandPlayEdit.reloadTextSelect( selector, null, true );
        }
        LandPlayEdit.reloadSpriteSelect( $( "[name='counter.progress.@sprite']" ) );
        $( "[name*='counter.']" ).each( function() {
            var name = $( this ).attr( "name" );
            $( this ).val( LandPlayEdit.editor.parse( LandPlayEdit.editor.json.config.setup, name ) );
        } );
        $( "#counterEditor" ).foundation( "open" );
    }

    export function updateCounter() {
        var container = {};
        $( "[name*='counter.']" ).each( function() {
            var field = {
                name: $( this ).attr( "name" ),
                value: $( this ).val()
            }
            var ignoreEmpty = true;
            container = LandPlayEdit.editor.walk( container, field, ignoreEmpty );
        } );
        LandPlayEdit.editor.json.config.setup.counter = container.counter;
        $( "#counterEditor" ).foundation( "close" );
        LandPlayEdit.saveData();
    }

    export function deleteCounter() {
        LandPlayEdit.confirm( i18next.t( "reveal.counter.delete" ), function() {
            LandPlayEdit.editor.json.config.setup.counter = {};
            $( "#counterForm" ).foundation( "resetForm" );
            $( "#counterEditor" ).foundation( "close" );
            LandPlayEdit.saveData();
        } );
    }

    export function validateCounter( name ) {
        var selector = "[name='" + name + "']";
        for ( var i = 0; i < LandPlayEdit.textSelects.length; i++ ) {
            var otherSelector = LandPlayEdit.textSelects[i];
            otherSelector = "[name='" + otherSelector + "']";
            if ( otherSelector != selector ) {
                var force = true;
                LandPlayEdit.reloadTextSelect( otherSelector, $( selector ).val(), force );
            }
        }
    }

    export function openEvents() {
        LandPlayEdit.openEventsDo( LandPlayEdit.editor.json.config.setup.event );
    }

    export function openEventsDo(
        events: Array<object>,
        container?: number,
        excludedCauses?: Array<string>,
        excludedEffects?: Array<string>,
        subcontainer?: number,
        addAll?: boolean,
        addThis?: boolean ) {
        if ( container == null ) {
            $( "[name='event_container_pos']" ).val( "" );
        } else {
            $( "[name='event_container_pos']" ).val( container );
        }
        if ( subcontainer == null ) {
            $( "[name='event_subcontainer_pos']" ).val( "" );
        } else {
            $( "[name='event_subcontainer_pos']" ).val( subcontainer );
        }
        $( "[name='event.@cause']" ).html( "" );
        var option = $( "<option>" );
        /*$( "[name='event.@cause']" ).append( option ); <- no need to fill with empty option, now is multiple*/
        for ( var key of Object.keys( LandPlay.LandEvent.Cause ) ) {
            if ( excludedCauses == null || excludedCauses.indexOf( key ) < 0 ) {
                var value = LandPlay.LandEvent.Cause[key].value;
                option = $( "<option>" );
                $( option ).attr( "value", value );
                var label = value;
                if ( i18next.exists( "option.cause." + value + ".label" ) ) {
                    label = i18next.t( "option.cause." + value + ".label" )
                }
                $( option ).html( label );
                $( "[name='event.@cause']" ).append( option );
            }
        }
        if ( $( "[name='event.@type'] option" ).length == 0 ) {
            var option = $( "<option>" );
            $( "[name='event.@type']" ).append( option );
            for ( var key of Object.keys( LandPlay.LandEvent.Effect ) ) {
                var value = LandPlay.LandEvent.Effect[key].value;
                option = $( "<option>" );
                $( option ).attr( "value", value );
                var label = value;
                if ( i18next.exists( "option.effect." + value + ".label" ) ) {
                    label = i18next.t( "option.effect." + value + ".label" )
                }
                $( option ).html( label );
                $( "[name='event.@type']" ).append( option );
            }
            if ( LandPlayEdit.plugin != null ) {
                LandPlayEdit.plugin.appendEffects( "[name='event.@type']" );
            }
        }
        var exceptions = null;
        var layers = false;
        var grids = false;
        LandPlayEdit.reloadSpriteSelect( $( "[name='event.@sprite']" ), null, null, addAll, exceptions, layers, grids, addThis )

        $( '#event-list' ).html( "" );
        /* find exiting generic events*/
        var eventFound = false;
        if ( events != null ) {
            for ( var i = 0; i < events.length; i++ ) {
                var event = events[i];
                if ( !eventFound ) {
                    eventFound = true;
                }
                LandPlayEdit.listRecord( "#event-list", i, event["@cause"] + " - " + event["@type"] + " - " + event["@sprite"] + " - " + event["@value"] );
            }
        }
        if ( !eventFound ) {
            var textDiv = $( '#templates > #tpl_event_no_item > div:first-child' ).clone( true );
            $( "#event-list" ).append( textDiv );
        } else {
            LandPlayEdit.activateList( "event" );
        }
        $( "#eventForm" ).hide();
        $( "#eventForm" ).foundation( "resetForm" );
        $( "#eventEditor" ).foundation( "open" );
    }

    export function createEvent() {
        $( "[name='event_pos']" ).val( "" );
        $( "#eventForm" ).show();
        $( '#eventForm' ).foundation( 'resetForm' );
    }

    export function editEvent( pos: number ) {
        var events: Array<object> = LandPlayEdit.getEventsCollection();
        var event = events[pos];
        $( "[name='event_pos']" ).val( pos );
        $( "[name*='event.@']" ).each( function() {
            var attr = $( this ).attr( "name" ).replace( "event.@", "" );
            $( this ).val( event["@" + attr] );
            LandPlayEdit.validateEvent( $( this ).attr( "name" ) );
        } );
        $( '#eventForm' ).show();
    }

    export function updateEvent() {
        var events: Array<object> = LandPlayEdit.getEventsCollection();
        var event = {};
        $( "[name*='event.@']" ).each( function() {
            var attr = $( this ).attr( "name" ).replace( "event.@", "" )
            event["@" + attr] = $( this ).val();
        } );
        if ( $( "[name='event_pos']" ).val() != "" ) {
            var pos = parseInt( $( "[name='event_pos']" ).val() );
            if ( !isNaN( pos ) ) {
                events.splice( pos, 1 );
            }
        }
        events.push( event );
        $( "#eventEditor" ).foundation( "close" );
        LandPlayEdit.saveData();
    }

    export function deleteEvent( pos ) {
        var events: Array<object> = LandPlayEdit.getEventsCollection();
        LandPlayEdit.confirm( i18next.t( "reveal.event.delete" ), function() {
            $( "#eventEditor" ).foundation( "close" );
            var i = parseInt( pos );
            if ( !isNaN( i ) ) {
                events.splice( pos, 1 );
            } else {
                console.warn( "Given position is not a number" );
            }
            LandPlayEdit.saveData();
            LandPlayEdit.openEvents();
        } );
    }

    export function getEventsCollection() {
        if ( LandPlayEdit.editor.json.config.setup.event == null ) {
            LandPlayEdit.editor.json.config.setup.event = [];
        }
        var events = LandPlayEdit.editor.json.config.setup.event;
        var collection = $( "[name='event_container_pos']" ).val();
        if ( collection != "" && LandPlayEdit.plugin != null ) {
            events = LandPlayEdit.plugin.getEventsCollection( $( "[name='event_container_pos']" ).val(), $( "[name='event_subcontainer_pos']" ).val() );
        }
        return events;
    }

    export function validateEvent( name ) {
        var selector = "[name='" + name + "']";
        var extended = allSpritesEffects.indexOf( $( selector ).val() ) > -1;
        switch ( name ) {
            case "event.@type":
                var type = $( selector ).val();
                switch ( type ) {
                    case LandPlay.LandEvent.Effect.TYPE_PAUSE.value:
                    case LandPlay.LandEvent.Effect.TYPE_SOLVE.value:
                    case LandPlay.LandEvent.Effect.TYPE_VALIDATE.value:
                        LandPlayEdit.toggleField( "event.@sprite", false );
                        LandPlayEdit.toggleField( "event.@value", false );
                        $( "[name='event.@sprite']" ).val( "" );
                        break;
                    case LandPlay.LandEvent.Effect.TYPE_ADDPOINTS.value:
                        LandPlayEdit.toggleField( "event.@sprite", false );
                        LandPlayEdit.toggleField( "event.@value", true, "number", i18next.t( "validator.number.required" ) );
                        $( "[name='event.@sprite']" ).val( "" );
                        break;
                    case LandPlay.LandEvent.Effect.TYPE_PLAYSOUND.value:
                        LandPlayEdit.toggleField( "event.@sprite", true );
                        LandPlayEdit.toggleField( "event.@value", false );
                        var force = true;
                        LandPlayEdit.reloadSoundSelect( $( "[name='event.@sprite']" ), null, force );
                        break;
                    case LandPlay.LandEvent.Effect.TYPE_LOCK.value:
                    case LandPlay.LandEvent.Effect.TYPE_BACK.value:
                    case LandPlay.LandEvent.Effect.TYPE_RUNAWAY.value:
                    case LandPlay.LandEvent.Effect.TYPE_DESTROY.value:
                        LandPlayEdit.toggleField( "event.@sprite", true );
                        LandPlayEdit.toggleField( "event.@value", false );
                        var force = true;
                        var exceptions, layers, grids = null;
                        var addThis = ( $( "[name='event_container_pos']" ).val() != "" );
                        LandPlayEdit.reloadSpriteSelect( $( "[name='event.@sprite']" ), null, force, extended, exceptions, layers, grids, addThis );

                        break;
                    case LandPlay.LandEvent.Effect.TYPE_VISIBLE.value:
                        LandPlayEdit.toggleField( "event.@sprite", true );
                        LandPlayEdit.toggleField( "event.@value", true, "(^true$|^false$)", i18next.t( "validator.boolean.required" ) );
                        var force = true;
                        LandPlayEdit.reloadSpriteSelect( $( "[name='event.@sprite']" ), null, force, extended );
                        break;
                    default:
                        if ( LandPlayEdit.plugin != null ) {
                            LandPlayEdit.plugin.validateEvent( name );
                        } else {
                            console.warn( "Effect '" + type + "' has not dependent selectors actions" );
                        }

                }
                break;
        }

        for ( var i = 0; i < LandPlayEdit.textSelects.length; i++ ) {
            var otherSelector = LandPlayEdit.textSelects[i];
            otherSelector = "[name='" + otherSelector + "']";
            if ( otherSelector != selector ) {
                var force = true;
                LandPlayEdit.reloadTextSelect( otherSelector, $( selector ).val(), force );
            }
        }
    }

    export function toggleField( name: string, required: boolean, pattern?: string, errMessage?: string ) {
        var field = $( "[name='" + name + "']" );
        var add = ( required ? "required" : "disabled" );
        var del = ( required ? "disabled" : "required" );
        $( field ).attr( add, add );
        $( field ).removeAttr( del );
        if ( pattern != null ) {
            $( field ).attr( "pattern", pattern );
        }
        if ( !required ) {
            $( field ).val( "" );
            customFieldError( $( field ).attr( "id" ), "" );
        } else if ( errMessage != null ) {
            customFieldError( $( field ).attr( "id" ), errMessage );
        }
    }

    export function editGrid() {
        $( "#gridEditor" ).foundation( "open" );
        var jsonSprite = LandPlayEdit.editor.getSprite( LandPlayEdit.sprite.id );
        if ( jsonSprite.grid != null ) {
            $( "[name*='grid.@']" ).each( function() {
                var attr = $( this ).attr( "name" ).replace( "grid.@", "" );
                $( this ).val( jsonSprite.grid["@" + attr] );
            } );
            var enabled = "length";
            var disabled = "selection";
            if ( jsonSprite.grid["@selection"] != "" ) {
                var enabled = "selection";
                var disabled = "length";
            }
            $( "[name='transient.grid.cells'][value=" + disabled + "]" ).prop( "checked", false );
            $( "[name='transient.grid.cells'][value=" + enabled + "]" ).prop( "checked", true );
            toggleField( "grid.@" + disabled, false );
            toggleField( "grid.@" + enabled, true );
        } else {
            $( "#gridForm" ).foundation( "resetForm" );
            $( "[name='transient.grid.cells'][value=length]" ).prop( "checked", true );
            toggleField( "grid.@length", true );
            toggleField( "grid.@selection", false );
        }


    }

    export function updateGrid() {
        var grid = {};
        $( "[name*='grid.@']" ).each( function() {
            var attr = $( this ).attr( "name" ).replace( "grid.@", "" );
            var value = $( this ).val();
            grid["@" + attr] = value;
        } );
        var index = LandPlayEdit.editor.getSpriteIndex( LandPlayEdit.sprite.id );
        if ( index > -1 ) {
            doDeleteGrid(); /* JIC length/selection is shorter */
            LandPlayEdit.editor.json.config.layout.sprite[index].grid = grid;
            LandPlayEdit.resetSprite( LandPlayEdit.sprite );
        }
        $( "#gridEditor" ).foundation( "close" );
    }

    export function deleteGrid() {
        LandPlayEdit.confirm( i18next.t( "reveal.grid.delete" ), function() {
            doDeleteGrid();
            $( "#gridEditor" ).foundation( "close" );
        } );
    }

    function doDeleteGrid() {
        var index = LandPlayEdit.editor.getSpriteIndex( LandPlayEdit.sprite.id );
        if ( index > -1 ) {
            destroyGroups( LandPlayEdit.sprite, true );
            delete LandPlayEdit.editor.json.config.layout.sprite[index].grid;
            LandPlayEdit.resetSprite( LandPlayEdit.sprite );
        }
    }

    export function validateGrid( name ) {
        var xoffset = parseInt( $( "[name='grid.@xoffset']" ).val() );
        var yoffset = parseInt( $( "[name='grid.@yoffset']" ).val() );
        var cols = parseInt( $( "[name='grid.@cols']" ).val() );
        var length = parseInt( $( "[name='grid.@length']" ).val() );

        if ( name == "transient.grid.cells" ) {
            var value = $( "[name='" + name + "']:checked" ).val();
            var other = ( value == "length" ? "selection" : "length" );
            LandPlayEdit.toggleField( "grid.@" + value, true );
            LandPlayEdit.toggleField( "grid.@" + other, false );
            $( "[name='grid.@" + other + "']" ).val( "" );
        }
    }

    export function openShapes() {
        /*TODO*/
    }

    export function createShape() {
        $( "[name='shape_pos']" ).val( "" );
        if ( $( "[name='shape.@type'] option" ).length == 0 ) {
            var option = $( "<option>" );
            $( "[name='shape.@type']" ).append( option );
            for ( var key of Object.keys( ShapeTypes ) ) {
                option = $( "<option>" );
                $( option ).attr( "value", ShapeTypes[key] );
                $( option ).html( key );
                $( "[name='shape.@type']" ).append( option );
            }
        }
        toggleField( "shape.@radius", false );
        toggleField( "shape.@width", false );
        toggleField( "shape.@height", false );

        $( "#shapeForm" ).show();
        $( '#shapeForm' ).foundation( 'resetForm' );
    }

    export function updateShape() {
        var jsonSprite: object = LandPlayEdit.editor.getSprite( LandPlayEdit.sprite.id );
        if ( jsonSprite.shape == null ) {
            jsonSprite.shape = [];
        }

        var container = {};
        $( "[name*='shape.']" ).each( function() {
            var field = {
                name: $( this ).attr( "name" ),
                value: $( this ).val()
            }
            container = LandPlayEdit.editor.walk( container, field );
        } );

        if ( $( "[name='shape_pos']" ).val() == "" ) {

        } else {
            var pos = parseInt( $( "[name='shape_pos']" ).val() );
            jsonSprite.shape.splice( pos, 1 );
        }
        jsonSprite.shape.push( container.shape );
        $( '#shapeEditor' ).foundation( 'close' );
        LandPlayEdit.resetSprite( LandPlayEdit.sprite );
        LandPlayEdit.saveData();
    }

    export function editShape( pos: number ) {
        LandPlayEdit.createShape();
        var jsonSprite = LandPlayEdit.editor.getSprite( LandPlayEdit.sprite.id );
        if ( jsonSprite != null ) {
            var shape = jsonSprite.shape[pos];
            $( "[name='shape_pos']" ).val( pos );
            $( "[name*='shape.']" ).each( function() {
                var name = $( this ).attr( "name" );
                $( this ).val( LandPlayEdit.editor.parse( { shape: shape }, name ) );
            } );
            $( "#shapeColor" ).colorpicker( "val", $( "#shapeColor" ).val() );
            $( "#shapeLineColor" ).colorpicker( "val", $( "#shapeLineColor" ).val() );
            LandPlayEdit.validateShape( "shape.@type" );
        }
    }

    export function deleteShape( pos ) {
        LandPlayEdit.confirm( i18next.t( "reveal.shape.delete" ), function() {
            var jsonSprite = LandPlayEdit.editor.getSprite( LandPlayEdit.sprite.id );
            var i = parseInt( pos );
            if ( !isNaN( i ) ) {
                if ( jsonSprite.shape != null && jsonSprite.shape.length > i ) {
                    jsonSprite.shape.splice( pos, 1 );
                }
            } else {
                console.warn( "Given position is not a number" );
            }
            $( "#shapeEditor" ).foundation( "close" );
            LandPlayEdit.resetSprite( LandPlayEdit.sprite );
            LandPlayEdit.saveData();
            LandPlayEdit.openShapes();
        } );
    }

    export function validateShape( name ) {
        if ( name == "shape.@type" ) {
            var enabled = [];
            var disabled = [];
            var value = $( "[name='" + name + "']" ).val();
            switch ( value ) {
                case ShapeTypes.Circle:
                    disabled = ["shape.@width", "shape.@height"];
                    enabled = ["shape.@radius"];
                    break;
                default:
                    enabled = ["shape.@width", "shape.@height"];
                    disabled = ["shape.@radius"];
            }
            for ( var i = 0; i < enabled.length; i++ ) {
                LandPlayEdit.toggleField( enabled[i], true );
            }
            for ( var i = 0; i < disabled.length; i++ ) {
                LandPlayEdit.toggleField( disabled[i], false );
            }
        }
    }

    export function openDepth() {
        $( '#depth-list' ).html( "" );
        /* find exiting generic events*/
        var spriteFound = false;
        if ( LandPlayEdit.editor.json.config.layout.sprite != null ) {
            for ( var i = LandPlayEdit.editor.json.config.layout.sprite.length - 1; i >= 0; i-- ) {
                var sprite = LandPlayEdit.editor.json.config.layout.sprite[i];
                if ( !spriteFound ) {
                    spriteFound = true;
                }
                var top = ( i == LandPlayEdit.editor.json.config.layout.sprite.length - 1 );
                var bottom = ( i == 0 );
                LandPlayEdit.listOrder( "#depth-list", i, sprite["@id"], top, bottom );
            }
        }
        if ( !spriteFound ) {
            var textDiv = $( '#templates > #tpl_depth_no_item > div:first-child' ).clone( true );
            $( "#depth-list" ).append( textDiv );
        } else {
            LandPlayEdit.activateList( "depth" );
        }
        $( "#depthEditor" ).foundation( "open" );
    }

    export function editValidation() {
        var force = true;
        LandPlayEdit.reloadImageSelect( $( "[name='validation.right.@key']" ), null, force );
        LandPlayEdit.reloadImageSelect( $( "[name='validation.wrong.@key']" ), null, force );
        $( "[name*='validation.']" ).each( function() {
            var name = $( this ).attr( "name" );
            var value = LandPlayEdit.editor.parse( LandPlayEdit.editor.json.config, name );

            if ( name == "validation.@async" ) {
                $( "[name='" + name + "'][value='" + value + "']" ).prop( "checked", true );
            } else {
                $( this ).val( value );
            }

        } );
        $( "#validationEditor" ).foundation( "open" );
    }

    export function updateValidation() {
        var container = {};
        $( "[name*='validation.']" ).each( function() {
            var field = {
                name: $( this ).attr( "name" ),
                value: $( this ).val()
            }
            var ignoreEmpty = true;
            container = LandPlayEdit.editor.walk( container, field, ignoreEmpty );
            if ( field.name == "validation.@async" ) {
                container.validation["@async"] = $( "[name='" + field.name + "']:checked" ).val();
            }
        } );
        LandPlayEdit.editor.json.config.validation = container.validation;
        $( "#validationEditor" ).foundation( "close" );
        LandPlayEdit.saveData();
    }

    export function deleteValidation() {
        LandPlayEdit.confirm( i18next.t( "reveal.validation.delete" ), function() {
            delete LandPlayEdit.editor.json.config.validation;
            $( "#validationForm" ).foundation( "resetForm" );
            $( "#validationEditor" ).foundation( "close" );
            LandPlayEdit.saveData();
        } );
    }

    export function validateValidation( name ) {
        var selector = "[name='" + name + "']";
        for ( var i = 0; i < LandPlayEdit.validationSelects.length; i++ ) {
            var otherSelector = LandPlayEdit.validationSelects[i];
            otherSelector = "[name='" + otherSelector + "']";
            if ( otherSelector != selector ) {
                var force = true;
                LandPlayEdit.reloadImageSelect( otherSelector, $( selector ).val(), force );
            }
        }
    }

    export function notEmptySelectValidator(
        $el,      /* jQuery element to validate */
        required, /* is the element required according to the `[required]` attribute */
        parent    /* parent of the jQuery element `$el` */
    ) {
        return ( $( "option", $el ).length > 0 );
    };

    export function reloadTextSelect( selector: string, value?: string, force?: boolean ) {
        var curVal = $( selector ).val();
        if ( force ) {
            $( selector ).html( "" );
        }
        if ( $( selector + " option" ).length == 0 || force ) {
            var option = $( "<option>" );
            $( selector ).append( option );
            if ( LandPlayEdit.lgEditor.texts != null ) {
                var keys = Object.keys( LandPlayEdit.lgEditor.texts );
                keys.sort();
                for ( var key of keys ) {
                    if ( key != value ) {
                        option = $( "<option>" );
                        $( option ).html( key );
                        $( option ).attr( "value", key );
                        if ( force && key == curVal ) {
                            $( option ).attr( "selected", "selected" );
                        }
                        $( selector ).append( option );
                    }

                }
            }
        }
    }

    export function reloadSpriteSelect( selector: string, value: string, force?: boolean, extended?: boolean, exceptions?: Array<string>, layers?: boolean, grids?: boolean, addThis?: boolean ) {
        if ( exceptions != null && !force ) {
            force = true;
            console.warn( "[reloadSpriteSelect]: options reload has been forced, since there are exceptions" );
        }
        if ( force ) {
            $( selector ).html( "" );
        }
        if ( $( " option", selector ).length == 0 || force ) {
            var option = $( "<option>" );
            $( selector ).append( option );
            var keys: Array<string> = new Array<object>();
            if ( LandPlayEdit.lgEditor.sprites != null ) {
                keys = Object.keys( LandPlayEdit.lgEditor.sprites )
            }
            keys.sort();
            if ( extended ) {
                option = $( "<option>" );
                $( option ).html( i18next.t( "reveal.event.sprite.all" ) );
                $( option ).attr( "value", LandPlay.LandEvent.ALL_SPRITES );
                $( selector ).append( option );
            }
            if ( addThis ) {
                option = $( "<option>" );
                $( option ).html( i18next.t( "reveal.event.sprite.this" ) );
                $( option ).attr( "value", LandPlay.LandEvent.THIS_SPRITE );
                $( selector ).append( option );
            }
            var uniqueKeys: Array<string> = [];
            for ( var i = 0; i < keys.length; i++ ) {
                var key = keys[i];
                if ( layers ) {
                    LandPlayEdit.groupSprites( selector, value, key, LandPlay.LandGame.LayersContainer, function( option: object, group: LandPlay.LandLayer ) {
                        $( option ).html( group.name + " (" + group.fromLayerNumber + " - " + group.toLayerNumber + ")" );
                        if ( uniqueKeys.indexOf( group.name ) < 0 ) {
                            uniqueKeys.push( group.name ); /*never repeat*/
                            return group.name + "|" + group.fromLayerNumber + "|" + group.toLayerNumber;
                        }
                        return null;
                    } );
                }
                if ( grids ) {
                    LandPlayEdit.groupSprites( selector, value, key, LandPlay.LandGame.GridsContainer, function( option: object, group: LandPlay.LandGrid ) {
                        $( option ).html( group.name + " (0 - " + ( group.gridLength - 1 ) + ")" );
                        if ( uniqueKeys.indexOf( group.name ) < 0 ) {
                            uniqueKeys.push( group.name ); /*never repeat*/
                            return group.name + "_*";
                        }
                        return null;
                    } );
                }
                if ( exceptions == null || exceptions.indexOf( key ) < 0 || key == value ) {
                    option = $( "<option>" );
                    $( option ).html( key );
                    $( option ).attr( "value", key );
                    if ( key == value ) {
                        $( option ).attr( "selected", "selected" );
                    }
                    $( selector ).append( option );
                }

            }
        }
    }

    export function groupSprites( selector: string, value: string, key: string, contName: string, setOptionAndGetKey ) {
        var group: LandPlay.LandLayer = LandPlayEdit.lgEditor.getGroup( LandPlayEdit.lgEditor.sprites[key] );
        if ( group != null && group.parent.name == contName ) {
            var option = $( "<option>" );
            var pseudoKey = setOptionAndGetKey( option, group );
            if ( pseudoKey != null ) {
                $( option ).attr( "value", pseudoKey );
                if ( pseudoKey == value ) {
                    $( option ).attr( "selected", "selected" );
                }
                $( selector ).append( option );
            }
        }
    }

    export function reloadSoundSelect( selector, value?: string, force?: boolean ) {
        LandPlayEdit.reloadAssetsSelect( LandPlay.AssetType.Sound, selector, value, force );
    }

    export function reloadImageSelect( selector, value?: string, force?: boolean ) {
        LandPlayEdit.reloadAssetsSelect( LandPlay.AssetType.Image, selector, value, force );
    }

    export function reloadAssetsSelect( type: LandPlay.AssetType, selector, value?: string, force?: boolean ) {
        var curVal = $( selector ).val();
        if ( force ) {
            $( selector ).html( "" );
        }
        if ( $( "option", selector ).length == 0 || force ) {
            var option = $( "<option>" );
            $( selector ).append( option );
            if ( LandPlayEdit.editor.json.config.assets[type] != null ) {
                var keys: Array<string> = [];
                var assets: Array<object> = LandPlayEdit.editor.json.config.assets[type];
                assets.forEach( function( item, index ) {
                    keys.push( item["@key"] );
                } );
                keys = keys.sort();
                for ( var i = 0; i < keys.length; i++ ) {
                    var key = keys[i];
                    if ( key != value ) {
                        var option = $( "<option>" );
                        $( option ).attr( "value", key );
                        $( option ).html( key );
                        if ( force && key == curVal ) {
                            $( option ).attr( "selected", "selected" );
                        }
                        $( selector ).append( option );
                    }
                }
            }
        }
    }

    function customFieldError( id: string, text: string ) {
        var selector = "[data-form-error-for=" + id + "]";
        $( selector ).addClass( "is-visible" );
        if ( text == null || text == "" ) {
            /*clear*/
            $( selector + " span" ).remove();
            $( selector ).removeClass( "is-visible" );
            $( "#" + id ).removeClass( "is-invalid-input" )
        } else {
            $( selector ).html( text );
        }
    }

    export function behaviour( type ) {
        switch ( type ) {
            case "retry":
                LandPlayEdit.editRetry();
                break;
            case "counter":
                LandPlayEdit.editCounter();
                break;
            case "events":
                LandPlayEdit.openEvents();
                break;
            case "depth":
                LandPlayEdit.openDepth();
                break;
            case "validation":
                LandPlayEdit.editValidation();
                break;
            case "save":
                LandPlayEdit.saveJson();
                break;
            case "clear":
                LandPlayEdit.clearActivity();
                break;
            default:
                if ( LandPlayEdit.plugin != false ) {
                    LandPlayEdit.plugin.behaviour( type );
                }
        }
    }

    export function success( text ) {
        showMessage( text, "success" );
    }
    export function warning( text ) {
        showMessage( text, "warning" );
    }
    export function alert( text ) {
        showMessage( text, "alert" );
    }

    function showMessage( text, type ) {
        var callout = $( "#tpl_message > div:first" ).clone( true );
        $( callout ).addClass( type );
        $( callout ).attr( "id", "message-" + messageIndex );
        $( "span:first", callout ).html( text );
        $( "#message-container" ).append( callout );
        var currentIndex = messageIndex;
        messageItvs.push( setInterval( function() {
            $( "#message-" + currentIndex ).remove();
            clearInterval( messageItvs[currentIndex] );
        }, 3000 ) );
        messageIndex++;
    }

    export function confirm( text: string, yes, no?, end?) {
        $( "#confirm" ).foundation( "open" );
        $( "#confirmText" ).html( text );
        $( '#confirmYes' ).unbind( 'click' );
        $( "#confirmYes" ).click( function( event ) {
            event.preventDefault();
            yes();
            if ( typeof end == "function" )
                end();
            $( "#confirm" ).foundation( "close" );
        } );
        $( '#confirmNo' ).unbind( 'click' );
        $( "#confirmNo" ).click( function( event ) {
            event.preventDefault();
            if ( typeof no == "function" ) {
                no();
                if ( typeof end == "function" )
                    end();
            }
            $( "#confirm" ).foundation( "close" );
        } );
        if ( yes == null ) {
            $( "#confirmText" ).html( "ERROR: No action taken on Accept, please use the correct callbacks" );
            return false;
        }
    }

    export function loadStyleEditorTemplate( destination, fieldName?: string ) {
        $( ".styleEditorPlace fieldset" ).remove();

        var editor = $( "#tpl_style_editor > fieldset:first" ).clone( true );
        if ( fieldName != null ) {
            $( ".textSelectedStyles", editor ).attr( "name", fieldName );
        }

        $( destination ).append( editor );

        if ( LandPlayEdit.textStyleAttributes == null ) {
            LandPlayEdit.textStyleAttributes = {};
            var text: Phaser.Text = LandPlayEdit.lgEditor.getGame().add.text( 0, 0, "" );
            for ( var attr of Object.keys( text.style ) ) {
                LandPlayEdit.textStyleAttributes[attr] = { "name": attr, "values": [] };
                var option = $( "<option>" );
                option.attr( "value", attr );
                option.html( attr );
                $( '.textStyleName' ).append( option );
            }
            text.destroy();
        }


    }

    export function loadDefaultTextStyle() {
        var text: Phaser.Text = LandPlayEdit.lgEditor.getGame().add.text( 0, 0, "" );
        $( ".textSelectedStyles" ).html( "" );
        for ( var key of Object.keys( text.style ) ) {
            var value = text.style[key];
            loadStyleOption( key, value );
        }
        text.destroy();
    }

    export function loadStyleOption( attr: string, value: string ) {
        var option = $( "<option>" );
        option.attr( "value", attr + ":" + value );
        option.html( attr + ': "' + value + '"' );
        $( ".textSelectedStyles" ).append( option );
    }

    export function addTextStyle() {
        var tmp: Phaser.Text = LandPlayEdit.lgEditor.getGame().add.text( 0, 0, "" );
        var attr = $( ".styleEditorPlace .textStyleName" ).val();
        var value = $( ".styleEditorPlace .textStyleValue" ).val();
        if ( typeof tmp.style[attr] == "number" && isNaN( parseInt( value ) ) ) {
            $( ".textStyleValueError" ).show();
        } else {
            if ( typeof tmp.style[attr] == "boolean" ) {
                value = ( value == "true" || value == "1" );
            }
            $( ".textSelectedStyles option[value*='" + attr + ":']" ).remove()
            loadStyleOption( attr, value );
        }
        tmp.destroy();
    }

    export function editTextStyle() {
        if ( $( ".textSelectedStyles option:selected" ).length == 1 ) {
            var pair = $( ".textSelectedStyles option:selected" ).val();
            var parts = pair.split( ":" );
            var name = parts[0];
            var value = parts[1];
            $( ".textStyleName option[selected]" ).removeAttr( "selected" );
            $( ".textStyleName" ).val( name );
            $( ".textStyleName option[value='" + name + "']" ).attr( "selected", "selected" );
            $( ".textStyleValue" ).val( value );
        }
    }

    export function delTextStyle() {
        LandPlayEdit.confirm( i18next.t( "reveal.text.style.delete" ), function() {
            $( ".textSelectedStyles option:selected" ).remove();
        } );
    }

    export function addTool( tool: object, group: string ) {
        if ( $( "#" + group + "_tools [data-" + tool.action + "='" + tool.key + "']" ).length == 0 ) {
            var textDiv = $( '#templates > #tpl_tool > a:first-child' ).clone( true );
            $( textDiv ).attr( "data-" + tool.action, tool.key );
            $( textDiv ).attr( "title", i18next.t( tool.label ) );
            $( 'i', textDiv ).addClass( tool.icon );

            $( "#" + group + "_tools" ).append( textDiv );
            $( "#" + group + "_tools" ).append( "&nbsp;" );
        }
    }

    export function changeButtonType() {
        var type = $( "#button_type_select" ).val();
        $( "#button_value_container" ).html( "" );
        var append = $( "<span>" );
        append.html( "No value expected" );
        if ( type != null && type != "" ) {
            var name = "button.@" + type;
            switch ( type ) {
                case LandPlay.LandButton.Types.TYPE_HREF.value:
                    append = $( "<input>" );
                    $( append ).attr( "name", name );
                    $( append ).attr( "type", "text" );
                    break;
                case LandPlay.LandButton.Types.TYPE_SOUND.value:
                    append = $( "<select>" );
                    $( append ).attr( "name", name );
                    LandPlayEdit.reloadSoundSelect( append );
                    break;
                default:
                    append = $( "<input>" );
                    $( append ).attr( "value", "true" );
                    $( append ).attr( "name", name );
                    $( append ).attr( "disabled", "disabled" );
                    $( append ).attr( "type", "text" );
            }
        }
        $( "#button_value_container" ).append( append );
    }

    export function parseFormButton() {
        var button = {};
        $( "[name*='button.@']" ).each( function() {
            var attr = $( this ).attr( "name" ).replace( "button.@", "" );
            button["@" + attr] = $( this ).val();
        } );
        return button;
    }

    export function updateButton() {
        var button = LandPlayEdit.parseFormButton();
        LandPlayEdit.editor.addButton( button );
        LandPlayEdit.saveData();
        LandPlayEdit.resetButtonForm();
        $( "#buttonEditor" ).foundation( "close" );
    }

    export function resetButtonForm() {
        $( '#button_type_select' ).val( "" );
        $( "#button_value_container" ).html( "" );
        $( '#buttonForm' ).foundation( "resetForm" );
    }


    export function updateSprite() {

        var children: object = {};
        if ( LandPlayEdit.sprite.id == undefined || LandPlayEdit.sprite.id == "" ) {
            /* it's the first used to create a sprite
             * hide it, just used as template*/
            LandPlayEdit.sprite.visible = false;
            /* deleting it causes unstability*/
        }
        var formSelector = "#spriteEditor form";
        var jsonSprite = LandPlayEdit.editor.getSpriteJson( formSelector );
        var persistent = LandPlayEdit.editor.getSprite( jsonSprite.sprite["@id"] );
        destroyGroups( jsonSprite.sprite );
        LandPlayEdit.renderSprite( LandPlayEdit.mergeSprites( jsonSprite.sprite, persistent ) );
    }

    export function getGroup( sprite: Phaser.Sprite ): Phaser.Group {
        return LandPlayEdit.lgEditor.getGroup( sprite );
        /*
        for ( var i = 0; i < sprite.game.world.children.length; i++ ) {
            var root = sprite.game.world.children[i];
            if ( root.name == LandPlay.LandGame.GridsContainer ) {
                for ( var j = 0; j < root.children.length; j++ ) {
                    var group = root.children[j];
                    if ( group.name == sprite.id ) {
                        return group;
                    }
                }
                break;
            }
        }
        return null;
        */
    }

    export function spriteToJson( sprite: Phaser.Sprite, oSprite?: object ) {
        var result = {};
        if ( oSprite == null && sprite.id != null ) {
            oSprite = LandPlayEdit.editor.getSprite( sprite.id );
        }
        for ( var i = 0; i < LandPlayEdit.spriteFields.length; i++ ) {
            var attribute = LandPlayEdit.spriteFields[i];
            var field = "";
            field = "@" + attribute;
            if ( sprite != undefined ) {
                var value = sprite[attribute];
                if ( oSprite != null && oSprite[field] != null && oSprite[field] != undefined ) {
                    if ( attribute == "margin-bottom" ) {
                        value = sprite.game.height - sprite.y;
                        delete result["@y"];
                    } else if ( attribute == "margin-right" ) {
                        value = sprite.game.width - sprite.x;
                        delete result["@x"];
                    }
                }
                if ( attribute == "visible" ) {
                    value = ( sprite.alpha == LandPlayEdit.lgEditor.GhostNumber ? "false" : "true" );
                } else if ( attribute == "frame" ) {
                    currentFrame = parseInt( value );
                    if ( isNaN( currentFrame ) ) {
                        value = "0";
                    }
                }
                if ( value != undefined ) {
                    result["@" + attribute] = value;
                }

            } else {
                console.warn( "No sprite found for the field '" + attribute + "'" )
            }
        }

        var layer: LandPlay.LandLayer = LandPlayEdit.lgEditor.getGroup( sprite );
        if ( layer != null && typeof layer.toJsonLayers == "function" ) {
            result["@key"] = result["@key"].replace( /(.*)(\d)$/g, "$1" );
            result.layers = layer.toJsonLayers();
        }
        /** get from persistent sprite subnodes that can not be retrieved from game itself */
        result = LandPlayEdit.mergeSprites( result, oSprite );
        return result;
    }

    /**
     * Running objects (original: from a Phaser.Sprite) don't have persistent reflected properties
     * Copy them from "persistent" to "original"
     * */
    export function mergeSprites( original: object, persistent: object ) {
        if ( persistent != null ) {
            if ( persistent.grid != undefined ) {
                original.grid = persistent.grid;
            }
            if ( persistent.text != undefined ) {
                original.text = persistent.text;
            }
            if ( persistent.shape != undefined ) {
                original.shape = persistent.shape;
            }
            if ( persistent.layers != undefined ) {
                original.layers = persistent.layers;
            }
            if ( persistent["@anchorx"] != undefined ) {
                original["@anchorx"] = persistent["@anchorx"];
            }
            if ( persistent["@anchory"] != undefined ) {
                original["@anchory"] = persistent["@anchory"];
            }
            if ( persistent.sprite != undefined ) {
                original.sprite = persistent.sprite;
            }
        }
        return original;
    }

    export function xmlReadyJson( jsonSprite: object ) {
        var json = JSON.stringify( jsonSprite );
        json = json.replaceAll( /&amp;/gi, "&" );
        json = json.replaceAll( /&/g, "&amp;" );
        return JSON.parse( json );
    }

    export function renderSprite( jsonSprite: object ) {
        var current = LandPlayEdit.lgEditor.renderSprite( LandPlayEdit.xmlReadyJson( jsonSprite ), null, null );
        //TODO replace LandPlayEdit.sprite with the first repetition sprite (grid)
        /** 
         * The first time a sprite turned into a bunch of sprites it's been destroyed
         * So we have to replace LandPlayEdit.sprite with the first of that sprites
         * */
        if ( LandPlayEdit.sprite == null ) {
            //TODO
        }
        var embedings = {};
        var unique = [];
        var sprites = [];
        for ( var key in LandPlayEdit.lgEditor.sprites ) {
            LandPlayEdit.sprite = LandPlayEdit.lgEditor.getPivot( LandPlayEdit.lgEditor.sprites[key] );
            /*
            var group: Phaser.Group = LandPlayEdit.lgEditor.getGroup( LandPlayEdit.lgEditor.sprites[key] );
            if ( group == null ) {
                group = LandPlayEdit.sprite.game.stage.getChildAt( 0 );
            }
            //not the full object yet, let's find it
            var childIndex = group.getChildIndex( LandPlayEdit.sprite );
            LandPlayEdit.sprite = group.getChildAt( childIndex );
            */
            /* just once, same pivot id among grid sprites*/
            if ( unique.indexOf( LandPlayEdit.sprite.id ) < 0 ) {
                unique.push( LandPlayEdit.sprite.id );

                if ( LandPlayEdit.sprite.key != "__default" && LandPlayEdit.sprite.id != null && LandPlayEdit.sprite.id != "" ) {
                    var parent = null;
                    var jsonSprite: object = LandPlayEdit.editor.getSprite( LandPlayEdit.sprite.id );
                    jsonSprite = LandPlayEdit.spriteToJson( LandPlayEdit.sprite, jsonSprite );
                    /** 
                     * sprite text children to JSON
                     **/
                    var withText = false;
                    for ( var i = 0; i < LandPlayEdit.sprite.children.length; i++ ) {
                        var curText = LandPlayEdit.sprite.getChildAt( i );
                        if ( curText.text != null ) {
                            if ( !withText ) {
                                withText = true;
                                jsonSprite.text = [];
                            }
                            jsonSprite.text.push( LandPlayEdit.lgEditor.text2Json( curText ) );
                        }
                    }
                    parent = LandPlayEdit.editor.getSpriteParent( jsonSprite["@id"] );
                    if ( jsonSprite["@id"] == current.sprite["@id"] /*&& children != null*/ ) {
                        /* subnodes from persisting json */
                        var auxSprite = LandPlayEdit.editor.getSprite( LandPlayEdit.sprite.id );
                        if ( auxSprite != null ) {
                            /* LandGame doesn't do it, just kills it*/
                            /**
                             * NO need to delete from sprites collection, just beacause whe are repainting it
                            delete LandPlayEdit.lgEditor.sprites[LandPlayEdit.sprite.id];
                            */
                            if ( auxSprite.grid != null ) {
                                jsonSprite.grid = auxSprite.grid;
                            }
                            if ( auxSprite.layers != null ) {
                                jsonSprite.layers = auxSprite.layers;
                            }
                        }
                        if ( parent != null ) {
                            /* rendering at the top of the functions means that it came out its parent*/
                            LandPlayEdit.lgEditor.sprites[parent["@id"]].addChild( LandPlayEdit.sprite );
                        }
                    }
                    if ( parent != null ) {
                        /* it is an embeded */
                        /* save it to lately embed, since we don't know the order inside LandGame.sprites structure*/
                        if ( embedings[parent["@id"]] == null ) {
                            embedings[parent["@id"]] = [];
                        }
                        embedings[parent["@id"]].push( jsonSprite );
                    } else {
                        sprites.push( jsonSprite );
                    }
                }
            }
        } /*end sprites*/
        /**
         * collect embedings*/
        for ( var key of Object.keys( embedings ) ) {
            LandPlayEdit.editor.addChildren( key, embedings[key] );
        }

        /** 
         * refresh sprite in config container 
         * */
        LandPlayEdit.editor.json.config.layout.sprite = sprites;
        LandPlayEdit.saveData();

    }

    export function saveData() {
        var json = LandPlayEdit.xmlReadyJson( LandPlayEdit.editor.toJSON() );
        localStorage.setItem( LandPlayEdit.storageKey, JSON.stringify( json ) );
    }

    export function deleteSprite() {
        var message = i18next.t( "reveal.sprite.delete" );

        var button = LandPlayEdit.editor.getButton( LandPlayEdit.sprite.id );
        if ( button != null ) {
            var btnType = "";
            for ( var key of Object.keys( button ) ) {
                if ( key != "@sprite" ) {
                    btnType += key + ": '" + button[key] + "',";
                }
            }
            message += i18next.t( "reveal.sprite.button", { button: btnType } );
        }

        LandPlayEdit.confirm( message, function() {
            var group: Phaser.Group = LandPlayEdit.lgEditor.getGroupByName( LandPlayEdit.sprite.id );
            if ( group != null ) {
                group.destroy( true );
            } else {
                LandPlayEdit.sprite.destroy();
            }
            LandPlayEdit.editor.removeSprite( LandPlayEdit.sprite.id );
            if ( button ) {
                LandPlayEdit.editor.deleteButton( LandPlayEdit.sprite.id );
            }
            LandPlayEdit.saveData();
            $( '#spriteEditor' ).foundation( 'close' );
        } );
    }

    export function openChildren() {
        $( '#children-list' ).html( "" );
        /* find exiting generic events*/
        var childrenFound = false;
        var jsonSprite = LandPlayEdit.editor.getSprite( LandPlayEdit.sprite.id );
        if ( jsonSprite != null && jsonSprite.sprite != null ) {
            for ( var i = jsonSprite.sprite.length - 1; i >= 0; i-- ) {
                var sprite = jsonSprite.sprite[i];
                if ( !childrenFound ) {
                    childrenFound = true;
                }
                LandPlayEdit.listRecord( "#children-list", i, sprite["@id"] );
            }
        }
        if ( !childrenFound ) {
            var textDiv = $( '#templates > #tpl_children_no_item > div:first-child' ).clone( true );
            $( "#children-list" ).append( textDiv );
        } else {
            LandPlayEdit.activateList( "children" );
        }
        $( '#spriteEditor' ).foundation( 'close' );
        $( '#childrenEditor' ).foundation( 'open' );
    }

    export function editChildren( pos: number ) {
        var jsonSprite = LandPlayEdit.editor.getSprite( LandPlayEdit.sprite.id );
        if ( jsonSprite != null ) {
            var child = jsonSprite.sprite[pos];
            LandPlayEdit.sprite = LandPlayEdit.lgEditor.sprites[child["@id"]];
            $( '#childrenEditor' ).foundation( 'close' );
            $( '#spriteEditor' ).foundation( 'open' );
            LandPlayEdit.editSprite();
        }
    }

    export function deleteChildren( pos ) {
        LandPlayEdit.confirm( i18next.t( "reveal.children.delete" ), function() {
            var jsonSprite = LandPlayEdit.editor.getSprite( LandPlayEdit.sprite.id );
            var i = parseInt( pos );
            if ( !isNaN( i ) ) {
                if ( jsonSprite.sprite != null && jsonSprite.sprite.length > i ) {
                    var child = jsonSprite.sprite[pos];
                    LandPlayEdit.editor.removeChild( jsonSprite["@id"], child );

                    var childSprite = LandPlayEdit.lgEditor.sprites[child["@id"]];
                    childSprite.game.world.addChild( childSprite );
                    LandPlayEdit.resetSprite( childSprite );
                }
            } else {
                console.warn( "Given position is not a number" );
            }
            $( "#childrenEditor" ).foundation( "close" );
            LandPlayEdit.resetSprite( LandPlayEdit.sprite );
            LandPlayEdit.saveData();
            LandPlayEdit.openChildren();
        } );
    }

}