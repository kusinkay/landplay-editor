Learn and PLay - Game Editor
=
Wysiwyg editor to build the basic environment to create a Learn and Play game.
You will need no XML skills (this time) to create them.

Requirements
-
- Zurb's Foundation (v 6.6.3)
- Font Awesome (v 5.14.0)
- i18next (v 2.0.22)
- https://github.com/mthh/loc-i18next

Thanks
-
- https://www.codexworld.com/file-upload-with-progress-bar-using-jquery-ajax-php/
- https://github.com/evoluteur/colorpicker
- Stefan Goessner for json2xml
- StickyChannel92 for [Open Zone White Slim Cursors](http://www.rw-designer.com/cursor-set/open-zone-white-slim) 