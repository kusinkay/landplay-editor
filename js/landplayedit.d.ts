/// <reference path="../typescript/landplay.d.ts" />
/// <reference path="../typescript/phaser.d.ts" />
/// <reference path="../typescript/jquery.d.ts" />
/// <reference path="translations.d.ts" />
declare module LandPlay {
    enum EditAction {
        Edit = 0,
        Position = 1,
        Text = 2,
        Frames = 3,
        Button = 4,
        Grid = 5,
        Shape = 6,
        Anchor = 7,
        Embed = 8,
    }
    class EditorMode extends LandGame {
        private action;
        private editor;
        /**
         * Callbacks for sprite
         */
        private sprite_click;
        private sprite_drop;
        private sprite_unnamed;
        private minFrameDim;
        textFields: {
            "@key": {
                "attr": string;
                "default": string;
            };
            "@label": {
                "attr": string;
            };
            "@x": {
                "attr": string;
                "default": number;
            };
            "@y": {
                "attr": string;
                "default": number;
            };
        };
        /**
         * The Sprite whose that is being edited*/
        private targetSprite;
        private destinationSprite;
        private bmd;
        private frameInfo;
        private frameCloseSprite;
        private anchor;
        EmptySpriteKey: string;
        ungroupedGridActions: EditAction[];
        private groups;
        /**
         * We can't operate at all with no visible sprites
         * So we stablish an alpha value to emulate visible=false
         * */
        private GhostNumber;
        constructor(xmlFile: string, params?: object);
        renderSprite(spriteNode: object, spParent: Phaser.Sprite, depth?: number, children?: object): object;
        /**
         * @override
         * */
        preRenderSprite(jqXmlnode: any): void;
        newSprite(key: any, params?: object): void;
        initClip(sprite: Phaser.Sprite, params?: object): void;
        private initLiner(sprite);
        showBounds(sprite: Phaser.Sprite, colors?: Array<number>): void;
        clearBounds(sprite: Phaser.Sprite): void;
        getGridPivot(sprite: Phaser.Sprite): any;
        getPivot(sprite: Phaser.Sprite, depth?: number): any;
        getGridGroup(sprite: Phaser.Sprite): any;
        getGroup(sprite: Phaser.Sprite, depth?: number): any;
        private isEmbeded(sprite);
        getGridPivotByName(name: string): any;
        getPivotByName(name: string): any;
        getGridGroupByName(name: string): any;
        getGroupByName(name: string, depth?: number): any;
        text2Json(text: Phaser.Text): {};
        private addDragEvents(sprite);
        setAction(action: LandPlay.EditAction): void;
        locateAllSprites(on: boolean): void;
        isDragable(): boolean;
        isDragableSprite(sprite: Phaser.Sprite): boolean;
        isEditable(): boolean;
        loadConf(): void;
        onConfigLoad(): void;
        update(): void;
        doEmbed(): void;
        private updateEmbed();
        private updateFrames();
        private updateAnchor();
        private clearAnchor();
        drawLine(color: string, xb: number, xe: number, yb: number, ye: number, lineWidth: number): void;
        onStageLoad(): void;
    }
}
declare module LandPlay {
    enum AssetType {
        Image = "image",
        Sound = "sound",
    }
    const EmptyImageAsset = "__EMPTY__";
    class Editor {
        /**
         * @deprecated located directly on json.config.assets
         **/
        private assets;
        private sprites;
        json: {
            "config": {
                "assets": {};
                "setup": {
                    "counter": {};
                    "event": any[];
                };
                "layout": {
                    "sprite": any[];
                };
                "button": any[];
                "retry": {};
            };
        };
        private serverSidePath;
        private frameAtts;
        constructor(args: object);
        getAssets(): {};
        addAsset(args: object, type: AssetType): void;
        updateAsset(args: object, type: AssetType): void;
        setFrames(key: string, frames: object): void;
        getAsset(key: string, type: AssetType): any;
        getAssetPos(key: string, type: AssetType): number;
        getSprite(id: string, sprites?: Array<object>, goDeeper?: boolean): any;
        getSpriteIndex(id: string, sprites?: Array<object>): number;
        removeSprite(id: string, sprites?: Array<object>): void;
        getSpriteParent(id: any): any;
        addChildren(id: string, children: Array<object>): void;
        removeChild(id: string, child: object): void;
        appendForm(formSelector: string): void;
        getSpriteXml(formSelector: string): any;
        getSpriteJson(formSelector: string): {
            "sprite": {};
        };
        addButton(button: object): void;
        getButton(sprite: string, position?: boolean): any;
        deleteButton(sprite: string): void;
        json2xml(json: any): any;
        /**
         * recursive: current config node appending the suffix
         */
        walk(node: object, field: object, ignoreEmpty?: boolean): object;
        parse(json: object, fieldName: string): any;
        toXMLURL(): string;
        toJSON(): {
            "config": {
                "assets": {};
                "setup": {
                    "counter": {};
                    "event": any[];
                };
                "layout": {
                    "sprite": any[];
                };
                "button": any[];
                "retry": {};
            };
        };
        private addAssetAtts(from, to, reverse?);
        fromJSON(json: any): void;
        toXML(indent?: boolean): any;
    }
}
declare module LandPlayEdit {
    class Plugin {
        protected toolBox: {
            behaviour: any[];
        };
        protected effects: any[];
        protected fields: {
            tabs: {
                setup: any[];
                layout: any[];
            };
        };
        static TplPath: string;
        private constraints;
        constructor(params: object);
        completeToolBox(): void;
        private customizeForms();
        private doLoadRemainingSprites(collection, formField, value, layers?, grids?);
        /** Loads the plugin HTML templates to the HTML templates section of the editor page*/
        loadTemplate(): void;
        abstract getTemplateName(): string;
        abstract behaviour(type: any): any;
        /**
         * Preview additional graphic elements
         * Launched from editor when Stage is ready
         * Override in each plugin
         ******* */
        preview(): void;
        validate(): boolean;
        abstract getEventsCollection(container: number, subcontainer: number): any;
        abstract validateEvent(name: any): any;
        abstract editRecord(type: string, pos: number): any;
        abstract deleteRecord(type: string, pos: number): any;
        updateRecord(collection: Array<object>, pos: number, record: object): object;
        modal(template: string, callback: any, params?: object): void;
        appendEffects(selector: string): void;
        protected hasItem(collection: Array<object>, pos: string): boolean;
        protected nextId(collection: Array<object>, field: string): number;
        protected checkSprites(collection: Array<object>, type: string, attrName?: string): void;
        protected checkFrames(sprite: string, length: number): boolean;
        addConstraint(constraint: LandPlayEdit.Constraint): void;
        hasConstraints(): boolean;
        showConstraints(): void;
        resetConstraints(): void;
    }
    class Constraint {
        text: string;
        toolBoxKey: string;
        toolBoxSection: string;
        constructor(toolBoxSection: string, toolBoxKey: string, text: string);
    }
}
declare module LandPlayEdit {
    module PluginFactory {
        function newInstance(mode: LandPlay.Mode): LandPlayEdit.Plugin;
    }
}
declare module LandPlayEdit {
}
declare module LandPlayEdit {
    /**
     * Sprite that its beeing edited right now (one at a time)
     */
    var sprite: Phaser.Sprite;
    var storageKey: string;
    /**
     * Space to temporary save information about the game config
     * */
    var editor: LandPlay.Editor;
    var plugin: LandPlayEdit.Plugin;
    var buttonActionKeys: any[];
    var textSelects: string[];
    var validationSelects: string[];
    function Start(): void;
    function localize(): void;
    function reInitFoundation(): void;
    /**
    * Interactive phaser game/canvas to graphically deal with sprites
    * */
    var lgEditor: LandPlay.EditorMode;
    /**
     * fields that match between sprite form and sprite object
     * */
    var spriteFields: string[];
    /**
     * inverse from textFields, loaded at Start
     */
    var textAttributes: {};
    var textStyleAttributes: any;
    var reservedSprites: string[];
    function initPlugin(): void;
    function editSprite(): void;
    function changeSpritePosition(name: string, value: string): void;
    function sprite_unnamed(sprite: Phaser.Sprite): void;
    function sprite_click(sprite: Phaser.Sprite, info?: object): void;
    function sprite_drop(sprite: any, info?: object): void;
    function listRecord(destSelector: string, i: number, label: string): void;
    function listOrder(destSelector: string, i: number, label: string, top: boolean, bottom: boolean): void;
    function activateList(prefix: string): void;
    function editRecord(type: string, pos: number): void;
    function deleteRecord(type: string, pos: number): void;
    function upRecord(type: string, pos: number): void;
    function downRecord(type: string, pos: number): void;
    function changeDepth(pos: number, offset: number): void;
    function saveJson(): void;
    function clearActivity(): void;
    function resetSprite(sprite: Phaser.Sprite): void;
    function sprite_framed(sprite: Phaser.Sprite, frames: object): void;
    function stage_load(): void;
    function loadLayout(): void;
    function loadPreview(): void;
    function addAssetRow(asset: any, subtype: any): void;
    function scaleSprite(updated: any): void;
    function updateForms(): boolean;
    function createText(): void;
    /**
     * To sprite JSON, as a new text node
     * */
    function updateText(): void;
    function parseStyleFromSelect(fieldName: string): string;
    function editText(pos: number): void;
    function deleteText(pos: any): void;
    function launchFrameEditor(key: string): void;
    function editFrames(asset: object): void;
    function editButton(): void;
    function editRetry(): void;
    function updateRetry(): void;
    function deleteRetry(): void;
    function validateRetry(): void;
    function editCounter(): void;
    function updateCounter(): void;
    function deleteCounter(): void;
    function validateCounter(name: any): void;
    function openEvents(): void;
    function openEventsDo(events: Array<object>, container?: number, excludedCauses?: Array<string>, excludedEffects?: Array<string>, subcontainer?: number, addAll?: boolean, addThis?: boolean): void;
    function createEvent(): void;
    function editEvent(pos: number): void;
    function updateEvent(): void;
    function deleteEvent(pos: any): void;
    function getEventsCollection(): any[];
    function validateEvent(name: any): void;
    function toggleField(name: string, required: boolean, pattern?: string, errMessage?: string): void;
    function editGrid(): void;
    function updateGrid(): void;
    function deleteGrid(): void;
    function validateGrid(name: any): void;
    function openShapes(): void;
    function createShape(): void;
    function updateShape(): void;
    function editShape(pos: number): void;
    function deleteShape(pos: any): void;
    function validateShape(name: any): void;
    function openDepth(): void;
    function editValidation(): void;
    function updateValidation(): void;
    function deleteValidation(): void;
    function validateValidation(name: any): void;
    function notEmptySelectValidator($el: any, required: any, parent: any): boolean;
    function reloadTextSelect(selector: string, value?: string, force?: boolean): void;
    function reloadSpriteSelect(selector: string, value: string, force?: boolean, extended?: boolean, exceptions?: Array<string>, layers?: boolean, grids?: boolean, addThis?: boolean): void;
    function groupSprites(selector: string, value: string, key: string, contName: string, setOptionAndGetKey: any): void;
    function reloadSoundSelect(selector: any, value?: string, force?: boolean): void;
    function reloadImageSelect(selector: any, value?: string, force?: boolean): void;
    function reloadAssetsSelect(type: LandPlay.AssetType, selector: any, value?: string, force?: boolean): void;
    function behaviour(type: any): void;
    function success(text: any): void;
    function warning(text: any): void;
    function alert(text: any): void;
    function confirm(text: string, yes: any, no?: any, end?: any): boolean;
    function loadStyleEditorTemplate(destination: any, fieldName?: string): void;
    function loadDefaultTextStyle(): void;
    function loadStyleOption(attr: string, value: string): void;
    function addTextStyle(): void;
    function editTextStyle(): void;
    function delTextStyle(): void;
    function addTool(tool: object, group: string): void;
    function changeButtonType(): void;
    function parseFormButton(): {};
    function updateButton(): void;
    function resetButtonForm(): void;
    function updateSprite(): void;
    function getGroup(sprite: Phaser.Sprite): Phaser.Group;
    function spriteToJson(sprite: Phaser.Sprite, oSprite?: object): {};
    /**
     * Running objects (original: from a Phaser.Sprite) don't have persistent reflected properties
     * Copy them from "persistent" to "original"
     * */
    function mergeSprites(original: object, persistent: object): object;
    function xmlReadyJson(jsonSprite: object): any;
    function renderSprite(jsonSprite: object): void;
    function saveData(): void;
    function deleteSprite(): void;
    function openChildren(): void;
    function editChildren(pos: number): void;
    function deleteChildren(pos: any): void;
}
declare module LandPlayEdit {
    module i18n {
        const ca_ES: {
            mainMenu: {
                tabs: {
                    assets: string;
                    setup: string;
                    layout: string;
                    preview: string;
                };
            };
            button: {
                update: string;
                delete: string;
            };
            validator: {
                required: string;
                number: {
                    expected: string;
                    required: string;
                };
                alpha_numeric: {
                    expected: string;
                    required: string;
                };
                identifier: {
                    expected: string;
                    required: string;
                };
                gtzero: {
                    expected: string;
                    required: string;
                };
                selection: {
                    expected: string;
                    required: string;
                };
                color: {
                    expected: string;
                    required: string;
                };
                dimension: {
                    expected: string;
                    required: string;
                };
                option: {
                    eqzero: string;
                };
                boolean: {
                    required: string;
                };
            };
            option: {
                dimension: {
                    pixels: string;
                    percentage: string;
                };
                boolean: {
                    yes: string;
                    no: string;
                };
                confirm: {
                    yes: string;
                    no: string;
                };
                outbound: {
                    cycle: string;
                    rebound: string;
                };
                pickable: {
                    true: string;
                    false: string;
                };
                async: {
                    true: string;
                    false: string;
                };
                mode: {
                    Arcade: {
                        label: string;
                    };
                    Catch: {
                        label: string;
                    };
                    DragDrop: {
                        label: string;
                    };
                    Paint: {
                        label: string;
                    };
                    Puzzle: {
                        label: string;
                    };
                    Simon: {
                        label: string;
                    };
                    Type: {
                        label: string;
                    };
                    WordSearch: {
                        label: string;
                    };
                };
                cause: {
                    right: {
                        label: string;
                    };
                    wrong: {
                        label: string;
                    };
                    tryout: {
                        label: string;
                    };
                    complete: {
                        label: string;
                    };
                    hint: {
                        label: string;
                    };
                    timeout: {
                        label: string;
                    };
                };
                effect: {
                    visible: {
                        label: string;
                    };
                    back: {
                        label: string;
                    };
                    runaway: {
                        label: string;
                    };
                    playsound: {
                        label: string;
                    };
                    lock: {
                        label: string;
                    };
                    addpoints: {
                        label: string;
                    };
                    destroy: {
                        label: string;
                    };
                    pause: {
                        label: string;
                    };
                    solve: {
                        label: string;
                    };
                    validate: {
                        label: string;
                    };
                };
                case: {
                    uppercase: string;
                    lowercase: string;
                    capitalize: string;
                };
            };
            editorForm: {
                assets: {
                    origin: {
                        title: string;
                        help: string;
                        upload: {
                            label: string;
                            help: string;
                        };
                        url: {
                            label: string;
                            help: string;
                        };
                    };
                    file: {
                        label: string;
                    };
                    url: {
                        label: string;
                    };
                    key: {
                        label: string;
                    };
                    upload: {
                        label: string;
                        value: string;
                    };
                    images: {
                        header: string;
                    };
                    sounds: {
                        header: string;
                    };
                };
                setup: {
                    mode: {
                        label: string;
                    };
                    width: {
                        label: string;
                    };
                    height: {
                        label: string;
                    };
                };
                layout: {
                    background: {
                        label: string;
                    };
                    loadLayout: {
                        label: string;
                    };
                    togglePanels: {
                        label: string;
                    };
                    sidePanels: {
                        header: string;
                    };
                    toolbox: {
                        header: string;
                        setup: {
                            header: string;
                        };
                        sprites: {
                            header: string;
                        };
                        behaviour: {
                            header: string;
                        };
                        Edit: {
                            tooltip: string;
                        };
                        Position: {
                            tooltip: string;
                        };
                        Text: {
                            tooltip: string;
                        };
                        Button: {
                            tooltip: string;
                        };
                        locate: {
                            tooltip: string;
                        };
                        Grid: {
                            tooltip: string;
                        };
                        Shape: {
                            tooltip: string;
                        };
                        retry: {
                            tooltip: string;
                        };
                        counter: {
                            tooltip: string;
                        };
                        events: {
                            tooltip: string;
                        };
                        frames: {
                            tooltip: string;
                        };
                        Depth: {
                            tooltip: string;
                        };
                        validation: {
                            tooltip: string;
                        };
                        Anchor: {
                            tooltip: string;
                        };
                        Embed: {
                            tooltip: string;
                        };
                        save: {
                            tooltip: string;
                        };
                        clear: {
                            tooltip: string;
                        };
                    };
                };
            };
            reveal: {
                generic: {
                    clear: string;
                };
                text: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    key: {
                        label: string;
                    };
                    label: {
                        label: string;
                    };
                    x: {
                        label: string;
                    };
                    y: {
                        label: string;
                    };
                    style: {
                        label: string;
                        value: string;
                        applied: string;
                        delete: string;
                    };
                };
                event: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    cause: {
                        label: string;
                        help: string;
                    };
                    effect: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                        all: string;
                        this: string;
                    };
                    value: {
                        label: string;
                    };
                };
                shape: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    type: {
                        label: string;
                    };
                    color: {
                        label: string;
                    };
                    radius: {
                        label: string;
                    };
                    width: {
                        label: string;
                    };
                    height: {
                        label: string;
                    };
                    line: {
                        width: {
                            label: string;
                        };
                        color: {
                            label: string;
                        };
                    };
                };
                sprite: {
                    delete: string;
                    embed: string;
                    id: {
                        label: string;
                    };
                    visible: {
                        label: string;
                    };
                    width: {
                        label: string;
                    };
                    height: {
                        label: string;
                    };
                    x: {
                        label: string;
                    };
                    marginRight: {
                        label: string;
                    };
                    y: {
                        label: string;
                    };
                    marginBottom: {
                        label: string;
                    };
                    tint: {
                        label: string;
                    };
                    angle: {
                        label: string;
                    };
                    frame: {
                        label: string;
                    };
                    layers: {
                        from: {
                            label: string;
                        };
                        to: {
                            label: string;
                        };
                    };
                    children: {
                        label: string;
                    };
                };
                frame: {
                    nav: {
                        previous: string;
                        next: string;
                    };
                };
                button: {
                    type: {
                        label: string;
                    };
                    value: {
                        label: string;
                    };
                };
                retry: {
                    delete: string;
                    retries: {
                        label: string;
                        help: string;
                        info: string;
                    };
                    points: {
                        legend: string;
                    };
                    from: {
                        label: string;
                    };
                    to: {
                        label: string;
                    };
                    step: {
                        label: string;
                    };
                    validator: {
                        from: string;
                        to: string;
                        toeqfrom: string;
                    };
                };
                counter: {
                    delete: string;
                    live: {
                        text: {
                            label: string;
                            help: string;
                        };
                    };
                    points: {
                        text: {
                            label: string;
                            help: string;
                        };
                    };
                    time: {
                        legend: string;
                        text: {
                            label: string;
                            help: string;
                        };
                        secs: {
                            label: string;
                        };
                    };
                    progress: {
                        sprite: {
                            label: string;
                            help: string;
                        };
                    };
                };
                grid: {
                    delete: string;
                    xoffset: {
                        label: string;
                        help: string;
                    };
                    yoffset: {
                        label: string;
                        help: string;
                    };
                    cols: {
                        label: string;
                        help: string;
                    };
                    length: {
                        label: string;
                    };
                    selection: {
                        label: string;
                    };
                };
                piece: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    outbound: {
                        label: string;
                        help: string;
                    };
                    movex: {
                        label: string;
                    };
                    movey: {
                        label: string;
                    };
                };
                target: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    events: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    sound: {
                        label: string;
                    };
                };
                weapon: {
                    sprite: {
                        label: string;
                    };
                    pickable: {
                        label: string;
                    };
                    brushpoint: {
                        label: string;
                        help: string;
                    };
                    toolcase: {
                        label: string;
                        help: string;
                    };
                };
                children: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                };
                container: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                        help: string;
                        empty: string;
                    };
                    random: {
                        label: string;
                        help: string;
                    };
                    position: {
                        help: string;
                    };
                    x: {
                        label: string;
                    };
                    y: {
                        label: string;
                    };
                    offset: {
                        help: string;
                    };
                    xoffset: {
                        label: string;
                    };
                    yoffset: {
                        label: string;
                    };
                    values: {
                        label: string;
                        help: string;
                    };
                    extraValues: {
                        label: string;
                        help: string;
                    };
                    answer: {
                        available: {
                            label: string;
                            help: string;
                        };
                        selected: {
                            label: string;
                            help: string;
                        };
                    };
                };
                board: {
                    delete: string;
                    table: {
                        label: string;
                        help: string;
                        random: {
                            label: string;
                            help: string;
                        };
                    };
                    available: {
                        label: string;
                        help: string;
                    };
                    selected: {
                        label: string;
                        help: string;
                    };
                };
                depth: {
                    empty: string;
                    list: {
                        header: string;
                    };
                    reload: {
                        label: string;
                    };
                };
                validation: {
                    delete: string;
                    async: {
                        legend: string;
                    };
                    marks: {
                        legend: string;
                    };
                    right: {
                        key: {
                            label: string;
                            help: string;
                        };
                    };
                    wrong: {
                        key: {
                            label: string;
                            help: string;
                        };
                    };
                    top: {
                        label: string;
                        help: string;
                    };
                    left: {
                        label: string;
                        help: string;
                    };
                    width: {
                        label: string;
                        help: string;
                    };
                    height: {
                        label: string;
                        help: string;
                    };
                };
                color: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    color: {
                        label: string;
                    };
                };
                canvas: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    color: {
                        label: string;
                    };
                };
                brush: {
                    sprite: {
                        label: string;
                    };
                    pickable: {
                        label: string;
                    };
                };
                simon: {
                    transition: {
                        label: string;
                        help: string;
                    };
                    maxseq: {
                        label: string;
                        help: string;
                    };
                    talk: {
                        label: string;
                        help: string;
                    };
                };
                pad: {
                    delete: string;
                    sprite: {
                        label: string;
                        help: string;
                    };
                    sound: {
                        label: string;
                        help: string;
                    };
                    selected: {
                        label: string;
                        help: string;
                    };
                };
                stage: {
                    speed: {
                        label: string;
                        help: string;
                    };
                    craftspeed: {
                        label: string;
                        help: string;
                    };
                    runawaySpeed: {
                        label: string;
                        help: string;
                    };
                    debug: {
                        label: string;
                        help: string;
                    };
                    craft: {
                        label: string;
                        help: string;
                    };
                    floor2d: {
                        label: string;
                        help: string;
                    };
                    ygravity: {
                        label: string;
                        help: string;
                    };
                    movex: {
                        label: string;
                        help: string;
                    };
                    movey: {
                        label: string;
                        help: string;
                    };
                    "y-scale": {
                        label: string;
                        help: string;
                    };
                    tolerance: {
                        label: string;
                        help: string;
                    };
                    "y-jump": {
                        label: string;
                        help: string;
                    };
                    "jump-touch": {
                        label: string;
                        help: string;
                    };
                    ybounce: {
                        label: string;
                        help: string;
                    };
                    animations: {
                        legend: string;
                    };
                    move: {
                        label: string;
                        help: string;
                    };
                    jump: {
                        label: string;
                        help: string;
                    };
                    forward: {
                        label: string;
                        help: string;
                    };
                    up: {
                        label: string;
                        help: string;
                    };
                    down: {
                        label: string;
                        help: string;
                    };
                    back: {
                        label: string;
                        help: string;
                    };
                };
                asteroid: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    events: {
                        label: string;
                    };
                    attach: {
                        label: string;
                    };
                    multiplicity: {
                        label: string;
                        help: string;
                    };
                    cycle: {
                        label: string;
                        help: string;
                    };
                    movex: {
                        label: string;
                        help: string;
                    };
                    movey: {
                        label: string;
                        help: string;
                    };
                    tics: {
                        label: string;
                        help: string;
                    };
                    xdif: {
                        label: string;
                        help: string;
                    };
                    ydif: {
                        label: string;
                        help: string;
                    };
                    ygravity: {
                        label: string;
                        help: string;
                    };
                    basedepth: {
                        label: string;
                        help: string;
                    };
                    tolerance: {
                        label: string;
                        help: string;
                    };
                };
                landscape: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    moveX: {
                        label: string;
                        help: string;
                    };
                    foreground: {
                        label: string;
                        help: string;
                    };
                };
                word: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    id: {
                        label: string;
                        help: string;
                    };
                    key: {
                        label: string;
                        help: string;
                    };
                    sprite: {
                        label: string;
                        help: string;
                    };
                    x: {
                        label: string;
                        help: string;
                    };
                    y: {
                        label: string;
                        help: string;
                    };
                    text: {
                        label: string;
                        help: string;
                    };
                    opentext: {
                        label: string;
                        help: string;
                        row: string;
                    };
                    split: {
                        legend: string;
                        help: string;
                        offsetx: {
                            label: string;
                            help: string;
                        };
                        offsety: {
                            label: string;
                            help: string;
                        };
                    };
                    offset: {
                        legend: string;
                    };
                    offsetx: {
                        label: string;
                        help: string;
                    };
                    offsety: {
                        label: string;
                        help: string;
                    };
                };
                tiles: {
                    case: {
                        label: string;
                        help: string;
                    };
                    width: {
                        label: string;
                        help: string;
                    };
                    height: {
                        label: string;
                        help: string;
                    };
                    cols: {
                        label: string;
                        help: string;
                    };
                    rows: {
                        label: string;
                        help: string;
                    };
                    color: {
                        label: string;
                        right: {
                            label: string;
                        };
                    };
                    colors: {
                        label: string;
                        help: string;
                        right: {
                            label: string;
                            help: string;
                        };
                        current: {
                            label: string;
                            help: string;
                        };
                    };
                };
                hiddenword: {
                    text: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                        help: string;
                    };
                    x: {
                        label: string;
                        help: string;
                    };
                    y: {
                        label: string;
                        help: string;
                    };
                    orientation: {
                        label: string;
                    };
                    preview: {
                        label: string;
                    };
                    check: {
                        label: string;
                    };
                };
            };
            plugin: {
                loaded: string;
                constraint: {
                    header: string;
                    catch: {
                        target: string;
                        weapon: string;
                        piece: string;
                        runaway: string;
                    };
                    paint: {
                        canvas: string;
                        brush: string;
                        color: string;
                        noColorSprites: string;
                    };
                    validation: {
                        required: string;
                        noButton: string;
                    };
                    sprite: {
                        notFound: string;
                    };
                    dragdrop: {
                        container: string;
                        nosprite: string;
                        event: {
                            zero: string;
                            gtone: string;
                        };
                    };
                    puzzle: {
                        piece: string;
                    };
                    simon: {
                        pad: {
                            nosprite: string;
                            nosound: string;
                            notenough: string;
                            noframes: string;
                        };
                        noprogress: string;
                        talk: {
                            noframes: string;
                        };
                    };
                    arcade: {
                        craft: string;
                        piece: {
                            empty: string;
                            noevents: string;
                        };
                    };
                    type: {
                        word: string;
                    };
                    wordsearch: {
                        hiddenword: string;
                        notextsprite: string;
                    };
                };
                toolbox: {
                    catch: {
                        piece: {
                            label: string;
                        };
                        target: {
                            label: string;
                        };
                        weapon: {
                            label: string;
                        };
                    };
                    paint: {
                        brush: {
                            label: string;
                        };
                        color: {
                            label: string;
                        };
                        canvas: {
                            label: string;
                        };
                    };
                    dragdrop: {
                        container: {
                            label: string;
                        };
                    };
                    puzzle: {
                        board: {
                            label: string;
                        };
                    };
                    simon: {
                        pad: {
                            label: string;
                        };
                    };
                    arcade: {
                        stage: {
                            label: string;
                        };
                        asteroid: {
                            label: string;
                        };
                        edge: {
                            label: string;
                        };
                        landscape: {
                            label: string;
                        };
                    };
                    type: {
                        word: {
                            label: string;
                        };
                    };
                    wordsearch: {
                        tiles: {
                            label: string;
                        };
                        hiddenword: {
                            label: string;
                        };
                        decorate: {
                            label: string;
                        };
                    };
                };
                field: {
                    setup: {
                        speed: {
                            label: string;
                            help: string;
                        };
                        runawaySpeed: {
                            label: string;
                            help: string;
                        };
                    };
                };
            };
            constraint: {
                asset: {
                    none: string;
                    keyExists: string;
                    novalid: string;
                    layers: {
                        unmatch: string;
                        nonconsecutive: string;
                        justimage: string;
                    };
                };
            };
            alert: {
                upload: {
                    failed: string;
                    novalid: string;
                };
                download: {
                    failed: string;
                    write: string;
                    nourl: string;
                };
                sprite: {
                    exists: string;
                };
                preview: {
                    nomode: string;
                };
            };
            warning: {
                color: {
                    empty: string;
                };
            };
            success: {
                upload: string;
                anchor: string;
            };
        };
    }
}
declare module LandPlayEdit {
    module i18n {
        const en_US: {
            mainMenu: {
                tabs: {
                    assets: string;
                    setup: string;
                    layout: string;
                    preview: string;
                };
            };
            button: {
                update: string;
                delete: string;
            };
            validator: {
                required: string;
                number: {
                    expected: string;
                    required: string;
                };
                alpha_numeric: {
                    expected: string;
                    required: string;
                };
                identifier: {
                    expected: string;
                    required: string;
                };
                gtzero: {
                    expected: string;
                    required: string;
                };
                selection: {
                    expected: string;
                    required: string;
                };
                color: {
                    expected: string;
                    required: string;
                };
                dimension: {
                    expected: string;
                    required: string;
                };
                option: {
                    eqzero: string;
                };
                boolean: {
                    required: string;
                };
            };
            option: {
                dimension: {
                    pixels: string;
                    percentage: string;
                };
                boolean: {
                    yes: string;
                    no: string;
                };
                confirm: {
                    yes: string;
                    no: string;
                };
                outbound: {
                    cycle: string;
                    rebound: string;
                };
                pickable: {
                    true: string;
                    false: string;
                };
                async: {
                    true: string;
                    false: string;
                };
                mode: {
                    Arcade: {
                        label: string;
                    };
                    Catch: {
                        label: string;
                    };
                    DragDrop: {
                        label: string;
                    };
                    Paint: {
                        label: string;
                    };
                    Puzzle: {
                        label: string;
                    };
                    Simon: {
                        label: string;
                    };
                    Type: {
                        label: string;
                    };
                    WordSearch: {
                        label: string;
                    };
                };
                cause: {
                    right: {
                        label: string;
                    };
                    wrong: {
                        label: string;
                    };
                    tryout: {
                        label: string;
                    };
                    complete: {
                        label: string;
                    };
                    hint: {
                        label: string;
                    };
                    timeout: {
                        label: string;
                    };
                };
                effect: {
                    visible: {
                        label: string;
                    };
                    back: {
                        label: string;
                    };
                    runaway: {
                        label: string;
                    };
                    playsound: {
                        label: string;
                    };
                    lock: {
                        label: string;
                    };
                    addpoints: {
                        label: string;
                    };
                    destroy: {
                        label: string;
                    };
                    pause: {
                        label: string;
                    };
                    solve: {
                        label: string;
                    };
                    validate: {
                        label: string;
                    };
                };
                case: {
                    uppercase: string;
                    lowercase: string;
                    capitalize: string;
                };
            };
            editorForm: {
                assets: {
                    origin: {
                        title: string;
                        help: string;
                        upload: {
                            label: string;
                            help: string;
                        };
                        url: {
                            label: string;
                            help: string;
                        };
                    };
                    file: {
                        label: string;
                    };
                    url: {
                        label: string;
                    };
                    key: {
                        label: string;
                    };
                    upload: {
                        label: string;
                        value: string;
                    };
                    images: {
                        header: string;
                    };
                    sounds: {
                        header: string;
                    };
                };
                setup: {
                    mode: {
                        label: string;
                    };
                    width: {
                        label: string;
                    };
                    height: {
                        label: string;
                    };
                };
                layout: {
                    background: {
                        label: string;
                    };
                    loadLayout: {
                        label: string;
                    };
                    togglePanels: {
                        label: string;
                    };
                    sidePanels: {
                        header: string;
                    };
                    toolbox: {
                        header: string;
                        setup: {
                            header: string;
                        };
                        sprites: {
                            header: string;
                        };
                        behaviour: {
                            header: string;
                        };
                        Edit: {
                            tooltip: string;
                        };
                        Position: {
                            tooltip: string;
                        };
                        Text: {
                            tooltip: string;
                        };
                        Button: {
                            tooltip: string;
                        };
                        locate: {
                            tooltip: string;
                        };
                        Grid: {
                            tooltip: string;
                        };
                        Shape: {
                            tooltip: string;
                        };
                        retry: {
                            tooltip: string;
                        };
                        counter: {
                            tooltip: string;
                        };
                        events: {
                            tooltip: string;
                        };
                        frames: {
                            tooltip: string;
                        };
                        Depth: {
                            tooltip: string;
                        };
                        validation: {
                            tooltip: string;
                        };
                        Anchor: {
                            tooltip: string;
                        };
                        Embed: {
                            tooltip: string;
                        };
                        save: {
                            tooltip: string;
                        };
                        clear: {
                            tooltip: string;
                        };
                    };
                };
            };
            reveal: {
                generic: {
                    clear: string;
                };
                text: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    key: {
                        label: string;
                    };
                    label: {
                        label: string;
                    };
                    x: {
                        label: string;
                    };
                    y: {
                        label: string;
                    };
                    style: {
                        label: string;
                        value: string;
                        applied: string;
                        delete: string;
                    };
                };
                event: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    cause: {
                        label: string;
                        help: string;
                    };
                    effect: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                        all: string;
                        this: string;
                    };
                    value: {
                        label: string;
                    };
                };
                shape: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    type: {
                        label: string;
                    };
                    color: {
                        label: string;
                    };
                    radius: {
                        label: string;
                    };
                    width: {
                        label: string;
                    };
                    height: {
                        label: string;
                    };
                    line: {
                        width: {
                            label: string;
                        };
                        color: {
                            label: string;
                        };
                    };
                };
                sprite: {
                    delete: string;
                    button: string;
                    embed: string;
                    id: {
                        label: string;
                    };
                    visible: {
                        label: string;
                    };
                    width: {
                        label: string;
                    };
                    height: {
                        label: string;
                    };
                    x: {
                        label: string;
                    };
                    marginRight: {
                        label: string;
                    };
                    y: {
                        label: string;
                    };
                    marginBottom: {
                        label: string;
                    };
                    tint: {
                        label: string;
                    };
                    angle: {
                        label: string;
                    };
                    frame: {
                        label: string;
                    };
                    layers: {
                        from: {
                            label: string;
                        };
                        to: {
                            label: string;
                        };
                    };
                    children: {
                        label: string;
                    };
                };
                frame: {
                    nav: {
                        previous: string;
                        next: string;
                    };
                };
                button: {
                    type: {
                        label: string;
                    };
                    value: {
                        label: string;
                    };
                };
                retry: {
                    delete: string;
                    retries: {
                        label: string;
                        help: string;
                        info: string;
                    };
                    points: {
                        legend: string;
                    };
                    from: {
                        label: string;
                    };
                    to: {
                        label: string;
                    };
                    step: {
                        label: string;
                    };
                    validator: {
                        from: string;
                        to: string;
                        toeqfrom: string;
                    };
                };
                counter: {
                    delete: string;
                    live: {
                        text: {
                            label: string;
                            help: string;
                        };
                    };
                    points: {
                        text: {
                            label: string;
                            help: string;
                        };
                    };
                    time: {
                        legend: string;
                        text: {
                            label: string;
                            help: string;
                        };
                        secs: {
                            label: string;
                        };
                    };
                    progress: {
                        sprite: {
                            label: string;
                            help: string;
                        };
                    };
                };
                grid: {
                    delete: string;
                    xoffset: {
                        label: string;
                        help: string;
                    };
                    yoffset: {
                        label: string;
                        help: string;
                    };
                    cols: {
                        label: string;
                        help: string;
                    };
                    length: {
                        label: string;
                    };
                    selection: {
                        label: string;
                    };
                };
                piece: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    outbound: {
                        label: string;
                        help: string;
                    };
                    movex: {
                        label: string;
                    };
                    movey: {
                        label: string;
                    };
                };
                target: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    events: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    sound: {
                        label: string;
                    };
                };
                weapon: {
                    sprite: {
                        label: string;
                    };
                    pickable: {
                        label: string;
                    };
                };
                depth: {
                    empty: string;
                    list: {
                        header: string;
                    };
                    reload: {
                        label: string;
                    };
                };
                validation: {
                    delete: string;
                    async: {
                        legend: string;
                    };
                    marks: {
                        legend: string;
                    };
                    right: {
                        key: {
                            label: string;
                            help: string;
                        };
                    };
                    wrong: {
                        key: {
                            label: string;
                            help: string;
                        };
                    };
                    top: {
                        label: string;
                        help: string;
                    };
                    left: {
                        label: string;
                        help: string;
                    };
                    width: {
                        label: string;
                        help: string;
                    };
                    height: {
                        label: string;
                        help: string;
                    };
                };
                color: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    color: {
                        label: string;
                    };
                };
                canvas: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    color: {
                        label: string;
                    };
                };
                brush: {
                    sprite: {
                        label: string;
                        help: string;
                    };
                    pickable: {
                        label: string;
                    };
                    brushpoint: {
                        label: string;
                        help: string;
                    };
                    toolcase: {
                        label: string;
                        help: string;
                    };
                };
                children: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                };
                container: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                        help: string;
                        empty: string;
                    };
                    random: {
                        label: string;
                        help: string;
                    };
                    position: {
                        help: string;
                    };
                    x: {
                        label: string;
                    };
                    y: {
                        label: string;
                    };
                    offset: {
                        help: string;
                    };
                    xoffset: {
                        label: string;
                    };
                    yoffset: {
                        label: string;
                    };
                    values: {
                        label: string;
                        help: string;
                    };
                    extraValues: {
                        label: string;
                        help: string;
                    };
                    answer: {
                        available: {
                            label: string;
                            help: string;
                        };
                        selected: {
                            label: string;
                            help: string;
                        };
                    };
                };
                board: {
                    delete: string;
                    table: {
                        label: string;
                        help: string;
                        random: {
                            label: string;
                            help: string;
                        };
                    };
                    available: {
                        label: string;
                        help: string;
                    };
                    selected: {
                        label: string;
                        help: string;
                    };
                };
                simon: {
                    transition: {
                        label: string;
                        help: string;
                    };
                    maxseq: {
                        label: string;
                        help: string;
                    };
                    talk: {
                        label: string;
                        help: string;
                    };
                };
                pad: {
                    delete: string;
                    sprite: {
                        label: string;
                        help: string;
                    };
                    sound: {
                        label: string;
                        help: string;
                    };
                    selected: {
                        label: string;
                        help: string;
                    };
                };
                stage: {
                    speed: {
                        label: string;
                        help: string;
                    };
                    craftspeed: {
                        label: string;
                        help: string;
                    };
                    runawaySpeed: {
                        label: string;
                        help: string;
                    };
                    debug: {
                        label: string;
                        help: string;
                    };
                    craft: {
                        label: string;
                        help: string;
                    };
                    floor2d: {
                        label: string;
                        help: string;
                    };
                    ygravity: {
                        label: string;
                        help: string;
                    };
                    movex: {
                        label: string;
                        help: string;
                    };
                    movey: {
                        label: string;
                        help: string;
                    };
                    "y-scale": {
                        label: string;
                        help: string;
                    };
                    tolerance: {
                        label: string;
                        help: string;
                    };
                    "y-jump": {
                        label: string;
                        help: string;
                    };
                    "jump-touch": {
                        label: string;
                        help: string;
                    };
                    ybounce: {
                        label: string;
                        help: string;
                    };
                    animations: {
                        legend: string;
                    };
                    move: {
                        label: string;
                        help: string;
                    };
                    jump: {
                        label: string;
                        help: string;
                    };
                    forward: {
                        label: string;
                        help: string;
                    };
                    up: {
                        label: string;
                        help: string;
                    };
                    down: {
                        label: string;
                        help: string;
                    };
                    back: {
                        label: string;
                        help: string;
                    };
                };
                edge: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    tolerance: {
                        label: string;
                        help: string;
                    };
                };
                asteroid: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    events: {
                        label: string;
                    };
                    attach: {
                        label: string;
                    };
                    multiplicity: {
                        label: string;
                        help: string;
                    };
                    cycle: {
                        label: string;
                        help: string;
                    };
                    movex: {
                        label: string;
                        help: string;
                    };
                    movey: {
                        label: string;
                        help: string;
                    };
                    tics: {
                        label: string;
                        help: string;
                    };
                    xdif: {
                        label: string;
                        help: string;
                    };
                    ydif: {
                        label: string;
                        help: string;
                    };
                    ygravity: {
                        label: string;
                        help: string;
                    };
                    basedepth: {
                        label: string;
                        help: string;
                    };
                    tolerance: {
                        label: string;
                        help: string;
                    };
                };
                landscape: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    moveX: {
                        label: string;
                        help: string;
                    };
                    foreground: {
                        label: string;
                        help: string;
                    };
                };
                word: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    id: {
                        label: string;
                        help: string;
                    };
                    key: {
                        label: string;
                        help: string;
                    };
                    sprite: {
                        label: string;
                        help: string;
                    };
                    x: {
                        label: string;
                        help: string;
                    };
                    y: {
                        label: string;
                        help: string;
                    };
                    text: {
                        label: string;
                        help: string;
                    };
                    opentext: {
                        label: string;
                        help: string;
                        row: string;
                    };
                    split: {
                        legend: string;
                        help: string;
                        offsetx: {
                            label: string;
                            help: string;
                        };
                        offsety: {
                            label: string;
                            help: string;
                        };
                    };
                    offset: {
                        legend: string;
                    };
                    offsetx: {
                        label: string;
                        help: string;
                    };
                    offsety: {
                        label: string;
                        help: string;
                    };
                };
                tiles: {
                    case: {
                        label: string;
                        help: string;
                    };
                    width: {
                        label: string;
                        help: string;
                    };
                    height: {
                        label: string;
                        help: string;
                    };
                    cols: {
                        label: string;
                        help: string;
                    };
                    rows: {
                        label: string;
                        help: string;
                    };
                    color: {
                        label: string;
                        help: string;
                        right: {
                            label: string;
                        };
                    };
                    colors: {
                        label: string;
                        help: string;
                        right: {
                            label: string;
                            help: string;
                        };
                        current: {
                            label: string;
                            help: string;
                        };
                    };
                };
                hiddenword: {
                    text: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                        help: string;
                    };
                    x: {
                        label: string;
                        help: string;
                    };
                    y: {
                        label: string;
                        help: string;
                    };
                    orientation: {
                        label: string;
                    };
                    preview: {
                        label: string;
                    };
                    check: {
                        label: string;
                    };
                };
            };
            plugin: {
                loaded: string;
                constraint: {
                    header: string;
                    catch: {
                        target: string;
                        weapon: string;
                        piece: string;
                        runaway: string;
                    };
                    paint: {
                        canvas: string;
                        brush: string;
                        color: string;
                        noColorSprites: string;
                    };
                    validation: {
                        required: string;
                        noButton: string;
                    };
                    sprite: {
                        notFound: string;
                    };
                    dragdrop: {
                        container: string;
                        nosprite: string;
                        event: {
                            zero: string;
                            gtone: string;
                        };
                    };
                    puzzle: {
                        piece: string;
                    };
                    simon: {
                        pad: {
                            nosprite: string;
                            nosound: string;
                            notenough: string;
                            noframes: string;
                        };
                        noprogress: string;
                        talk: {
                            noframes: string;
                        };
                    };
                    arcade: {
                        craft: string;
                        piece: {
                            empty: string;
                            noevents: string;
                        };
                    };
                    type: {
                        word: string;
                    };
                    wordsearch: {
                        hiddenword: string;
                        notextsprite: string;
                    };
                };
                toolbox: {
                    catch: {
                        piece: {
                            label: string;
                        };
                        target: {
                            label: string;
                        };
                        weapon: {
                            label: string;
                        };
                    };
                    paint: {
                        brush: {
                            label: string;
                        };
                        color: {
                            label: string;
                        };
                        canvas: {
                            label: string;
                        };
                    };
                    dragdrop: {
                        container: {
                            label: string;
                        };
                    };
                    puzzle: {
                        board: {
                            label: string;
                        };
                    };
                    simon: {
                        pad: {
                            label: string;
                        };
                    };
                    arcade: {
                        stage: {
                            label: string;
                        };
                        asteroid: {
                            label: string;
                        };
                        edge: {
                            label: string;
                        };
                        landscape: {
                            label: string;
                        };
                    };
                    type: {
                        word: {
                            label: string;
                        };
                    };
                    wordsearch: {
                        tiles: {
                            label: string;
                        };
                        hiddenword: {
                            label: string;
                        };
                        decorate: {
                            label: string;
                        };
                    };
                };
                field: {
                    setup: {
                        speed: {
                            label: string;
                            help: string;
                        };
                        runawaySpeed: {
                            label: string;
                            help: string;
                        };
                    };
                };
            };
            constraint: {
                asset: {
                    none: string;
                    keyExists: string;
                    novalid: string;
                    layers: {
                        unmatch: string;
                        nonconsecutive: string;
                        justimage: string;
                    };
                };
            };
            alert: {
                upload: {
                    failed: string;
                    novalid: string;
                };
                download: {
                    failed: string;
                    write: string;
                    nourl: string;
                };
                sprite: {
                    exists: string;
                };
                preview: {
                    nomode: string;
                };
            };
            warning: {
                color: {
                    empty: string;
                };
            };
            success: {
                upload: string;
                anchor: string;
            };
        };
    }
}
declare module LandPlayEdit {
    module i18n {
        const es_ES: {
            mainMenu: {
                tabs: {
                    assets: string;
                    setup: string;
                    layout: string;
                    preview: string;
                };
            };
            button: {
                update: string;
                delete: string;
            };
            validator: {
                required: string;
                number: {
                    expected: string;
                    required: string;
                };
                alpha_numeric: {
                    expected: string;
                    required: string;
                };
                identifier: {
                    expected: string;
                    required: string;
                };
                gtzero: {
                    expected: string;
                    required: string;
                };
                selection: {
                    expected: string;
                    required: string;
                };
                color: {
                    expected: string;
                    required: string;
                };
                dimension: {
                    expected: string;
                    required: string;
                };
                option: {
                    eqzero: string;
                };
                boolean: {
                    required: string;
                };
            };
            option: {
                dimension: {
                    pixels: string;
                    percentage: string;
                };
                boolean: {
                    yes: string;
                    no: string;
                };
                confirm: {
                    yes: string;
                    no: string;
                };
                outbound: {
                    cycle: string;
                    rebound: string;
                };
                pickable: {
                    true: string;
                    false: string;
                };
                async: {
                    true: string;
                    false: string;
                };
                mode: {
                    Arcade: {
                        label: string;
                    };
                    Catch: {
                        label: string;
                    };
                    DragDrop: {
                        label: string;
                    };
                    Paint: {
                        label: string;
                    };
                    Puzzle: {
                        label: string;
                    };
                    Simon: {
                        label: string;
                    };
                    Type: {
                        label: string;
                    };
                    WordSearch: {
                        label: string;
                    };
                };
                cause: {
                    right: {
                        label: string;
                    };
                    wrong: {
                        label: string;
                    };
                    tryout: {
                        label: string;
                    };
                    complete: {
                        label: string;
                    };
                    hint: {
                        label: string;
                    };
                    timeout: {
                        label: string;
                    };
                };
                effect: {
                    visible: {
                        label: string;
                    };
                    back: {
                        label: string;
                    };
                    runaway: {
                        label: string;
                    };
                    playsound: {
                        label: string;
                    };
                    lock: {
                        label: string;
                    };
                    addpoints: {
                        label: string;
                    };
                    destroy: {
                        label: string;
                    };
                    pause: {
                        label: string;
                    };
                    solve: {
                        label: string;
                    };
                    validate: {
                        label: string;
                    };
                };
                case: {
                    uppercase: string;
                    lowercase: string;
                    capitalize: string;
                };
            };
            editorForm: {
                assets: {
                    origin: {
                        title: string;
                        help: string;
                        upload: {
                            label: string;
                            help: string;
                        };
                        url: {
                            label: string;
                            help: string;
                        };
                    };
                    file: {
                        label: string;
                    };
                    url: {
                        label: string;
                    };
                    key: {
                        label: string;
                    };
                    upload: {
                        label: string;
                        value: string;
                    };
                    images: {
                        header: string;
                    };
                    sounds: {
                        header: string;
                    };
                };
                setup: {
                    mode: {
                        label: string;
                    };
                    width: {
                        label: string;
                    };
                    height: {
                        label: string;
                    };
                };
                layout: {
                    background: {
                        label: string;
                    };
                    loadLayout: {
                        label: string;
                    };
                    togglePanels: {
                        label: string;
                    };
                    sidePanels: {
                        header: string;
                    };
                    toolbox: {
                        header: string;
                        setup: {
                            header: string;
                        };
                        sprites: {
                            header: string;
                        };
                        behaviour: {
                            header: string;
                        };
                        Edit: {
                            tooltip: string;
                        };
                        Position: {
                            tooltip: string;
                        };
                        Text: {
                            tooltip: string;
                        };
                        Button: {
                            tooltip: string;
                        };
                        locate: {
                            tooltip: string;
                        };
                        Grid: {
                            tooltip: string;
                        };
                        Shape: {
                            tooltip: string;
                        };
                        retry: {
                            tooltip: string;
                        };
                        counter: {
                            tooltip: string;
                        };
                        events: {
                            tooltip: string;
                        };
                        frames: {
                            tooltip: string;
                        };
                        Depth: {
                            tooltip: string;
                        };
                        validation: {
                            tooltip: string;
                        };
                        Anchor: {
                            tooltip: string;
                        };
                        Embed: {
                            tooltip: string;
                        };
                        save: {
                            tooltip: string;
                        };
                        clear: {
                            tooltip: string;
                        };
                    };
                };
            };
            reveal: {
                generic: {
                    clear: string;
                };
                text: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    key: {
                        label: string;
                    };
                    label: {
                        label: string;
                    };
                    x: {
                        label: string;
                    };
                    y: {
                        label: string;
                    };
                    style: {
                        label: string;
                        value: string;
                        applied: string;
                        delete: string;
                    };
                };
                event: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    cause: {
                        label: string;
                        help: string;
                    };
                    effect: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                        all: string;
                        this: string;
                    };
                    value: {
                        label: string;
                    };
                };
                shape: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    type: {
                        label: string;
                    };
                    color: {
                        label: string;
                    };
                    radius: {
                        label: string;
                    };
                    width: {
                        label: string;
                    };
                    height: {
                        label: string;
                    };
                    line: {
                        width: {
                            label: string;
                        };
                        color: {
                            label: string;
                        };
                    };
                };
                sprite: {
                    delete: string;
                    embed: string;
                    id: {
                        label: string;
                    };
                    visible: {
                        label: string;
                    };
                    width: {
                        label: string;
                    };
                    height: {
                        label: string;
                    };
                    x: {
                        label: string;
                    };
                    marginRight: {
                        label: string;
                    };
                    y: {
                        label: string;
                    };
                    marginBottom: {
                        label: string;
                    };
                    tint: {
                        label: string;
                    };
                    angle: {
                        label: string;
                    };
                    frame: {
                        label: string;
                    };
                    layers: {
                        from: {
                            label: string;
                        };
                        to: {
                            label: string;
                        };
                    };
                    children: {
                        label: string;
                    };
                };
                frame: {
                    nav: {
                        previous: string;
                        next: string;
                    };
                };
                button: {
                    type: {
                        label: string;
                    };
                    value: {
                        label: string;
                    };
                };
                retry: {
                    delete: string;
                    retries: {
                        label: string;
                        help: string;
                        info: string;
                    };
                    points: {
                        legend: string;
                    };
                    from: {
                        label: string;
                    };
                    to: {
                        label: string;
                    };
                    step: {
                        label: string;
                    };
                    validator: {
                        from: string;
                        to: string;
                        toeqfrom: string;
                    };
                };
                counter: {
                    delete: string;
                    live: {
                        text: {
                            label: string;
                            help: string;
                        };
                    };
                    points: {
                        text: {
                            label: string;
                            help: string;
                        };
                    };
                    time: {
                        legend: string;
                        text: {
                            label: string;
                            help: string;
                        };
                        secs: {
                            label: string;
                        };
                    };
                    progress: {
                        sprite: {
                            label: string;
                            help: string;
                        };
                    };
                };
                grid: {
                    delete: string;
                    xoffset: {
                        label: string;
                        help: string;
                    };
                    yoffset: {
                        label: string;
                        help: string;
                    };
                    cols: {
                        label: string;
                        help: string;
                    };
                    length: {
                        label: string;
                    };
                    selection: {
                        label: string;
                    };
                };
                piece: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    outbound: {
                        label: string;
                        help: string;
                    };
                    movex: {
                        label: string;
                    };
                    movey: {
                        label: string;
                    };
                };
                target: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    events: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    sound: {
                        label: string;
                    };
                };
                weapon: {
                    sprite: {
                        label: string;
                        help: string;
                    };
                    pickable: {
                        label: string;
                    };
                    brushpoint: {
                        label: string;
                        help: string;
                    };
                    toolcase: {
                        label: string;
                        help: string;
                    };
                };
                children: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                };
                container: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                        help: string;
                        empty: string;
                    };
                    random: {
                        label: string;
                        help: string;
                    };
                    position: {
                        help: string;
                    };
                    x: {
                        label: string;
                    };
                    y: {
                        label: string;
                    };
                    offset: {
                        help: string;
                    };
                    xoffset: {
                        label: string;
                    };
                    yoffset: {
                        label: string;
                    };
                    values: {
                        label: string;
                        help: string;
                    };
                    extraValues: {
                        label: string;
                        help: string;
                    };
                    answer: {
                        available: {
                            label: string;
                            help: string;
                        };
                        selected: {
                            label: string;
                            help: string;
                        };
                    };
                };
                board: {
                    delete: string;
                    table: {
                        label: string;
                        help: string;
                        random: {
                            label: string;
                            help: string;
                        };
                    };
                    available: {
                        label: string;
                        help: string;
                    };
                    selected: {
                        label: string;
                        help: string;
                    };
                };
                depth: {
                    empty: string;
                    list: {
                        header: string;
                    };
                    reload: {
                        label: string;
                    };
                };
                validation: {
                    delete: string;
                    async: {
                        legend: string;
                    };
                    marks: {
                        legend: string;
                    };
                    right: {
                        key: {
                            label: string;
                            help: string;
                        };
                    };
                    wrong: {
                        key: {
                            label: string;
                            help: string;
                        };
                    };
                    top: {
                        label: string;
                        help: string;
                    };
                    left: {
                        label: string;
                        help: string;
                    };
                    width: {
                        label: string;
                        help: string;
                    };
                    height: {
                        label: string;
                        help: string;
                    };
                };
                color: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    color: {
                        label: string;
                    };
                };
                canvas: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    color: {
                        label: string;
                    };
                };
                brush: {
                    sprite: {
                        label: string;
                    };
                    pickable: {
                        label: string;
                    };
                };
                simon: {
                    transition: {
                        label: string;
                        help: string;
                    };
                    maxseq: {
                        label: string;
                        help: string;
                    };
                    talk: {
                        label: string;
                        help: string;
                    };
                };
                pad: {
                    delete: string;
                    sprite: {
                        label: string;
                        help: string;
                    };
                    sound: {
                        label: string;
                        help: string;
                    };
                    selected: {
                        label: string;
                        help: string;
                    };
                };
                stage: {
                    speed: {
                        label: string;
                        help: string;
                    };
                    craftspeed: {
                        label: string;
                        help: string;
                    };
                    runawaySpeed: {
                        label: string;
                        help: string;
                    };
                    debug: {
                        label: string;
                        help: string;
                    };
                    craft: {
                        label: string;
                        help: string;
                    };
                    floor2d: {
                        label: string;
                        help: string;
                    };
                    ygravity: {
                        label: string;
                        help: string;
                    };
                    movex: {
                        label: string;
                        help: string;
                    };
                    movey: {
                        label: string;
                        help: string;
                    };
                    "y-scale": {
                        label: string;
                        help: string;
                    };
                    tolerance: {
                        label: string;
                        help: string;
                    };
                    "y-jump": {
                        label: string;
                        help: string;
                    };
                    "jump-touch": {
                        label: string;
                        help: string;
                    };
                    ybounce: {
                        label: string;
                        help: string;
                    };
                    animations: {
                        legend: string;
                    };
                    move: {
                        label: string;
                        help: string;
                    };
                    jump: {
                        label: string;
                        help: string;
                    };
                    forward: {
                        label: string;
                        help: string;
                    };
                    up: {
                        label: string;
                        help: string;
                    };
                    down: {
                        label: string;
                        help: string;
                    };
                    back: {
                        label: string;
                        help: string;
                    };
                };
                asteroid: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    events: {
                        label: string;
                    };
                    attach: {
                        label: string;
                    };
                    multiplicity: {
                        label: string;
                        help: string;
                    };
                    cycle: {
                        label: string;
                        help: string;
                    };
                    movex: {
                        label: string;
                        help: string;
                    };
                    movey: {
                        label: string;
                        help: string;
                    };
                    tics: {
                        label: string;
                        help: string;
                    };
                    xdif: {
                        label: string;
                        help: string;
                    };
                    ydif: {
                        label: string;
                        help: string;
                    };
                    ygravity: {
                        label: string;
                        help: string;
                    };
                    basedepth: {
                        label: string;
                        help: string;
                    };
                    tolerance: {
                        label: string;
                        help: string;
                    };
                };
                landscape: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    moveX: {
                        label: string;
                        help: string;
                    };
                    foreground: {
                        label: string;
                        help: string;
                    };
                };
                word: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    id: {
                        label: string;
                        help: string;
                    };
                    key: {
                        label: string;
                        help: string;
                    };
                    sprite: {
                        label: string;
                        help: string;
                    };
                    x: {
                        label: string;
                        help: string;
                    };
                    y: {
                        label: string;
                        help: string;
                    };
                    text: {
                        label: string;
                        help: string;
                    };
                    opentext: {
                        label: string;
                        help: string;
                        row: string;
                    };
                    split: {
                        legend: string;
                        help: string;
                        offsetx: {
                            label: string;
                            help: string;
                        };
                        offsety: {
                            label: string;
                            help: string;
                        };
                    };
                    offset: {
                        legend: string;
                    };
                    offsetx: {
                        label: string;
                        help: string;
                    };
                    offsety: {
                        label: string;
                        help: string;
                    };
                };
                tiles: {
                    case: {
                        label: string;
                        help: string;
                    };
                    width: {
                        label: string;
                        help: string;
                    };
                    height: {
                        label: string;
                        help: string;
                    };
                    cols: {
                        label: string;
                        help: string;
                    };
                    rows: {
                        label: string;
                        help: string;
                    };
                    color: {
                        label: string;
                        help: string;
                        right: {
                            label: string;
                        };
                    };
                    colors: {
                        label: string;
                        help: string;
                        right: {
                            label: string;
                            help: string;
                        };
                        current: {
                            label: string;
                            help: string;
                        };
                    };
                };
                hiddenword: {
                    text: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                        help: string;
                    };
                    x: {
                        label: string;
                        help: string;
                    };
                    y: {
                        label: string;
                        help: string;
                    };
                    orientation: {
                        label: string;
                    };
                    preview: {
                        label: string;
                    };
                    check: {
                        label: string;
                    };
                };
            };
            plugin: {
                loaded: string;
                constraint: {
                    header: string;
                    catch: {
                        target: string;
                        weapon: string;
                        piece: string;
                        runaway: string;
                    };
                    paint: {
                        canvas: string;
                        brush: string;
                        color: string;
                        noColorSprites: string;
                    };
                    validation: {
                        required: string;
                        noButton: string;
                    };
                    sprite: {
                        notFound: string;
                    };
                    dragdrop: {
                        container: string;
                        nosprite: string;
                        event: {
                            zero: string;
                            gtone: string;
                        };
                    };
                    puzzle: {
                        piece: string;
                    };
                    simon: {
                        pad: {
                            nosprite: string;
                            nosound: string;
                            notenough: string;
                            noframes: string;
                        };
                        noprogress: string;
                        talk: {
                            noframes: string;
                        };
                    };
                    arcade: {
                        craft: string;
                        piece: {
                            empty: string;
                            noevents: string;
                        };
                    };
                    type: {
                        word: string;
                    };
                    wordsearch: {
                        hiddenword: string;
                        notextsprite: string;
                    };
                };
                toolbox: {
                    catch: {
                        piece: {
                            label: string;
                        };
                        target: {
                            label: string;
                        };
                        weapon: {
                            label: string;
                        };
                    };
                    paint: {
                        brush: {
                            label: string;
                        };
                        color: {
                            label: string;
                        };
                        canvas: {
                            label: string;
                        };
                    };
                    dragdrop: {
                        container: {
                            label: string;
                        };
                    };
                    puzzle: {
                        board: {
                            label: string;
                        };
                    };
                    simon: {
                        pad: {
                            label: string;
                        };
                    };
                    arcade: {
                        stage: {
                            label: string;
                        };
                        asteroid: {
                            label: string;
                        };
                        edge: {
                            label: string;
                        };
                        landscape: {
                            label: string;
                        };
                    };
                    type: {
                        word: {
                            label: string;
                        };
                    };
                    wordsearch: {
                        tiles: {
                            label: string;
                        };
                        hiddenword: {
                            label: string;
                        };
                        decorate: {
                            label: string;
                        };
                    };
                };
                field: {
                    setup: {
                        speed: {
                            label: string;
                            help: string;
                        };
                        runawaySpeed: {
                            label: string;
                            help: string;
                        };
                    };
                };
            };
            constraint: {
                asset: {
                    none: string;
                    keyExists: string;
                    novalid: string;
                    layers: {
                        unmatch: string;
                        nonconsecutive: string;
                        justimage: string;
                    };
                };
            };
            alert: {
                upload: {
                    failed: string;
                    novalid: string;
                };
                download: {
                    failed: string;
                    write: string;
                    nourl: string;
                };
                sprite: {
                    exists: string;
                };
                preview: {
                    nomode: string;
                };
            };
            warning: {
                color: {
                    empty: string;
                };
            };
            success: {
                upload: string;
                anchor: string;
            };
        };
    }
}
