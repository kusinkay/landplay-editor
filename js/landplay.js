var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <reference path='phaser.d.ts' />
var LandPlay;
(function (LandPlay) {
    var CoreObject = (function () {
        function CoreObject() {
            /**
             * defined debug level
             * 		0: lowest level, logs at this level always will be shown
             *
             */
            this.debug = 4;
            /**
             * This array stores the errors to be shown afterwards
             */
            this.errors = new Array();
        }
        /**
         * Log text is showed if the corresponding debug level is set
         */
        CoreObject.prototype.log = function (text, _debug) {
            if (_debug == null) {
                _debug = 0;
            }
            if (_debug <= this.debug) {
                var d;
                d = new Date();
                var message = "[" + this.className + "]" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds() + ":" + text;
                if (this.logTextField == undefined) {
                    console.debug(message);
                }
                else {
                    this.logTextField.text = message + "\n" + this.logTextField.text;
                }
            }
        };
        CoreObject.prototype.error = function (text) {
            console.error(text);
        };
        /**
         * get Phaser.Sprite object ('MovieClip' in ActionScript, here is the reason of the name)
         * from its key
         * @param name: id
         * @return LandSprite
         * //TODO
         */
        CoreObject.prototype.getMovie = function (name) {
            /*
            var path: Array;
            path = name.split(".");
            var m: MovieClip;
            m = _root;
            for (var i = 0; i < path.length; i++) {
                m = m[path[i]];
            }
            */
            this.error("Better use the one declared at LandGame");
        };
        CoreObject.prototype.setDebug = function (debug) {
            this.debug = debug;
        };
        CoreObject.prototype.colorToInt = function (tcolor) {
            if (tcolor == null) {
                return null;
            }
            tcolor = tcolor.replace("#", "0x");
            return parseInt(tcolor);
        };
        CoreObject.prototype.checkOverlap = function (a, b) {
            this.log('checkOverloap', 4);
            if (b != null) {
                var boundsA = a.getBounds();
                var boundsB = b.getBounds();
                return Phaser.Rectangle.intersects(boundsA, boundsB);
                //return Phaser.Rectangle.containsPoint(boundsB, tool.position);
            }
            else {
                this.log('checking with pointer: ' + a.getBounds(), 4);
                var x = a.game.input.activePointer.x;
                var y = a.game.input.activePointer.y;
                var p = new Phaser.Point(x, y);
                return Phaser.Rectangle.containsPoint(a.getBounds(), p);
                //return (a.x <= x && x <= a.x + a.width && a.y <= y && y <= a.y + a.height);
            }
        };
        CoreObject.prototype.parseSelection = function (selection) {
            var result = new Array();
            var pPositions = selection.split(',');
            for (var i = 0; i < pPositions.length; i++) {
                if (isNaN(pPositions[i])) {
                    var interval = pPositions[i].split('-');
                    for (var j = parseInt(interval[0]); j <= parseInt(interval[1]); j++) {
                        result.push(j);
                    }
                }
                else {
                    result.push(parseInt(pPositions[i]));
                }
            }
            return result;
        };
        CoreObject.prototype.randPos = function (from, to, len) {
            if (from > to) {
                var aux = from;
                from = to;
                to = aux;
            }
            if (len == null) {
                len = to - from;
            }
            var result = new Array();
            for (var i = from; i <= to; i++) {
                result.push(i);
            }
            return this.rand(result).slice(0, len + 1);
        };
        /**
         * If len is greater than array.length, there could be repeating items
         */
        CoreObject.prototype.rand = function (array, len) {
            var assignat = new Array();
            if (len != null && len != array.length) {
                for (var i = 0; i < len; i++) {
                    var r = Math.floor((Math.random() * (array.length) - 1) + 1);
                    assignat[i] = array[r];
                }
            }
            else {
                for (var i = 0; i < array.length; i++) {
                    var r = Math.floor((Math.random() * (array.length) - 1) + 1);
                    while (assignat[r] != undefined) {
                        r = Math.floor((Math.random() * (array.length) - 1) + 1);
                    }
                    assignat[r] = array[i];
                }
            }
            return assignat;
        };
        CoreObject.prototype.random = function (n) {
            return Math.floor((Math.random() * (n) - 1) + 1);
        };
        CoreObject.prototype.randRange = function (min, max) {
            var randomNum = Math.floor(Math.random() * (max - min + 1)) + min;
            return randomNum;
        };
        CoreObject.prototype.getKeys = function (obj, filter) {
            var name, result = [];
            for (name in obj) {
                if ((!filter || filter.test(name)) && obj.hasOwnProperty(name)) {
                    result[result.length] = name;
                }
            }
            return result;
        };
        return CoreObject;
    }());
    LandPlay.CoreObject = CoreObject;
})(LandPlay || (LandPlay = {}));
var LandPlay;
(function (LandPlay) {
    var Scorm = (function () {
        function Scorm() {
            this.nFindAPITries = 0;
            this.API = null;
            this.maxTries = 500;
            //Constants
            this.SCORM_TRUE = "true";
            this.SCORM_FALSE = "false";
            this.SCORM_NO_ERROR = "0";
            //Since the Unload handler will be called twice, from both the onunload
            //and onbeforeunload events, ensure that we only call Terminate once.
            this.terminateCalled = false;
            //Track whether or not we successfully this.initialized.
            this.initialized = false;
            this.pageNum = 1;
            this.pageMax = 1;
            this.passThreshold = 50;
            this.files = [];
        }
        Scorm.prototype.loadDOM = function (node) {
            var classRef = this;
            var organizations;
            var organization;
            if ($(node).attr('id') == null) {
                console.error("lms node requires id (urn) attribute");
                return;
            }
            else {
                classRef.id = $(node).attr('id');
                var doctype = null;
                classRef.manifest = document.implementation.createDocument(Scorm.NS_URI, "manifest", doctype);
                organizations = classRef.manifest.createElement("organizations");
                organizations.setAttribute("default", classRef.id);
                organization = classRef.manifest.createElement("organization");
                organization.setAttribute("identifier", classRef.id);
                organization.setAttributeNS(Scorm.NS_ADLSEC_URI, "objectivesGlobalToSystem", "false");
            }
            if ($('lms > title', node).length == 0) {
                console.error("lms node requires a title node");
                return;
            }
            else {
                $(node).find('lms > title').each(function () {
                    var title = classRef.manifest.createElementNS(Scorm.NS_URI, "title");
                    title.nodeValue = $(this).text();
                    organization.appendChild(title);
                });
            }
            /********PAGE****************/
            var resources = classRef.manifest.createElementNS(Scorm.NS_URI, "resources");
            var resource = classRef.manifest.createElement("resource");
            var item = classRef.manifest.createElement("item");
            if ($('lms > pager', node).length == 0) {
                console.error("lms node requires a pager node");
                return;
            }
            else {
                $(node).find('lms > pager').each(function () {
                    if ($(this).attr('page') != null) {
                        var pNum = parseInt($(this).attr('page'));
                        if (!isNaN(pNum)) {
                            classRef.pageNum = pNum;
                            var pMax = parseInt($(this).attr('max'));
                            if (!isNaN(pMax)) {
                                if (pMax < pNum) {
                                    console.error("max attribute in lms>pager can't be lower that page");
                                    return;
                                }
                                classRef.pageMax = pMax;
                            }
                            else {
                                classRef.pageMax = classRef.pageNum;
                            }
                        }
                    }
                    if ($('title', this).length == 0) {
                        console.error("lms>pager node requires a title node");
                        return;
                    }
                    else {
                        $(node).find('lms > pager > title').each(function () {
                            classRef.pageTitle = $(this).text();
                            var title = classRef.manifest.createElementNS(Scorm.NS_URI, "title");
                            title.nodeValue = ($(this).text());
                            item.appendChild(title);
                        });
                    }
                });
            }
            {
                item.setAttribute("identifier", "item_" + classRef.pageNum);
                item.setAttribute("identifierref", "resource_" + classRef.pageNum);
                {
                    var sequencing = classRef.manifest.createElementNS(Scorm.NS_IMSSS_URI, "sequencing");
                    {
                        var deliveryControls = classRef.manifest.createElementNS(Scorm.NS_IMSSS_URI, "deliveryControls");
                        deliveryControls.setAttribute("completionSetByContent", "true");
                        deliveryControls.setAttribute("objectiveSetByContent", "true");
                        sequencing.appendChild(deliveryControls);
                    }
                    item.appendChild(sequencing);
                }
                organization.appendChild(item);
            }
            {
                var sequencing = classRef.manifest.createElementNS(Scorm.NS_IMSSS_URI, "sequencing");
                {
                    var controlMode = classRef.manifest.createElementNS(Scorm.NS_IMSSS_URI, "controlMode");
                    controlMode.setAttribute("choice", "false");
                    controlMode.setAttribute("flow", "false");
                    sequencing.appendChild(controlMode);
                }
                organization.appendChild(sequencing);
            }
            organizations.appendChild(organization);
            classRef.manifest.documentElement.appendChild(organizations);
            resource.setAttribute("identifier", "resource_" + classRef.pageNum);
            resource.setAttribute("type", "webcontent");
            resource.setAttributeNS(Scorm.NS_ADLCP_URI, "scormType", "sco");
            resource.setAttribute("href", "LandPlay/TODO.html");
            for (var i = 0; i < classRef.files.length; i++) {
                var file = classRef.manifest.createElement("file");
                file.setAttribute("href", classRef.files[i].href);
                resource.appendChild(file);
            }
            resources.appendChild(resource);
            classRef.manifest.documentElement.appendChild(resources);
        };
        Scorm.prototype.addFile = function (file) {
            this.files.push(file);
        };
        /**
         * // The this.ScanForAPI() function searches for an object named API_1484_11
             // in the window that is passed into the function.  If the object is
             // found a reference to the object is returned to the calling function.
             // If the instance is found the SCO now has a handle to the LMS
             // provided this.API Instance.  The function searches a maximum number
             // of parents of the current window.  If no object is found the
             // function returns a null reference.  This function also reassigns a
             // value to the win parameter passed in, based on the number of
             // parents.  At the end of the function call, the win variable will be
             // set to the upper most parent in the chain of parents.
         * @param win
         */
        Scorm.prototype.ScanForAPI = function (win) {
            while ((win.API_1484_11 == null) && (win.parent != null)
                && (win.parent != win)) {
                this.nFindAPITries++;
                if (this.nFindAPITries > this.maxTries) {
                    return null;
                }
                win = win.parent;
            }
            return win.API_1484_11;
        };
        // The this.GetAPI() function begins the process of searching for the LMS
        // provided this.API Instance.  The function takes in a parameter that
        // represents the current window.  The function is built to search in a
        // specific order and stop when the LMS provided this.API Instance is found.
        // The function begins by searching the current window�s parent, if the
        // current window has a parent.  If the this.API Instance is not found, the
        // function then checks to see if there are any opener windows.  If
        // the window has an opener, the function begins to look for the
        // this.API Instance in the opener window.
        Scorm.prototype.GetAPI = function (win) {
            if ((win.parent != null) && (win.parent != win)) {
                this.API = this.ScanForAPI(win.parent);
            }
            if ((this.API == null) && (win.opener != null)) {
                this.API = this.ScanForAPI(win.opener);
            }
        };
        Scorm.prototype.ScormProcessInitialize = function () {
            var result;
            this.GetAPI(window);
            if (this.API == null) {
                console.error("ERROR - Could not establish a connection with the LMS.\n\nYour results may not be recorded.");
                return;
            }
            if (this.pageNum == 1) {
                result = this.API.Initialize("");
                if (result == this.SCORM_FALSE) {
                    var errorNumber = this.API.GetLastError();
                    var errorString = this.API.GetErrorString(errorNumber);
                    var diagnostic = this.API.GetDiagnostic(errorNumber);
                    var errorDescription = "Number: " + errorNumber + "\nDescription: " + errorString + "\nDiagnostic: " + diagnostic;
                    console.error("Error - Could not initialize communication with the LMS.\n\nYour results may not be recorded.\n\n" + errorDescription);
                    return;
                }
            }
            this.initialized = this.API.Initialized;
        };
        Scorm.prototype.ScormProcessTerminate = function () {
            var result;
            //Don't terminate if we haven't this.initialized or if we've already terminated
            if (this.initialized == false || this.terminateCalled == true) {
                return;
            }
            result = this.API.Terminate("");
            this.terminateCalled = true;
            if (result == this.SCORM_FALSE) {
                var errorNumber = this.API.GetLastError();
                var errorString = this.API.GetErrorString(errorNumber);
                var diagnostic = this.API.GetDiagnostic(errorNumber);
                var errorDescription = "Number: " + errorNumber + "\nDescription: " + errorString + "\nDiagnostic: " + diagnostic;
                console.error("Error - Could not terminate communication with the LMS.\n\nYour results may not be recorded.\n\n" + errorDescription);
                return;
            }
        };
        /*
        The onload and onunload event handlers are assigned in launchpage.html because more processing needs to
        occur at these times in this example.
        */
        //window.onload = this.ScormProcessInitialize;
        //window.onunload = this.ScormProcessTerminate;
        //window.onbeforeunload = this.ScormProcessTerminate;
        //There are situations where a GetValue call is expected to have an error
        //and should not alert the user.
        Scorm.prototype.ScormProcessGetValue = function (element, checkError) {
            var result;
            if (this.initialized == false || this.terminateCalled == true) {
                return;
            }
            result = this.API.GetValue(element);
            if (checkError == true && result == "") {
                var errorNumber = this.API.GetLastError();
                if (errorNumber != this.SCORM_NO_ERROR) {
                    var errorString = this.API.GetErrorString(errorNumber);
                    var diagnostic = this.API.GetDiagnostic(errorNumber);
                    var errorDescription = "Number: " + errorNumber + "\nDescription: " + errorString + "\nDiagnostic: " + diagnostic;
                    console.error("Error - Could not retrieve a value from the LMS.\n\n" + errorDescription);
                    return "";
                }
            }
            return result;
        };
        Scorm.prototype.ScormProcessSetValue = function (element, value) {
            var result;
            if (this.initialized == false || this.terminateCalled == true) {
                return;
            }
            result = this.API.SetValue(element, value);
            if (result == this.SCORM_FALSE) {
                var errorNumber = this.API.GetLastError();
                var errorString = this.API.GetErrorString(errorNumber);
                var diagnostic = this.API.GetDiagnostic(errorNumber);
                var errorDescription = "Number: " + errorNumber + "\nDescription: " + errorString + "\nDiagnostic: " + diagnostic;
                console.error("Error - Could not store a value in the LMS.\n\nYour results may not be recorded.\n\n" + errorDescription);
                return;
            }
        };
        Scorm.prototype.doStart = function () {
            //record the time that the learner started the SCO so that we can report the total time
            this.startTimeStamp = new Date();
            //initialize communication with the LMS
            this.ScormProcessInitialize();
            //it's a best practice to set the completion status to incomplete when
            //first launching the course (if the course is not already completed)
            var completionStatus = this.ScormProcessGetValue("cmi.completion_status", true);
            if (completionStatus == "unknown") {
                this.ScormProcessSetValue("cmi.completion_status", "incomplete");
            }
            //see if the user stored a bookmark previously (don't check for errors
            //because cmi.location may not be this.initialized
            var bookmark = this.ScormProcessGetValue("cmi.location", false);
            //if there isn't a stored bookmark, start the user at the first page
            if (bookmark == "") {
                this.currentPage = 0;
            }
            else {
                /*
                //if there is a stored bookmark, prompt the user to resume from the previous location
                if ( confirm( "Would you like to resume from where you previously left off?" ) ) {
                    currentPage = parseInt( bookmark, 10 );
                }
                else {
                    currentPage = 0;
                }
                */
            }
            //declare objective
            if (this.pageMax > 1) {
                var p = this.pageNum - 1;
                this.ScormProcessSetValue("cmi.objectives." + p.toString() + ".id", "obj" + p.toString() + "." + this.id);
                this.ScormProcessSetValue("cmi.objectives." + p.toString() + ".completion_status", "incomplete");
            }
            //this.goToPage();
        };
        Scorm.prototype.RecordTest = function (score) {
            if (this.pageMax > 1) {
                var p = this.pageNum - 1;
                this.RecordTestByContext(score, "cmi.objectives." + p.toString());
                score = 0;
                var counted = 0;
                for (var i = 0; i < this.pageNum; i++) {
                    var raw = parseInt(this.ScormProcessGetValue("cmi.objectives." + i + ".score.raw", false));
                    if (isNaN(raw)) {
                        console.error("No raw score for objective " + i);
                    }
                    else {
                        counted++;
                        score += raw;
                    }
                }
                if (counted > 0) {
                    //average
                    score = score / counted;
                }
            }
            if (this.pageNum == this.pageMax) {
                this.RecordTestByContext(score, "cmi");
            }
        };
        Scorm.prototype.RecordTestByContext = function (score, context) {
            this.ScormProcessSetValue(context + ".score.raw", score);
            this.ScormProcessSetValue(context + ".score.min", "0");
            this.ScormProcessSetValue(context + ".score.max", "100");
            var scaledScore = score / 100;
            this.ScormProcessSetValue(context + ".score.scaled", scaledScore);
            if (score >= this.passThreshold) {
                this.ScormProcessSetValue(context + ".success_status", "passed");
            }
            else {
                this.ScormProcessSetValue(context + ".success_status", "failed");
            }
            this.ScormProcessSetValue(context + ".completion_status", "completed");
        };
        //SCORM requires time to be formatted in a specific way
        Scorm.prototype.ConvertMilliSecondsIntoSCORM2004Time = function (intTotalMilliseconds) {
            var ScormTime = "";
            var HundredthsOfASecond; //decrementing counter - work at the hundreths of a second level because that is all the precision that is required
            var Seconds; // 100 hundreths of a seconds
            var Minutes; // 60 seconds
            var Hours; // 60 minutes
            var Days; // 24 hours
            var Months; // assumed to be an "average" month (figures a leap year every 4 years) = ((365*4) + 1) / 48 days - 30.4375 days per month
            var Years; // assumed to be 12 "average" months
            var HUNDREDTHS_PER_SECOND = 100;
            var HUNDREDTHS_PER_MINUTE = HUNDREDTHS_PER_SECOND * 60;
            var HUNDREDTHS_PER_HOUR = HUNDREDTHS_PER_MINUTE * 60;
            var HUNDREDTHS_PER_DAY = HUNDREDTHS_PER_HOUR * 24;
            var HUNDREDTHS_PER_MONTH = HUNDREDTHS_PER_DAY * (((365 * 4) + 1) / 48);
            var HUNDREDTHS_PER_YEAR = HUNDREDTHS_PER_MONTH * 12;
            HundredthsOfASecond = Math.floor(intTotalMilliseconds / 10);
            Years = Math.floor(HundredthsOfASecond / HUNDREDTHS_PER_YEAR);
            HundredthsOfASecond -= (Years * HUNDREDTHS_PER_YEAR);
            Months = Math.floor(HundredthsOfASecond / HUNDREDTHS_PER_MONTH);
            HundredthsOfASecond -= (Months * HUNDREDTHS_PER_MONTH);
            Days = Math.floor(HundredthsOfASecond / HUNDREDTHS_PER_DAY);
            HundredthsOfASecond -= (Days * HUNDREDTHS_PER_DAY);
            Hours = Math.floor(HundredthsOfASecond / HUNDREDTHS_PER_HOUR);
            HundredthsOfASecond -= (Hours * HUNDREDTHS_PER_HOUR);
            Minutes = Math.floor(HundredthsOfASecond / HUNDREDTHS_PER_MINUTE);
            HundredthsOfASecond -= (Minutes * HUNDREDTHS_PER_MINUTE);
            Seconds = Math.floor(HundredthsOfASecond / HUNDREDTHS_PER_SECOND);
            HundredthsOfASecond -= (Seconds * HUNDREDTHS_PER_SECOND);
            if (Years > 0) {
                ScormTime += Years + "Y";
            }
            if (Months > 0) {
                ScormTime += Months + "M";
            }
            if (Days > 0) {
                ScormTime += Days + "D";
            }
            //check to see if we have any time before adding the "T"
            if ((HundredthsOfASecond + Seconds + Minutes + Hours) > 0) {
                ScormTime += "T";
                if (Hours > 0) {
                    ScormTime += Hours + "H";
                }
                if (Minutes > 0) {
                    ScormTime += Minutes + "M";
                }
                if ((HundredthsOfASecond + Seconds) > 0) {
                    ScormTime += Seconds;
                    if (HundredthsOfASecond > 0) {
                        ScormTime += "." + HundredthsOfASecond;
                    }
                    ScormTime += "S";
                }
            }
            if (ScormTime == "") {
                ScormTime = "0S";
            }
            ScormTime = "P" + ScormTime;
            return ScormTime;
        };
        Scorm.NS_URI = "http://www.imsglobal.org/xsd/imscp_v1p1";
        Scorm.NS_XSI_URI = "http://www.w3.org/2001/XMLSchema-instance";
        Scorm.NS_ADLCP_URI = "http://www.adlnet.org/xsd/adlcp_v1p3";
        Scorm.NS_ADLSEC_URI = "http://www.adlnet.org/xsd/adlseq_v1p3";
        Scorm.NS_ADLNAV_URI = "http://www.adlnet.org/xsd/adlnav_v1p3";
        Scorm.NS_IMSSS_URI = "http://www.imsglobal.org/xsd/imsss";
        return Scorm;
    }());
    LandPlay.Scorm = Scorm;
})(LandPlay || (LandPlay = {}));
/// <reference path='CoreObject.ts' />
/// <reference path='phaser.d.ts' />
/// <reference path='jquery.d.ts' />
/// <reference path='scorm/Scorm.ts' />
var LandPlay;
(function (LandPlay) {
    var Mode;
    (function (Mode) {
        Mode["Arcade"] = "Arcade";
        Mode["Catch"] = "Catch";
        Mode["DragDrop"] = "DragDrop";
        Mode["Paint"] = "Paint";
        Mode["Puzzle"] = "Puzzle";
        Mode["Simon"] = "Simon";
        Mode["Type"] = "Type";
        Mode["WordSearch"] = "WordSearch";
    })(Mode = LandPlay.Mode || (LandPlay.Mode = {}));
    var LandGame = (function (_super) {
        __extends(LandGame, _super);
        function LandGame() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.version = "1.0.17.28.prealpha";
            _this.assets = [];
            _this.sounds = [];
            _this.images = [];
            _this.maps = [];
            /**
             * Indica si la asignacion debe ser evaluada de forma sincrona o no
             */
            _this.validate = false;
            _this.validateIconXoffset = 0;
            _this.validateIconYoffset = 0;
            /**
             * Puntos acumulados en la pantalla
             */
            _this.points = 0;
            _this.canvasWidth = 800;
            _this.canvasHeight = 600;
            _this.DOMcontainer = 'content';
            _this.scorm = null;
            /**
             * Event causes that only can happen once
             **/
            _this.oneShotEventCauses = [
                LandEvent.CAUSE_COMPLETE
            ];
            /***
             * Already fired event causes
             **/
            _this.firedEventCauses = [];
            /**
             * Posibles respuestas que pueden ser elegidas por el jugador
             */
            _this.responses = new Array();
            /**
             * Etiquetas textuales alternativas de las posibles respuestas
             */
            _this.responseTags = new Array(); //array
            _this.responseExtraTags = new Array(); //array
            /**
             * Indica que se desea reorganizar las opciones, usado habitualmente
             * para que si se juega dos veces el mismo juego no siempre esten
             * las opciones en la misma posición de la pantalla.
             */
            _this.randFlag = false;
            /**
             * Array unidimensional de recipientes definidos
             */
            _this.recipients = new Array();
            _this.respostes = new Array();
            _this.pickable = false;
            /**
             * Steps that take to consider stage cleared
             */
            _this.steps = 0;
            _this.grids = null;
            _this.layers = null;
            return _this;
        }
        LandGame.prototype.init = function (xmlFile, params) {
            var classRef = this;
            if (params != null) {
                if (params.DOMcontainer != null) {
                    classRef.DOMcontainer = params.DOMcontainer;
                }
            }
            if (xmlFile.indexOf("<") == 0) {
                classRef.parseXml(xmlFile);
            }
            else {
                $.ajax({
                    type: "GET",
                    url: xmlFile,
                    dataType: "xml",
                    success: function (xml) {
                        classRef.assets.push({ "href": xmlFile });
                        classRef.parseXml(xml);
                    }
                });
            }
        };
        LandGame.prototype.parseXml = function (xml) {
            if (typeof xml == "string") {
                var xmlDoc = $.parseXML(xml);
                this.xml = $(xmlDoc);
            }
            else {
                /* already parsed */
                this.xml = xml;
            }
            this.loadConf();
            this.loaded();
            this.onConfigLoad();
            this.xmlLoaded = true;
        };
        LandGame.prototype.updateCounter = function () {
            if (this.textCounter != null) {
                if (this.texts[this.textCounter] != null) {
                    this.texts[this.textCounter].text = this.left;
                }
                else {
                    console.warn("counter '" + this.textCounter + "' does not exist");
                }
            }
            if (this.textTime != null && this.remainingSecs != null) {
                if (this.timer == null) {
                    this.timer = this.game.time.create(false);
                    this.timer.loop(1000, this.updateCounter, this);
                    this.timer.start();
                }
                var sec_num = this.remainingSecs; // don't forget the second param
                var hours = Math.floor(sec_num / 3600);
                var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
                var seconds = sec_num - (hours * 3600) - (minutes * 60);
                var shours = '' + hours, sminutes = '' + minutes, sseconds = '' + seconds;
                if (hours < 10) {
                    shours = "0" + hours;
                }
                if (minutes < 10) {
                    sminutes = "0" + minutes;
                }
                if (seconds < 10) {
                    sseconds = "0" + seconds;
                }
                this.texts[this.textTime].text = shours + ':' + sminutes + ':' + sseconds;
                //this.log('entra tics', 4);
                if (this.remainingSecs == 0) {
                    this.timer.stop();
                    this.triggerEvents(LandEvent.CAUSE_TIMEOUT);
                }
                this.remainingSecs--;
            }
        };
        LandGame.prototype.updateProgress = function () {
            if (this.progress != null) {
                var mc = this.getMovie(this.progress);
                if (mc != null) {
                    var p = this.getProgressPerc();
                    if (p == 0 && this.progressMask != null) {
                        //game has turned back to 0
                        this.progressMask.destroy();
                        this.progressMask = null;
                    }
                    if (this.progressMask == null) {
                        this.progressMask = this.game.add.graphics(mc.x, mc.y);
                        this.progressMask.angle = mc.angle;
                    }
                    this.progressMask.beginFill(0x00FF00);
                    this.progressMask.drawRect(0, 0, mc.width * p, mc.height);
                    mc.mask = this.progressMask;
                }
            }
        };
        LandGame.prototype.getProgressPerc = function () {
            this.error('You must override getProgressPerc method in every mode');
            return 0.5;
        };
        LandGame.prototype.createAsset = function (url, key, frameWidth, frameHeight, frameMax) {
            this.assets.push({ href: url });
            return new Asset(url, key, frameWidth, frameHeight, frameMax);
        };
        LandGame.prototype.loadConf = function () {
            var classRef = this;
            $(this.xml).find('assets').each(function () {
                $(this).find('sound').each(function () {
                    var asset = classRef.createAsset($(this).attr('url'), $(this).attr('key'));
                    classRef.sounds.push(asset);
                });
                $(this).find('image').each(function () {
                    var imgNode = $(this);
                    if ($(this).children('layers').length > 0) {
                        $(this).children('layers').each(function () {
                            var from = parseInt($(this).attr("from"));
                            var to = parseInt($(this).attr("to"));
                            for (var i = from; i <= to; i++) {
                                var url = imgNode.attr('url').replace(/\(\?\)/g, i + "");
                                var key = imgNode.attr('key') + i;
                                var asset = classRef.createAsset(url, key, parseInt(imgNode.attr('frameWidth')), parseInt(imgNode.attr('frameHeight')), parseInt(imgNode.attr('frameMax')));
                                classRef.images.push(asset);
                            }
                        });
                    }
                    else {
                        var asset = classRef.createAsset($(this).attr('url'), $(this).attr('key'), parseInt($(this).attr('frameWidth')), parseInt($(this).attr('frameHeight')), parseInt($(this).attr('frameMax')));
                        classRef.images.push(asset);
                    }
                });
                $(this).find('map').each(function () {
                    var asset = classRef.createAsset($(this).attr('url'), $(this).attr('key'));
                    classRef.maps.push(asset);
                });
            });
            $(this.xml).find('retry').each(function () {
                classRef.setRetries($(this));
            });
            $(this.xml).find('setup').each(function () {
                if ($(this).attr('width') != null) {
                    var width = $(this).attr('width').trim();
                    var percPos = width.indexOf('%');
                    if (percPos >= 0 && percPos == width.length - 1 && !isNaN(parseInt(width))) {
                        classRef.canvasWidth = width;
                    }
                    else if (!isNaN(parseInt(width))) {
                        classRef.canvasWidth = parseInt(width);
                    }
                }
                if ($(this).attr('height') != null) {
                    var height = $(this).attr('height').trim();
                    var percPos = height.indexOf('%');
                    if (percPos >= 0 && percPos == height.length - 1 && !isNaN(parseInt(height))) {
                        classRef.canvasHeight = height;
                    }
                    else if (!isNaN(parseInt(height))) {
                        classRef.canvasHeight = parseInt(height);
                    }
                }
                $(this).find('lms').each(function () {
                    if ($(this).attr('type') == 'scorm') {
                        classRef.scorm = new LandPlay.Scorm();
                        for (var i = 0; i < classRef.assets.length; i++) {
                            classRef.scorm.addFile(classRef.assets[i]);
                        }
                        classRef.scorm.loadDOM($(this));
                    }
                });
            });
            /*register option gone to DragDrop*/
            $(this.xml).find('validation').each(function () {
                classRef.validate = $(this).attr('async').length > 0 && $(this).attr('async') == 'true';
                if ($(this).attr('top') != null) {
                    classRef.validateIconYoffset = parseInt($(this).attr('top'));
                }
                if ($(this).attr('left') != null) {
                    classRef.validateIconXoffset = parseInt($(this).attr('left'));
                }
                if ($(this).attr('width') != null) {
                    classRef.validateIconWidth = $(this).attr('width');
                }
                if ($(this).attr('height') != null) {
                    classRef.validateIconHeight = $(this).attr('height');
                }
                $(this).children('right').each(function () {
                    classRef.rightValidate = $(this).attr('key');
                });
                $(this).children('wrong').each(function () {
                    classRef.wrongValidate = $(this).attr('key');
                });
            });
            this.itemEvents = this.setEvents($(this.xml));
            $(this.xml).find('button').each(function () {
                var tipo = 'href';
                var value = '#';
                if ($(this).attr('href') != null) {
                    tipo = 'href';
                    value = $(this).attr('href');
                }
                if ($(this).attr('sound') != null) {
                    tipo = 'sound';
                    value = $(this).attr('sound');
                }
                if ($(this).attr('validate') != null) {
                    tipo = 'validate';
                    value = $(this).attr('validate');
                }
                if ($(this).attr('reset') != null) {
                    tipo = 'reset';
                    value = $(this).attr('reset');
                }
                if ($(this).attr('hint') != null) {
                    tipo = 'hint';
                    value = $(this).attr('hint');
                }
                var button = new LandButton($(this).attr('sprite'), tipo, value);
                if (classRef.buttons == null) {
                    classRef.buttons = new Array();
                }
                classRef.buttons.push(button);
            });
            this.setClipEvents();
            $(this.xml).find('counter').each(function () {
                $(this).children('life').each(function () {
                    classRef.textCounter = $(this).attr('text');
                });
                $(this).children('points').each(function () {
                    classRef.textPoints = $(this).attr('text');
                });
                $(this).children('time').each(function () {
                    classRef.textTime = $(this).attr('text');
                    classRef.remainingSecs = parseInt($(this).attr('secs'));
                });
                $(this).children('progress').each(function () {
                    if ($(this).attr('sprite') != null) {
                        classRef.progress = $(this).attr('sprite');
                    }
                });
            });
        };
        LandGame.prototype.addSprite = function (id, sprite) {
            if (this.sprites == null) {
                this.sprites = new Array();
            }
            if (sprite.alive) {
                sprite.id = id;
                this.sprites[id] = sprite;
            }
        };
        LandGame.prototype.setEvents = function (option, clear, children) {
            var classRef = this;
            var itemEvents = new Array();
            if (!children) {
                $(option).find('setup > event').each(function () {
                    itemEvents = classRef.setEvent($(this), itemEvents);
                });
            }
            else {
                $(option).children('event').each(function () {
                    itemEvents = classRef.setEvent($(this), itemEvents);
                });
            }
            return itemEvents;
        };
        LandGame.prototype.setAnimations = function (jqXmlnode, sprite) {
            $(jqXmlnode).children('animation').each(function () {
                if ($(this).attr('name') != null && $(this).attr('frames') != null) {
                    var rate = null;
                    var frames = new Array();
                    var sframes = $(this).attr('frames').split(',');
                    for (var i = 0; i < sframes.length; i++) {
                        frames.push(parseInt(sframes[i]));
                    }
                    if ($(this).attr('rate') != null) {
                        rate = parseInt($(this).attr('rate'));
                    }
                    sprite.animations.add($(this).attr('name'), frames, rate);
                }
            });
        };
        LandGame.prototype.setEvent = function (option, itemEvents) {
            if (itemEvents == null) {
                itemEvents = new Array();
            }
            var causes = $(option).attr('cause').split(',');
            for (var i = 0; i < causes.length; i++) {
                var event = new LandEvent(causes[i], $(option).attr('type'), $(option).attr('sprite'), $(option).attr('value'));
                itemEvents.push(event);
            }
            return itemEvents;
        };
        LandGame.prototype.addText = function (id, text) {
            if (this.texts == null) {
                this.texts = new Array();
            }
            text.name = id;
            this.texts[id] = text;
        };
        LandGame.prototype.setRetries = function (option) {
            if (this.from == undefined) {
                this.from = parseInt($(option).attr('from'));
                this.retries = parseInt($(option).attr('retries'));
                this.to = parseInt($(option).attr('to'));
                this.step = parseInt($(option).attr('step'));
                this.left = this.retries;
            }
            else {
                this.log("retry on option will not be applied. A retry option has been applied.", 0);
            }
        };
        /**
         * Actualizar los atributos de reintento. Usar en caso de fallo.
         */
        LandGame.prototype.retry = function () {
            var pts;
            if (this.from != undefined && this.to != undefined && this.step != undefined) {
                if (this.from >= this.to) {
                    this.left = this.left - this.step;
                }
                else {
                    this.left = this.left + this.step;
                }
                this.updateCounter();
                this.log("left:" + this.left + " / from:" + this.from + " / to:" + this.to + " / retries:" + this.retries + "", 4);
                if (this.retries == 0) {
                    //Puede hacer tantos intentos como quiera
                    return 1;
                }
                else {
                    return this.left;
                }
            }
            else {
                this.error("No Retry parameters defined.");
            }
        };
        LandGame.prototype.tryout = function () {
            return this.left != undefined && this.left <= 0;
        };
        LandGame.prototype.registerOption = function (option) {
            this.log('Abstract method: registerOption. Please, override me', 0);
        };
        LandGame.prototype.preRenderSprite = function (jqXmlnode) {
            /* reserved to override in mode */
        };
        LandGame.prototype.getAlternativeTags = function (option) {
            var classRef = this;
            var sprite = "__all__";
            var spriteIds = new Array();
            //if (this.responseTags == undefined) {
            if ($(option).attr('sprite') != null) {
                sprite = $(option).attr('sprite');
            }
            if ($(option).attr('values') != null) {
                var extraValues = $(option).attr('extra-values');
                var groups = sprite.match(/(.*)_\*/i);
                if (groups != null && groups.length > 1) {
                    var i = 0;
                    while (classRef.sprites[groups[1] + '_' + i] != null) {
                        spriteIds.push(groups[1] + '_' + i);
                        i++;
                    }
                    var wrongRecipient = groups[1] + '_wrong';
                    if (extraValues != null) {
                        /*create a hidden sprite to assign wrong, extra values*/
                        var wRedSp = classRef.game.add.sprite(classRef.game.width + 100, classRef.game.height + 100, groups[1] + '_0', 0);
                        wRedSp.name = wrongRecipient;
                        classRef.addSprite(wrongRecipient, wRedSp);
                    }
                }
                else {
                    spriteIds.push(sprite);
                }
                for (var i = 0; i < spriteIds.length; i++) {
                    this.responseTags[spriteIds[i]] = $(option).attr('values').split('|');
                }
                if (extraValues != null) {
                    this.responseExtraTags = extraValues.split('|');
                }
            }
            //}
        };
        /**
         * Asignar las etiquetas textuales a las respuestas
         */
        LandGame.prototype.assignTags = function () {
            var keys = this.getKeys(this.responseTags);
            if (keys != null && keys.length > 0) {
                for (var i = 0; i < this.responses.length; i++) {
                    this.log('assign tag to sprite "' + this.responses[i] + '"');
                    var container = "__all__";
                    //find its container
                    var containers = this.getKeys(this.assignacions);
                    this.log('keys for assignacions:' + containers, 4);
                    for (var j = 0; j < containers.length; j++) {
                        this.log('key for ' + j + ': ' + containers[j], 4);
                        if (this.assignacions[containers[j]].indexOf(this.responses[i]) > -1) {
                            this.log('found container for "' + this.responses[i] + '": "' + containers[j] + '"', 4);
                            container = containers[j];
                        }
                        else {
                            this.log('NOT found container for "' + this.responses[i] + '"', 4);
                        }
                    }
                    var responseTags = this.responseTags[container];
                    if (responseTags == null) {
                        var groups = container.match(/(.*)(_\d)/i);
                        if (groups != null && groups.length > 1) {
                            responseTags = this.responseTags[groups[1] + '_*'];
                        }
                    }
                    var assignedTag;
                    if (responseTags != null && responseTags.length > 0) {
                        responseTags = this.rand(responseTags);
                        assignedTag = responseTags[0];
                    }
                    else {
                        assignedTag = this.rand(this.responseExtraTags)[0];
                    }
                    if (assignedTag != null) {
                        if (this.getMovie(this.responses[i] + ".textTag") != null) {
                            this.getMovie(this.responses[i] + ".textTag").text = assignedTag;
                            this.log('assign tag "' + assignedTag + '" to sprite "' + this.responses[i] + '"');
                        }
                        else {
                            //this.log(":( Text '" + this.responseTags[i] + "' won't be put on " + _root[responses[i]]._name + ": it doesn't exist", 3);
                        }
                    }
                }
            }
        };
        LandGame.prototype.setRandom = function (option) {
            if ($(option).attr('random') == 'true') {
                this.randFlag = true;
            }
            else if ($(option).attr('random') == 'false') {
                this.randFlag = false;
            }
        };
        LandGame.prototype.setClipEvents = function () {
            for (var i = 0; i < this.responses.length; i++) {
                this.initClip(this.responses[i]);
            }
        };
        LandGame.prototype.initClip = function (clip) {
            this.log("Abstract method!! please define method 'initClip' at subclass", 0);
        };
        LandGame.prototype.hashResponsesPosition = function () {
            this.hashMoviesPosition(this.responses);
        };
        LandGame.prototype.hashMoviesPosition = function (movies) {
            if (this.randFlag) {
                var myMovies = this.rand(movies);
                this.log('rand result ' + myMovies, 4);
                for (var i = 0; i < movies.length; i++) {
                    this.log(" " + myMovies[i] + " exchanged by " + movies[i] + "", 3);
                    this.getMovie(myMovies[i])._x = this.getMovie(movies[i]).x;
                    this.getMovie(myMovies[i])._y = this.getMovie(movies[i]).y;
                }
                //Actualizamos las posiciones iniciales
                for (var i = 0; i < movies.length; i++) {
                    this.getMovie(myMovies[i]).x = this.getMovie(myMovies[i])._x;
                    this.getMovie(myMovies[i]).y = this.getMovie(myMovies[i])._y;
                }
            }
        };
        LandGame.prototype.loaded = function () {
            this.game = new Phaser.Game(this.canvasWidth, this.canvasHeight, Phaser.AUTO, this.DOMcontainer, {});
            var boot = new Boot(this.images, this.sounds);
            this.game.state.add('Boot', boot);
            var preloader = new Preloader(this.images, this.sounds, this.maps);
            this.game.state.add('Preloader', preloader);
            this.landstage = new LandStage(this);
            this.game.state.add('LandStage', this.landstage);
            this.game.state.start('Boot');
        };
        LandGame.prototype.triggerEvents = function (cause, sprite) {
            if (this.oneShotEventCauses.indexOf(cause) > -1) {
                if (this.firedEventCauses.indexOf(cause) > -1) {
                    //it'll never happen twice
                    return;
                }
                else {
                    this.firedEventCauses.push(cause);
                }
            }
            var itemEvents = this.itemEvents;
            if (sprite != null) {
                itemEvents = this.getMovie(sprite).itemEvents;
            }
            this.log('entra en LandGame.triggerEvents: ' + cause, 4);
            if (itemEvents == null) {
                return;
            }
            this.log(itemEvents, 4);
            for (var i = 0; i < itemEvents.length; i++) {
                this.log(itemEvents[i].evtType, 4);
                if (itemEvents[i].cause == cause) {
                    itemEvents[i].trigger();
                    this.log(itemEvents[i].evtType + "(" + itemEvents[i].sprite + ")", 4);
                    if (itemEvents[i].sprite == LandEvent.THIS_SPRITE) {
                        itemEvents[i].sprite = sprite;
                    }
                    switch (itemEvents[i].evtType) {
                        case LandEvent.TYPE_BACK:
                            if (itemEvents[i].sprite == LandEvent.ALL_SPRITES) {
                                for (var j = 0; j < this.responses.length; j++) {
                                    var id = this.responses[j];
                                    this.back(id);
                                }
                            }
                            else {
                                this.back(itemEvents[i].sprite);
                            }
                            break;
                        case LandEvent.TYPE_VISIBLE:
                            this.visible(itemEvents[i].sprite, itemEvents[i].value == "true");
                            break;
                        case LandEvent.TYPE_PLAYSOUND:
                            this.log("Playing " + itemEvents[i].sprite + " sound", 4);
                            this.game.sound.play(itemEvents[i].sprite);
                            break;
                        case LandEvent.TYPE_LOCK:
                            this.lock(itemEvents[i].sprite);
                            break;
                        case LandEvent.TYPE_ADDPOINTS:
                            this.addpoints(parseInt(itemEvents[i].value));
                            break;
                        case LandEvent.TYPE_DESTROY:
                            this.destroy(itemEvents[i].sprite);
                            break;
                        case LandEvent.TYPE_PAUSE:
                            this.game.paused = true;
                            break;
                        case LandEvent.TYPE_SOLVE:
                            this.solve();
                            break;
                        case LandEvent.TYPE_VALIDATE:
                            this.checkValidate();
                            break;
                    }
                }
            }
            this.clearEvents(LandEvent.TYPE_BACK);
            this.clearEvents(LandEvent.TYPE_PLAYSOUND);
            this.clearEvents(LandEvent.TYPE_VISIBLE);
            this.clearEvents(LandEvent.TYPE_LOCK);
            this.clearEvents(LandEvent.TYPE_ADDPOINTS);
            this.clearEvents(LandEvent.TYPE_VALIDATE);
            if (cause == LandEvent.CAUSE_COMPLETE || cause == LandEvent.CAUSE_TRYOUT) {
                if (this.scorm != null) {
                    if (this.scorm.pageNum == this.scorm.pageMax) {
                        this.scorm.ScormProcessTerminate();
                    }
                }
            }
        };
        LandGame.prototype.triggerRight = function (sprite) {
            this.triggerEvents(LandEvent.CAUSE_RIGHT, sprite);
        };
        LandGame.prototype.triggerWrong = function (sprite) {
            if (this.retry() > 0) {
                this.triggerEvents(LandEvent.CAUSE_WRONG, sprite);
            }
            else {
                //TARGET wrong
                this.triggerEvents(LandEvent.CAUSE_TRYOUT, sprite);
            }
        };
        LandGame.prototype.clearEvents = function (tipo) {
            var tmp = [];
            for (var i = 0; i < this.itemEvents.length; i++) {
                if (this.itemEvents[i].evtType != tipo || !this.tryout()) {
                    tmp.push(this.itemEvents[i]);
                }
            }
            this.itemEvents = tmp;
        };
        LandGame.prototype.hasCause = function (piece, cause) {
            for (var i = 0; i < piece.itemEvents.length; i++) {
                if (piece.itemEvents[i].cause == cause) {
                    return true;
                }
            }
            return false;
        };
        LandGame.prototype.checkValidate = function () {
            this.log('checkValidate', 4);
            this.resetValidate();
            var sprites = this.collectValidable();
            //los que estan OK
            var right = sprites[0].length;
            for (var i = 0; i < sprites[0].length; i++) {
                var target = this.sprites[sprites[0][i]];
                this.addCheckIcon(target, this.rightValidate);
            }
            //los que estan KO
            var wrong = sprites[1].length;
            for (var i = 0; i < sprites[1].length; i++) {
                var target = this.sprites[sprites[1][i]];
                this.addCheckIcon(target, this.wrongValidate);
            }
            if (sprites[1].length == 0) {
                this.triggerEvents(LandEvent.CAUSE_COMPLETE);
            }
            if (this.scorm != null) {
                this.scorm.RecordTest(Math.round((right / (right + wrong)) * 100));
            }
        };
        LandGame.prototype.solve = function () {
            this.error('You must override this methode "solve" in your mode');
        };
        LandGame.prototype.hint = function () {
            this.error('You must override this methode "hint" in your mode');
        };
        /**
         * Reset items
         */
        LandGame.prototype.reset = function () {
            for (var i = 0; i < this.respostes.length; i++) {
                var m = (this.getMovie(this.respostes[i]));
                m.kill();
            }
            for (var i = 0; i < this.recipients.length; i++) {
                var m = (this.getMovie(this.recipients[i]));
                m.kill();
            }
        };
        /**
         * Checks if "movieName" is the name of a right answer within the "recipient"
         */
        LandGame.prototype.goodAnswer = function (movieName, recipient) {
            var ind2 = -1;
            this.log("Does '" + movieName + "' match with '" + recipient + "'?", 4);
            if (this.assignacions[recipient] != null) {
                ind2 = this.assignacions[recipient].indexOf(movieName);
            }
            return (ind2 >= 0);
        };
        LandGame.prototype.getAsset = function (key) {
            for (var i = 0; i < this.images.length; i++) {
                if (this.images[i].key == key) {
                    return this.images[i];
                }
            }
            return null;
        };
        LandGame.prototype.getGame = function () {
            return this.game;
        };
        LandGame.prototype.getLayers = function () {
            if (this.layers == null) {
                this.layers = this.game.add.group(this.game.world, LandPlay.LandGame.LayersContainer);
            }
            return this.layers;
        };
        LandGame.prototype.getGrids = function () {
            if (this.grids == null) {
                this.grids = this.game.add.group(this.game.world, LandPlay.LandGame.GridsContainer);
            }
            return this.grids;
        };
        LandGame.prototype.getMovie = function (name) {
            var names = name.split('.');
            if (name.indexOf('.') > 0) {
                name = names[0];
            }
            if (this.sprites != null) {
                if (names.length > 1) {
                    var sprite = this.sprites[name];
                    for (var i = 0; i < sprite.children.length; i++) {
                        var cspr = sprite.getChildAt(i);
                        if (cspr.name == names[1]) {
                            return cspr;
                        }
                    }
                }
                else {
                    return this.sprites[name];
                }
            }
            else {
                return null;
            }
        };
        LandGame.prototype.resetValidate = function () {
            if (this.validateGroup != null) {
                this.validateGroup.removeAll();
            }
        };
        LandGame.prototype.addCheckIcon = function (target, status) {
            if (this.validateGroup == null) {
                this.validateGroup = this.game.add.group();
            }
            var check = this.game.add.sprite(target.x + this.validateIconXoffset, target.y + this.validateIconYoffset, status, null, this.validateGroup);
            if (this.validateIconWidth != null) {
                if (isNaN(parseInt(this.validateIconWidth))) {
                    var ratio = parseInt(this.validateIconWidth.replace('%', '')) / 100;
                    check.width = check.width * ratio;
                }
                else {
                    check.width = parseInt(this.validateIconWidth);
                }
            }
            if (this.validateIconHeight != null) {
                if (isNaN(parseInt(this.validateIconHeight))) {
                    var ratio = parseInt(this.validateIconHeight.replace('%', '')) / 100;
                    check.height = check.height * ratio;
                }
                else {
                    check.height = parseInt(this.validateIconHeight);
                }
            }
        };
        LandGame.prototype.collectValidable = function () {
            this.error("collectValidable function must be overriden at mode class");
            return [];
        };
        LandGame.prototype.back = function (id) {
            var sprite = this.sprites[id];
            this.log("volviendo a (" + sprite._x + ", " + sprite._y + ")", 4);
            sprite.x = sprite._x;
            sprite.y = sprite._y;
        };
        LandGame.prototype.visible = function (id, value) {
            var sprite = this.sprites[id];
            this.log("visible: " + value + " a (" + id + ")", 4);
            sprite.visible = value;
        };
        LandGame.prototype.getPosPointer = function () {
            var point = new Phaser.Point(this.game.input.activePointer.x, this.game.input.activePointer.y);
            return point;
        };
        LandGame.prototype.getNumber = function (opt, name) {
            if ($(opt).attr(name + 'min') != null && $(opt).attr(name + 'max') != null) {
                var min = parseInt($(opt).attr(name + 'min'));
                var max = parseInt($(opt).attr(name + 'max'));
                return this.randRange(Math.min(min, max), Math.max(min, max));
            }
            else if ($(opt).attr(name) != null) {
                var parts = $(opt).attr(name).split(',');
                if (parts.length == 2) {
                    var min = parseInt(parts[0]);
                    var max = parseInt(parts[1]);
                    return this.randRange(Math.min(min, max), Math.max(min, max));
                }
                else if (parts.length == 1) {
                    return parseInt($(opt).attr(name));
                }
            }
            return null;
        };
        LandGame.prototype.addpoints = function (value) {
            this.log('sumamos ' + value + ' a ' + this.points, 4);
            this.points += value;
            this.texts[this.textPoints].text = this.points;
        };
        LandGame.prototype.lock = function (id) {
            var sprites = new Array();
            if (id == LandEvent.ALL_SPRITES) {
                sprites = this.responses;
            }
            else {
                sprites.push(id);
            }
            for (var i = 0; i < sprites.length; i++) {
                var sprite = this.sprites[sprites[i]];
                this.log("disable (" + sprite.key + ")", 4);
                sprite.inputEnabled = false;
                sprite.events.onInputDown.removeAll(sprite);
                sprite.events.onInputUp.removeAll(sprite);
                sprite.events.onDragStart.forget();
                if (sprite.input != null) {
                    sprite.input.disableDrag();
                }
                sprite.craftEnabled = false;
            }
        };
        LandGame.prototype.destroy = function (id) {
            var sprite = this.sprites[id];
            sprite.destroy();
        };
        LandGame.prototype.onConfigLoad = function () {
            this.log("Override onConfigLoad", 0);
        };
        LandGame.prototype.onStageLoad = function () {
            this.updateCounter();
            if (this.buttons != null) {
                for (var i = 0; i < this.buttons.length; i++) {
                    this.buttons[i].activate(this);
                }
            }
            if (this.scorm != null) {
                this.scorm.doStart();
            }
        };
        LandGame.prototype.overTrasparentPixel = function (sprite, x, y) {
            var posX, posY;
            posX = (x != null ? x /*- sprite.x*/ : sprite.game.input.activePointer.x - sprite.x);
            posY = (y != null ? y /*- sprite.y*/ : sprite.game.input.activePointer.y - sprite.y);
            if (x == null && y == null) {
                this.log('mouse: (' + posX + ', ' + posY + '), sprite: ' + sprite.id, 4);
            }
            if (posX < 0 || posY < 0) {
                this.log('lower out', 4);
                return true;
            }
            if (posX > sprite.width || posY > sprite.height) {
                this.log('higher out', 4);
                return true;
            }
            if (this.bitMaps == null) {
                this.bitMaps = new Array();
            }
            var bmd = this.bitMaps[sprite.id];
            if (bmd == null) {
                bmd = sprite.game.make.bitmapData(sprite.width, sprite.height, sprite.key, true);
                this.bitMaps[sprite.id] = bmd;
                //sprite.game.add.sprite(0, 0, bmd);
            }
            bmd.draw(sprite.key, 0, 0, sprite.width, sprite.height);
            bmd.update(0, 0, sprite.width, sprite.height);
            var hex = bmd.getPixel(posX, posY);
            if (hex['a'] == 0) {
                if (x == null && y == null) {
                    this.log('real transparence', 4);
                }
                return true;
            }
            return (false);
            //return bmd.getPixel(posX, posY) == 0;
        };
        LandGame.prototype.debugBitMap = function (canvas) {
            if (canvas.debug) {
                this.log('canvas to debug: ' + canvas.id, 4);
                if (this.debugBitMapGroup == null) {
                    this.debugBitMapGroup = canvas.game.add.group();
                }
                else {
                    this.debugBitMapGroup.removeAll();
                }
                var steps = Math.round(canvas.width / 25);
                for (var x = 0; x < canvas.width; x = x + steps) {
                    for (var y = 0; y < canvas.height; y = y + steps) {
                        var char = '';
                        if (this.overTrasparentPixel(canvas, x, y)) {
                            char = '+';
                        }
                        var txt = canvas.game.add.text(x + canvas.x, y + canvas.y, char, { fontSize: '10px', fill: '#FFF' });
                        txt.stroke = '#000';
                        txt.strokeThickness = 2;
                        txt.anchor.x = 0.5;
                        txt.anchor.y = 0.5;
                        txt.fontSize = 10;
                        this.debugBitMapGroup.add(txt);
                    }
                }
            }
        };
        /**
         * changes z of the given sprite over the other this.sprites
         */
        LandGame.prototype.bringTop = function (sprite) {
            //convert associative to int array
            var sSprites = new Array();
            var keys = this.getKeys(this.sprites);
            for (var i = 0; i < keys.length; i++) {
                sSprites.push(this.sprites[keys[i]]);
            }
            sSprites = sSprites.sort(function (sp1, sp2) {
                return sp2.z - sp1.z;
            });
            this.log("sorted depths " + (sSprites), 4);
            //exchange sprite depth with the least one
            var group = sSprites[0].parent;
            if (group == null) {
                group = this.game.world;
            }
            group.swap(sSprites[0], this.getMovie(sprite));
        };
        LandGame.prototype.update = function () {
            this.log("Override update", 0);
        };
        LandGame.prototype.render = function () {
            /*this.log('render, can be overriden', 4);*/
        };
        LandGame.GridsContainer = "__lgStageGridsGroupName__";
        LandGame.LayersContainer = "__lgStageLayersGroupName__";
        return LandGame;
    }(LandPlay.CoreObject));
    LandPlay.LandGame = LandGame;
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot(images, sounds) {
            var _this = _super.call(this) || this;
            _this.images = images;
            _this.sounds = sounds;
            return _this;
        }
        Boot.prototype.preload = function () {
            for (var i = 0; i < this.images.length; i++) {
                //here only load preloadBar
                if (this.images[i].key == 'preloadBar') {
                    this.load.image(this.images[i].key, this.images[i].url);
                }
            }
        };
        Boot.prototype.create = function () {
            //  Unless you specifically need to support multitouch I would recommend setting this to 1
            this.input.maxPointers = 1;
            //  Phaser will automatically pause if the browser tab the game is in loses focus. You can disable that here:
            this.stage.disableVisibilityChange = true;
            if (this.game.device.desktop) {
                //  If you have any desktop specific settings, they can go in here
                //this.game.scale.pageAlignHorizontally = true;
            }
            else {
                var ratio = this.game.world.width / this.game.world.height;
                //  Same goes for mobile settings.
                /* vist a l'exemple*/
                this.game.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;
                this.game.scale.minWidth = 240;
                this.game.scale.minHeight = this.game.scale.minWidth / ratio;
                this.game.scale.maxWidth = 600;
                this.game.scale.maxHeight = this.game.scale.maxWidth / ratio;
                this.game.scale.maxWidth = window.innerWidth;
                this.game.scale.maxHeight = window.innerHeight;
                this.game.scale.forceLandscape = true;
                this.game.scale.pageAlignHorizontally = true;
                //this.game.scale.setScreenSize(true);
                this.game.scale.refresh();
            }
            this.game.state.start('Preloader', true, false);
        };
        return Boot;
    }(Phaser.State));
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader(images, sounds, maps) {
            var _this = _super.call(this) || this;
            _this.images = images;
            _this.sounds = sounds;
            _this.maps = maps;
            return _this;
        }
        Preloader.prototype.preload = function () {
            //  Set-up our preloader sprite
            this.preloadBar = this.add.sprite(200, 250, 'preloadBar');
            this.load.setPreloadSprite(this.preloadBar);
            for (var i = 0; i < this.images.length; i++) {
                if (!isNaN(this.images[i].frameWidth) && !isNaN(this.images[i].frameHeight) && !isNaN(this.images[i].frameMax)) {
                    this.load.spritesheet(this.images[i].key, this.images[i].url, this.images[i].frameWidth, this.images[i].frameHeight, this.images[i].frameMax);
                }
                else {
                    this.load.image(this.images[i].key, this.images[i].url);
                }
            }
            for (var i = 0; i < this.sounds.length; i++) {
                this.load.audio(this.sounds[i].key, this.sounds[i].url);
            }
            for (var i = 0; i < this.maps.length; i++) {
                this.load.tilemap(this.maps[i].key, this.maps[i].url, null, Phaser.Tilemap.TILED_JSON);
            }
        };
        Preloader.prototype.create = function () {
            var tween = this.add.tween(this.preloadBar).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.startMainMenu, this);
        };
        Preloader.prototype.startMainMenu = function () {
            this.game.state.start('LandStage', true, false);
        };
        return Preloader;
    }(Phaser.State));
    var LandButton = (function () {
        function LandButton(sprite, tipo, value) {
            this.sprite = sprite;
            this.tipo = tipo;
            this.value = value;
        }
        LandButton.prototype.activate = function (lg) {
            if (!lg.validate && (this.tipo == LandButton.Types.TYPE_VALIDATE.value || this.tipo == LandButton.Types.TYPE_RESET.value)) {
                //no validable, hide the button
                lg.sprites[this.sprite].visible = false;
                return;
            }
            var sprite = lg.sprites[this.sprite];
            sprite.inputEnabled = true;
            var classRef = this;
            sprite.events.onInputDown.add(function (sprite) {
                if (classRef.tipo == LandButton.Types.TYPE_SOUND.value) {
                    for (var i = 0; i < lg.sounds.length; i++) {
                        if (lg.sounds[i].key == classRef.value) {
                            sprite.game.sound.play(lg.sounds[i].key);
                            break;
                        }
                    }
                }
                if (classRef.tipo == LandButton.Types.TYPE_VALIDATE.value) {
                    if (lg.validate) {
                        lg.checkValidate();
                    }
                }
                if (classRef.tipo == LandButton.Types.TYPE_RESET.value) {
                    lg.resetValidate();
                }
                if (classRef.tipo == LandButton.Types.TYPE_HINT.value) {
                    lg.hint();
                }
                if (classRef.tipo == LandButton.Types.TYPE_HREF.value) {
                    location.href = classRef.value;
                }
            }, this);
            if (sprite.animations.frameData != null && sprite.animations.frameTotal > 1) {
                sprite.events.onInputOver.add(function (sprite) {
                    sprite.frame = 1;
                });
                sprite.events.onInputOut.add(function (sprite) {
                    sprite.frame = 0;
                });
            }
        };
        LandButton.Types = {
            TYPE_HREF: { value: "href" },
            TYPE_SOUND: { value: "sound" },
            TYPE_VALIDATE: { value: "validate" },
            TYPE_RESET: { value: "reset" },
            TYPE_HINT: { value: "hint" }
        };
        return LandButton;
    }());
    LandPlay.LandButton = LandButton;
    var LandEvent = (function () {
        function LandEvent(cause, evtType, sprite, value) {
            this.cause = cause;
            this.evtType = evtType;
            this.sprite = sprite;
            this.value = value;
        }
        LandEvent.prototype.trigger = function () {
        };
        /** @deprecated see Cause */
        LandEvent.CAUSE_RIGHT = 'right';
        /** @deprecated see Cause */
        LandEvent.CAUSE_WRONG = 'wrong';
        /** @deprecated see Cause */
        LandEvent.CAUSE_TRYOUT = 'tryout';
        /** @deprecated see Cause */
        LandEvent.CAUSE_COMPLETE = 'complete';
        /** @deprecated see Cause */
        LandEvent.CAUSE_HINT = 'hint';
        /** @deprecated see Cause */
        LandEvent.CAUSE_TIMEOUT = 'timeout';
        LandEvent.Cause = {
            CAUSE_RIGHT: { value: LandEvent.CAUSE_RIGHT },
            CAUSE_WRONG: { value: LandEvent.CAUSE_WRONG },
            CAUSE_TRYOUT: { value: LandEvent.CAUSE_TRYOUT },
            CAUSE_COMPLETE: { value: LandEvent.CAUSE_COMPLETE },
            CAUSE_HINT: { value: LandEvent.CAUSE_HINT },
            CAUSE_TIMEOUT: { value: LandEvent.CAUSE_TIMEOUT }
        };
        /** @deprecated see Effect */
        LandEvent.TYPE_VISIBLE = 'visible';
        /** @deprecated see Effect */
        LandEvent.TYPE_BACK = 'back';
        /** @deprecated see Effect */
        LandEvent.TYPE_RUNAWAY = 'runaway';
        /** @deprecated see Effect */
        LandEvent.TYPE_PLAYSOUND = 'playsound';
        /** @deprecated see Effect */
        LandEvent.TYPE_LOCK = 'lock';
        /** @deprecated see Effect */
        LandEvent.TYPE_ADDPOINTS = 'addpoints';
        /** @deprecated see Effect */
        LandEvent.TYPE_DESTROY = 'destroy';
        /** @deprecated see Effect */
        LandEvent.TYPE_PAUSE = 'pause';
        /** @deprecated see Effect */
        LandEvent.TYPE_SOLVE = 'solve';
        /** @deprecated see Effect */
        LandEvent.TYPE_VALIDATE = 'validate';
        LandEvent.Effect = {
            TYPE_VISIBLE: { value: LandEvent.TYPE_VISIBLE },
            TYPE_BACK: { value: LandEvent.TYPE_BACK },
            TYPE_RUNAWAY: { value: LandEvent.TYPE_RUNAWAY },
            TYPE_PLAYSOUND: { value: LandEvent.TYPE_PLAYSOUND },
            TYPE_LOCK: { value: LandEvent.TYPE_LOCK },
            TYPE_ADDPOINTS: { value: LandEvent.TYPE_ADDPOINTS },
            TYPE_DESTROY: { value: LandEvent.TYPE_DESTROY },
            TYPE_PAUSE: { value: LandEvent.TYPE_PAUSE },
            TYPE_SOLVE: { value: LandEvent.TYPE_SOLVE },
            TYPE_VALIDATE: { value: LandEvent.TYPE_VALIDATE }
        };
        LandEvent.ALL_SPRITES = '__all__';
        LandEvent.THIS_SPRITE = '__this__';
        return LandEvent;
    }());
    LandPlay.LandEvent = LandEvent;
    var LandSprite = (function (_super) {
        __extends(LandSprite, _super);
        function LandSprite(game, x, y, key) {
            var _this = _super.call(this, game, x, y, key) || this;
            _this.busy = false;
            _this.iNum = 0;
            _this.isClonated = false;
            _this.tics = 0;
            _this.moveX = 0;
            _this.moveY = 1;
            _this.cycle = false;
            _this.gotten = false;
            _this.runningAway = false;
            _this.ydifmin = 0;
            _this.ydifmax = 0;
            _this.basedepth = 0;
            _this.tolerance = 0;
            //piece is flat on the floor, on a 2-dim floor
            _this.floored = false;
            _this.isLandscape = false;
            _this.gapchecked = false;
            _this.depthIsSet = false;
            _this.paused = false;
            _this.craftEnabled = true;
            _this.bullets = [];
            _this._x = x;
            _this._y = y;
            _this = _super.call(this, x, y, key) || this;
            return _this;
        }
        LandSprite.prototype.embedChild = function (child) {
            child.pieceContainer = this;
            this._children.push(child);
        };
        return LandSprite;
    }(Phaser.Sprite));
    LandPlay.LandSprite = LandSprite;
    var LandStage = (function (_super) {
        __extends(LandStage, _super);
        function LandStage(landgame) {
            var _this = _super.call(this) || this;
            _this.landgame = landgame;
            return _this;
        }
        LandStage.prototype.preload = function () {
        };
        /**
         * recursiva, para poder anidar sprites
         */
        LandStage.prototype.renderSprite = function (classRef, spParent, jqXmlnode, depth, group) {
            classRef.landgame.preRenderSprite(jqXmlnode);
            if (depth == null) {
                depth = 0;
            }
            var spriteNode = jqXmlnode;
            if ($(jqXmlnode).children('layers').length > 0 && depth == 0) {
                $(jqXmlnode).children('layers').each(function () {
                    var from = parseInt($(this).attr("from"));
                    var to = parseInt($(this).attr("to"));
                    var idPattern = $(spriteNode).attr('id');
                    var keyPattern = $(spriteNode).attr('key');
                    var layersGroup = new LandLayer(classRef.landgame, from, to, idPattern);
                    for (var i = from; i <= to; i++) {
                        $(spriteNode).attr('id', idPattern + i);
                        $(spriteNode).attr('key', keyPattern + i);
                        classRef.renderSprite(classRef, null, spriteNode, depth + 1, layersGroup);
                    }
                });
                return;
            }
            var x;
            var y;
            if ($(jqXmlnode).attr('x')) {
                x = parseInt($(jqXmlnode).attr('x'));
            }
            if ($(jqXmlnode).attr('y')) {
                y = parseInt($(jqXmlnode).attr('y'));
            }
            if ($(jqXmlnode).attr('margin-right')) {
                x = classRef.game.width - parseInt($(jqXmlnode).attr('margin-right'));
            }
            if ($(jqXmlnode).attr('margin-bottom')) {
                y = classRef.game.height - parseInt($(jqXmlnode).attr('margin-bottom'));
            }
            var key = $(jqXmlnode).attr('key');
            var id = key;
            if ($(jqXmlnode).attr('id') != null) {
                id = $(jqXmlnode).attr('id');
            }
            var sprite;
            var frame = null;
            if ($(jqXmlnode).attr('frame') != null) {
                var frameValue = $(jqXmlnode).attr('frame');
                if (frameValue == 'random') {
                    var asset = classRef.landgame.getAsset(key);
                    if (asset != null) {
                        if (!isNaN(asset.frameMax)) {
                            //TODO valor random del sprite
                            frame = classRef.landgame.randRange(0, asset.frameMax - 1);
                        }
                    }
                }
                else {
                    frame = parseInt(frameValue);
                }
            }
            if (spParent == null) {
                if (group != null) {
                    sprite = group.create(x, y, key, frame);
                }
                else {
                    sprite = classRef.game.add.sprite(x, y, key, frame);
                }
            }
            else {
                sprite = spParent.addChild(classRef.game.make.sprite(x, y, key, frame));
            }
            classRef.landgame.log("add sprite " + id + " (" + key + ")", 4);
            if ($(jqXmlnode).attr('angle') != null) {
                var angle = parseInt($(jqXmlnode).attr('angle'));
                sprite.angle = angle;
            }
            var ratio = null;
            if ($(jqXmlnode).attr('width') != null) {
                if ($(jqXmlnode).attr('width').indexOf('%') > 0) {
                    ratio = parseInt($(jqXmlnode).attr('width').replace('%', '')) / 100;
                    sprite.width = sprite.width * ratio;
                }
                else {
                    var formerw = sprite.width;
                    sprite.width = parseInt($(jqXmlnode).attr('width'));
                    ratio = sprite.width / formerw;
                }
            }
            if ($(jqXmlnode).attr('height') != null) {
                if ($(jqXmlnode).attr('height').indexOf('%') > 0) {
                    ratio = parseInt($(jqXmlnode).attr('height').replace('%', '')) / 100;
                    sprite.height = sprite.height * ratio;
                }
                else {
                    sprite.height = parseInt($(jqXmlnode).attr('height'));
                }
            }
            else if (ratio != null) {
                sprite.height = sprite.height * ratio;
            }
            if ($(jqXmlnode).attr('tint') != null) {
                sprite.tint = classRef.landgame.colorToInt($(jqXmlnode).attr('tint'));
            }
            $(jqXmlnode).children('shape').each(function () {
                classRef.landgame.log('shape: entering', 4);
                var grafics = sprite.game.add.graphics(0, 0); //sprite.x, sprite.y);
                $(this).children('line').each(function () {
                    var width = 1;
                    if ($(this).attr('width') != null) {
                        width = parseInt($(this).attr('width'));
                    }
                    var nColor = 0x000000;
                    if ($(this).attr('color') != null) {
                        var color = $(this).attr('color').replace('#', '0x');
                        var nColor = parseInt(color);
                    }
                    grafics.lineStyle(width, nColor);
                });
                var nColor = 0xFFFFFF;
                if ($(this).attr('color') != null) {
                    var color = $(this).attr('color').replace('#', '0x');
                    var nColor = parseInt(color);
                }
                classRef.landgame.log('shape: grafics created "' + grafics + '"', 4);
                grafics.beginFill(nColor);
                if ($(this).attr('type') == 'square') {
                    classRef.landgame.log('shape: drawRect', 4);
                    grafics.drawRect(0, 0, parseInt($(this).attr('width')), parseInt($(this).attr('height')));
                }
                else if ($(this).attr('type') == 'circle') {
                    classRef.landgame.log('shape: drawCircle', 4);
                    grafics.drawCircle(0, 0, parseInt($(this).attr('radius')));
                }
                grafics.endFill();
                grafics.boundsPadding = 0;
                grafics.cacheAsBitmap = true;
                /*
                sprite.setTexture(
                    grafics.generateTexture(100, PIXI.scaleModes.DEFAULT, this.game)
                    );
                */
                sprite.addChild(grafics);
                /*
                for (var child in sprite.children) {
                    classRef.landgame.log('child width: ' + sprite.children[child].width, 4);
                    classRef.landgame.debugVar = child;
                    if (sprite.width < sprite.children[child].width) {
                        sprite.width = sprite.children[child].width;
                    }
                }
                */
                /*
                //example seen at http://www.html5gamedevs.com/topic/9180-using-phasergraphics-as-phasersprite/
                var graphics = game.add.graphics();
                graphics.boundsPadding = 0;
                // do this to stop the graphics object having a 10 pixel invisible border
                var sprite = game.add.sprite(game.world.centerX, game.world.centerY, null);
                sprite.addChild(graphics);
                // from now on, the graphics will be positioned at 0, 0 relative to the sprite, and if given a body, it should encompass the bounds of the graphics object attached
                */
            }); //end shape
            $(jqXmlnode).find('text').each(function () {
                var style = { fontSize: '10px', fill: '#FFF' };
                if ($(this).attr('style') != null) {
                    style = JSON.parse($(this).attr('style'));
                }
                var x = 0;
                var y = 0;
                if ($(this).attr("x") != null) {
                    x = parseInt($(this).attr("x"));
                }
                if ($(this).attr("y") != null) {
                    y = parseInt($(this).attr("y"));
                }
                var txt = sprite.game.add.text(x, y, $(this).attr("label"), style);
                if ($(this).attr('stroke') != null) {
                    txt.stroke = $(this).attr('stroke');
                    txt.strokeThickness = 3;
                }
                classRef.landgame.addText($(this).attr("key"), txt);
                sprite.addChild(txt);
            }); //end text
            $(jqXmlnode).find('grid').each(function () {
                if (depth == 0) {
                    var xoffset = parseInt($(this).attr("xoffset"));
                    var yoffset = parseInt($(this).attr("yoffset"));
                    var cols = parseInt($(this).attr("cols"));
                    var newNode = $(jqXmlnode).clone(false);
                    var reset_x = parseInt($(jqXmlnode).attr("x"));
                    var reset_id = $(jqXmlnode).attr("id");
                    if (classRef.landgame.grids == null) {
                        classRef.landgame.getGrids();
                    }
                    var selection = new Array();
                    var sSelection;
                    if ($(this).attr('selection')) {
                        sSelection = $(this).attr('selection');
                    }
                    else if ($(this).attr("length")) {
                        var length = parseInt($(this).attr("length"));
                        sSelection = '0-' + (length - 1);
                    }
                    selection = classRef.landgame.parseSelection(sSelection);
                    var group = new LandGrid(classRef.landgame, selection.length, reset_id);
                    for (var i = 0; i < selection.length; i++) {
                        $(newNode).attr('id', reset_id + '_' + i);
                        var frame = selection[i];
                        $(newNode).attr('frame', frame);
                        if (i > 0 && i % cols == 0) {
                            /*saltamos a la fila siguiente*/
                            $(newNode).attr('x', reset_x);
                            $(newNode).attr('y', parseInt($(newNode).attr('y')) + yoffset);
                        }
                        else if (i > 0) {
                            $(newNode).attr('x', parseInt($(newNode).attr('x')) + xoffset);
                        }
                        classRef.renderSprite(classRef, null, newNode, depth + 1, group);
                    }
                    /*borramos el original, que es una muestra*/
                    delete classRef.landgame.sprites[reset_id];
                    sprite.destroy();
                    return null;
                }
            });
            classRef.landgame.setAnimations(jqXmlnode, sprite);
            sprite._x = sprite.x;
            sprite._y = sprite.y;
            sprite.fixedToCamera = false;
            if ($(jqXmlnode).attr('fixedToCamera') != null) {
                sprite.fixedToCamera = $(jqXmlnode).attr('fixedToCamera') == 'true';
            }
            classRef.landgame.addSprite(id, sprite);
            if ($(jqXmlnode).attr('visible') == "false") {
                sprite.visible = false;
            }
            if ($(jqXmlnode).attr('alpha') != null) {
                var alpha = parseFloat($(jqXmlnode).attr('alpha'));
                if (!isNaN(alpha)) {
                    sprite.alpha = alpha;
                }
            }
            $(jqXmlnode).children('sprite').each(function () {
                //recursividad
                classRef.renderSprite(classRef, sprite, this, depth + 1);
            });
            if ($(jqXmlnode).attr('anchorx') != null) {
                if ($(jqXmlnode).attr('anchorx').indexOf("%") >= 0) {
                    sprite.anchor.x = parseInt($(jqXmlnode).attr('anchorx').replace('%', '')) / 100;
                }
                if ($(jqXmlnode).attr('anchory').indexOf("%") >= 0) {
                    sprite.anchor.y = parseInt($(jqXmlnode).attr('anchory').replace('%', '')) / 100;
                }
            }
        };
        LandStage.prototype.create = function () {
            var classRef = this;
            $(this.landgame.xml).find('layout').each(function () {
                if ($(this).attr('background-color') != null) {
                    classRef.game.stage.backgroundColor = $(this).attr('background-color');
                }
                $(this).find('layout>sprite').each(function () {
                    var spriteNode = this;
                    classRef.renderSprite(classRef, null, this);
                });
            });
            this.landgame.getLayers(); /*just to initialize*/
            this.landgame.getGrids(); /*just to initialize*/
            this.landgame.onStageLoad();
        };
        LandStage.prototype.update = function () {
            this.landgame.update();
        };
        LandStage.prototype.render = function () {
            this.landgame.render();
        };
        return LandStage;
    }(Phaser.State));
    var Asset = (function () {
        function Asset(url, key, frameWidth, frameHeight, frameMax) {
            this.url = url;
            this.key = key;
            this.frameWidth = frameWidth;
            this.frameHeight = frameHeight;
            this.frameMax = frameMax;
        }
        Asset.prototype.toString = function () {
            return this.key;
        };
        return Asset;
    }());
    LandPlay.Asset = Asset;
    var LandLayer = (function (_super) {
        __extends(LandLayer, _super);
        function LandLayer(landgame, from, to, name, addToStage, enableBody, physicsBodyType) {
            var _this = this;
            _this.fromLayerNumber = from;
            _this.toLayerNumber = to;
            _this = _super.call(this, landgame.getGame(), landgame.getLayers(), name, addToStage, enableBody, physicsBodyType) || this;
            return _this;
        }
        LandLayer.prototype.toJsonLayers = function () {
            return { "@from": this.fromLayerNumber, "@to": this.toLayerNumber };
        };
        return LandLayer;
    }(Phaser.Group));
    LandPlay.LandLayer = LandLayer;
    var LandGrid = (function (_super) {
        __extends(LandGrid, _super);
        function LandGrid(landgame, length, name, addToStage, enableBody, physicsBodyType) {
            var _this = this;
            _this.gridLength = length;
            _this = _super.call(this, landgame.getGame(), landgame.getGrids(), name, addToStage, enableBody, physicsBodyType) || this;
            return _this;
        }
        return LandGrid;
    }(Phaser.Group));
    LandPlay.LandGrid = LandGrid;
})(LandPlay || (LandPlay = {}));
/// <reference path='LandGame.ts' />
var LandPlay;
(function (LandPlay) {
    /**
     * Soporte a todos los modos de juego que requieran de una herramienta:
     * - Catch
     * - Paint
     */
    var ToolEvents = (function (_super) {
        __extends(ToolEvents, _super);
        function ToolEvents() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        ToolEvents.TYPE_RELEASE = 'release';
        return ToolEvents;
    }(LandPlay.LandEvent));
    var Tool = (function (_super) {
        __extends(Tool, _super);
        function Tool() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Tool.prototype.setupTool = function () {
            var classRef = this;
            $(this.xml).find('weapon').each(function () {
                classRef.setupTool00($(this));
            });
            $(this.xml).find('brush').each(function () {
                classRef.setupTool00($(this));
                classRef.setupTool01($(this));
            });
            if (this.pickable) {
                var tool = this.getTool();
                tool.inputEnabled = !this.picked;
                tool.events.onInputDown.add(function (tool) {
                    classRef.picked = true;
                    tool.events.onInputDown.dispose();
                    tool.inputEnabled = false;
                }, this);
            }
        };
        Tool.prototype.setupTool00 = function (node) {
            this.toolId = node.attr('sprite');
            if (node.attr('pickable') != null) {
                this.pickable = node.attr('pickable') == 'true';
            }
        };
        Tool.prototype.setupTool01 = function (node) {
            //TO be overriden
        };
        Tool.prototype.triggerEvents = function (cause, sprite) {
            _super.prototype.triggerEvents.call(this, cause, sprite);
            for (var i = 0; i < this.itemEvents.length; i++) {
                this.log('cause: ' + cause, 4);
                if (this.itemEvents[i].cause == cause) {
                    if (this.itemEvents[i].evtType == ToolEvents.TYPE_RELEASE) {
                        this.log('soltamos tool ' + this.itemEvents[i].sprite, 4);
                        this.release(this.itemEvents[i].sprite);
                    }
                }
            }
            this.clearEvents(ToolEvents.TYPE_RELEASE);
        };
        Tool.prototype.release = function (key) {
            if (this.pickable) {
                this.picked = false;
            }
            else {
                this.pickable = true;
            }
        };
        Tool.prototype.update = function () {
            if (this.toolId != null) {
                var tool = this.getTool();
                if (tool != null) {
                    if (!this.pickable || this.picked) {
                        tool.x = tool.game.input.activePointer.x;
                        tool.y = tool.game.input.activePointer.y;
                    }
                    if (tool.game.input.activePointer.isDown) {
                        tool.frame = 1;
                    }
                    else {
                        tool.frame = 0;
                    }
                }
            }
        };
        Tool.prototype.getTool = function () {
            return this.sprites[this.toolId];
        };
        return Tool;
    }(LandPlay.LandGame));
    LandPlay.Tool = Tool;
})(LandPlay || (LandPlay = {}));
/// <reference path="../Tool.ts" />
var LandPlay;
(function (LandPlay) {
    var Catch = (function (_super) {
        __extends(Catch, _super);
        function Catch(xmlFile, params) {
            var _this = _super.call(this) || this;
            /**
             * Multiplicador de velocidad a la que se mueven las piezas
             *
             */
            _this.speed = 1;
            /**
             * Velocidad por la que se multiplica la velocidad del moviemiento
             * de las piezas cuando huyen.
             */
            _this.runawaySpeed = 4;
            _this.className = LandPlay.Mode.Catch;
            _this.init(xmlFile, params);
            return _this;
        }
        Catch.prototype.onConfigLoad = function () {
            var classRef = this;
            $(this.xml).find('setup').each(function () {
                if ($(this).attr('runawaySpeed') != null) {
                    classRef.runawaySpeed = parseInt($(this).attr('runawaySpeed'));
                }
                if ($(this).attr('speed') != null) {
                    classRef.speed = parseInt($(this).attr('speed'));
                }
            });
        };
        Catch.prototype.update = function () {
            var classRef = this;
            //TOOL
            //alert(this.sprites[this.weapon].key);
            _super.prototype.update.call(this);
            for (var i = 0; i < this.pieces.length; i++) {
                this.pieces[i].x += this.pieces[i].movex;
                if (this.pieces[i].movex > 0) {
                    //hacia la derecha
                    if (this.pieces[i].x > this.pieces[i].game.width) {
                        if (this.pieces[i].outbound == Piece.CYCLE) {
                            this.pieces[i].x = 0;
                        }
                        else if (this.pieces[i].outbound == Piece.REBOUND) {
                            this.pieces[i].movex = -1 * this.pieces[i].movex;
                        }
                        else {
                            this.pieces[i].kill();
                        }
                    }
                }
                else {
                    if (this.pieces[i].x < 0) {
                        //hacia la izquierda
                        if (this.pieces[i].outbound == Piece.CYCLE) {
                            this.pieces[i].x = this.pieces[i].game.width;
                        }
                        else if (this.pieces[i].outbound == Piece.REBOUND) {
                            this.pieces[i].movex = -1 * this.pieces[i].movex;
                        }
                        else {
                            this.pieces[i].kill();
                        }
                    }
                }
                if (this.pieces[i].movey != null) {
                    this.pieces[i].y += this.pieces[i].movey;
                    if (this.pieces[i].movey > 0) {
                        if (this.pieces[i].y > this.pieces[i].game.height) {
                            if (this.pieces[i].outbound == Piece.CYCLE) {
                                this.pieces[i].y = 0;
                            }
                            else if (this.pieces[i].outbound == Piece.REBOUND) {
                                this.pieces[i].movey = -1 * this.pieces[i].movey;
                            }
                            else {
                                this.pieces[i].kill();
                            }
                        }
                    }
                    else {
                        if (this.pieces[i].y < 0) {
                            if (this.pieces[i].outbound == Piece.CYCLE) {
                                this.pieces[i].y = this.pieces[i].game.height;
                            }
                            else if (this.pieces[i].outbound == Piece.REBOUND) {
                                this.pieces[i].movey = -1 * this.pieces[i].movey;
                            }
                            else {
                                this.pieces[i].kill();
                            }
                        }
                    }
                }
            }
        };
        Catch.prototype.onStageLoad = function () {
            var classRef = this;
            this.setupTool();
            $(this.xml).find('piece').each(function () {
                if (classRef.pieces == null) {
                    classRef.pieces = new Array();
                    classRef.responses = new Array();
                }
                classRef.log(JSON.stringify(classRef.sprites), 4);
                var piece = classRef.sprites[$(this).attr('sprite')];
                if (piece != null) {
                    if ($(this).attr('movex') != null) {
                        piece.movex = parseInt($(this).attr('movex'));
                    }
                    if ($(this).attr('movey') != null) {
                        piece.movey = parseInt($(this).attr('movey'));
                    }
                    if ($(this).attr('outbound') != null) {
                        piece.outbound = $(this).attr('outbound');
                    }
                    else {
                        piece.outbound = Piece.CYCLE;
                    }
                    piece.inputEnabled = true;
                    piece.events.onInputDown.add(function (piece) {
                        if (!classRef.pickable || classRef.pickable && classRef.picked) {
                            /*
                            if (classRef.pickable && classRef.picked){
                                
                                //release the pickable weapon
                                classRef.picked = false;
                                classRef.getWeapon().events.onInputDown.forget();
                                
                            }
                            */
                            if (classRef.targetValues.indexOf(piece.id) >= 0) {
                                classRef.log(piece.key + ' encontrado en ' + classRef.targetValues, 4);
                                //throw my right events
                                classRef.triggerEvents(LandPlay.LandEvent.CAUSE_RIGHT, piece.id);
                                //throw right generic events
                                classRef.triggerRight();
                                if (classRef.caughtPieces == null) {
                                    classRef.caughtPieces = new Array();
                                }
                                classRef.caughtPieces.push(piece.id);
                                if (classRef.caughtPieces.length == classRef.targetValues.length) {
                                    classRef.triggerEvents(LandPlay.LandEvent.CAUSE_COMPLETE);
                                }
                            }
                            else {
                                classRef.log(piece.key + ' NO encontrado en ' + classRef.targetValues, 4);
                                classRef.triggerWrong();
                            }
                        }
                    }, this);
                    classRef.pieces.push(piece);
                    classRef.responses.push(piece.id);
                }
                else {
                    classRef.log($(this).attr('sprite') + ' not found in sprites array', 0);
                }
            });
            $(this.xml).find('targets').each(function () {
                $(this).find('target').each(function () {
                    if (classRef.targetValues == null) {
                        classRef.targetValues = new Array();
                    }
                    classRef.targetValues.push($(this).attr('sprite'));
                    if (classRef.targetSounds == null) {
                        classRef.targetSounds = new Array();
                    }
                    classRef.targetSounds.push($(this).attr('sound'));
                    var sprite = classRef.getMovie($(this).attr('sprite'));
                    sprite.itemEvents = classRef.setEvents($(this), false, true);
                    //classRef.debugVar = sprite.itemEvents;
                });
            });
            if (this.targetSounds != null && this.targetSounds.length > 0) {
                //TODO que suene el primero
                var weapon = this.getWeapon();
                weapon.game.sound.play(this.targetSounds[0]);
            }
            _super.prototype.onStageLoad.call(this);
        };
        Catch.prototype.getWeapon = function () {
            return this.getTool();
        };
        Catch.prototype.triggerEvents = function (cause, sprite) {
            _super.prototype.triggerEvents.call(this, cause, sprite);
            var itemEvents = this.itemEvents;
            if (sprite != null) {
                this.log('loading sprite events form ' + sprite, 4);
                itemEvents = this.getMovie(sprite).itemEvents;
            }
            for (var i = 0; i < itemEvents.length; i++) {
                if (itemEvents[i].cause == cause) {
                    this.log('cause: ' + itemEvents[i].cause + ', effect: ' + itemEvents[i].evtType, 4);
                    if (itemEvents[i].evtType == CatchEvents.TYPE_RUNAWAY) {
                        var all = (itemEvents[i].sprite == CatchEvents.ALL_SPRITES);
                        this.runaway(itemEvents[i].sprite, all);
                    }
                    if (itemEvents[i].evtType == CatchEvents.TYPE_FREEZE) {
                        var all = (itemEvents[i].sprite == CatchEvents.ALL_SPRITES);
                        if (itemEvents[i].sprite == CatchEvents.THIS_SPRITE) {
                            itemEvents[i].sprite = sprite;
                        }
                        this.freeze(itemEvents[i].sprite, all);
                    }
                }
            }
            this.clearEvents(CatchEvents.TYPE_RUNAWAY);
            this.clearEvents(CatchEvents.TYPE_FREEZE);
        };
        Catch.prototype.runaway = function (sprite, all) {
            for (var j = 0; j < this.pieces.length; j++) {
                if (this.pieces[j].key == sprite || all) {
                    this.pieces[j].movex = this.pieces[j].movex * this.runawaySpeed;
                    if (this.pieces[j].movey != null) {
                        this.pieces[j].movey = this.pieces[j].movey * this.runawaySpeed;
                    }
                    this.pieces[j].outbound = Piece.DIE;
                }
            }
        };
        Catch.prototype.collectValidable = function () {
            var result = [];
            //the catched pieces
            result[0] = this.caughtPieces;
            //the uncatched ones
            result[1] = [];
            for (var i = 0; i < this.targetValues.length; i++) {
                if (this.caughtPieces.indexOf(this.targetValues[i]) < 0) {
                    result[1].push(this.targetValues[i]);
                }
            }
            return result;
        };
        Catch.prototype.freeze = function (sprite, all) {
            for (var j = 0; j < this.pieces.length; j++) {
                if (this.pieces[j].id == sprite || all) {
                    this.pieces[j].movex = 0;
                    this.pieces[j].movey = 0;
                }
            }
        };
        return Catch;
    }(LandPlay.Tool));
    LandPlay.Catch = Catch;
    var CatchEvents = (function (_super) {
        __extends(CatchEvents, _super);
        function CatchEvents() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        CatchEvents.TYPE_RUNAWAY = 'runaway';
        CatchEvents.TYPE_FREEZE = 'freeze';
        CatchEvents.TYPE_LOAD = 'load';
        return CatchEvents;
    }(LandPlay.LandEvent));
    LandPlay.CatchEvents = CatchEvents;
    var Piece = (function (_super) {
        __extends(Piece, _super);
        function Piece() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.movex = 0;
            _this.movey = 0;
            _this.outbound = Piece.CYCLE;
            return _this;
        }
        Piece.prototype.runaway = function (runawaySpeed) {
            this.movex = this.movex * runawaySpeed;
            this.movey = this.movey * runawaySpeed;
            this.outbound = Piece.DIE;
        };
        Piece.prototype.toString = function () {
            return this.outbound;
        };
        Piece.prototype.embedChild = function (piece) {
            _super.prototype.embedChild.call(this, piece);
        };
        Piece.CYCLE = 'cycle';
        Piece.REBOUND = 'rebound';
        Piece.DIE = 'die';
        return Piece;
    }(LandPlay.LandSprite));
    LandPlay.Piece = Piece;
})(LandPlay || (LandPlay = {}));
/// <reference path="../Tool.ts" />
var LandPlay;
(function (LandPlay) {
    var Paint = (function (_super) {
        __extends(Paint, _super);
        function Paint(xmlFile, params) {
            var _this = _super.call(this) || this;
            /**
             * Array unidimensional que representa los nombres de instancia
             * de las movies de los botes de pintura
             */
            _this.pots = [];
            /**
             * Array unidimensional que representa los nombres de instancia
             * de las movies de los lienzos
             */
            _this.canvases = [];
            _this.className = LandPlay.Mode.Paint;
            _this.init(xmlFile, params);
            return _this;
        }
        Paint.prototype.onConfigLoad = function () {
            var classRef = this;
        };
        Paint.prototype.onStageLoad = function () {
            var classRef = this;
            this.setupTool();
            $(this.xml).find("color").each(function () {
                var color = classRef.sprites[$(this).attr('sprite')];
                color.color = $(this).attr('color');
                classRef.pots.push(color);
                color.tint = classRef.colorToInt(color.color);
            });
            $(this.xml).find('canvas').each(function () {
                var canvasNode = this;
                if ($(this).children('layers').length > 0) {
                    $(this).children('layers').each(function () {
                        var from = parseInt($(this).attr("from"));
                        var to = parseInt($(this).attr("to"));
                        var spritePattern = $(canvasNode).attr('sprite');
                        for (var i = from; i <= to; i++) {
                            var canvas = classRef.sprites[spritePattern + i];
                            if ($(canvasNode).attr('color') != null) {
                                canvas.color = classRef.getPot($(canvasNode).attr('color')).color;
                            }
                            canvas.debug = ($(canvasNode).attr('debug') != null && $(canvasNode).attr('debug') == 'true');
                            classRef.canvases.push(canvas);
                            classRef.steps++;
                        }
                    });
                }
                else {
                    var canvas = classRef.sprites[$(this).attr('sprite')];
                    if ($(canvasNode).attr('color') != null) {
                        canvas.color = classRef.getPot($(canvasNode).attr('color')).color;
                    }
                    canvas.debug = ($(this).attr('debug') != null && $(this).attr('debug') == 'true');
                    classRef.canvases.push(canvas);
                    classRef.steps++;
                }
            });
            for (var i = 0; i < this.pots.length; i++) {
                var pot = this.pots[i];
                pot.inputEnabled = true;
                pot.events.onInputDown.add(function (pot) {
                    if (!classRef.pickable || classRef.pickable && classRef.picked) {
                        var brush = classRef.getTool();
                        if (classRef.brushPoint != null) {
                            brush = classRef.sprites[classRef.brushPoint];
                        }
                        brush.tint = classRef.colorToInt(pot.color);
                    }
                }, this);
            }
            for (var i = 0; i < this.canvases.length; i++) {
                var canvas = this.canvases[i];
                canvas.inputEnabled = true;
                canvas.events.onInputOver.add(function (canvas) {
                    /*
                    if (canvas.debug) {
                        //classRef.debugBitMap(canvas);
                    }
                    canvas.events.onInputOver.dispose();
                    */
                }, this);
                canvas.events.onInputDown.add(function (canvas) {
                    classRef.log('click canvas: ' + canvas.id, 4);
                    if (!classRef.pickable || classRef.pickable && classRef.picked) {
                        if (classRef.overTrasparentPixel(canvas)) {
                            canvas = null;
                            //alert('transp');
                            for (var i = 0; i < classRef.canvases.length; i++) {
                                if (!classRef.overTrasparentPixel(classRef.canvases[i])) {
                                    canvas = classRef.canvases[i];
                                    break;
                                }
                            }
                            if (canvas != null) {
                                classRef.log('alternative canvas to tint: ' + canvas.id, 4);
                            }
                        }
                        var brush = this.getTool();
                        if (classRef.brushPoint != null) {
                            brush = classRef.sprites[classRef.brushPoint];
                        }
                        if (canvas != null) {
                            classRef.log("tint " + canvas.id, 4);
                            classRef.debugBitMap(canvas);
                            canvas.tint = brush.tint;
                            if (!classRef.validate) {
                                classRef.log(canvas.tint + '=' + classRef.colorToInt(canvas.color) + '?', 4);
                                if (canvas.tint == classRef.colorToInt(canvas.color)) {
                                    classRef.steps--;
                                    var ok = true;
                                    for (var i = 0; i < classRef.canvases.length; i++) {
                                        if (classRef.canvases[i].tint != classRef.colorToInt(classRef.canvases[i].color)) {
                                            ok = false;
                                            break;
                                        }
                                    }
                                    if (ok) {
                                        classRef.triggerEvents(LandPlay.LandEvent.CAUSE_COMPLETE);
                                    }
                                    classRef.triggerRight();
                                }
                                else {
                                    classRef.triggerWrong();
                                }
                            }
                        }
                        else {
                            //todo eran transparentes
                        }
                    }
                }, this);
            }
            _super.prototype.onStageLoad.call(this);
        };
        Paint.prototype.setupTool01 = function (node) {
            var classRef = this;
            if ($(node).attr('brushpoint') != null) {
                classRef.brushPoint = $(node).attr('brushpoint');
            }
        };
        Paint.prototype.getPot = function (id) {
            for (var i = 0; i < this.pots.length; i++) {
                if (this.pots[i].id == id) {
                    return this.pots[i];
                }
            }
            return null;
        };
        Paint.prototype.getCanvas = function (id) {
            for (var i = 0; i < this.canvases.length; i++) {
                if (this.canvases[i].id == id) {
                    return this.canvases[i];
                }
            }
            return null;
        };
        Paint.prototype.update = function () {
            _super.prototype.update.call(this);
        };
        Paint.prototype.triggerEvents = function (cause) {
            _super.prototype.triggerEvents.call(this, cause);
            if (this.itemEvents == null) {
                return;
            }
            for (var i = 0; i < this.itemEvents.length; i++) {
                this.log('cause: ' + cause, 4);
                if (this.itemEvents[i].cause == cause) {
                    if (this.itemEvents[i].evtType == PaintEvents.TYPE_TINT) {
                        this.log('tintamos ' + this.itemEvents[i].sprite, 4);
                        this.sprites[this.itemEvents[i].sprite].tint = this.colorToInt(this.itemEvents[i].value);
                    }
                }
            }
            this.clearEvents(PaintEvents.TYPE_TINT);
        };
        Paint.prototype.validatePaint = function (canvas) {
            var result = false;
            this.log(canvas.tint + '=' + this.colorToInt(canvas.color) + '?', 4);
            if (canvas.tint == this.colorToInt(canvas.color)) {
                /* TODO see if would it be good to have retries in assync mode
                this.steps--;
                if (this.steps == 0) {
                    this.triggerEvents(LandEvent.CAUSE_COMPLETE);
                }
                */
                return true;
            }
            else {
                return false;
            }
        };
        Paint.prototype.collectValidable = function () {
            var result = [];
            //the right paintings
            result[0] = [];
            //the wrong ones
            result[1] = [];
            var cright = 0;
            var cwrong = 0;
            for (var i = 0; i < this.canvases.length; i++) {
                if (this.validatePaint(this.canvases[i])) {
                    result[0][cright] = this.canvases[i].id;
                    cright++;
                }
                else {
                    result[1][cwrong] = this.canvases[i].id;
                    cwrong++;
                }
            }
            return result;
        };
        return Paint;
    }(LandPlay.Tool));
    LandPlay.Paint = Paint;
    var Paintable = (function (_super) {
        __extends(Paintable, _super);
        function Paintable() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.color = '#FFFFFF';
            return _this;
        }
        return Paintable;
    }(LandPlay.LandSprite));
    LandPlay.Paintable = Paintable;
    var Canvas = (function (_super) {
        __extends(Canvas, _super);
        function Canvas() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return Canvas;
    }(Paintable));
    LandPlay.Canvas = Canvas;
    var Pot = (function (_super) {
        __extends(Pot, _super);
        function Pot(x, y, key) {
            return _super.call(this, x, y, key) || this;
        }
        return Pot;
    }(Paintable));
    LandPlay.Pot = Pot;
    var PaintEvents = (function (_super) {
        __extends(PaintEvents, _super);
        function PaintEvents() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        PaintEvents.TYPE_TINT = 'tint';
        return PaintEvents;
    }(LandPlay.LandEvent));
    LandPlay.PaintEvents = PaintEvents;
})(LandPlay || (LandPlay = {}));
/// <reference path="../LandGame.ts" />
var LandPlay;
(function (LandPlay) {
    /**
     * @class DragDrop
     *
     * Realiza los ejercicios en los que, para su resolución, es necesario arrastrar elementos
     *
     *
     * @version 1.0, 18-04-2005
     *		- Permite autoevaluar ejercicios de una sola respuesta de forma automatica
     *		- Varios recipientes
     * @TODO: Evaluar de forma asincrona las respuestas
     *
     * @author Juane Puig <juanepm@tinet.org>
     *
     */
    var DragDrop = (function (_super) {
        __extends(DragDrop, _super);
        /**
         * Constructor
         */
        function DragDrop(xmlFile, params) {
            var _this = _super.call(this) || this;
            _this.eventsAsGeneric = false;
            /* para los containers/answers multiples (en grid) */
            _this.cntAnswers = 0;
            _this.cntContainers = 0;
            _this.className = LandPlay.Mode.DragDrop;
            _this.recipients = new Array();
            _this.assignacions = new Array();
            _this.eleccions = new Array();
            _this.recippos = new Array();
            _this.recipoffset = new Array();
            if (xmlFile != undefined) {
                _this.validate = false;
                _this.log("Open the file (" + xmlFile + ")", 3);
                _this.reset();
                //Cridem el iniciador de la superclasse
                _this.init(xmlFile, params);
                //Aplicamos eventos drag y drop
                _this.extendMovieClip();
            }
            else {
                _this.log("xml not given or subclass instantiated", 3);
            }
            return _this;
        }
        /**
         * Aplicamos eventos drag y drop
         */
        DragDrop.prototype.extendMovieClip = function () {
            var classRef = this;
            /**********
            Additional Methodes for LandSprite
            ************/
            /**
             * Makes no sense for Phaser
             */
            /**********
            Metodes afegits a MovieClip
            ************/
        };
        /**
         * Registra el recipiente y sus respuestas validas
         * al tiempo que genera un nuevo espacio para permitir responder a esa pregunta
         */
        DragDrop.prototype.addRecip = function (name, respostes) {
            //Nuevo recipiente
            this.recipients.push(name);
            //Array para sus valores posibles
            this.assignacions[name] = respostes;
            //Array para las elecciones
            this.eleccions.push(new Array());
        };
        /**
         * Registra la opcion, que puede ser:
         *		- un recipiente o pregunta (registra tambien sus respuestas)
         *		- una pieza de un puzzle ya posicionado
         *		- un tablero de un puzzle sin posicionar (las piezas son los nodos hijos, los registra)
         */
        DragDrop.prototype.registerOption = function (option) {
            var classRef = this;
            if ($(option).attr('sprite') == null && this.subclass == "Puzzle") {
                // it takes a sprite, not alllowed when puzzle
                this.errors.push((option)[0].localName + " sprite is missing");
            }
            else {
                // Right objects within the recipient
                // Puzzle hasn't children
                if ((option)[0].localName == 'piece') {
                    //It's a puzzle with their pieces in its place, NO BOARD
                    //pieces will swap among themselves
                    //@TODO: carregar els events
                    if (this.subclass == "Puzzle") {
                        //First level options are recipients too
                        this.addAnswer($(option).attr('sprite'));
                        /* 	comes from ActionScript version
                            dont't need to parse here, LandGame will
                        this.setEvents($(option));
                        */
                        if ($(option).attr('attach') != undefined) {
                            this.log("Attach: " + $(option).attr('attach') + "->" + $(option).attr('sprite') + "", 3);
                            this.getMovie($(option).attr('sprite')).attachMovie($(option).attr('attach'), $(option).attr('attach'), 0);
                        }
                    }
                }
                else {
                    //It's a common DragDrop o a puzzle with board
                    var ar = new Array();
                    $(option).children('answer').each(function () {
                        //Saving right answers, will be associated in addRecip
                        var spriteKey = $(this).attr('sprite');
                        var answers = new Array();
                        var groups = spriteKey.match(/(.*)(_\*)/i);
                        if (groups != null && groups.length > 1) {
                            var i = 0;
                            while (classRef.sprites[groups[1] + '_' + i] != null) {
                                spriteKey = groups[1] + '_' + i;
                                ar.push(spriteKey);
                                answers.push(spriteKey);
                                classRef.cntAnswers++;
                                i++;
                            }
                        }
                        else {
                            ar.push(spriteKey);
                            answers.push(spriteKey);
                        }
                        for (var i = 0; i < answers.length; i++) {
                            //Saving avaliable answers
                            classRef.addAnswer(answers[i]);
                            var mc = classRef.getMovie(answers[i]);
                            //Saving answer events
                            mc.itemEvents = classRef.setEvents($(this), false, true);
                        }
                    });
                    this.log(ar.toString(), 3);
                }
                var point = new Phaser.Point(parseInt($(option).attr('x')), parseInt($(option).attr('y')));
                this.recippos.push(point);
                var pointOffset = new Phaser.Point(parseInt($(option).attr('xoffset')), parseInt($(option).attr('yoffset')));
                this.recipoffset.push(pointOffset);
                //Afegim recipient i les seves respostes correctes
                //pero nomes si hi ha movie definida
                if ($(option).attr('sprite') != undefined) {
                    if ((option)[0].localName == "container" || (option)[0].localName == "piece") {
                        var nameRec;
                        if ($(option).attr('attach') != undefined) {
                            nameRec = $(option).attr('sprite') + "." + $(option).attr('attach');
                        }
                        else {
                            nameRec = $(option).attr('sprite');
                        }
                        var groups = nameRec.match(/(.*)(_\*)/i);
                        if (groups != null && groups.length > 1) {
                            /**
                             * quitamos la posicion de referencia, es multiple*/
                            classRef.recippos.slice(0, this.recippos.length - 1);
                            classRef.recipoffset.slice(0, this.recipoffset.length - 1);
                            /* quitamos la posicion de referencia, es multiple*/
                            /*buscamos todos los coincidentes con xxxx_i */
                            var i = 0;
                            while (this.sprites[groups[1] + '_' + i] != null) {
                                var spId = groups[1] + '_' + i;
                                /**
                                 * actualizamos posiciones de recipientes*/
                                classRef.recippos.push(point);
                                classRef.recipoffset.push(pointOffset);
                                /*actualizamos posiciones de recipientes*/
                                classRef.addRecip(spId, ar);
                                classRef.cntContainers++;
                                i++;
                            }
                        }
                        else {
                            classRef.addRecip(nameRec, ar);
                        }
                    }
                }
            }
            if (classRef.cntAnswers > 0 && classRef.cntContainers > 0) {
                classRef.log('Tenemos asignaciones repetitivas', 4);
            }
        };
        DragDrop.prototype.onStageLoad = function () {
            var classRef = this;
            $(this.xml).find('container').each(function () {
                classRef.registerOption($(this));
                classRef.getAlternativeTags($(this));
                classRef.setRandom($(this));
            });
            this.setClipEvents();
            this.hashResponsesPosition();
            this.hashRecipientsPositions();
            this.allocateGrids();
            this.assignTags();
            _super.prototype.onStageLoad.call(this);
        };
        DragDrop.prototype.hashRecipientsPositions = function () {
            this.hashMoviesPosition(this.recipients);
        };
        /**
         * No importa la distribucion que se haya hecho hasta ahora
         * se hara en funcion de los grids habilitados
         */
        DragDrop.prototype.allocateGrids = function () {
            if (this.cntAnswers > 0 && this.cntContainers > 0) {
                if (this.cntAnswers != this.responses.length) {
                    console.error("answer wildcard doesn't match with responses sprites");
                }
                if (this.cntContainers != this.recipients.length) {
                    console.error("container wildcard doesn't match with container sprites");
                }
                var values = this.responseTags[this.recipients[0]];
                var max = values.length;
                //if (max>this.recipients.length) max = this.recipients.length;
                var rand = this.randPos(0, max - 1);
                /**
                 * redistribuimos las asignaciones
                 */
                this.assignacions = {};
                for (var i = 0; i < this.cntContainers; i++) {
                    /*puede haber containers sin respuesta*/
                    this.assignacions[this.recipients[i]] = new Array();
                }
                for (var i = 0; i < max; i++) {
                    var pos = rand[i];
                    var recipient = this.recipients[pos];
                    this.assignacions[recipient].push(this.responses[i]);
                }
                /**
                 * redistribuimos los tags
                 */
                this.responseTags = {};
                for (var i = 0; i < this.cntContainers; i++) {
                    /*puede haber containers sin respuesta*/
                    this.responseTags[this.recipients[i]] = new Array();
                }
                for (var i = 0; i < max; i++) {
                    var pos = rand[i];
                    var recipient = this.recipients[pos];
                    this.responseTags[recipient].push(values[pos]);
                }
            }
        };
        DragDrop.prototype.update = function () {
        };
        /**
         * Inicializa los clips que pueden ser movidos
         *		- Guarda su posicion inicial
         *		- Les aplica las funciones Drag y Drop
         */
        DragDrop.prototype.initClip = function (name) {
            var classRef = this;
            var sprite = this.getMovie(name);
            sprite.x = sprite._x;
            sprite.y = sprite._y;
            this.log("I'm " + name + "(" + sprite.x + ":" + sprite.y + ")");
            sprite.inputEnabled = true;
            sprite.input.enableDrag(true);
            sprite.events.onDragStart.add(function (sprite) {
                if (classRef.subclass == 'Puzzle') {
                    classRef.bringTop(sprite.id);
                }
                /*alert("HOLA");*/
                //former arrossega
            }, sprite);
            sprite.events.onDragStop.add(function (sprite) {
                var recipient;
                //Buscamos si estamos dejando sobre un recipiente
                for (var i = 0; i < classRef.recipients.length; i++) {
                    var r = classRef.getMovie(classRef.recipients[i]);
                    classRef.log('checkin container ' + r.id, 4);
                    if (sprite.id != r.id) {
                        //except himself
                        if (classRef.checkOverlap(r, (classRef.subclass == "Puzzle" ? null : sprite))) {
                            recipient = i;
                            break;
                        }
                    }
                }
                if (recipient == undefined) {
                    //No ha seleccionado recipiente
                    classRef.log("container not selected", 3);
                    sprite.x = sprite._x;
                    sprite.y = sprite._y;
                }
                else {
                    var cntSprite = classRef.getMovie(classRef.recipients[recipient]);
                    classRef.log("container selected (" + recipient + ")", 3);
                    var x = classRef.recippos[recipient].x;
                    var y = classRef.recippos[recipient].y;
                    //Les aplicamos el offset para el siguiente elemento
                    var xoffset = classRef.recipoffset[recipient].x;
                    var yoffset = classRef.recipoffset[recipient].y;
                    classRef.recippos[recipient].x = x + xoffset;
                    classRef.recippos[recipient].y = y + yoffset;
                    if (classRef.subclass == "Puzzle") {
                        var destMc;
                        if (classRef.recipients[recipient].indexOf(".") > 0) {
                            destMc = classRef.getMovie(classRef.recipients[recipient])._parent._name;
                        }
                        else {
                            destMc = classRef.recipients[recipient];
                        }
                        if (classRef.table == null) {
                            classRef.swapPositions(sprite.id, destMc);
                        }
                        else {
                            //drop in the nearest cell of the table
                            var p = classRef.getPosPointer();
                            var x = sprite.width / 2 + classRef.table.x; //anchor 50%
                            var curx = x;
                            var distx = sprite.width;
                            var debugGridX = [];
                            while (x < classRef.table.x + classRef.table.width) {
                                debugGridX.push(x);
                                if (Math.abs(p.x - x) < distx) {
                                    curx = x;
                                    distx = Math.abs(p.x - x);
                                    classRef.log('minX: ' + curx, 4);
                                }
                                x += sprite.width;
                            }
                            var y = sprite.height / 2 + classRef.table.y; //anchor 50%
                            var cury = y;
                            var disty = sprite.height;
                            while (y < classRef.table.y + classRef.table.height) {
                                if (Math.abs(p.y - y) < disty) {
                                    cury = y;
                                    disty = Math.abs(p.y - y);
                                    classRef.log('minY: ' + cury, 4);
                                }
                                y += sprite.height;
                            }
                            sprite.x = curx /*+ classRef.table.x*/;
                            sprite.y = cury /*+ classRef.table.y*/;
                            classRef.log('piece move: (' + curx + ',' + cury + ')', 4);
                            classRef.log('debugGridX: ' + debugGridX, 4);
                        }
                    }
                    else {
                        //Dejamos en recipiente
                        classRef.log("Pick off at container.:" + cntSprite.id, 3);
                        this.x = cntSprite._x + x;
                        this.y = cntSprite._y + y;
                        classRef.log("going to (" + this._x + ", " + this._y + ")", 3);
                        //Guardamos eleccion
                        classRef.eleccions[recipient].push(this.id);
                    }
                    //validacio sincrona
                    if (!classRef.validate) {
                        if (classRef.subclass == "Puzzle") {
                            classRef.validatePuzzle();
                        }
                        else {
                            if (!classRef.goodAnswer(this.id, cntSprite.id)) {
                                this.x = this._x;
                                this.y = this._y;
                                //Ejecutar eventos de fallo y logica de retries
                                if (classRef.retry() == 0) {
                                    classRef.log("Retries are ended!!");
                                    classRef.triggerEvents(LandPlay.LandEvent.CAUSE_TRYOUT);
                                }
                                classRef.log("Bad answer");
                                classRef.triggerEvents(LandPlay.LandEvent.CAUSE_WRONG);
                            }
                            else {
                                //run Right events
                                classRef.log("Good answer");
                                if (classRef.validateDragDrop()) {
                                    if (!classRef.eventsAsGeneric) {
                                        //all answers are right
                                        classRef.triggerEvents(LandPlay.LandEvent.CAUSE_COMPLETE);
                                    }
                                }
                                //generic events
                                classRef.triggerEvents(LandPlay.LandEvent.CAUSE_RIGHT);
                                //this sprite events
                                classRef.triggerEvents(LandPlay.LandEvent.CAUSE_RIGHT, sprite.id);
                                /* no need to addPoint, specific events will
                                classRef.addPoints(classRef.countPoints());
                                */
                            }
                        }
                    }
                }
            }, sprite);
        };
        /**
         * Intercambia las posiciones de las dos movies
         * @param m1, m2:nombres de las pelis en formato "movie.innermovie"
         * m1 recuperara la posicio inicial
         */
        DragDrop.prototype.swapPositions = function (m1, m2) {
            var x, _x, y, _y;
            var sm1 = this.getMovie(m1);
            var sm2 = this.getMovie(m2);
            this.log("(" + m1 + ") exchanged by (" + m2 + ")", 3);
            sm1.x = sm1._x;
            sm1.y = sm1._y;
            this.log("m1 before (" + sm1.x + ", " + sm1.y + ")", 3);
            this.log("m2 before (" + sm2.x + ", " + sm2.y + ")", 3);
            x = sm1.x;
            _x = sm1._x;
            y = sm1.y;
            _y = sm1._y;
            sm1.x = sm2.x;
            sm1.y = sm2.y;
            sm1._x = sm2._x;
            sm1._y = sm2._y;
            sm2.x = x;
            sm2.y = y;
            sm2._x = _x;
            sm2._y = _y;
            this.log("m1 after (" + sm1.x + ", " + sm1.y + ")", 3);
            this.log("m2 after (" + sm2.x + ", " + sm2.y + ")", 3);
            //@TODO: mirar que passa que se mueve todo el root
            /* viene de ActionScript
            _root._x = 0;
            _root._y = 0;
            */
        };
        /**
         * Registrar respuesta posible
         */
        DragDrop.prototype.addAnswer = function (name) {
            this.log('added answer: ' + name, 4);
            this.responses.push(name);
        };
        /**
         * checks if all answers are right
         */
        DragDrop.prototype.validateDragDrop = function () {
            var found;
            for (var i = 0; i < this.recipients.length; i++) {
                var recipient = this.recipients[i];
                this.log("for " + recipient, 4);
                found = true;
                for (var k = 0; k < this.assignacions[recipient].length; k++) {
                    if (k == 0 && this.assignacions[recipient][0] == undefined /*no response expected*/
                        || this.eleccions[i].indexOf(this.assignacions[recipient][k]) >= 0) {
                        found = true;
                    }
                    else {
                        found = false;
                        break;
                    }
                }
                if (!found)
                    return false;
            }
            return true;
        };
        DragDrop.prototype.validatePuzzle = function () {
            this.error('To be overriden at Puzzle');
        };
        return DragDrop;
    }(LandPlay.LandGame));
    LandPlay.DragDrop = DragDrop;
})(LandPlay || (LandPlay = {}));
/// <reference path="DragDrop.ts" /> 
var LandPlay;
(function (LandPlay) {
    /**
     * @class Puzzle
     * @see DragDrop, LandPlay
     * To resolve this exercise the user must put the pieces in the correct order and position
     * (a puzzle... much easier to play than to explain)
     *
     *
     * @author Juane Puig <juanepm@tinet.org>
     *
     */
    var Puzzle = (function (_super) {
        __extends(Puzzle, _super);
        function Puzzle(xmlFile, params) {
            var _this = _super.call(this, xmlFile, params) || this;
            //Mezclamos las piezas siempre
            _this.randFlag = true;
            // Indicamos a la superclase que estamos desde la subclase Puzzle
            // así hará el tratamiento especializado
            _this.subclass = LandPlay.Mode.Puzzle;
            _this.className = _this.subclass;
            //this.init(xmlFile, params);
            _this.extendMovieClip();
            return _this;
        }
        /**
         * init "key" sprite to be draggable
         *
         * @precondition Se asume que, inicialmente, cada respuesta en su posición correcta
         * 	de ahí que  por defecto siempre tengamos el randFlag a true en el constructor
         *  para garantizar que se hace la mezcla
         *
         * overrides DragDrop's methode
         */
        Puzzle.prototype.initClip = function (nom) {
            this.log('enter initClip', 4);
            var classRef = this;
            var sprite = this.getMovie(nom);
            sprite._realx = sprite._x;
            sprite._realy = sprite._y;
            sprite.x = sprite._x;
            sprite.y = sprite._y;
            _super.prototype.initClip.call(this, nom);
            /*
            this.bringTop(nom);
            this.log("I am " + nom + "(" + sprite.x + ":" + sprite.y + ")", 3);

            sprite.inputEnabled = true;
            sprite.input.enableDrag(true);
            sprite.events.onDragStart.add(function() {

            }, sprite);

            sprite.events.onDragStop.add(function() {
                if (eval(this._dropTarget)._name != undefined) {
                    classRef.log("trying to drop over " + eval(this._dropTarget)._name, 3);
                    this.swapDepths(0);
                    this.deixa();
                } else {
                    this.stopDrag();
                    this.x = this._x;
                    this.y = this._y;
                }

            }, sprite);
            */
        };
        /**
         * Realiza la verificación de todas las posiciones del puzzle, ejecutando los
         * Eventos de acierto cargados, en caso que sea correcto.
         * Si la vlidacion es sincrona sera llamado desde el prototipo "arrossega", definido en
         * DragDrop
         */
        Puzzle.prototype.validatePuzzle = function () {
            var res = true;
            var mc;
            if (this.table == null) {
                for (var i = 0; res && i < this.responses.length; i++) {
                    mc = this.getMovie(this.responses[i]);
                    res = (mc._x == mc._realx && mc._y == mc._realy);
                }
            }
            else {
                res = true;
                var valtable = this.collectValidable();
                res = (valtable[1].length == 0);
            }
            if (res) {
                this.log("PUZZLE COMPLETED!!", 3);
                this.triggerEvents(LandPlay.LandEvent.CAUSE_COMPLETE);
                //better use event
                //this.addPoints(countPoints());
            }
            else {
                this.log("PUZZLE incomplet!!", 3);
                if (this.retry() == 0) {
                    this.log("Retries are ended!!");
                    this.triggerEvents(LandPlay.LandEvent.CAUSE_TRYOUT);
                }
            }
        };
        Puzzle.prototype.collectValidable = function () {
            var classRef = this;
            var result = [];
            //the right paintings
            result[0] = [];
            //the wrong ones
            result[1] = [];
            var cright = 0;
            var cwrong = 0;
            if (this.table == null) {
                var temp = new Array();
                for (var i = 0; i < this.responses.length; i++) {
                    temp.push(this.sprites[this.responses[i]]);
                }
                /* order by their positions */
                temp.sort(function (a, b) {
                    if (a.y == b.y) {
                        return a.x - b.x;
                    }
                    else {
                        return a.y - b.y;
                    }
                });
                for (var i = 0; i < this.responses.length; i++) {
                    if (this.responses[i] == temp[i].id) {
                        result[0].push(this.responses[i]);
                    }
                    else {
                        result[1].push(this.responses[i]);
                    }
                }
            }
            else {
                /* first piece as cell dimensions of the puzzle*/
                var piece = this.getMovie(this.responses[0]);
                var i = 0;
                for (var row = piece.height / 2 + this.table.y; row < this.table.y + this.table.height; row += piece.height) {
                    for (var col = piece.width / 2 + this.table.x; col < this.table.x + this.table.width; col += piece.width) {
                        this.log('(' + col + ',' + row + ')', 4);
                        if (i < this.responses.length) {
                            var p = this.getMovie(this.responses[i]);
                            p.updateCrop();
                            if (!Phaser.Rectangle.containsPoint(p.getBounds(), new Phaser.Point(col, row))) {
                                result[1][cwrong] = this.responses[i];
                                cwrong++;
                                this.log('piece ' + this.responses[i] + ' fail', 4);
                            }
                            else {
                                result[0][cright] = this.responses[i];
                                cright++;
                                this.log('piece ' + this.responses[i] + ' ok', 4);
                            }
                        }
                        else {
                            console.warn("response " + i + " out of the bounds of the table");
                        }
                        i++;
                    }
                }
            }
            return result;
        };
        Puzzle.prototype.onStageLoad = function () {
            var classRef = this;
            $(this.xml).find('piece').each(function () {
                classRef.registerOption($(this));
            });
            $(this.xml).find('table').each(function () {
                classRef.setRandom($(this));
                var sprite = $(this).attr('sprite');
                classRef.table = classRef.getMovie(sprite);
                classRef.recipients = [];
                classRef.recipients.push(sprite);
            });
            _super.prototype.onStageLoad.call(this);
        };
        return Puzzle;
    }(LandPlay.DragDrop));
    LandPlay.Puzzle = Puzzle;
})(LandPlay || (LandPlay = {}));
/// <reference path="../LandGame.ts" />
var LandPlay;
(function (LandPlay) {
    /**
     * @class Simon
     * @see LandPlay
     *
     * @version 1.0, 13-11-2005
     *
     * @author Juane Puig <juanepm@tinet.org>
     *
     */
    var Simon = (function (_super) {
        __extends(Simon, _super);
        /**
         * Constructor
         */
        function Simon(xmlFile, params) {
            var _this = _super.call(this) || this;
            /**
             * milisegundos entre mostrar un elemento de la secuencia i el siguiente
             *
             */
            _this.transition = 1000;
            /**
             * unidimensional array with sprite names
             * Pads that can take part of the sequence
             * Alias for responses
             */
            _this.pads = new Array();
            _this.padsounds = new Array();
            /**
             * index of the sequence
             */
            _this.indSeq = 0;
            /**
             * Farest point of the sequence
             * it increases as long a game successes
             */
            _this.farest = 0;
            /**
             * sequence is auto playing
             */
            _this.playing = false;
            _this.talking = false;
            /**
             * sequence length
             */
            _this.maxseq = 0;
            _this.className = LandPlay.Mode.Simon;
            _this.pads = new Array();
            _this.sequence = new Array();
            if (xmlFile != undefined) {
                //validate = false;
                _this.log("Open the file (" + xmlFile + ")", 3);
                _this.reset();
                _this.init(xmlFile, params);
                _this.extendMovieClip();
            }
            else {
                _this.log("xml not given o subclass instantiated", 3);
            }
            return _this;
        }
        /**
         * former MovieClip prototype extention, not used
         */
        Simon.prototype.extendMovieClip = function () {
            var classRef = this;
            /*
            LandSprite.prototype.select = function() {

            }


            MovieClip.prototype.XXX = function() {
            }
            */
        };
        /**
         * xml nodes parsing for simon game
         *		- setup
         *		- pads
         *		- ...
         */
        Simon.prototype.registerOption = function (option) {
            var opt = option;
            var classRef = this;
            switch ((opt)[0].localName) {
                case "pad":
                    if ($(opt).attr('sprite') != undefined) {
                        this.log("Pad " + $(opt).attr('sprite') + " found", 4);
                        this.pads.push($(opt).attr('sprite'));
                        if ($(opt).attr('sound') != undefined) {
                            this.log("Sound " + $(opt).attr('sound') + " found", 4);
                            this.padsounds.push($(opt).attr('sound'));
                        }
                    }
                    break;
                case "setup":
                    if ($(opt).attr('transition') != undefined) {
                        this.transition = parseInt($(opt).attr('transition'));
                    }
                    if ($(opt).attr('maxseq') != undefined) {
                        this.maxseq = parseInt($(opt).attr('maxseq'));
                    }
                    //if (opt.attributes.rand=="true"){
                    this.randFlag = true;
                    //}
                    if ($(opt).attr('talk') != undefined) {
                        this.mcTalk = this.sprites[$(opt).attr('talk')];
                        this.log("Defining Simon's mouth " + $(opt).attr('talk') + "(" + this.mcTalk.id + ")", 4);
                        this.initTalk();
                    }
                    this.itemEvents = this.setEvents(opt, true);
                    break;
            }
        };
        Simon.prototype.initTalk = function () {
            var classRef = this;
            this.mcTalk.inputEnabled = true;
            this.mcTalk.events.onInputDown.add(function (talk) {
                classRef.start();
                talk.events.onInputDown.forget();
                talk.inputEnabled = false;
            }, this);
        };
        Simon.prototype.getProgressPerc = function () {
            var p = this.farest / this.maxseq;
            this.log("perc: " + (p * 100) + "%", 4);
            return p;
        };
        Simon.prototype.initClip = function (nom) {
        };
        Simon.prototype.setPadsBehaviour = function () {
            var classRef = this;
            for (var i = 0; i < this.pads.length; i++) {
                var mc = this.getMovie(this.pads[i]);
                mc.inputEnabled = true;
                mc.events.onInputUp.add(function (piece) {
                    piece.frame = 0;
                }, this);
                mc.events.onInputDown.add(function (piece) {
                    piece.frame = 1;
                    var mname = piece.id;
                    /*
                    See if this is a pad
                    whilst Simon is not playing its sequence
                    */
                    if (classRef.pads.indexOf(mname) > -1 && !classRef.talking) {
                        classRef.log("Clip " + this.id + " is pad", 4);
                        if (classRef.playing) {
                            classRef.log("Sequence is playing. No action is taken", 4);
                        }
                        else {
                            var res = classRef.addResponse(piece.id);
                            //if (res > -1) {
                            //play its sound
                            var p = classRef.sequence.indexOf(mname);
                            var sound = classRef.padsounds[p];
                            var snd = classRef.game.sound.play(sound);
                            //}
                            switch (res) {
                                case 1:
                                    classRef.log("Last, farest pad of the sequence and no error", 4);
                                    /*No need to diable input
                                    for (var i = 0; i < classRef.pads.length; i++) {
                                        var mc = classRef.getMovie(classRef.pads[i]);
                                        mc.events.onInputOut.forget();
                                    }*/
                                    //Increasing sequence
                                    classRef.farest++;
                                    if (classRef.farest < classRef.maxseq) {
                                        snd.onStop.add(function () {
                                            //and start sequence again
                                            //once last sound ends
                                            classRef.playSequence();
                                            classRef.updateProgress();
                                        }, this);
                                    }
                                    else {
                                        snd.onStop.add(function () {
                                            //rest mode
                                            classRef.mcTalk.frame = 0;
                                            //enable start button
                                            classRef.updateProgress();
                                            classRef.initTalk();
                                            classRef.triggerEvents(LandPlay.LandEvent.CAUSE_COMPLETE);
                                        }, this);
                                    }
                                    break;
                                case 0:
                                    classRef.log("No mistakes, but still not at the end", 4);
                                    classRef.indSeq++;
                                    var sound = classRef.padsounds[p];
                                    snd.onStop.forget();
                                    break;
                                case -1:
                                    //Error!
                                    classRef.log("Error", 4);
                                    snd.onStop.add(function () {
                                        classRef.triggerEvents(LandPlay.LandEvent.CAUSE_WRONG);
                                        classRef.clearSequence();
                                        if (classRef.retry() == 0) {
                                            classRef.triggerEvents(LandPlay.LandEvent.CAUSE_TRYOUT);
                                        }
                                        else {
                                            classRef.start();
                                            classRef.updateProgress();
                                        }
                                    }, this);
                                    break;
                            }
                        }
                        delete this.onRelease;
                    }
                }, mc);
            }
        };
        /**
         * Restart items
         */
        Simon.prototype.reset = function () {
            _super.prototype.reset.call(this);
        };
        Simon.prototype.hashPieces = function () {
            this.log("pads avaliable " + this.pads.toString(), 4);
            this.sequence = this.rand(this.pads, this.maxseq);
            this.log("sequence hashed " + this.sequence, 4);
            var tmp = new Array();
            for (var k = 0; k <= this.sequence.length; k++) {
                var j = this.pads.indexOf(this.sequence[k]);
                if (j >= 0) {
                    tmp.push(this.padsounds[j]);
                }
            }
            this.padsounds = tmp;
        };
        Simon.prototype.start = function () {
            var classRef = this;
            if (this.sequence.length == 0) {
                //get a hashed sequence
                this.hashPieces();
                this.setPadsBehaviour();
            }
            this.indSeq = 0;
            this.farest = 0;
            this.playSequence();
        };
        Simon.prototype.onStageLoad = function () {
            var classRef = this;
            $(this.xml).find('setup').each(function () {
                classRef.registerOption($(this));
            });
            $(this.xml).find('pad').each(function () {
                classRef.registerOption($(this));
            });
            this.setClipEvents();
            this.updateProgress();
            _super.prototype.onStageLoad.call(this);
        };
        Simon.prototype.update = function () {
        };
        Simon.prototype.next = function () {
            var classRef = this;
            if (this.indSeq < this.padsounds.length) {
                var sound = this.padsounds[this.indSeq];
                this.log("Playing sound " + sound + " ...", 4);
                var snd = this.game.sound.play(sound);
                this.sprites[this.sequence[this.indSeq]].frame = 1;
                /**
                 * Each sound ending starts the next one
                 */
                snd.onStop.add(function (sound) {
                    classRef.log("Sound " + sound.key + " ended", 4);
                    classRef.sprites[classRef.sequence[classRef.indSeq]].frame = 0;
                    classRef.indSeq++;
                    if (classRef.indSeq <= classRef.farest) {
                        if (classRef.playing) {
                            classRef.log("calling next", 4);
                            classRef.next();
                        }
                    }
                    else {
                        if (classRef.mcTalk != null) {
                            //listen mode
                            classRef.mcTalk.frame = 2;
                        }
                        classRef.talking = false;
                        classRef.playing = false;
                        for (var i = 0; i < classRef.pads.length; i++) {
                            var mc = classRef.getMovie(classRef.pads[i]);
                            mc.onRelease = function () { this.select(); };
                        }
                        classRef.indSeq = 0;
                    }
                    sound.onStop.removeAll(this);
                }, this);
            }
        };
        Simon.prototype.playSequence = function () {
            //this._itv = setInterval(this.doPlaySequence, this.transition);
            this.doPlaySequence();
        };
        Simon.prototype.doPlaySequence = function () {
            this.playing = true;
            this.talking = true;
            for (var i = 0; i < this.pads.length; i++) {
                var mc = this.getMovie(this.pads[i]);
                mc.events.onInputUp.forget();
            }
            if (this.mcTalk != null) {
                //playing mode
                this.mcTalk.frame = 1;
            }
            this.indSeq = 0;
            this.next();
            clearInterval(this._itv);
        };
        Simon.prototype.clearSequence = function () {
        };
        Simon.prototype.addResponse = function (button) {
            var res;
            this.log("At position " + this.indSeq + " of the sequence", 4);
            if (this.sequence[this.indSeq] == button) {
                if (this.indSeq == this.farest) {
                    res = 1;
                }
                else {
                    res = 0;
                }
            }
            else {
                res = -1;
            }
            return res;
        };
        return Simon;
    }(LandPlay.LandGame));
    LandPlay.Simon = Simon;
})(LandPlay || (LandPlay = {}));
/// <reference path="../../LandGame.ts" />
var LandPlay;
(function (LandPlay) {
    /**
     * @class Shot
     *
     * @version 1.0, 23/11/2016
     * @see https://www.phaser.io/tutorials/coding-tips-007
     * @author Juane Puig <juanepm@tinet.org>
     *
     */
    var Bullet = (function (_super) {
        __extends(Bullet, _super);
        function Bullet(game, key) {
            var _this = _super.call(this, game, 0, 0, key) || this;
            //how fast the bullet should grow in size as it travels
            _this.scaleSpeed = 0;
            _this.game = game;
            _this.texture.baseTexture.scaleMode = PIXI.scaleModes.NEAREST;
            _this.anchor.set(0.5);
            _this.checkWorldBounds = true;
            _this.outOfBoundsKill = true;
            _this.exists = false;
            _this.tracking = false;
            _this.scaleSpeed = 0;
            _this.events.onOutOfBounds.add(function () {
                console.debug('BULLET out of bounds');
            }, _this);
            return _this;
        }
        Bullet.prototype.fire = function (x, y, angle, speed, gx, gy) {
            console.log('Bullet fire: ' + this.id + ', x: ' + x
                + ', y: ' + y + ', speed: ' + speed);
            gx = gx || 0;
            gy = gy || 0;
            this.game.physics.arcade.enable(this);
            this.reset(x, y);
            this.scale.set(1);
            this.game.physics.arcade.velocityFromAngle(angle, speed, this.body.velocity);
            this.angle = angle;
            this.body.gravity.set(gx, gy);
        };
        Bullet.prototype.update = function () {
            if (this.tracking) {
                this.rotation = Math.atan2(this.body.velocity.y, this.body.velocity.x);
            }
            if (this.scaleSpeed > 0) {
                this.scale.x += this.scaleSpeed;
                this.scale.y += this.scaleSpeed;
            }
            if (this.exists) {
                //console.log(this.x + ', ' + this.y);
            }
        };
        ;
        return Bullet;
    }(Phaser.Sprite));
    LandPlay.Bullet = Bullet;
    var BulletsPack = (function (_super) {
        __extends(BulletsPack, _super);
        function BulletsPack(game, name, conf, padding) {
            var _this = _super.call(this, game, game.world, name, false, true, Phaser.Physics.ARCADE) || this;
            // is the time the player is allowed to shoot again. 
            _this.nextFire = 0;
            //is the speed the bullets this particular weapon fires travel at.
            _this.bulletSpeed = 200;
            //the rate at which this weapon fires. The lower the number, the higher the firing rate.
            _this.fireRate = 500;
            //makes it easy to shoot pieces
            _this.padding = 10;
            _this.game = game;
            _this.nextFire = 0;
            _this.bulletSpeed = 600;
            _this.fireRate = 100;
            _this.conf = conf;
            if (padding != null)
                _this.padding = padding;
            var top = (_this.conf.remains >= 0 ? _this.conf.remains : 64);
            _this.load(top);
            return _this;
        }
        BulletsPack.prototype.load = function (top) {
            top = (top ? top : 1);
            top = top * this.conf.rows.length;
            for (var i = 0; i < top; i++) {
                var bullet = this.add(new Bullet(this.game, this.conf.key), true);
                bullet.id = this.conf.key + '_' + this.children.length;
                bullet.body.width = bullet.body.width + this.padding;
                bullet.body.height = bullet.body.height + this.padding;
            }
        };
        BulletsPack.prototype.isInfinite = function () {
            return this.conf.remains <= -1;
        };
        BulletsPack.prototype.fire = function (source) {
            if (this.game.time.time < this.nextFire) {
                return;
            }
            console.log('source: ' + source.id);
            var x = source.x + 10;
            var y = source.y + 10;
            for (var i = 0; i < this.conf.rows.length; i++) {
                var bullet = null;
                if (this.conf.remains >= 0) {
                    //search for the first living not visible
                    for (var j = 0; j < this.children.length; j++) {
                        if (!this.getAt(j).visible && this.getAt(j).alive) {
                            bullet = this.getAt(j);
                            break;
                        }
                    }
                    //bullet = this.getFirstAlive();
                }
                else {
                    //infinite armor
                    bullet = this.getFirstExists(false);
                }
                if (bullet != null) {
                    bullet.fire(x + this.conf.rows[i].x, y + this.conf.rows[i].y, this.conf.rows[i].angle, this.conf.rows[i].speed, this.conf.rows[i].gx, this.conf.rows[i].gy);
                }
            }
            this.nextFire = this.game.time.time + this.fireRate;
        };
        return BulletsPack;
    }(Phaser.Group));
    LandPlay.BulletsPack = BulletsPack;
    /**
     * For two or more shoot types
     */
    var BulletCombo = (function (_super) {
        __extends(BulletCombo, _super);
        function BulletCombo(key) {
            var _this = _super.call(this) || this;
            _this.bulletsPacks = [];
            _this.key = key;
            return _this;
        }
        BulletCombo.prototype.add = function (pack) {
            this.bulletsPacks.push(pack);
        };
        BulletCombo.prototype.fire = function (source) {
            for (var i = 0; i < this.bulletsPacks.length; i++) {
                this.bulletsPacks[i].fire(source);
            }
        };
        BulletCombo.prototype.reset = function () {
            for (var i = 0; i < this.bulletsPacks.length; i++) {
                this.bulletsPacks[i].visible = false;
                this.bulletsPacks[i].callAll('reset', null, 0, 0);
                this.bulletsPacks[i].setAll('exists', false);
            }
        };
        BulletCombo.prototype.isInfinite = function () {
            if (this.bulletsPacks.length > 0) {
                return this.bulletsPacks[0].isInfinite();
            }
            return false;
        };
        BulletCombo.prototype.getArmor = function () {
            if (this.bulletsPacks.length > 0) {
                var count = 0;
                for (var i = 0; i < this.bulletsPacks[0].children.length; i++) {
                    var bullet = this.bulletsPacks[0].getAt(i);
                    if (bullet.alive && !bullet.visible) {
                        count++;
                    }
                }
                return count / this.bulletsPacks[0].conf.rows.length;
            }
        };
        BulletCombo.KEYS = {
            'spacebar': Phaser.Keyboard.SPACEBAR,
            'control': Phaser.Keyboard.CONTROL
        };
        return BulletCombo;
    }(LandPlay.CoreObject));
    LandPlay.BulletCombo = BulletCombo;
    var BulletConfig = (function (_super) {
        __extends(BulletConfig, _super);
        function BulletConfig(key) {
            var _this = _super.call(this) || this;
            _this.rows = [];
            _this.remains = -1;
            _this.key = key;
            return _this;
        }
        //one shot may have more than one trajectories
        BulletConfig.prototype.addRow = function (x, y, angle, speed, gx, gy) {
            this.rows.push(new BulletConfigRow(x, y, angle, speed, gx, gy));
        };
        return BulletConfig;
    }(LandPlay.CoreObject));
    LandPlay.BulletConfig = BulletConfig;
    var BulletConfigRow = (function (_super) {
        __extends(BulletConfigRow, _super);
        function BulletConfigRow(x, y, angle, speed, gx, gy) {
            var _this = _super.call(this) || this;
            _this.speed = 200;
            _this.x = x;
            _this.y = y;
            _this.angle = angle;
            _this.gx = gx;
            _this.gy = gy;
            _this.speed = speed;
            return _this;
        }
        return BulletConfigRow;
    }(LandPlay.CoreObject));
    LandPlay.BulletConfigRow = BulletConfigRow;
})(LandPlay || (LandPlay = {}));
/// <reference path="../LandGame.ts" />
/// <reference path="Catch.ts" />
/// <reference path="arcade/Weapon.ts" />
var LandPlay;
(function (LandPlay) {
    /**
     * @class Arcade
     * @see LandPlay
     *
     * @version 1.0, 20/05/2016
     *
     * @author Juane Puig <juanepm@tinet.org>
     *
     */
    var Arcade = (function (_super) {
        __extends(Arcade, _super);
        /**
         * Constructor
         */
        function Arcade(xmlFile, params) {
            var _this = _super.call(this) || this;
            /**
             * Pixels of tolerance to soften edges limits
             *
             */
            _this.tolerance = 0;
            /**
             * relative speed for pieces+landscape+craft
             */
            _this.speed = 1;
            /**
             * relative speed for craft
             */
            _this.craftSpeed = 1;
            /**
             * speed the pieces+craft go when they runaway
             */
            _this.runawaySpeed = 4;
            /**
             * groups of clonated pieces
             */
            _this.piecesGroups = new Array();
            /**
             * right cursor key animation
             */
            _this.forwardFrame = 'right';
            /**
             * left cursor key animation
             */
            _this.backFrame = 'left';
            /**
             * up cursor key animation
             */
            _this.upFrame = 'up';
            /**
             * down cursor key animation
             */
            _this.downFrame = 'down';
            _this.jumpTouch = false;
            _this.yJump = 300;
            _this.yScale = 0.0;
            /**
             * remaning stage time
             * 0: Undefined
             */
            _this.time = 0;
            /**
             * floor has 2 dimensions,
             * up/down key cursor move craft up/down (y-axis) the stage
             */
            _this.floor2d = false;
            _this.className = LandPlay.Mode.Arcade;
            _this.pieces = new Array();
            _this.endStage = new Array();
            _this.beginStage = new Array();
            _this.caughtPieces = new Array();
            _this.backpieces = new Array();
            if (xmlFile != undefined) {
                _this.log("Open the file (" + xmlFile + ")", 3);
                _this.reset();
                _this.init(xmlFile, params);
                _this.extendMovieClip();
                _this.hashPieces();
            }
            else {
                _this.log("xml not given o subclass instantiated", 3);
            }
            return _this;
        }
        Arcade.prototype.onStageLoad = function () {
            var classRef = this;
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            $(this.xml).find('setup').each(function () {
                classRef.registerOption($(this));
            });
            $(this.xml).find('edge').each(function () {
                classRef.registerOption($(this));
            });
            $(this.xml).find('landscape').each(function () {
                //landscapes behind of craft
                classRef.registerOption($(this));
            });
            $(this.xml).find('craft').each(function () {
                classRef.registerOption($(this));
            });
            $(this.xml).find('config > piece').each(function () {
                //in front of craft
                classRef.registerOption($(this));
            });
            this.cursors = this.game.input.keyboard.createCursorKeys();
            //  We need to enable physics on the player
            this.game.physics.arcade.enable(this.mcCraft);
            this.game.camera.follow(this.mcCraft);
            if (this.edgesLayer != null) {
                this.edgesLayer.resizeWorld();
            }
            //do not leave craft get out the world edges
            this.mcCraft.body.collideWorldBounds = true;
            _super.prototype.onStageLoad.call(this);
        };
        Arcade.prototype.shootPiece = function (bullet, piece) {
            if (this.hasCause(piece, LandPlay.LandEvent.CAUSE_WRONG)) {
                this.log('shooting bad piece', 4);
                piece.kill();
                bullet.kill();
                this.triggerEvents(LandPlay.LandEvent.CAUSE_RIGHT);
            }
        };
        Arcade.prototype.touchPiece = function (craft, piece) {
            this.log('piece ' + craft.id + ' overloaps piece ' + piece.id, 4);
            if (piece.pieceContainer != null && piece.body.velocity.x == 0 && piece.body.velocity.y == 0) {
                /*is throwable and it is still*/
                return;
            }
            piece = this.getMovie(piece.id);
            for (var i = 0; i < piece.itemEvents.length; i++) {
                if (piece.itemEvents[i].cause == LandPlay.LandEvent.CAUSE_RIGHT) {
                    //there is at least one right
                    //trigger generic right events
                    this.triggerEvents(LandPlay.LandEvent.CAUSE_RIGHT);
                    if (this.caughtPieces == null) {
                        this.caughtPieces.push(piece.id);
                    }
                    break;
                }
                else if (piece.itemEvents[i].cause == LandPlay.LandEvent.CAUSE_WRONG) {
                    //there is at least one wrong
                    //trigger generic wrong events
                    this.triggerEvents(LandPlay.LandEvent.CAUSE_WRONG);
                    if (this.retry() == 0) {
                        //trigger generic tryout events
                        this.triggerEvents(LandPlay.LandEvent.CAUSE_TRYOUT);
                    }
                    break;
                }
            }
            //trigger specific right/wrong events
            this.triggerEvents(LandPlay.LandEvent.CAUSE_RIGHT, piece.id);
            this.triggerEvents(LandPlay.LandEvent.CAUSE_WRONG, piece.id);
        };
        Arcade.prototype.update = function () {
            var classRef = this;
            if (this.craft == null) {
                this.log('craft not ready', 4);
                return;
            }
            if (!this.mcCraft.inWorld) {
                /* moved to onOutOfBounds
                //ladies & gentleman, craft has left the building
                //this.mcCraft.game.paused = true;
                this.landscapeGroup.setAll('body.velocity.x', 0);
                this.landscapeGroup.setAll('body.velocity.y', 0);

                this.landscapeForeGroup.setAll('body.velocity.x', 0);
                this.landscapeForeGroup.setAll('body.velocity.y', 0);

                this.landscapeForeGroup.setAll('body.velocity.x', 0);
                this.landscapeForeGroup.setAll('body.velocity.y', 0);

                for (var i = 0; i < this.piecesGroups.length; i++) {
                    //for recycled throwable pieces
                    this.piecesGroups[i].setAll('body.velocity.x', 0);
                    this.piecesGroups[i].setAll('body.velocity.y', 0);

                }
                */
                return;
            }
            var bullets = this.castBullets(this.mcCraft);
            if (this.mcCraft.bullets != null) {
                for (var i = 0; i < bullets.length; i++) {
                    if (this.game.input.keyboard.isDown(bullets[i].key)) {
                        bullets[i].fire(this.mcCraft);
                        if (bullets[i].text != null) {
                            this.texts[bullets[i].text].text = bullets[i].getArmor();
                        }
                    }
                }
            }
            if (this.edgesLayer != null) {
                this.game.physics.arcade.collide(this.mcCraft, this.edgesLayer);
            }
            //edges collision control
            this.game.physics.arcade.collide(this.mcCraft, this.edgesGroup);
            if (this.mcCraft.craftEnabled == null || this.mcCraft.craftEnabled) {
                //release speed
                this.mcCraft.body.velocity.x = 0;
                if (this.floor2d) {
                    //stop y-move when no key pressed
                    this.mcCraft.body.velocity.y = 0;
                }
                else {
                    //let gravity do its work
                }
                if (this.game.input.activePointer.isDown) {
                    if (this.game.input.activePointer.x < this.mcCraft.x) {
                        this.mcCraft.body.velocity.x = -(this.craftSpeed * this.mcCraft.moveX);
                        this.moveCraft(this.backFrame);
                    }
                    if (this.game.input.activePointer.x > this.mcCraft.x) {
                        this.mcCraft.body.velocity.x = (this.craftSpeed * this.mcCraft.moveX);
                        this.moveCraft(this.forwardFrame);
                    }
                    if (this.game.input.activePointer.y < this.mcCraft.y) {
                        this.mcCraft.body.velocity.y = -(this.craftSpeed * this.mcCraft.moveY);
                        this.moveCraft(this.upFrame);
                    }
                    if (this.game.input.activePointer.y > this.mcCraft.y) {
                        this.mcCraft.body.velocity.y = (this.craftSpeed * this.mcCraft.moveY);
                        this.moveCraft(this.downFrame);
                    }
                    //end mouse
                }
                else if ((this.cursors.left.isDown || this.cursors.right.isDown || this.cursors.up.isDown || this.cursors.down.isDown)) {
                    //there is some cursor pressed
                    if (this.cursors.left.isDown) {
                        this.mcCraft.body.velocity.x = -(this.craftSpeed * this.mcCraft.moveX);
                        if (this.cursors.up.isDown) {
                            //both pressed left+up
                            this.moveCraft(this.upFrame);
                        }
                        else if (this.cursors.down.isDown) {
                            //both pressed left+down
                            this.moveCraft(this.downFrame);
                        }
                        else {
                            //  Move to the left
                            this.moveCraft(this.backFrame);
                        }
                    }
                    if (this.cursors.right.isDown) {
                        this.log(JSON.stringify(this.mcCraft.body.touching), 4);
                        this.mcCraft.body.velocity.x = (this.craftSpeed * this.mcCraft.moveX);
                        if (this.cursors.up.isDown) {
                            //both pressed right+up
                            this.moveCraft(this.upFrame);
                        }
                        else if (this.cursors.down.isDown) {
                            //both pressed right+down
                            this.moveCraft(this.downFrame);
                        }
                        else {
                            //  Move to the right
                            this.moveCraft(this.forwardFrame);
                        }
                    }
                    if (this.cursors.up.isDown) {
                        this.log(JSON.stringify(this.mcCraft.body.touching), 4);
                        this.log('touching down: ' + this.mcCraft.body.touching['down'], 4);
                        if (this.floor2d) {
                            this.mcCraft.body.velocity.y = -(this.craftSpeed * this.mcCraft.moveY);
                            this.moveCraft(this.upFrame);
                            this.mcCraft.scale.set(this.mcCraft.scale.x - this.yScale, this.mcCraft.scale.y - this.yScale);
                        }
                        else {
                            //jump
                            if (!this.jumpTouch || this.mcCraft.body.touching['down']) {
                                this.mcCraft.body.velocity.y = -this.yJump;
                            }
                        }
                    }
                    if (this.cursors.down.isDown /*&& this.mcCraft.body.touching.down*/) {
                        if (this.floor2d) {
                            this.mcCraft.body.velocity.y = (this.craftSpeed * this.mcCraft.moveY);
                            this.moveCraft(this.downFrame);
                            this.mcCraft.scale.set(this.mcCraft.scale.x + this.yScale, this.mcCraft.scale.y + this.yScale);
                        }
                        else {
                            //shrink! TODO
                            //this.mcCraft.body.velocity.y = -350;
                        }
                    }
                    //this.suitToMaze();
                }
                else {
                    //no cursor key pressed
                    this.moveCraft(this.moveFrame);
                }
            }
            var alive = 0;
            for (var i = 0; i < this.piecesGroups.length; i++) {
                //edge pieces collision
                /*
                this.game.physics.arcade.collide(this.piecesGroups[i], this.edgesGroup);
                */
                if (this.mcCraft.craftEnabled == null || this.mcCraft.craftEnabled) {
                    //craft is not running away
                    //check pieces collisions
                    //this.game.physics.arcade.collide(this.mcCraft, this.piecesGroups[i]);
                    this.game.physics.arcade.overlap(this.mcCraft, this.piecesGroups[i], this.touchPiece, null, this);
                    var bullets = this.castBullets(this.mcCraft);
                    for (var k = 0; k < bullets.length; k++) {
                        for (var j = 0; j < bullets[k].bulletsPacks.length; j++) {
                            this.game.physics.arcade.overlap(bullets[k].bulletsPacks[j], this.piecesGroups[i], this.shootPiece, null, this);
                        }
                    }
                }
                alive += this.piecesGroups[i].countLiving();
                this.piecesGroups[i].forEach(this.recyclePiece, this);
            }
            if (alive == 0) {
                //no pieces on the scene
                this.triggerEvents(LandPlay.LandEvent.CAUSE_COMPLETE);
            }
            //this.game.world.sort('y', Phaser.Group.SORT_ASCENDING);
        };
        Arcade.prototype.render = function () {
            if (this.mcCraft.debug) {
                this.game.debug.body(this.mcCraft);
                for (var i = 0; i < this.piecesGroups.length; i++) {
                    this.piecesGroups[i].forEachAlive(this.debugPiece, this);
                }
                var bullets = this.castBullets(this.mcCraft);
                for (var i = 0; i < bullets.length; i++) {
                    for (var j = 0; j < bullets[i].bulletsPacks.length; j++) {
                        bullets[i].bulletsPacks[j].forEachAlive(this.debugPiece, this);
                    }
                }
            }
        };
        Arcade.prototype.castBullets = function (piece) {
            var bullets = piece.bullets;
            return bullets;
        };
        Arcade.prototype.debugPiece = function (piece) {
            if (piece.inWorld) {
                this.game.debug.body(piece);
            }
        };
        Arcade.prototype.suitToMaze = function () {
            var rect = this.mcCraft.getBounds();
            var points = new Array();
            if (this.mcCraft.body.velocity.x < 0) {
                //moving left
                points.push(new Phaser.Point(rect.x, rect.y));
                points.push(new Phaser.Point(rect.x, rect.y /* + rect.height*/));
                this.debugBitMap(this.getMovie('maze'));
            }
            if (this.mcCraft.body.velocity.x > 0) {
                //moving right
                points.push(new Phaser.Point(rect.x /* + rect.width*/, rect.y));
                points.push(new Phaser.Point(rect.x /* + rect.width*/, rect.y /* + rect.height*/));
            }
            if (this.mcCraft.body.velocity.y < 0) {
                //moving up
                points.push(new Phaser.Point(rect.x, rect.y));
                points.push(new Phaser.Point(rect.x /* + rect.width*/, rect.y));
            }
            if (this.mcCraft.body.velocity.y > 0) {
                //moving down
                points.push(new Phaser.Point(rect.x, rect.y /* + rect.height*/));
                points.push(new Phaser.Point(rect.x /* + rect.width*/, rect.y /* + rect.height*/));
            }
            var txoca = false;
            for (var i = 0; i < points.length; i++) {
                if (!this.overTrasparentPixel(this.getMovie('maze'), points[i].x, points[i].y)) {
                    this.log('txoca (' + points[i].x + ', ' + points[i].y + ')', 4);
                    this.mcCraft.body.velocity.set(0, 0);
                    txoca = true;
                    break;
                }
            }
            this.sprites['alarm'].visible = txoca;
        };
        /**
         * craft goes to animation, if it exists
         */
        Arcade.prototype.moveCraft = function (frame) {
            this.animate(this.mcCraft, frame);
        };
        /**
         * go to animation, if exits
         */
        Arcade.prototype.animate = function (sprite, frame, loop) {
            if (sprite.animations.getAnimation(frame)) {
                sprite.animations.play(frame, null, loop);
            }
        };
        /**
         * kill the piece once is out the world
         */
        Arcade.prototype.recyclePiece = function (piece) {
            if (piece.inWorld) {
                //moved to onEnterBounds
                //piece.body.velocity.y = piece.moveY * this.speed;
            }
            if (piece.x + (piece.pieceContainer != null ? piece.pieceContainer.x : 0) + piece.width <= 0) {
                //not move to onOutofBounds: they born died
                piece.kill();
            }
        };
        /**
         * Aplicamos prototipos a la MovieClip
         */
        Arcade.prototype.extendMovieClip = function () {
            var classRef = this;
            /**********
            from ActionScript, not used
            ************/
        };
        Arcade.prototype.addRecip = function (name, respostes) {
        };
        /**
         *	more xml items
         *		- craft
         *		- piece
         * 		- edge
         * 		- setup
         */
        Arcade.prototype.registerOption = function (opt) {
            this.log('registerOption: ' + (opt)[0].localName, 4);
            var classRef = this;
            switch ((opt)[0].localName) {
                case "craft":
                    var pick = false;
                    if ($(opt).attr('pickable') == "true") {
                        pick = true;
                    }
                    if ($(opt).attr('move') != undefined) {
                        this.moveFrame = $(opt).attr('move');
                    }
                    if ($(opt).attr('move') != undefined) {
                        this.forwardFrame = $(opt).attr('forward');
                    }
                    if ($(opt).attr('stop') != undefined) {
                        this.stopFrame = $(opt).attr('stop');
                    }
                    if ($(opt).attr('back') != undefined) {
                        this.backFrame = $(opt).attr('back');
                    }
                    if ($(opt).attr('jump') != undefined) {
                        this.jumpFrame = $(opt).attr('jump');
                    }
                    //this.setTool($(opt).attr('sprite'), $(opt).attr('brushpoint'), $(opt).attr('pot'), pick, parseInt($(opt).attr('offsetx')), parseInt($(opt).attr('offsety')));
                    if ($(opt).attr('sprite') != undefined) {
                        this.craft = $(opt).attr('sprite');
                    }
                    else {
                        this.error('you will need a sprite att to your craft');
                    }
                    //TODO this.setShot(opt.childNodes[0]);
                    this.mcCraft = this.getMovie(this.craft);
                    if ($(opt).attr('movex') != undefined) {
                        this.mcCraft.moveX = parseInt($(opt).attr('movex'));
                    }
                    if ($(opt).attr('movey') != undefined) {
                        this.mcCraft.moveY = parseInt($(opt).attr('movey'));
                    }
                    this.log("-------------------------", 3);
                    this.log("Craft: " + this.mcCraft.id + " !!", 3);
                    this.log("-------------------------", 3);
                    //  We need to enable physics on the player
                    this.game.physics.arcade.enable(this.mcCraft);
                    //this.mcCraft.body.gravity.y = 0;
                    this.mcCraft.body.collideWorldBounds = true;
                    if ($(opt).attr('tolerance') != null) {
                        var tolerance = parseInt($(opt).attr('tolerance'));
                        this.mcCraft.body.setSize(this.mcCraft.body.width - tolerance * 2, this.mcCraft.body.height - tolerance * 2, tolerance, tolerance);
                    }
                    this.mcCraft.busy = false;
                    if ($(opt).attr('jump-touch') == 'true') {
                        this.jumpTouch = true;
                    }
                    if ($(opt).attr('y-jump') != null) {
                        this.yJump = parseInt($(opt).attr('y-jump'));
                    }
                    if ($(opt).attr('debug') == 'true') {
                        this.mcCraft.debug = true;
                    }
                    if (this.mcCraft.debug) {
                        /*
                        var grafics: Phaser.Graphics = this.game.add.graphics(0, 0);
                        grafics.beginFill();
                        grafics.lineStyle(4, 0x00FF00);
                        var r: PIXI.Rectangle = this.mcCraft.getBounds();
                        grafics.drawRect(0, 0, r.width, r.height);
                        grafics.endFill();
                        this.mcCraft.addChild(grafics);
                        
                        if (((false)) || this.sprites['maze'].debug) {
                            this.sprites['maze'].enabeInput = true;
                            this.sprites['maze'].events.onInputDown.add(function() {
                                classRef.log('va!', 4);
                                classRef.sprites['alert'].visible = !classRef.overTrasparentPixel(classRef.sprites['maze']);

                            }, this);
                        }
                        */
                    }
                    //move landscape behind the craft
                    if (this.landscapeGroup != null) {
                        this.game.world.swap(this.mcCraft, this.landscapeGroup);
                    }
                    if (!this.floor2d) {
                        //It might be jumps
                        //Player physics properties. Give the little guy a slight bounce.
                        this.mcCraft.body.bounce.y = 0.4;
                        this.mcCraft.body.gravity.y = 300;
                        if ($(opt).attr('ygravity') != null) {
                            this.mcCraft.body.gravity.y = parseInt($(opt).attr('ygravity'));
                        }
                        if ($(opt).attr('ybounce') != null) {
                            this.mcCraft.body.bounce.y = parseFloat($(opt).attr('ybounce'));
                        }
                    }
                    else {
                        if ($(opt).attr('y-scale') != null) {
                            this.yScale = parseFloat($(opt).attr('y-scale'));
                        }
                    }
                    this.mcCraft.checkWorldBounds = true;
                    this.mcCraft.events.onOutOfBounds.add(function () {
                        //ladies & gentleman, craft has left the building
                        //this.mcCraft.game.paused = true;
                        this.landscapeGroup.setAll('body.velocity.x', 0);
                        this.landscapeGroup.setAll('body.velocity.y', 0);
                        this.landscapeForeGroup.setAll('body.velocity.x', 0);
                        this.landscapeForeGroup.setAll('body.velocity.y', 0);
                        this.landscapeForeGroup.setAll('body.velocity.x', 0);
                        this.landscapeForeGroup.setAll('body.velocity.y', 0);
                        for (var i = 0; i < this.piecesGroups.length; i++) {
                            //for recycled throwable pieces
                            this.piecesGroups[i].setAll('body.velocity.x', 0);
                            this.piecesGroups[i].setAll('body.velocity.y', 0);
                        }
                    }, this);
                    this.setBullets(opt, this.mcCraft);
                    break;
                case "edge":
                    if ($(opt).attr('sprite') != undefined) {
                        this.edges = $(opt).attr('sprite');
                        if ($(opt).attr('sprite') != null) {
                            if (this.edgesGroup == null) {
                                this.edgesGroup = this.game.add.group();
                                this.edgesGroup.enableBody = true;
                            }
                            var mc = this.getMovie($(opt).attr('sprite'));
                            this.edgesGroup.add(mc);
                        }
                        this.edgesGroup.setAll("body.immovable", true);
                    }
                    var map;
                    $(opt).children('automap').each(function () {
                        classRef.log('build a map based on: ' + $(this).attr('basedon'), 3);
                        //var tm: LandSprite = new LandSprite(0, 0, $(opt).attr('tilemap'));
                        var tm = classRef.game.make.sprite(0, 0, $(this).attr('spritesheet'));
                        classRef.log('tilemap sprite: ' + tm, 3);
                        if (tm.animations.frameData != null) {
                            {
                                //there is an sprite we want to build a layer based on it
                                classRef.log('tilemap with ' + tm.animations.frameTotal + ' frames', 3);
                                var json = classRef.spriteToTilesetJson(classRef.getMovie($(this).attr('basedon')), tm.animations.frameTotal, $(this));
                                if (json !== null) {
                                    classRef.log('you better copy next json to a map file, and use it as edge>map:', 0);
                                    classRef.log('----------------------------------------', 0);
                                    classRef.log(JSON.stringify(json), 3);
                                    classRef.log('----------------------------------------', 0);
                                    classRef.game.load.tilemap('map', null, json, Phaser.Tilemap.TILED_JSON);
                                    map = classRef.game.add.tilemap('map');
                                }
                            }
                        }
                    });
                    $(opt).children('map').each(function () {
                        map = classRef.game.add.tilemap($(this).attr('key'));
                    });
                    if (map != null) {
                        map.addTilesetImage($(opt).attr('tilemap'));
                        this.edgesLayer = map.createLayer('tileset_from_maze'); // + mc.id);
                        map.setCollisionBetween(1, 12);
                    }
                    if ($(opt).attr('tolerance') != undefined) {
                        this.tolerance = parseInt($(opt).attr('tolerance'));
                    }
                    break;
                case "target":
                    if ($(opt).attr('values') != undefined) {
                        this.targets = $(opt).attr('values').split("|");
                        this.targets = this.rand(this.targets);
                        var mc = classRef.getMovie($(opt).attr('sprite'));
                        if (this.getMovie(mc.id + ".textTag") != null) {
                            this.getMovie(mc.id + ".textTag").text = this.targets[0];
                            this.log('assign tag "' + this.targets[0] + '" to sprite "' + mc.id + '"');
                        }
                    }
                    break;
                case "targets":
                    if ($(opt).attr('movie') != undefined) {
                        this.setTargets(opt.childNodes);
                        this.tagTargetMovie = $(opt).attr('movie');
                        //Cargamos el primer objetivo
                        this.loadTarget(0);
                    }
                    break;
                case 'landscape':
                    this.nestedLaunchPiece(opt);
                    break;
                case 'piece':
                    this.nestedLaunchPiece(opt);
                    break;
                case "setup":
                    if ($(opt).attr('speed') != undefined) {
                        this.speed = parseInt($(opt).attr('speed'));
                    }
                    if ($(opt).attr('craftspeed') != undefined) {
                        this.craftSpeed = parseInt($(opt).attr('craftspeed'));
                    }
                    if ($(opt).attr('runawaySpeed') != undefined) {
                        this.runawaySpeed = parseInt($(opt).attr('runawaySpeed'));
                    }
                    //if (opt.attributes.rand=="true"){
                    this.randFlag = true;
                    //}
                    if ($(opt).attr('time') != undefined) {
                        this.time = parseInt($(opt).attr('time'));
                        var date = new Date();
                        this.second = date.getSeconds();
                    }
                    if ($(opt).attr('floor2d') == "true") {
                        this.floor2d = true;
                    }
                    this.endStage[0] = parseInt(($(opt).attr('endStageX') != undefined) ? $(opt).attr('endStageX') : '0');
                    this.endStage[1] = parseInt(($(opt).attr('endStageY') != undefined) ? $(opt).attr('endStageY') : '0');
                    this.beginStage[0] = parseInt(($(opt).attr('beginStageX') != undefined) ? $(opt).attr('beginStageX') : '0');
                    this.beginStage[1] = parseInt(($(opt).attr('beginStageY') != undefined) ? $(opt).attr('beginStageY') : '0');
                    this.itemEvents = this.setEvents(opt, true);
                    break;
            }
        };
        Arcade.prototype.setBullets = function (opt, sprite) {
            var classRef = this;
            var bullets = [];
            $(opt).children('bullets').each(function () {
                $(this).children('combo').each(function () {
                    var key = Phaser.Keyboard.SPACEBAR;
                    if ($(this).attr('kbkey') != null) {
                        key = Phaser.Keyboard[$(this).attr('kbkey')];
                    }
                    var combo = new LandPlay.BulletCombo(key);
                    var remains = -1; //infinite by default
                    if ($(this).attr('remains') != null) {
                        remains = parseInt($(this).attr('remains'));
                    }
                    combo.text = $(this).attr('counter');
                    $(this).children('config').each(function () {
                        classRef.log('bullet conf.');
                        var key = $(this).attr('key');
                        var config = new LandPlay.BulletConfig(key);
                        config.remains = remains;
                        if (remains >= 0 && combo.text != null) {
                            classRef.texts[combo.text].text = remains;
                        }
                        $(this).children('row').each(function () {
                            var x = 0;
                            var y = 0;
                            var angle = parseInt($(this).attr('angle'));
                            if (isNaN(angle))
                                angle = 0;
                            var speed = parseInt($(this).attr('speed'));
                            if (isNaN(speed))
                                speed = 100;
                            var gx = parseInt($(this).attr('gx'));
                            if (isNaN(gx))
                                gx = 0;
                            var gy = parseInt($(this).attr('gy'));
                            if (isNaN(gy))
                                gy = 0;
                            config.addRow(x, y, angle, speed, gx, gy);
                        });
                        var padding = parseInt($(this).attr('padding'));
                        if (isNaN(padding))
                            padding = null;
                        var fireRate = parseInt($(this).attr('fire-rate'));
                        var pack = new LandPlay.BulletsPack(sprite.game, 'name', config, padding);
                        if (!isNaN(fireRate)) {
                            pack.fireRate = fireRate;
                        }
                        classRef.log('fireRate: ' + pack.fireRate);
                        combo.add(pack);
                    });
                    bullets.push(combo);
                });
            });
            sprite.bullets = bullets;
        };
        Arcade.prototype.spriteToTilesetJson = function (shape, maxFrame, opt) {
            var result = new Object();
            var spritesheet = $(opt).attr('spritesheet');
            var image;
            for (var i = 0; i < this.images.length; i++) {
                if (this.images[i].key == spritesheet) {
                    image = this.images[i];
                }
            }
            if (image == null) {
                this.error('could not find spritesheet: ' + spritesheet);
                return null;
            }
            if ($(opt).attr('tilesetwidth') == null) {
                this.error('tilesetwidth must be set');
                return null;
            }
            if ($(opt).attr('tilesetheight') == null) {
                this.error('tilesetheight must be set');
                return null;
            }
            var grainX = image.frameHeight;
            var grainY = image.frameWidth;
            result['tilesets'] = new Array();
            {
                var tileset = new Object();
                tileset['name'] = spritesheet;
                tileset['image'] = image.url;
                tileset['tileheight'] = image.frameHeight;
                tileset['tilewidth'] = image.frameWidth;
                tileset['imagewidth'] = $(opt).attr('tilesetwidth');
                tileset['imageheight'] = $(opt).attr('tilesetheight');
                result['tilesets'].push(tileset);
            }
            var json = new Object();
            json['x'] = shape.x;
            json['y'] = shape.y;
            json['visible'] = true;
            json['type'] = 'tilelayer';
            json['properties'] = new Object();
            json['properties']['collides'] = true;
            json['opacity'] = 1;
            json['name'] = 'tileset_from_' + shape.id;
            var stepsX = 0;
            var stepsY = 0;
            json['data'] = new Array();
            for (var y = shape.y + (grainY / 2); y < shape.y + shape.height /*- grain*/; y = y + grainY) {
                stepsX = 0;
                for (var x = shape.x + (grainX / 2); x < shape.x + shape.width /*- grain*/; x = x + grainX) {
                    if (this.overTrasparentPixel(shape, x, y)) {
                        json['data'].push(0);
                    }
                    else {
                        json['data'].push(this.randRange(1, maxFrame - 1));
                    }
                    stepsX++;
                }
                stepsY++;
            }
            json['width'] = stepsX;
            json['height'] = stepsY;
            result['layers'] = new Array();
            result['layers'].push(json);
            result['orientation'] = 'orthogonal';
            result['renderorder'] = 'right-down';
            result['version'] = '1';
            result['width'] = stepsX;
            result['height'] = stepsY;
            result['tileheight'] = grainY;
            result['tilewidth'] = grainX;
            return result;
        };
        Arcade.prototype.initClip = function (nom) {
        };
        Arcade.prototype.nestedLaunchPiece = function (opt, parent) {
            var classRef = this;
            switch ((opt)[0].localName) {
                case 'landscape':
                    var mc;
                    mc = this.getMovie($(opt).attr('sprite'));
                    mc.checkWorldBounds = true;
                    var quisoc = mc.id;
                    var onsoc = mc._y + mc.height;
                    var onsocz = mc.z;
                    mc.isLandscape = true;
                    if ($(opt).attr('floored') == "true") {
                        //landscape is flat on the floor (floored2d)
                        mc.floored = true;
                    }
                    //record for landscape, but I think it is no necessary: since it is on a group
                    if (mc.id != "" && mc.id != undefined) {
                        this.backpieces.push(mc.id);
                    }
                    if ($(opt).attr('foreground') == "true") {
                        if (this.landscapeForeGroup == null) {
                            this.landscapeForeGroup = this.game.add.physicsGroup(Phaser.Physics.ARCADE);
                        }
                        this.landscapeForeGroup.add(mc);
                    }
                    else {
                        if (this.landscapeGroup == null) {
                            this.landscapeGroup = this.game.add.physicsGroup(Phaser.Physics.ARCADE);
                        }
                        this.landscapeGroup.add(mc);
                    }
                    mc.events.onOutOfBounds.add(function (piece) {
                        //recycle here
                        classRef.log(mc.id + ' landscape out of  B O U N D S ', 4);
                        piece.x = this.game.world.width;
                        if (!classRef.mcCraft.runningAway) {
                            classRef.buildPiece(mc, opt);
                        }
                    }, this);
                    this.buildPiece(mc, opt);
                    if (parent == null) {
                        this.launchPiece(mc);
                    }
                    break;
                case 'piece':
                    //behind craft
                    if ($(opt).attr('multiplicity') != undefined && ($(opt).attr('attach') != undefined || $(opt).attr('sprite') != undefined)) {
                        // creating cloned pieces
                        if (parent != null) {
                            this.debugVar = parent;
                            this.log('clonating pieces children of ' + parent.id, 4);
                        }
                        this.clonatePieces(opt, parent);
                    }
                    else {
                        var mc;
                        mc = this.getMovie($(opt).attr('sprite'));
                        mc.iNum = 0;
                        if ($(opt).attr('landscape') != "true" && $(opt).attr('tics') == undefined) {
                            var piece = this.getMovie($(opt).attr('sprite'));
                            this.pieces.push(piece);
                        }
                        if ($(opt).attr('attach') != undefined) {
                            this.log("Attach: " + $(opt).attr('attach') + "->" + $(opt).attr('movie') + " (" + $(opt).attr('attach') + "_" + mc.iNum + ")", 3);
                            mc.idName = $(opt).attr('attach');
                            mc.isClonated = true;
                            if ($(opt).attr('tics') != undefined) {
                                mc.tics = parseInt($(opt).attr('tics'));
                            }
                            else {
                            }
                        }
                        this.buildPiece(mc, opt);
                        if (parent == null) {
                            this.launchPiece(mc);
                        }
                    } //End else "not cloned pieces"
                    break;
            }
        };
        Arcade.prototype.buildPiece = function (mc, opt, parent) {
            var classRef = this;
            //save piece events from XML 
            mc.itemEvents = this.setEvents(opt, true, true);
            if (parent != null) {
                //parent.embedChild(mc);
                this.log('padre ' + parent.id + ' es de tipo ' + typeof parent, 4);
                mc.pieceContainer = parent;
                //parent.embedChild(mc);
                //parent.addChild(mc);
                mc.x = this.getNumber($(opt), 'xdif');
                ;
                mc.y = this.getNumber($(opt), 'ydif');
                ;
            }
            //define its moves
            mc.moveX = this.getNumber($(opt), 'moveX');
            if (mc.moveX == null) {
                mc.moveX = 0;
            }
            mc.moveY = this.getNumber($(opt), 'moveY');
            if (mc.moveY == null) {
                mc.moveY = 0;
            }
            if (parent != null) {
                mc.throwMove = new Phaser.Point();
                mc.throwMove.x = mc.moveX;
                if (mc.throwMove.x == 0) {
                    /* comentado, es relativo al padre
                    mc.throwMove.x = parent.moveX;
                    */
                }
                mc.throwMove.y = mc.moveY;
                /* comentado, si es relativo al padre
                mc.moveX = parent.moveX;
                mc.moveY = parent.moveY;
                */
                mc.moveX = 0;
                mc.moveY = 0;
            }
            if ($(opt).attr('ygravity') != undefined) {
                mc.gravity = new Phaser.Point(0, parseInt($(opt).attr('ygravity')));
            }
            //cyclic piece?
            mc.cycle = ($(opt).attr('cycle') == "true");
            if ($(opt).attr('basedepth') != null) {
                mc.basedepth = parseInt($(opt).attr('basedepth'));
            }
            $(opt).children('throwing').each(function () {
                $(this).children('piece').each(function () {
                    classRef.log($(this).attr('attach') + ' piece child of ' + mc.id, 4);
                    classRef.nestedLaunchPiece($(this), mc);
                });
            });
        };
        Arcade.prototype.launchPiece = function (mc, parent) {
            var classRef = this;
            if (mc.gravity != null) {
                mc.body.gravity = mc.gravity;
            }
            //no bounce?
            mc.body.bounce.x = 0;
            mc.body.bounce.y = 0;
            if (mc.basedepth != null) {
                mc.body.setSize(mc.width, mc.basedepth, 0, mc.height - mc.basedepth);
            }
            //relative move of the piece
            mc.body.velocity.set(mc.moveX * this.speed, 0); // y when inWorld mc.moveY * this.speed);
            if (mc.isLandscape) {
                mc.body.immovable = true;
            }
            else {
                //otherwise, no collission while cursors pressed
                mc.body.customSeparateX = true;
                mc.body.customSeparateY = true;
                //as far as suggest its tics
                if (parent == null) {
                    mc.x += -mc.moveX * mc.tics;
                    this.log('space between (' + this.game.time.fpsMax + ' * ' + mc.moveX + ' * ' + mc.tics + '): ' + mc.x, 4);
                }
            }
        };
        /**
         * Record answer, not used
         */
        Arcade.prototype.addAnswer = function (name) {
        };
        /**
         * no effect, by now
         */
        Arcade.prototype.reset = function () {
            for (var i = 0; i < this.responses.length; i++) {
                this.getMovie(this.respostes[i]).kill();
            }
        };
        /**
         * Evident, is not it? But not used
         */
        Arcade.prototype.setShot = function (opt) {
            //see Catch.setShot
        };
        /**
         * Not used
         */
        Arcade.prototype.caught = function () {
            //see Catch.caught
        };
        Arcade.prototype.triggerEvents = function (cause, pieceId) {
            _super.prototype.triggerEvents.call(this, cause, pieceId);
            var piece = null;
            //piece events
            if (pieceId != null) {
                piece = this.getMovie(pieceId);
            }
            if (piece != null && piece.itemEvents != null) {
                for (var i = 0; i < piece.itemEvents.length; i++) {
                    if (piece.itemEvents[i].cause == cause) {
                        this.log('arcade event: ' + piece.itemEvents[i].evtType, 4);
                        if (piece.itemEvents[i].evtType == LandPlay.CatchEvents.TYPE_LOAD) {
                            this.log('event: ' + piece.itemEvents[i].evtType, 4);
                            var key = piece.itemEvents[i].sprite;
                            var value = parseInt(piece.itemEvents[i].value);
                            if (isNaN(value)) {
                                value = 0;
                            }
                            var bullets = this.castBullets(this.mcCraft);
                            for (var j = 0; j < bullets.length; j++) {
                                if (bullets[j].key == Phaser.Keyboard[key]) {
                                    for (var k = 0; k < bullets[j].bulletsPacks.length; k++) {
                                        bullets[j].bulletsPacks[k].load(value);
                                        if (bullets[j].text != null) {
                                            this.texts[bullets[j].text].text = bullets[j].getArmor();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //global events
            for (var i = 0; i < this.itemEvents.length; i++) {
                if (this.itemEvents[i].cause == cause) {
                    this.log('arcade event: ' + this.itemEvents[i].evtType, 4);
                    if (this.itemEvents[i].evtType == LandPlay.CatchEvents.TYPE_RUNAWAY) {
                        var all = (this.itemEvents[i].sprite == LandPlay.CatchEvents.ALL_SPRITES);
                        this.runaway(this.itemEvents[i].sprite, all);
                    }
                }
            }
            this.clearEvents(LandPlay.CatchEvents.TYPE_RUNAWAY);
        };
        Arcade.prototype.runaway = function (sprite, all) {
            if (all) {
                for (var i = 0; i < this.piecesGroups.length; i++) {
                    this.piecesGroups[i].setAll('body.velocity.x', -this.speed * this.runawaySpeed);
                }
                //if not enabled then no collision and no cursor moves
                this.mcCraft.craftEnabled = false;
                this.mcCraft.body.velocity.x = this.speed * this.runawaySpeed;
                //let craft go out world
                this.mcCraft.body.collideWorldBounds = false;
                this.moveCraft(this.forwardFrame);
            }
        };
        /**
         * not used
         */
        Arcade.prototype.evaluate = function (pieceName, cleared) {
            this.log("Piece hunted: " + pieceName, 3);
            var mcpiece = this.getMovie(pieceName);
            //@TODO: see what happens when there are more targets in the stage
            if (this.assignacions[0] != undefined) {
                var pieceNameWC; //wildcards
                var path = pieceName.split("_");
                pieceNameWC = path[0];
                if (path.length > 1) {
                    pieceNameWC = pieceNameWC + "_*";
                }
                if (this.assignacions[0].indexOf(pieceName) != -1 || this.assignacions[0].indexOf(pieceNameWC) != -1) {
                    if (!mcpiece.gotten || mcpiece.gotten == undefined) {
                        this.log("CORRECT piece: " + pieceName, 3);
                        this.triggerEvents(LandPlay.LandEvent.CAUSE_RIGHT);
                        this.triggerEvents(LandPlay.LandEvent.CAUSE_RIGHT, mcpiece.id);
                        mcpiece.gotten = true;
                        this.caughtPieces.push(pieceName);
                        /* setup an event instead
                        this.addPoints(this.countPoints());
                        */
                    }
                }
                else {
                    this.log("IN-CORRECT piece: " + pieceName, 3);
                    var mc = this.getMovie(pieceName);
                    if (!cleared) {
                        this.triggerEvents(LandPlay.LandEvent.CAUSE_WRONG);
                        //inmunity a few secs
                        this.getMovie(this.craft).immunity = true;
                        if (this.retry() == 0) {
                            this.triggerEvents(LandPlay.LandEvent.CAUSE_TRYOUT);
                        }
                    }
                    else {
                        //game is completed, do not take wrong into account
                        //junp if there is such animation
                        this.moveCraft(this.jumpFrame);
                    }
                }
            }
        };
        /**
         * not used
         */
        Arcade.prototype.setTargets = function (nodes) {
            var node;
            for (var i = 0; i < nodes.length; i++) {
                node = nodes[i];
                this.targets.push($(node).attr('textTag'));
                this.targetSounds.push($(node).attr('sound'));
                this.targetValues.push($(node).attr('value').split('|'));
            }
        };
        /**
         * not used
         */
        Arcade.prototype.loadTarget = function (t) {
            var mc = this.getMovie(this.tagTargetMovie);
            if (this.getMovie(mc.id + ".textTag") != null) {
                this.getMovie(mc.id + ".textTag").text = this.targets[t];
                this.log('assign tag "' + this.targets[t] + '" to sprite "' + mc.id + '"');
            }
            this.game.sound.play(this.targetSounds[t]);
            if (this.assignacions == undefined)
                this.assignacions = [];
            this.assignacions[t] = this.targetValues[t];
        };
        /**
         * not used
         */
        Arcade.prototype.doNextTarget = function () {
            this.loadTarget(1);
        };
        /**
         * not used see runaway
         */
        Arcade.prototype.doRunaway = function (eve) {
            //Habilitado para la piezas de caza: huyen cuando falla
            //		if (eve.attributes.movie=="all"){
            //acceleramos su movimeinto
            var mc;
            for (var i = 0; i < this.pieces.length; i++) {
                this.log("Runaway " + this.pieces[i] + "!!", 4);
                mc = (this.pieces[i]);
                mc.moveX *= this.runawaySpeed;
                mc.moveY *= this.runawaySpeed;
                mc.runningAway = true;
                mc.cycle = false;
            }
        };
        Arcade.prototype.getCaughtPieces = function () {
            return this.caughtPieces;
        };
        Arcade.prototype.hashPieces = function () {
            if (this.randFlag) {
                this.log("Hash Flag enabled", 4);
                var before = new Array();
                for (var i = 0; i < this.pieces.length; i++) {
                    before.push(this.pieces[i].id);
                }
                var after = this.rand(before);
                for (var i = 0; i < before.length; i++) {
                    this.log(" " + before[i] + " exchanged by " + after[i] + "", 4);
                    this.sprites[after[i]]._x = this.sprites[before[i]]._x;
                    this.sprites[after[i]]._y = this.sprites[before[i]]._y;
                    this.sprites[after[i]].x = this.sprites[before[i]].x;
                    this.sprites[after[i]].y = this.sprites[before[i]].y;
                }
            }
        };
        Arcade.prototype.clonatePieces = function (opt, parent) {
            var ok = false;
            var classRef = this;
            /*var crf = this.getMovie(this.craft);*/
            if ($(opt).attr('multiplicity') != undefined &&
                ($(opt).attr('attach') != undefined || $(opt).attr('sprite') != undefined) &&
                $(opt).attr('ticsmin') != undefined &&
                $(opt).attr('ticsmax') != undefined &&
                $(opt).attr('mcref') != undefined) {
                ok = true;
            }
            //var grp: Phaser.Group = this.game.add.physicsGroup(Phaser.Physics.ARCADE);
            var grp = this.game.add.group();
            grp.enableBody = true;
            for (var i = 0; i < parseInt($(opt).attr('multiplicity')); i++) {
                var piece;
                this.log("Creating " + $(opt).attr('attach') + " instance", 4);
                var tics = this.getNumber($(opt), 'tics');
                var iMcName = $(opt).attr('attach') + "_" + i + (parent != null ? '_' + parent.id : '');
                var yref = this.getNumber($(opt), 'ydif') + (parent != null ? parent.y : 0);
                //var xref = this.randRange(parseInt($(opt).attr('xdifmin')), parseInt($(opt).attr('xdifmax')));
                var xref = this.game.world.width;
                xref = this.getNumber($(opt), 'xdif');
                if (xref == null) {
                    xref = this.game.world.width;
                }
                else {
                    if (parent != null) {
                        xref += parent.x;
                    }
                }
                this.log("tics " + tics + " / pos(" + xref + "," + yref + ")", 4);
                if ($(opt).attr('attach') != null) {
                    this.log('clon from image "' + $(opt).attr('attach') + '"', 4);
                    piece = grp.create(xref, yref, $(opt).attr('attach'));
                    if (parent != null) {
                        this.log('parent: ' + parent.id, 4);
                        //piece = parent.game.add.sprite(0, 0, $(opt).attr('attach'));
                        //parent.addChild(piece);
                        //this.game.physics.enable(piece);
                    }
                    else {
                    }
                }
                else if ($(opt).attr('sprite') != null) {
                    this.log('clon from sprite "' + $(opt).attr('sprite') + '"', 4);
                    piece = this.getMovie($(opt).attr('sprite'));
                    piece = grp.add(piece);
                    this.game.physics.enable(piece);
                    this.animate(piece, 'move', true);
                    piece.x = xref;
                    piece.y = yref;
                }
                this.setAnimations(opt, piece);
                this.animate(piece, 'move', true);
                piece.clontics = tics * (i + 1);
                piece.clonticscopy = piece.clontics;
                piece.tics = piece.clontics;
                piece.visible = true;
                piece.id = iMcName;
                piece.ydifmin = parseInt($(opt).attr('ydifmin'));
                piece.ydifmax = parseInt($(opt).attr('ydifmax'));
                //this.mcCraft.swapDepths( -(pieces.length)*10 - 1)
                //trace(crf.getDepth())
                //crf.swapDepths((-1)*piece.getDepth());
                //trace(crf.getDepth())
                this.pieces.push(piece);
                $(opt).attr('sprite', iMcName);
                if ($(opt).attr('basedepth') != undefined) {
                    piece.basedepth = parseInt($(opt).attr('basedepth'));
                }
                if ($(opt).attr('tolerance') != undefined) {
                    piece.tolerance = parseInt($(opt).attr('tolerance'));
                }
                if ($(opt).attr('floored') == "true") {
                    //La pieza esta plana sobre el suelo
                    piece.floored = true;
                }
                /*
                for (var j = 0; j < opt.childNodes.length; j++) {
                    if (opt.childNodes[j].nodeName == "event") {
                        opt.childNodes[j].attributes.movie = iMcName;
                    }
                }
                */
                piece.itemEvents = this.setEvents($(opt), true, true);
                this.sprites[piece.id] = piece;
                piece.checkWorldBounds = true;
                piece.events.onEnterBounds.add(function () {
                    //classRef.log(this.id + ' enters B O U N D S ', 4);
                    if (this.pieceContainer == null) {
                        // it is a free piece, let us give it a y move
                        this.body.velocity.y = this.moveY * classRef.speed;
                    }
                    else {
                        //piece to throw
                        if (this.tics != null) {
                            //TODO apply y once tics pass
                            classRef.log('' + piece.id + ' will be thrown once ' + this.tics + ' secs pass by ', 4);
                            var thisPiece = this;
                            var launchItv = setInterval(function () {
                                thisPiece.body.velocity.y = thisPiece.throwMove.y * classRef.speed;
                                thisPiece.body.velocity.x = thisPiece.throwMove.x * classRef.speed;
                                classRef.log('throwing ' + thisPiece.id + ' (' + thisPiece.throwMove.x + ', ' + thisPiece.throwMove.y + ') ' +
                                    ' pos (' + thisPiece.x + ',' + thisPiece.y + ')', 4);
                                clearInterval(launchItv);
                            }, this.tics * 1000);
                        }
                    }
                }, piece);
                piece.checkWorldBounds = true;
                piece.events.onOutOfBounds.add(function () {
                    if (this.pieceContainer != null) {
                        if (!classRef.mcCraft.inWorld) {
                            this.kill();
                        }
                    }
                    else {
                        //NOPE, they born died
                        //this.kill();
                    }
                }, piece);
                if (parent != null) {
                    this.debugVar = piece;
                }
                this.buildPiece(piece, opt, parent);
                this.launchPiece(piece, parent);
            } //end of multiplicity
            if (parent != null) {
                parent.addChild(grp);
            }
            this.piecesGroups.push(grp);
            return ok;
        };
        /**
         * not used, better use this.game.paused=true
         */
        Arcade.prototype.pauseAll = function () {
            //paramos las piezas
            for (var i = 0; i < this.piecesGroups.length; i++) {
                this.piecesGroups[i].setAll("body.velocity.x", 0);
                this.piecesGroups[i].setAll("body.velocity.y", 0);
            }
            this.mcCraft.paused = true;
            for (var i = 0; i < this.backpieces.length; i++) {
                this.log("Unloading background " + this.backpieces[i], 4);
            }
        };
        Arcade.prototype.resumeAll = function () {
            this.mcCraft.paused = false;
        };
        Arcade.prototype.disableKeyboard = function () {
            this.craftSpeed = 0;
            this.mcCraft.play(this.moveFrame);
            this.mcCraft.destroy();
        };
        Arcade.prototype.disableXMove = function () {
            this.craftSpeed = 0;
        };
        Arcade.prototype.clearAll = function (onStage) {
            for (var i = 0; i < this.pieces.length; i++) {
                //avoid recycle pieces that get to the end
                (this.pieces[i]).cycle = false;
                if ((this.pieces[i])._x > this.game.width || onStage) {
                    //kill the pieces out of the world
                    (this.pieces[i]).kill();
                }
            }
        };
        /**
         * not used
         */
        Arcade.prototype.movePiece = function (piece) {
            var classRef = this;
            //@TODO: see what happens when they move along y-axis
            var gap = 100;
            if (classRef.floor2d) {
                if (!piece.isLandscape) {
                    //if (true){
                    if (piece._x < this.game.width) {
                        //piece is in the world
                        if (!piece.gapchecked) {
                            for (var i = 0; i < classRef.pieces.length; i++) {
                                var pieza = classRef.pieces[i];
                                var mcPieza = (pieza);
                                if (classRef.checkOverlap(piece, mcPieza)) {
                                    //or piece._x+piece._width+(gap/2)>mcPieza._x VOLIEM AFEGIR DISTANCIA DE SEGURETAT (pero entravem en un "el uno por el otro...") pieza.substr(0, 7)=="mcTronc" and
                                    //if another piece touches this one add distance
                                    if (pieza.id != piece.id) {
                                        //never move that one before it
                                        //otherwise it jumps a lot if it is on scene
                                        if (mcPieza._x >= piece._x) {
                                            classRef.log("separate overlap piece " + mcPieza.id + " from " + piece.id + "", 1);
                                            mcPieza._x = mcPieza._x + gap + piece.width;
                                            piece.frame = 0;
                                        }
                                        else {
                                            classRef.log("separate overlap piece " + piece.id + " from " + mcPieza.id + "", 1);
                                            piece._x = piece._x + gap + mcPieza.width;
                                            mcPieza.frame = 0;
                                        }
                                    }
                                }
                            }
                            piece.gapchecked = true;
                        }
                        piece.visible = true;
                        if (piece.floored) {
                            /* better done at sort in update
                            piece.swapDepths(piece._y);
                            */
                        }
                        else {
                            if (piece.id.substr(0, 7) == "mcTronc") {
                                var n = piece.id;
                            }
                            if (!piece.depthIsSet) {
                                //TODO: what if y changes
                                /* better done at sort in update
                                piece.swapDepths(piece._y + piece.height);
                                */
                                piece.depthIsSet = true;
                            }
                        }
                    }
                }
                else {
                    // is landscape
                    //clue from David @ LaFactoria (best practise, does not consume resources)
                    if (piece._x > -piece.width) {
                        //piece._x -= _root.VelositatAbres;
                    }
                    else {
                        piece._x = this.game.width;
                    }
                    if (piece.landscapeMounted == undefined) {
                        var quisoc = piece.id;
                        var onsoc = piece._y + piece.height;
                        var onsocz = piece.z;
                        //trace(onsocz);
                        if (piece.floored) {
                            /* sort in update
                            piece.swapDepths(piece._y);
                            */
                        }
                        else {
                            /* sort in update
                            piece.swapDepths(piece._y + piece.height);
                            */
                        }
                        //piece.landscapeMounted=true;
                    }
                }
            }
            if (piece.clontics != undefined) {
                if (piece.clontics <= 0) {
                    piece._x = piece._x + (piece.moveX);
                }
                else {
                    piece.clontics--;
                }
                //see recycleLandscape
                if (piece._x + piece.width < 0 && piece.cycle) {
                    piece._x = 800;
                    piece._y = classRef.randRange((piece.ydifmin), (piece.ydifmax));
                    piece.clontics = piece.clonticscopy;
                    piece.gapchecked = false;
                    piece.depthIsSet = false;
                    //right, recycled pieces that could be gotten again
                    piece.gotten = false;
                    piece.frame = 0;
                }
                //Deleted if they go out of world
            }
            else {
                piece._x = piece._x + (piece.moveX);
            }
            if (piece.isClonated) {
                var lastDup, prevDup;
                lastDup = this[piece.idName + "_" + piece.iNum];
                if (piece.iNum > 0) {
                    prevDup = this[piece.idName + "_" + (piece.iNum - 1)];
                    //classRef.log("anterior: " add prevDup._name ,4);
                }
                if (piece.tics == undefined) {
                    //previous version
                }
                else {
                    //a piece that clones every X tics
                    if (piece.ticsTmp == undefined) {
                        piece.ticsTmp = piece.tics;
                    }
                    if (piece.ticsTmp == 0) {
                        piece.ticsTmp = piece.tics;
                        piece.iNum++;
                        /* TODO
                        piece.attachMovie(
                            piece.idName,
                            piece.idName + "_" + piece.iNum,
                            -10 * piece.iNum, { _x: (piece._parent._width) });
                        //trace(classRef.getMovieName(this[piece.idName add "_" add piece.iNum]));
                        classRef.pieces.push(classRef.getMovieName(this[piece.idName + "_" + piece.iNum]));
                        */
                        classRef.log("Clonning piece (" + this[piece.idName + "_" + piece.iNum]._name + ")", 4);
                    }
                    else {
                        piece.ticsTmp--;
                    }
                }
            }
            if (piece._x > (classRef.endStage[0])) {
                if (piece.cycle) {
                    classRef.log("At starting point", 4);
                    piece._x = (classRef.beginStage[0]);
                }
                else {
                    //@nocycle
                    classRef.log("Back to my first position&&hide", 4);
                    piece._x = piece.x;
                    piece._y = piece.y;
                    piece.visible = false;
                }
            }
            //piece._y = piece._y + int(piece.moveY);
        };
        Arcade.prototype.clearBackgrounds = function () {
            for (var i = 0; i < this.backpieces.length; i++) {
                this.log("Unloading background " + this.backpieces[i], 4);
                this.getMovie(this.backpieces[i]).destroy();
            }
        };
        return Arcade;
    }(LandPlay.LandGame));
    LandPlay.Arcade = Arcade;
})(LandPlay || (LandPlay = {}));
/// <reference path='../phaser.d.ts' />
var LandPlay;
(function (LandPlay) {
    var LandField = (function () {
        function LandField() {
        }
        return LandField;
    }());
    LandPlay.LandField = LandField;
    var TextField = (function (_super) {
        __extends(TextField, _super);
        function TextField(sprite, value, offsetx, offsety, hide) {
            var _this = _super.call(this) || this;
            _this.maxlength = 10;
            _this.size = 10;
            _this.txtcolor = 0x000000;
            _this.cursor = TextField.CURSOR_VERTICAL;
            _this._focus = false;
            _this._cursorOn = true;
            _this._lock = false;
            _this._sprite = sprite;
            if (value == null) {
                value = '';
            }
            if (offsetx == null) {
                offsetx = 0;
            }
            if (offsety == null) {
                offsety = 0;
            }
            var style = { fontSize: '10px', fill: '#FFF' };
            _this.text = _this._sprite.game.add.text(_this._sprite.x + offsetx, _this._sprite.y + offsety, value, style);
            _this.text.name = 'textField';
            _this._value = _this.text.text;
            if (hide) {
                _this.text.text = '';
            }
            return _this;
        }
        TextField.prototype.validate = function () {
            return this.realText().toUpperCase() == this._value.toUpperCase();
        };
        TextField.prototype.render = function () {
        };
        TextField.prototype.focus = function () {
            this._focus = true;
            this._itv = setInterval(this.toggleCursor, 500, this);
        };
        TextField.prototype.endFocus = function () {
            clearInterval(this._itv);
            this.text.text = this.realText();
        };
        TextField.prototype.realText = function () {
            var res = this.text.text;
            if (this.hasCursor()) {
                res = this.text.text.substring(0, this.text.text.length - 1);
            }
            return res.trim();
        };
        TextField.prototype.getSize = function () {
            this.clear();
            return this.realText().length;
        };
        TextField.prototype.isFull = function () {
            return this.getSize() == this.maxlength;
        };
        TextField.prototype.isEmpty = function () {
            return this.realText() == '';
        };
        TextField.prototype.empty = function () {
            this.text.text = '';
        };
        TextField.prototype.getSprite = function () {
            return this._sprite;
        };
        TextField.prototype.addChar = function (char) {
            this._lock = true;
            if (this.hasCursor()) {
                this.text.text = this.text.text.substring(0, this.text.text.length - 1);
            }
            if (!this.isFull()) {
                this.text.text += char;
            }
            if (this._cursorOn) {
                //this.text.text += this.cursor;
            }
            this._lock = false;
            this.clear();
        };
        TextField.prototype.solve = function () {
            this.text.text = this._value;
        };
        TextField.prototype.deleteChar = function () {
            var limit = 1;
            if (this.hasCursor() && this.text.text.length >= 2) {
                limit = 2;
            }
            this.text.text = this.text.text.substring(0, this.text.text.length - limit);
            /*
            if (this.hasCursor()) {
                this.text.text += this.cursor;
            }
            */
            this.clear();
        };
        TextField.prototype.clear = function () {
            if (this.realText() == ' ') {
                this.text.text = '';
            }
            /* I do not know where it comes from, but there is always a space at the beginning*/
            if (this.text.text.substring(0, 1) == ' ') {
                this.text.text = this.text.text.substring(1);
            }
        };
        TextField.prototype.hasCursor = function () {
            return this.text.text.substring(this.text.text.length - 1) == this.cursor;
        };
        TextField.prototype.toggleCursor = function (tObj) {
            if (!tObj._lock) {
                var text = tObj.text;
                if (tObj._cursorOn) {
                    tObj.text.text += tObj.cursor;
                }
                else {
                    if (tObj.hasCursor()) {
                        tObj.text.text = tObj.text.text.substring(0, tObj.text.text.length - 1);
                    }
                }
                tObj._cursorOn = !tObj._cursorOn;
            }
        };
        TextField.CURSOR_VERTICAL = '|';
        TextField.CURSOR_HORIZONTAL = '_';
        return TextField;
    }(LandField));
    LandPlay.TextField = TextField;
})(LandPlay || (LandPlay = {}));
/// <reference path="../LandGame.ts" />
/// <reference path="../fields/LandField.ts" />
this;
var LandPlay;
(function (LandPlay) {
    var Type = (function (_super) {
        __extends(Type, _super);
        function Type(xmlFile, params) {
            var _this = _super.call(this) || this;
            _this.textFields = new Array();
            _this.tabIndex = 0;
            _this.className = LandPlay.Mode.Type;
            if (xmlFile != undefined) {
                _this.log("Open the file (" + xmlFile + ")", 3);
                _this.reset();
                _this.init(xmlFile, params);
            }
            else {
                _this.log("xml not given o subclass instantiated", 3);
            }
            return _this;
        }
        Type.prototype.onStageLoad = function () {
            var classRef = this;
            this.game.input.enabled = true;
            this.game.input.keyboard.enabled = true;
            $(this.xml).find('setup').each(function () {
                classRef.registerOption($(this));
            });
            $(this.xml).find('word').each(function () {
                if ($(this).attr('text') == null) {
                    classRef.error('no text attr. for word');
                }
                else if ($(this).attr('x') == null || $(this).attr('y') == null) {
                    classRef.error('initial pos (x, y) of "word" must be set');
                }
                else if ($(this).attr('key') == null && $(this).attr('sprite') == null) {
                    classRef.error('an image "key" must be set in "word"');
                }
                else {
                    classRef.registerOption($(this));
                }
            });
            this.game.input.keyboard.onUpCallback = function (event) {
                classRef.log('keyCode: ' + event.keyCode, 4);
                if (event.keyCode == 8) {
                    //back key
                    if (classRef.focused().isEmpty()) {
                        classRef.tabulate(-1);
                    }
                    else {
                        classRef.focused().deleteChar();
                    }
                }
                else if (event.keyCode == 9) {
                    //tab key
                    classRef.game.canvas.focus();
                    classRef.tabulate();
                }
                else {
                    if (event.key.length == 1) {
                        classRef.focused().addChar(event.key);
                        classRef.log('char add: "' + event.key + '"', 4);
                        if (classRef.focused().isFull()) {
                            classRef.tabulate();
                        }
                    }
                }
                classRef.log('size: ' + classRef.focused().getSize() + ', maxlen: ' + classRef.focused().maxlength, 4);
                classRef.log('text: "' + classRef.focused().text.text + '"');
            };
            _super.prototype.onStageLoad.call(this);
        };
        Type.prototype.addTextField = function (text, key, refx, refy, offsetx, offsety, style, sprite) {
            var classRef = this;
            var chrsp;
            if (key != null) {
                chrsp = this.game.add.sprite(refx, refy, key);
            }
            else if (sprite != null) {
                chrsp = this.sprites[sprite];
            }
            var x = 0;
            var y = 0;
            if (offsetx != null) {
                x = offsetx;
            }
            if (offsety != null) {
                y = offsety;
            }
            var t = new LandPlay.TextField(chrsp, text, x, y, true);
            t.maxlength = text.length;
            if (style != null) {
                t.text.setStyle(style);
            }
            this.textFields.push(t);
            chrsp.inputEnabled = true;
            chrsp.events.onInputDown.add(function (txtspr) {
                for (var j = 0; j < classRef.textFields.length; j++) {
                    classRef.textFields[j].endFocus();
                    if (txtspr == classRef.textFields[j].getSprite()) {
                        classRef.tabIndex = j;
                        classRef.textFields[j].focus();
                    }
                }
            }, this);
            chrsp.name = key;
            return t;
        };
        Type.prototype.registerOption = function (opt) {
            var classRef = this;
            switch ((opt)[0].localName) {
                case 'word':
                    var refx = parseInt($(opt).attr('x'));
                    var refy = parseInt($(opt).attr('y'));
                    var style = {};
                    if ($(opt).attr('style') != null) {
                        style = JSON.parse($(opt).attr('style'));
                    }
                    if ($(opt).children('split').length == 0) {
                        var tf = classRef.addTextField($(opt).attr('text'), $(opt).attr('key'), refx, refy, parseInt($(opt).attr('offsetx')), parseInt($(opt).attr('offsety')), style, $(opt).attr('sprite'));
                        classRef.addSprite(tf.getSprite().name, tf.getSprite());
                    }
                    else {
                        $(opt).children('split').each(function () {
                            var chars = $(opt).attr('text').split('');
                            classRef.log('chars split: ' + chars);
                            var offsetx = parseInt($(this).attr('offsetx'));
                            var offsety = parseInt($(this).attr('offsety'));
                            for (var i = 0; i < chars.length; i++) {
                                var style = null;
                                if ($(opt).attr('style') != null) {
                                    style = JSON.parse($(opt).attr('style'));
                                }
                                var tf = classRef.addTextField(chars[i], $(opt).attr('key'), refx + (i * offsetx), refy + (i * offsety), parseInt($(opt).attr('offsetx')), parseInt($(opt).attr('offsety')), style, null);
                                var key = tf.getSprite().key;
                                if ($(opt).attr('id') != null) {
                                    key = $(opt).attr('id');
                                }
                                tf.getSprite().name = key + '_' + i;
                                if (i == 0) {
                                    tf.focus();
                                }
                                classRef.addSprite(tf.getSprite().name, tf.getSprite());
                            }
                        });
                    }
                    break;
            }
        };
        Type.prototype.tabulate = function (sense) {
            if (!sense) {
                sense = 1;
            }
            var next = this.tabIndex + sense;
            if (next < 0) {
                next = this.textFields.length + next;
            }
            else {
                next = ((this.tabIndex + sense) % this.textFields.length);
            }
            this.focused().endFocus();
            this.tabIndex = next;
            this.log('next tab: ' + this.tabIndex);
            this.focused().focus();
        };
        Type.prototype.focused = function () {
            return this.textFields[this.tabIndex];
        };
        Type.prototype.checkValidate = function () {
            _super.prototype.checkValidate.call(this);
            var ok = true;
            for (var i = 0; i < this.textFields.length; i++) {
                if (this.textFields[i].validate()) {
                }
                else {
                    this.textFields[i].empty();
                    ok = false;
                }
            }
            if (!ok) {
                this.triggerEvents(LandPlay.LandEvent.CAUSE_WRONG);
                if (this.retry() == 0) {
                    this.triggerEvents(LandPlay.LandEvent.CAUSE_TRYOUT);
                }
            }
            else {
                this.triggerEvents(LandPlay.LandEvent.CAUSE_COMPLETE);
            }
        };
        Type.prototype.hint = function () {
            this.log('entering hint...', 4);
            var all = true;
            for (var i = 0; i < this.textFields.length; i++) {
                if (!this.textFields[i].validate()) {
                    all = false;
                    break;
                }
            }
            if (!all) {
                var r = this.random(this.textFields.length);
                while (this.textFields[r].validate()) {
                    r = this.random(this.textFields.length);
                }
                this.textFields[r].solve();
                this.triggerEvents(LandPlay.LandEvent.CAUSE_HINT);
                if (this.retry() == 0) {
                    this.triggerEvents(LandPlay.LandEvent.CAUSE_TRYOUT);
                }
            }
            else {
                this.log('No left chars to give a hint', 1);
            }
            this.log('exiting hint...', 4);
        };
        Type.prototype.solve = function () {
            for (var i = 0; i < this.textFields.length; i++) {
                this.textFields[i].solve();
            }
        };
        Type.prototype.collectValidable = function () {
            var result = [];
            //the right texts
            result[0] = [];
            //the wrong ones
            result[1] = [];
            var cright = 0;
            var cwrong = 0;
            for (var i = 0; i < this.textFields.length; i++) {
                if (this.textFields[i].validate()) {
                    result[0][cright] = this.textFields[i].getSprite().name;
                    cright++;
                }
                else {
                    result[1][cwrong] = this.textFields[i].getSprite().name;
                    cwrong++;
                }
            }
            return result;
        };
        Type.prototype.update = function () {
        };
        Type.prototype.render = function () {
        };
        return Type;
    }(LandPlay.LandGame));
    LandPlay.Type = Type;
})(LandPlay || (LandPlay = {}));
/// <reference path='mode/Catch.ts' />
/// <reference path='mode/Paint.ts' />
/// <reference path='mode/DragDrop.ts' />
/// <reference path='mode/Puzzle.ts' />
/// <reference path='mode/Simon.ts' />
/// <reference path='mode/Arcade.ts' />
/// <reference path='mode/Type.ts' />
var dummy = 'dummy';
/*just to ensure saving this file will gather all the files in compilation*/
/// <reference path="../LandGame.ts" />
/// <reference path="../LandGame.ts" />
/// <reference path="../fields/LandField.ts" />
this;
var LandPlay;
(function (LandPlay) {
    /**
     * Thanks: joshmorony for https://www.joshmorony.com/part-1-building-a-word-search-game-in-html5-with-phaser/
     */
    var WordSearch = (function (_super) {
        __extends(WordSearch, _super);
        function WordSearch(xmlFile, params) {
            var _this = _super.call(this) || this;
            /**
             * Declare assets that will be used as tiles
             * */
            _this.tileLetters = [
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
                'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
                'W', 'X', 'Y', 'Z'
            ];
            _this.case = 'uppercase';
            /**
             * What colours will be used for our tiles?
             */
            _this.tileColors = [
                '#ffffff'
            ];
            _this.currentColor = '#FF0000';
            _this.rightColors = ['#878e26', '#268e89', '#8e266e', '#e67e13'];
            /**
             * Set the width and height for the tiles
             */
            _this.tileWidth = WordSearch.DEFAULT_WIDTH;
            _this.tileHeight = WordSearch.DEFAULT_HEIGHT;
            _this.gridCols = WordSearch.DEFAULT_COLS;
            _this.gridRows = WordSearch.DEFAULT_ROWS;
            _this.guessing = false;
            _this.currentWord = [];
            _this.currentWordEdges = null;
            _this.correctWords = [];
            /**
             * words to spread in the grid
             */
            _this.words = [];
            /**
             * 2d letter offset for each word with values -1, 0 or 1
             * forbidden combination {x: 0, y: 0}
             */
            _this.wordOrientations = [];
            _this.wordPositions = [];
            _this.rightLetters = new Array();
            _this.rightWords = new Array();
            _this.wordSprites = [];
            _this.className = LandPlay.Mode.WordSearch;
            if (xmlFile != undefined) {
                _this.log("Open the file (" + xmlFile + ")", 3);
                _this.reset();
                _this.init(xmlFile, params);
            }
            else {
                _this.log("xml not given o subclass instantiated", 3);
            }
            return _this;
        }
        WordSearch.prototype.onStageLoad = function () {
            var classRef = this;
            this.game.input.enabled = true;
            $(this.xml).find('setup').each(function () {
                classRef.registerOption($(this));
            });
            $(this.xml).find('words').each(function () {
                classRef.words = [];
                //classRef.wordOrientations = [];
                //classRef.wordPositions = [];
                $(this).find('word').each(function () {
                    var x = parseInt($(this).attr('x'));
                    var y = parseInt($(this).attr('y'));
                    var xoffset = parseInt($(this).attr('xoffset'));
                    var yoffset = parseInt($(this).attr('yoffset'));
                    var text = $(this).attr('text');
                    if (text == null) {
                        classRef.error('no text attr. for word');
                    }
                    else if (isNaN(x) || x == null || isNaN(y) || y == null) {
                        classRef.error('initial pos (x, y) of "word" must be set');
                    }
                    else if (isNaN(xoffset) || xoffset == null || isNaN(yoffset) || yoffset == null) {
                        classRef.error('orientation (xoffset, yoffset) of "word" must be set');
                    }
                    else if (xoffset == 0 && yoffset == 0) {
                        classRef.error('orientation (0, 0) of "word" is not allowed');
                    }
                    else {
                        if ($(this).attr('sprite') != null) {
                            var oText = classRef.getMovie($(this).attr('sprite') + ".textTag");
                            if (oText != null) {
                                oText.text = text;
                            }
                            classRef.wordSprites.push($(this).attr('sprite'));
                        }
                        classRef.wordPositions.push({ x: x, y: y });
                        classRef.wordOrientations.push({ x: xoffset, y: yoffset });
                        classRef.words.push(text);
                    }
                });
            });
            this.tiles = this.game.add.group();
            this.addaptGrid();
            this.tileGrid = [];
            for (var i = 0; i < this.gridCols; i++) {
                this.tileGrid[i] = [];
                this.rightLetters[i] = new Array();
                for (var j = 0; j < this.gridRows; j++) {
                    this.tileGrid[i][j] = null;
                    this.rightLetters[i][j] = null;
                }
            }
            /**
             * Keep a reference to the total grid width and height
             */
            this.boardWidth = this.gridCols * this.tileWidth;
            this.boardHeight = this.gridRows * this.tileHeight;
            /**
            * We want to keep a buffer on the left and top so that the grid can be centered
            */
            this.leftBuffer = (this.game.width - this.boardWidth) / 2;
            this.topBuffer = (this.game.height - this.boardHeight) / 2;
            this.disposeWords();
            this.initTiles();
            this.game.input.onDown.add(function () { classRef.guessing = true; }, classRef);
            this.game.input.onUp.add(function () { classRef.guessing = false; }, classRef);
            /**
             *   A buffer for how much of the tile activates a select
             */
            this.selectBuffer = classRef.tileWidth / 8;
            _super.prototype.onStageLoad.call(this);
        };
        WordSearch.prototype.registerOption = function (opt) {
            var classRef = this;
            $(this.xml).find('tiles').each(function () {
                if ($(this).attr('colors') != null) {
                    classRef.tileColors = $(this).attr('colors').split(',');
                }
                if ($(this).attr('case') != null) {
                    classRef.case = $(this).attr('case');
                }
                var width = parseInt($(this).attr('width'));
                var height = parseInt($(this).attr('height'));
                if (isNaN(width) || isNaN(height)) {
                    if (!isNaN(width)) {
                        height = width;
                    }
                    else if (!isNaN(height)) {
                        width = height;
                    }
                }
                if (!isNaN(width))
                    classRef.tileWidth = width;
                if (!isNaN(height))
                    classRef.tileHeight = height;
                var cols = parseInt($(this).attr('cols'));
                var rows = parseInt($(this).attr('rows'));
                if (isNaN(cols) || isNaN(rows)) {
                    if (!isNaN(cols)) {
                        rows = cols;
                    }
                    else if (!isNaN(rows)) {
                        cols = rows;
                    }
                }
                if (!isNaN(cols))
                    classRef.gridCols = cols;
                if (!isNaN(rows))
                    classRef.gridRows = rows;
                $(this).find('colors').each(function () {
                    if ($(this).attr('current') != null) {
                        classRef.currentColor = $(this).attr('current');
                    }
                    if ($(this).attr('right') != null) {
                        classRef.rightColors = $(this).attr('right').split(',');
                    }
                });
            });
        };
        WordSearch.prototype.disposeWords = function () {
            for (var i = 0; i < this.words.length; i++) {
                //while (!this.tryPosition(i));
                this.write(i, this.wordPositions[i].x, this.wordPositions[i].y);
            }
        };
        WordSearch.prototype.tryPosition = function (i) {
            var x = this.random(this.gridCols);
            var y = this.random(this.gridRows);
            var length = this.words[i].length;
            var orient = this.wordOrientations[i];
            if (x + orient.x * length < this.gridCols
                && x + orient.x * length >= 0) {
                if (y + orient.y * length < this.gridRows
                    && y + orient.y * length >= 0) {
                    this.write(i, x, y);
                    return true;
                }
            }
            return false;
        };
        WordSearch.prototype.write = function (word, x, y) {
            var letters = this.words[word].split("");
            var ori = this.wordOrientations[word];
            for (var i = 0; i < letters.length; i++) {
                if (this.tileLetters.indexOf(letters[i].toUpperCase()) >= 0) {
                    this.rightLetters[x][y] = letters[i];
                    x = x + ori.x;
                    y = y + ori.y;
                }
            }
        };
        WordSearch.prototype.getTilesWord = function (i) {
            var result = "";
            var letters = this.words[i].split("");
            for (var i = 0; i < letters.length; i++) {
                if (this.tileLetters.indexOf(letters[i].toUpperCase()) >= 0) {
                    result += letters[i];
                }
            }
            return result;
        };
        /**
         * //addapt grid dimensions to the longest word
        */
        WordSearch.prototype.addaptGrid = function () {
            var max = 0;
            for (var i = 0; i < this.words.length; i++) {
                var l = this.getTilesWord(i).length;
                if (l > max) {
                    max = l;
                }
            }
            if (this.gridCols < max) {
                this.gridCols = max;
            }
            if (this.gridRows < max) {
                this.gridRows = max;
            }
        };
        WordSearch.prototype.initTiles = function () {
            var me = this;
            //Loop through each column in the grid
            for (var i = 0; i < me.gridCols; i++) {
                //Loop through each position in a specific column, starting from the top
                for (var j = 0; j < me.gridRows; j++) {
                    //Add the tile to the game at this grid position
                    var tile = me.addTile(i, j);
                    //Keep a track of the tiles position in our tileGrid
                    me.tileGrid[i][j] = tile;
                }
            }
        };
        WordSearch.prototype.addTile = function (x, y) {
            var me = this;
            //Create a random data generator to use later
            var seed = Date.now();
            me.rdm = new Phaser.RandomDataGenerator([seed]);
            //Choose a random tile to add
            var tileLetter = me.tileLetters[me.rdm.integerInRange(0, me.tileLetters.length - 1)];
            if (this.case != 'uppercase') {
                tileLetter = tileLetter.toLowerCase();
            }
            if (this.rightLetters[x][y] != null) {
                tileLetter = this.rightLetters[x][y];
            }
            var tileColor = me.tileColors[me.rdm.integerInRange(0, me.tileColors.length - 1)];
            var tileToAdd = me.createTile(tileLetter, tileColor);
            //Add the tile at the correct x position, but add it to the top of the game (so we can slide it in)
            var tile = me.tiles.create(me.leftBuffer + (x * me.tileWidth) + me.tileWidth / 2, 0, tileToAdd);
            //Animate the tile into the correct vertical position
            me.game.add.tween(tile).to({ y: me.topBuffer + (y * me.tileHeight + (me.tileHeight / 2)) }, 500, Phaser.Easing.Linear.In, true);
            //Set the tiles anchor point to the center
            tile.anchor.setTo(0.5, 0.5);
            //Keep track of the type of tile that was added
            tile.tileLetter = tileLetter;
            return tile;
        };
        WordSearch.prototype.createTile = function (letter, color) {
            var me = this;
            var tile = me.game.add.bitmapData(me.tileWidth, me.tileHeight);
            tile.ctx.rect(5, 5, me.tileWidth - 5, me.tileHeight - 5);
            tile.ctx.fillStyle = color;
            tile.ctx.fill();
            tile.ctx.font = '30px Arial';
            tile.ctx.textAlign = 'center';
            tile.ctx.textBaseline = 'middle';
            tile.ctx.fillStyle = '#fff';
            if (color == '#ffffff') {
                tile.ctx.fillStyle = '#000000';
            }
            tile.ctx.fillText(letter, me.tileWidth / 2, me.tileHeight / 2);
            return tile;
        };
        WordSearch.prototype.checkValidate = function () {
            if (this.validate) {
                _super.prototype.checkValidate.call(this);
            }
        };
        WordSearch.prototype.hint = function () {
            this.log('entering hint...', 4);
            //TODO
            this.log('exiting hint...', 4);
        };
        WordSearch.prototype.solve = function () {
            //TODO
        };
        WordSearch.prototype.collectValidable = function () {
            var result = [];
            //the right texts
            result[0] = [];
            //the wrong ones
            result[1] = [];
            for (var i = 0; i < this.words.length; i++) {
                var pos = this.correctWords.indexOf(this.getTilesWord(i));
                if (pos >= 0) {
                    result[0].push(this.wordSprites[i]);
                }
                else {
                    result[1].push(this.wordSprites[i]);
                }
            }
            return result;
        };
        WordSearch.prototype.getDictionary = function () {
            var tileWords = [];
            for (var i = 0; i < this.words.length; i++) {
                tileWords.push(this.getTilesWord(i));
            }
            return " " + tileWords.join(" ") + " ";
        };
        WordSearch.prototype.update = function () {
            var me = this;
            if (me.guessing) {
                //Get the location of where the pointer is currently
                var hoverX = me.game.input.x;
                var hoverY = me.game.input.y;
                //Figure out what position on the grid that translates to
                var hoverPosX = Math.floor((hoverX - me.leftBuffer) / me.tileWidth);
                var hoverPosY = Math.floor((hoverY - me.topBuffer) / me.tileHeight);
                //Check that we are within the game bounds
                if (hoverPosX >= 0 && hoverPosX < me.tileGrid.length && hoverPosY >= 0 && hoverPosY < me.tileGrid[0].length) {
                    //Grab the tile being hovered over
                    var hoverTile = me.tileGrid[hoverPosX][hoverPosY];
                    //Figure out the bounds of the tile
                    var tileLeftPosition = me.leftBuffer + (hoverPosX * me.tileWidth);
                    var tileRightPosition = me.leftBuffer + (hoverPosX * me.tileWidth) + me.tileWidth;
                    var tileTopPosition = me.topBuffer + (hoverPosY * me.tileHeight);
                    var tileBottomPosition = me.topBuffer + (hoverPosY * me.tileHeight) + me.tileHeight;
                    //If the player is hovering over the tile set it to be active. The buffer is provided here so that the tile is only selected
                    //if the player is hovering near the center of the tile
                    if (!hoverTile.isActive && hoverX > tileLeftPosition + me.selectBuffer && hoverX < tileRightPosition - me.selectBuffer
                        && hoverY > tileTopPosition + me.selectBuffer && hoverY < tileBottomPosition - me.selectBuffer) {
                        //Set the tile to be active
                        hoverTile.isActive = true;
                        console.log(hoverTile.tileLetter);
                    }
                    if (me.currentWordEdges == null) {
                        me.currentWordEdges = { start: { x: hoverPosX, y: hoverPosY } };
                    }
                    else {
                        //see if they are aligned
                        var xdif = hoverPosX - me.currentWordEdges.start.x;
                        var ydif = hoverPosY - me.currentWordEdges.start.y;
                        var aligned = false;
                        if (xdif == 0 || ydif == 0) {
                            aligned = true;
                        }
                        else if (xdif == ydif || xdif == -ydif) {
                            aligned = true;
                        }
                        if (aligned) {
                            me.currentWord = [];
                            var xsense = (xdif < 0 ? -1 : 1);
                            var ysense = (ydif < 0 ? -1 : 1);
                            for (var i = me.currentWordEdges.start.x; (xsense == 1 && i <= hoverPosX || xsense == -1 && i >= hoverPosX); i = i + xsense) {
                                for (var j = me.currentWordEdges.start.y; (ysense == 1 && j <= hoverPosY || ysense == -1 && j >= hoverPosY); j = j + ysense) {
                                    console.debug('recording letter in (' + i + ', ' + j + ')');
                                    if (xdif != 0 && ydif != 0) {
                                        //there is some diagonal
                                        if (Math.abs(i - me.currentWordEdges.start.x) == Math.abs(j - me.currentWordEdges.start.y)) {
                                            me.currentWord.push(me.tileGrid[i][j]);
                                        }
                                    }
                                    else {
                                        me.currentWord.push(me.tileGrid[i][j]);
                                    }
                                }
                            }
                            console.debug("aligned (" + me.currentWordEdges.start.x + "," + me.currentWordEdges.start.x + "):(" + hoverPosX + "," + hoverPosY + ")!!");
                            me.markSelection();
                        }
                    }
                }
            }
            else {
                this.currentWordEdges = null;
                if (me.currentWord.length > 0) {
                    var guessedWord = '';
                    //Build a string out of all of the active tiles
                    for (var i = 0; i < me.currentWord.length; i++) {
                        guessedWord += me.currentWord[i].tileLetter;
                        me.currentWord[i].isActive = false;
                    }
                    //Check to see if this word exists in our dictionary
                    if (me.getDictionary().indexOf(' ' + guessedWord + ' ') > -1 && guessedWord.length > 1) {
                        //Check to see that the word has not already been guessed
                        if (me.correctWords.indexOf(guessedWord) == -1) {
                            console.log("correct!: " + guessedWord);
                            //Add this sprites word to the array of tiles
                            me.rightWords.push(me.currentWord);
                            //Add this string word to the already guessed word
                            me.correctWords.push(guessedWord);
                            me.triggerRight();
                            me.checkValidate();
                            if (me.correctWords.length == me.words.length) {
                                me.triggerEvents(LandPlay.LandEvent.CAUSE_COMPLETE);
                            }
                        }
                    }
                    else {
                        console.log("incorrect!: " + guessedWord);
                        me.triggerWrong();
                    }
                    //Reset the current word
                    me.currentWord = [];
                    me.markSelection();
                }
            }
        };
        WordSearch.prototype.markRightWords = function () {
            for (var i = 0; i < this.rightWords.length; i++) {
                var rightColor = this.rightColors[i % this.rightColors.length];
                for (var j = 0; j < this.rightWords[i].length; j++) {
                    this.rightWords[i][j].tint = this.colorToInt(rightColor);
                }
            }
        };
        WordSearch.prototype.markSelection = function () {
            var me = this;
            for (var i = 0; i < me.tiles.children.length; i++) {
                me.tiles.children[i].tint = me.colorToInt("#FFFFFF");
            }
            for (var i = 0; i < me.currentWord.length; i++) {
                me.currentWord[i].tint = me.colorToInt(me.currentColor);
            }
            me.markRightWords();
        };
        WordSearch.prototype.render = function () {
        };
        WordSearch.DEFAULT_WIDTH = 50;
        WordSearch.DEFAULT_HEIGHT = 50;
        WordSearch.DEFAULT_COLS = 5;
        WordSearch.DEFAULT_ROWS = 5;
        return WordSearch;
    }(LandPlay.LandGame));
    LandPlay.WordSearch = WordSearch;
})(LandPlay || (LandPlay = {}));
//# sourceMappingURL=landplay.js.map