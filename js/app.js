/**
 * @deprecated Development under TypeScript and landplayedit.js resultant
 */

var LandPlayEdit = {};

/**
 * Sprite that its beeing edited right now (one at a time)
 */
LandPlayEdit.sprite = null;

LandPlayEdit.storageKey = "LandPlayConfig";
/** 
 * Space to temporary save information about the game config 
 * */
LandPlayEdit.editor = new LandPlay.Editor({});

LandPlayEdit.Start = function(){
	var currentWork = localStorage.getItem(LandPlayEdit.storageKey);
	if (currentWork!=undefined){
		LandPlayEdit.editor.fromJSON(JSON.parse(currentWork));
		/**
		 * Load other values from saved json
		 */
		for(var key of Object.keys(LandPlayEdit.editor.json.config.setup)){
			$("[name='config.setup." + key + "'").val(LandPlayEdit.editor.json.config.setup[key]);
		}
		for(var key of Object.keys(LandPlayEdit.editor.json.config.layout)){
			$("[name='config.layout." + key + "'").val(LandPlayEdit.editor.json.config.layout[key]);
		}
		/**
		 * Load assets on assets' tab 
		 */
		for(var subtype of Object.keys(LandPlayEdit.editor.assets)){
			for(var key of Object.keys(LandPlayEdit.editor.assets[subtype])){
				if (LandPlayEdit.reservedSprites.indexOf(key)<0){
					LandPlayEdit.addAssetRow(LandPlayEdit.editor.assets[subtype][key], subtype);
				}
			}
		}
	}
}

/** 
 * Interactive phaser game/canvas to graphically deal with sprites 
 * */
LandPlayEdit.lgEditor;

/** 
 * fields that match between sprite form and sprite object 
 * */
LandPlayEdit.spriteFields = [
	"id", "key", "width", "height", "x" , "y", "angle", "tint"/*,
	"text.label", "text.key"*/
	];

LandPlayEdit.reservedSprites = ["preloadBar"];

LandPlayEdit.editSprite = function(){
	for (var i=0; i<LandPlayEdit.spriteFields.length; i++){
		var attribute = LandPlayEdit.spriteFields[i];
		var sprite = LandPlayEdit.sprite;
		var field = "";
		var pos = attribute.lastIndexOf(".");
		if (pos>-1){
			/*embeded fiels as text.**/
			var canonical = attribute.substr(0, pos);
			field = canonical + ".@" + attribute.substr(pos+1);
			if (canonical=="text"){
				alert(LandPlayEdit.sprite['id'] + ".textTag");
				sprite = LandPlayEdit.lgEditor.getMovie(LandPlayEdit.sprite['id'] + ".textTag");
			}
			
		}else{
			/*simple*/
			field = "@" + attribute;
		}
		if (sprite!=undefined){
			$("input[name='sprite." + field + "']").val(sprite[attribute]);
		}else{
			console.warn("No sprite found for the field '" + attribute + "'")
		}
		
	}
}

LandPlayEdit.sprite_click = function(sprite){
	LandPlayEdit.sprite = sprite;
	if (LandPlayEdit.sprite.id==undefined || LandPlayEdit.sprite.id==""){
		$("input[name='sprite.@id']").removeAttr("readonly");
	}else{
		$("input[name='sprite.@id']").attr("readonly", "readonly");
	}
	LandPlayEdit.editSprite();
	if (LandPlayEdit.lgEditor.action==LandPlay.EditAction.Text){
		console.debug(sprite);
		$('#textEditor').foundation('open');
	}else{
		$('#spriteEditor').foundation('open');
	}
}

LandPlayEdit.sprite_drop = function(sprite){
	LandPlayEdit.sprite = sprite;
	if (LandPlayEdit.sprite.id!=undefined && LandPlayEdit.sprite.id!=""){
		/** quiet load of the form, do not show the edit form */
		LandPlayEdit.editSprite();
		/*just to save the new position*/
		LandPlayEdit.renderSprite();
	}
}

LandPlayEdit.stage_load = function(){
	$("#side_panels").show();
}

LandPlayEdit.loadLayout = function(){
	$("#side_panels").hide();
	try{
		LandPlayEdit.editor.appendForm("#editorForm");
		$('#content').html("");
		/*LandPlayEdit.lgEditor = eval("new LandPlay." + $('select[name="layout.mode"]').val() + "(editor.toXML())");*/
		LandPlayEdit.lgEditor = new LandPlay.EditorMode(LandPlayEdit.editor.toXML(),{
			"editor": LandPlayEdit.editor,
			"sprite_click": LandPlayEdit.sprite_click,
			"sprite_drop": LandPlayEdit.sprite_drop,
			"stage_load": LandPlayEdit.stage_load
		});
		
		$("#available_assets .assets_table").html("");
		for(var key of Object.keys(LandPlayEdit.editor.assets.image)){
			if (LandPlayEdit.reservedSprites.indexOf(key)<0){
				var assetLi = $('#templates #tpl_available_asset_row li:first-child').clone(true);
				$('a', assetLi).html(LandPlayEdit.editor.assets.image[key].key);
				$('a', assetLi).attr("href", LandPlayEdit.editor.assets.image[key].url);
				$('a', assetLi).attr("data-key", LandPlayEdit.editor.assets.image[key].key);
				$("#available_assets .assets_table").append(assetLi);
			}
		}
		
		LandPlayEdit.saveData();
	} catch (e){
		console.error(e);
	}
}

LandPlayEdit.addAssetRow = function(asset, subtype){
	var assetLi = $('#templates #tpl_asset_row li:first-child').clone();
	$('a', assetLi).html(asset.key);
	$('a', assetLi).attr("href", asset.url);
	$("#" + subtype + "_assets .assets_table").append(assetLi);
}

LandPlayEdit.scaleSprite = function(updated){
	
	if ($("input[name='transient.sprite.scaled']:checked").length>0){
		var rate = LandPlayEdit.sprite.height / LandPlayEdit.sprite.width;
		var name = $(updated).attr("name");
		var other;
		switch(name){
		case "sprite.@width":
			other = "sprite.@height";
			break;
		case "sprite.height":
			other = "sprite.@width";
			rate = 1/rate;
			break;
		}
		$("input[name='" + other + "']").val(Math.round( $(updated).val() * rate ));
	}
}

LandPlayEdit.updateForms = function(){
	LandPlayEdit.spriteUpdate();
	$('#spriteEditor, #textEditor').foundation('close');
}

LandPlayEdit.spriteUpdate = function(){
	if(LandPlayEdit.sprite.id==undefined || LandPlayEdit.sprite.id=="") {
		/* it's the first used to create a sprite
		 * hide it, just used as template*/
		LandPlayEdit.sprite.visible = false;
		/* deleting it causes unstability*/
	}
	LandPlayEdit.renderSprite();
}

LandPlayEdit.renderSprite = function(){
	var formSelector = "#spriteEditor form";
	if (LandPlayEdit.lgEditor.action==LandPlay.EditAction.Text){
		/* include text fields*/
		//formSelector += ", #textEditor form";
	}
	LandPlayEdit.lgEditor.renderSprite(formSelector);
	/** 
	 * refresh sprite in config container 
	 * */
	LandPlayEdit.editor.json.config.layout.sprite = [];
	for (var key in LandPlayEdit.lgEditor.sprites){
		LandPlayEdit.sprite = LandPlayEdit.lgEditor.sprites[key];
		if (LandPlayEdit.sprite.key!="__default" && LandPlayEdit.sprite.id!=null && LandPlayEdit.sprite.id!=""){
			/** 
			 * quiet load of the form, do not show the edit form 
			 * just to limit sprite attributes to the ones defined in the form
			 * */
			LandPlayEdit.editSprite();
			LandPlayEdit.editor.json.config.layout.sprite.push( LandPlayEdit.editor.getSpriteJson(formSelector).sprite );
		}
	}
	
	LandPlayEdit.saveData();
	
}

LandPlayEdit.saveData = function(){
	localStorage.setItem(LandPlayEdit.storageKey, JSON.stringify(LandPlayEdit.editor.toJSON()));
}

LandPlayEdit.spriteDelete = function(){
	LandPlayEdit.sprite.destroy();
}