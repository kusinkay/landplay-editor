declare module LandPlayEdit {
    module i18n {
        const ca_ES: {
            mainMenu: {
                tabs: {
                    assets: string;
                    setup: string;
                    layout: string;
                    preview: string;
                };
            };
            button: {
                update: string;
                delete: string;
            };
            validator: {
                required: string;
                number: {
                    expected: string;
                    required: string;
                };
                alpha_numeric: {
                    expected: string;
                    required: string;
                };
                gtzero: {
                    expected: string;
                    required: string;
                };
                selection: {
                    expected: string;
                    required: string;
                };
            };
            option: {
                dimension: {
                    pixels: string;
                    percentage: string;
                };
                boolean: {
                    yes: string;
                    no: string;
                };
                outbound: {
                    cycle: string;
                    rebound: string;
                };
                pickable: {
                    true: string;
                    false: string;
                };
            };
            editorForm: {
                assets: {
                    file: {
                        label: string;
                    };
                    key: {
                        label: string;
                    };
                    upload: {
                        label: string;
                        value: string;
                    };
                    images: {
                        header: string;
                    };
                    sounds: {
                        header: string;
                    };
                };
                setup: {
                    mode: {
                        label: string;
                    };
                    width: {
                        label: string;
                    };
                    height: {
                        label: string;
                    };
                };
                layout: {
                    background: {
                        label: string;
                    };
                    loadLayout: {
                        label: string;
                    };
                    togglePanels: {
                        label: string;
                    };
                    sidePanels: {
                        header: string;
                    };
                    toolbox: {
                        header: string;
                        sprites: {
                            header: string;
                        };
                        behaviour: {
                            header: string;
                        };
                        Edit: {
                            tooltip: string;
                        };
                        Position: {
                            tooltip: string;
                        };
                        Text: {
                            tooltip: string;
                        };
                        Button: {
                            tooltip: string;
                        };
                        locate: {
                            tooltip: string;
                        };
                        Grid: {
                            tooltip: string;
                        };
                        Shape: {
                            tooltip: string;
                        };
                        retry: {
                            tooltip: string;
                        };
                        counter: {
                            tooltip: string;
                        };
                        events: {
                            tooltip: string;
                        };
                        frames: {
                            tooltip: string;
                        };
                    };
                };
            };
            reveal: {
                text: {
                    empty: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    key: {
                        label: string;
                    };
                    label: {
                        label: string;
                    };
                    x: {
                        label: string;
                    };
                    y: {
                        label: string;
                    };
                    style: {
                        label: string;
                        value: string;
                        applied: string;
                    };
                };
                event: {
                    empty: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    cause: {
                        label: string;
                    };
                    effect: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    value: {
                        label: string;
                    };
                };
                shape: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    type: {
                        label: string;
                    };
                    color: {
                        label: string;
                    };
                    radius: {
                        label: string;
                    };
                    width: {
                        label: string;
                    };
                    height: {
                        label: string;
                    };
                    line: {
                        width: {
                            label: string;
                        };
                        color: {
                            label: string;
                        };
                    };
                };
                sprite: {
                    delete: string;
                    id: {
                        label: string;
                    };
                    visible: {
                        label: string;
                    };
                    width: {
                        label: string;
                    };
                    height: {
                        label: string;
                    };
                    x: {
                        label: string;
                    };
                    marginRight: {
                        label: string;
                    };
                    y: {
                        label: string;
                    };
                    marginBottom: {
                        label: string;
                    };
                    tint: {
                        label: string;
                    };
                    angle: {
                        label: string;
                    };
                    frame: {
                        label: string;
                    };
                    layers: {
                        from: {
                            label: string;
                        };
                        to: {
                            label: string;
                        };
                    };
                };
                frame: {
                    nav: {
                        previous: string;
                        next: string;
                    };
                };
                button: {
                    type: {
                        label: string;
                    };
                    value: {
                        label: string;
                    };
                };
                retry: {
                    delete: string;
                    retries: {
                        label: string;
                        help: string;
                        info: string;
                    };
                    points: {
                        legend: string;
                    };
                    from: {
                        label: string;
                    };
                    to: {
                        label: string;
                    };
                    step: {
                        label: string;
                    };
                    validator: {
                        from: string;
                        to: string;
                        toeqfrom: string;
                    };
                };
                counter: {
                    live: {
                        text: {
                            label: string;
                            help: string;
                        };
                    };
                    points: {
                        text: {
                            label: string;
                            help: string;
                        };
                    };
                    time: {
                        legend: string;
                        text: {
                            label: string;
                            help: string;
                        };
                        secs: {
                            label: string;
                        };
                    };
                    progress: {
                        sprite: {
                            label: string;
                            help: string;
                        };
                    };
                };
                grid: {
                    delete: string;
                    xoffset: {
                        label: string;
                        help: string;
                    };
                    yoffset: {
                        label: string;
                        help: string;
                    };
                    cols: {
                        label: string;
                        help: string;
                    };
                    length: {
                        label: string;
                    };
                    selection: {
                        label: string;
                    };
                };
                piece: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    outbound: {
                        label: string;
                        help: string;
                    };
                    movex: {
                        label: string;
                    };
                    movey: {
                        label: string;
                    };
                };
                target: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    events: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    sound: {
                        label: string;
                    };
                };
                weapon: {
                    sprite: {
                        label: string;
                    };
                    pickable: {
                        label: string;
                    };
                };
            };
            plugin: {
                loaded: string;
                constraint: {
                    header: string;
                    catch: {
                        target: string;
                        weapon: string;
                        piece: string;
                        runaway: string;
                    };
                };
                toolbox: {
                    catch: {
                        piece: {
                            label: string;
                        };
                        target: {
                            label: string;
                        };
                        weapon: {
                            label: string;
                        };
                    };
                };
                field: {
                    setup: {
                        speed: {
                            label: string;
                            help: string;
                        };
                        runawaySpeed: {
                            label: string;
                            help: string;
                        };
                    };
                };
            };
            constraint: {
                asset: {
                    keyExists: string;
                    novalid: string;
                    layers: {
                        unmatch: string;
                        nonconsecutive: string;
                        justimage: string;
                    };
                };
            };
            alert: {
                upload: {
                    failed: string;
                    novalid: string;
                };
            };
            success: {
                upload: string;
            };
        };
    }
}
declare module LandPlayEdit {
    module i18n {
        const en_US: {
            mainMenu: {
                tabs: {
                    assets: string;
                    setup: string;
                    layout: string;
                    preview: string;
                };
            };
            button: {
                update: string;
                delete: string;
            };
            validator: {
                required: string;
                number: {
                    expected: string;
                    required: string;
                };
                alpha_numeric: {
                    expected: string;
                    required: string;
                };
                gtzero: {
                    expected: string;
                    required: string;
                };
                selection: {
                    expected: string;
                    required: string;
                };
                color: {
                    expected: string;
                    required: string;
                };
            };
            option: {
                dimension: {
                    pixels: string;
                    percentage: string;
                };
                boolean: {
                    yes: string;
                    no: string;
                };
                outbound: {
                    cycle: string;
                    rebound: string;
                };
                pickable: {
                    true: string;
                    false: string;
                };
            };
            editorForm: {
                assets: {
                    file: {
                        label: string;
                    };
                    key: {
                        label: string;
                    };
                    upload: {
                        label: string;
                        value: string;
                    };
                    images: {
                        header: string;
                    };
                    sounds: {
                        header: string;
                    };
                };
                setup: {
                    mode: {
                        label: string;
                    };
                    width: {
                        label: string;
                    };
                    height: {
                        label: string;
                    };
                };
                layout: {
                    background: {
                        label: string;
                    };
                    loadLayout: {
                        label: string;
                    };
                    togglePanels: {
                        label: string;
                    };
                    sidePanels: {
                        header: string;
                    };
                    toolbox: {
                        header: string;
                        sprites: {
                            header: string;
                        };
                        behaviour: {
                            header: string;
                        };
                        Edit: {
                            tooltip: string;
                        };
                        Position: {
                            tooltip: string;
                        };
                        Text: {
                            tooltip: string;
                        };
                        Button: {
                            tooltip: string;
                        };
                        locate: {
                            tooltip: string;
                        };
                        Grid: {
                            tooltip: string;
                        };
                        Shape: {
                            tooltip: string;
                        };
                        retry: {
                            tooltip: string;
                        };
                        counter: {
                            tooltip: string;
                        };
                        events: {
                            tooltip: string;
                        };
                        frames: {
                            tooltip: string;
                        };
                    };
                };
            };
            reveal: {
                text: {
                    empty: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    key: {
                        label: string;
                    };
                    label: {
                        label: string;
                    };
                    x: {
                        label: string;
                    };
                    y: {
                        label: string;
                    };
                    style: {
                        label: string;
                        value: string;
                        applied: string;
                    };
                };
                event: {
                    empty: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    cause: {
                        label: string;
                    };
                    effect: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    value: {
                        label: string;
                    };
                };
                shape: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    type: {
                        label: string;
                    };
                    color: {
                        label: string;
                    };
                    radius: {
                        label: string;
                    };
                    width: {
                        label: string;
                    };
                    height: {
                        label: string;
                    };
                    line: {
                        width: {
                            label: string;
                        };
                        color: {
                            label: string;
                        };
                    };
                };
                sprite: {
                    delete: string;
                    id: {
                        label: string;
                    };
                    visible: {
                        label: string;
                    };
                    width: {
                        label: string;
                    };
                    height: {
                        label: string;
                    };
                    x: {
                        label: string;
                    };
                    marginRight: {
                        label: string;
                    };
                    y: {
                        label: string;
                    };
                    marginBottom: {
                        label: string;
                    };
                    tint: {
                        label: string;
                    };
                    angle: {
                        label: string;
                    };
                    frame: {
                        label: string;
                    };
                    layers: {
                        from: {
                            label: string;
                        };
                        to: {
                            label: string;
                        };
                    };
                };
                frame: {
                    nav: {
                        previous: string;
                        next: string;
                    };
                };
                button: {
                    type: {
                        label: string;
                    };
                    value: {
                        label: string;
                    };
                };
                retry: {
                    delete: string;
                    retries: {
                        label: string;
                        help: string;
                        info: string;
                    };
                    points: {
                        legend: string;
                    };
                    from: {
                        label: string;
                    };
                    to: {
                        label: string;
                    };
                    step: {
                        label: string;
                    };
                    validator: {
                        from: string;
                        to: string;
                        toeqfrom: string;
                    };
                };
                counter: {
                    live: {
                        text: {
                            label: string;
                            help: string;
                        };
                    };
                    points: {
                        text: {
                            label: string;
                            help: string;
                        };
                    };
                    time: {
                        legend: string;
                        text: {
                            label: string;
                            help: string;
                        };
                        secs: {
                            label: string;
                        };
                    };
                    progress: {
                        sprite: {
                            label: string;
                            help: string;
                        };
                    };
                };
                grid: {
                    delete: string;
                    xoffset: {
                        label: string;
                        help: string;
                    };
                    yoffset: {
                        label: string;
                        help: string;
                    };
                    cols: {
                        label: string;
                        help: string;
                    };
                    length: {
                        label: string;
                    };
                    selection: {
                        label: string;
                    };
                };
                piece: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    outbound: {
                        label: string;
                        help: string;
                    };
                    movex: {
                        label: string;
                    };
                    movey: {
                        label: string;
                    };
                };
                target: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    events: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    sound: {
                        label: string;
                    };
                };
                weapon: {
                    sprite: {
                        label: string;
                    };
                    pickable: {
                        label: string;
                    };
                };
                color: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    color: {
                        label: string;
                    };
                };
                canvas: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    color: {
                        label: string;
                    };
                };
                brush: {
                    sprite: {
                        label: string;
                    };
                    pickable: {
                        label: string;
                    };
                };
            };
            plugin: {
                loaded: string;
                constraint: {
                    header: string;
                    catch: {
                        target: string;
                        weapon: string;
                        piece: string;
                        runaway: string;
                    };
                    paint: {
                        canvas: string;
                        brush: string;
                        color: string;
                        noColorSprites: string;
                    };
                };
                toolbox: {
                    catch: {
                        piece: {
                            label: string;
                        };
                        target: {
                            label: string;
                        };
                        weapon: {
                            label: string;
                        };
                    };
                    paint: {
                        brush: {
                            label: string;
                        };
                        color: {
                            label: string;
                        };
                        canvas: {
                            label: string;
                        };
                    };
                };
                field: {
                    setup: {
                        speed: {
                            label: string;
                            help: string;
                        };
                        runawaySpeed: {
                            label: string;
                            help: string;
                        };
                    };
                };
            };
            constraint: {
                asset: {
                    keyExists: string;
                    novalid: string;
                    layers: {
                        unmatch: string;
                        nonconsecutive: string;
                        justimage: string;
                    };
                };
            };
            alert: {
                upload: {
                    failed: string;
                    novalid: string;
                };
            };
            success: {
                upload: string;
            };
        };
    }
}
declare module LandPlayEdit {
    module i18n {
        const es_ES: {
            mainMenu: {
                tabs: {
                    assets: string;
                    setup: string;
                    layout: string;
                    preview: string;
                };
            };
            button: {
                update: string;
                delete: string;
            };
            validator: {
                required: string;
                number: {
                    expected: string;
                    required: string;
                };
                alpha_numeric: {
                    expected: string;
                    required: string;
                };
                gtzero: {
                    expected: string;
                    required: string;
                };
                selection: {
                    expected: string;
                    required: string;
                };
            };
            option: {
                dimension: {
                    pixels: string;
                    percentage: string;
                };
                boolean: {
                    yes: string;
                    no: string;
                };
                outbound: {
                    cycle: string;
                    rebound: string;
                };
                pickable: {
                    true: string;
                    false: string;
                };
            };
            editorForm: {
                assets: {
                    file: {
                        label: string;
                    };
                    key: {
                        label: string;
                    };
                    upload: {
                        label: string;
                        value: string;
                    };
                    images: {
                        header: string;
                    };
                    sounds: {
                        header: string;
                    };
                };
                setup: {
                    mode: {
                        label: string;
                    };
                    width: {
                        label: string;
                    };
                    height: {
                        label: string;
                    };
                };
                layout: {
                    background: {
                        label: string;
                    };
                    loadLayout: {
                        label: string;
                    };
                    togglePanels: {
                        label: string;
                    };
                    sidePanels: {
                        header: string;
                    };
                    toolbox: {
                        header: string;
                        sprites: {
                            header: string;
                        };
                        behaviour: {
                            header: string;
                        };
                        Edit: {
                            tooltip: string;
                        };
                        Position: {
                            tooltip: string;
                        };
                        Text: {
                            tooltip: string;
                        };
                        Button: {
                            tooltip: string;
                        };
                        locate: {
                            tooltip: string;
                        };
                        Grid: {
                            tooltip: string;
                        };
                        Shape: {
                            tooltip: string;
                        };
                        retry: {
                            tooltip: string;
                        };
                        counter: {
                            tooltip: string;
                        };
                        events: {
                            tooltip: string;
                        };
                        frames: {
                            tooltip: string;
                        };
                    };
                };
            };
            reveal: {
                text: {
                    empty: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    key: {
                        label: string;
                    };
                    label: {
                        label: string;
                    };
                    x: {
                        label: string;
                    };
                    y: {
                        label: string;
                    };
                    style: {
                        label: string;
                        value: string;
                        applied: string;
                    };
                };
                event: {
                    empty: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    cause: {
                        label: string;
                    };
                    effect: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    value: {
                        label: string;
                    };
                };
                shape: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    type: {
                        label: string;
                    };
                    color: {
                        label: string;
                    };
                    radius: {
                        label: string;
                    };
                    width: {
                        label: string;
                    };
                    height: {
                        label: string;
                    };
                    line: {
                        width: {
                            label: string;
                        };
                        color: {
                            label: string;
                        };
                    };
                };
                sprite: {
                    delete: string;
                    id: {
                        label: string;
                    };
                    visible: {
                        label: string;
                    };
                    width: {
                        label: string;
                    };
                    height: {
                        label: string;
                    };
                    x: {
                        label: string;
                    };
                    marginRight: {
                        label: string;
                    };
                    y: {
                        label: string;
                    };
                    marginBottom: {
                        label: string;
                    };
                    tint: {
                        label: string;
                    };
                    angle: {
                        label: string;
                    };
                    frame: {
                        label: string;
                    };
                    layers: {
                        from: {
                            label: string;
                        };
                        to: {
                            label: string;
                        };
                    };
                };
                frame: {
                    nav: {
                        previous: string;
                        next: string;
                    };
                };
                button: {
                    type: {
                        label: string;
                    };
                    value: {
                        label: string;
                    };
                };
                retry: {
                    delete: string;
                    retries: {
                        label: string;
                        help: string;
                        info: string;
                    };
                    points: {
                        legend: string;
                    };
                    from: {
                        label: string;
                    };
                    to: {
                        label: string;
                    };
                    step: {
                        label: string;
                    };
                    validator: {
                        from: string;
                        to: string;
                        toeqfrom: string;
                    };
                };
                counter: {
                    live: {
                        text: {
                            label: string;
                            help: string;
                        };
                    };
                    points: {
                        text: {
                            label: string;
                            help: string;
                        };
                    };
                    time: {
                        legend: string;
                        text: {
                            label: string;
                            help: string;
                        };
                        secs: {
                            label: string;
                        };
                    };
                    progress: {
                        sprite: {
                            label: string;
                            help: string;
                        };
                    };
                };
                grid: {
                    delete: string;
                    xoffset: {
                        label: string;
                        help: string;
                    };
                    yoffset: {
                        label: string;
                        help: string;
                    };
                    cols: {
                        label: string;
                        help: string;
                    };
                    length: {
                        label: string;
                    };
                    selection: {
                        label: string;
                    };
                };
                piece: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    outbound: {
                        label: string;
                        help: string;
                    };
                    movex: {
                        label: string;
                    };
                    movey: {
                        label: string;
                    };
                };
                target: {
                    empty: string;
                    delete: string;
                    list: {
                        header: string;
                    };
                    new: {
                        label: string;
                    };
                    events: {
                        label: string;
                    };
                    sprite: {
                        label: string;
                    };
                    sound: {
                        label: string;
                    };
                };
                weapon: {
                    sprite: {
                        label: string;
                    };
                    pickable: {
                        label: string;
                    };
                };
            };
            plugin: {
                loaded: string;
                constraint: {
                    header: string;
                    catch: {
                        target: string;
                        weapon: string;
                        piece: string;
                        runaway: string;
                    };
                };
                toolbox: {
                    catch: {
                        piece: {
                            label: string;
                        };
                        target: {
                            label: string;
                        };
                        weapon: {
                            label: string;
                        };
                    };
                };
                field: {
                    setup: {
                        speed: {
                            label: string;
                            help: string;
                        };
                        runawaySpeed: {
                            label: string;
                            help: string;
                        };
                    };
                };
            };
        }, constraint: {
            asset: {
                keyExists: "El identificador '{{key}}' ya existe como '{{subtype}}', ¿sobrescribir?";
                novalid: "Por favor selecciona un fichero válido {{sample}}. Estás subiendo un {{type}}";
                layers: {
                    unmatch: "Los ficheros seleccionados no tienen el mismo patrón (ej.: cualquiera_<<numero>>.png)";
                    nonconsecutive: "Los ficheros seleccionados no tienen sufijos numéricos correlativos.";
                    justimage: "La selección múltiple de ficheros solo es posible con imágenes.";
                };
            };
        }, alert: {
            upload: {
                failed: "La subida de archivos ha fallado, por favor intentar de nuevo.";
                novalid: "POr favor selecciona un fichero válido.";
            };
        }, success: {
            upload: "¡El fichero se ha subido satisfactoriamente!";
        };
    }
}
