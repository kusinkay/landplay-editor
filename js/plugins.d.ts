/// <reference path="../typescript/landplay.d.ts" />
/// <reference path="../typescript/phaser.d.ts" />
/// <reference path="../typescript/jquery.d.ts" />
/// <reference path="translations.d.ts" />
declare module LandPlayEdit {
    module PluginFactory {
        function newInstance(mode: LandPlay.Mode): LandPlayEdit.Plugin;
    }
}
declare module LandPlay {
    enum EditAction {
        Edit = 0,
        Position = 1,
        Text = 2,
        Frames = 3,
        Button = 4,
        Grid = 5,
        Shape = 6,
        Anchor = 7,
        Embed = 8,
    }
    class EditorMode extends LandGame {
        private action;
        private editor;
        /**
         * Callbacks for sprite
         */
        private sprite_click;
        private sprite_drop;
        private sprite_unnamed;
        private minFrameDim;
        textFields: {
            "@key": {
                "attr": string;
                "default": string;
            };
            "@label": {
                "attr": string;
            };
            "@x": {
                "attr": string;
                "default": number;
            };
            "@y": {
                "attr": string;
                "default": number;
            };
        };
        /**
         * The Sprite whose that is being edited*/
        private targetSprite;
        private destinationSprite;
        private bmd;
        private frameInfo;
        private frameCloseSprite;
        private anchor;
        EmptySpriteKey: string;
        ungroupedGridActions: EditAction[];
        private groups;
        /**
         * We can't operate at all with no visible sprites
         * So we stablish an alpha value to emulate visible=false
         * */
        private GhostNumber;
        constructor(xmlFile: string, params?: object);
        renderSprite(spriteNode: object, spParent: Phaser.Sprite, depth?: number, children?: object): object;
        /**
         * @override
         * */
        preRenderSprite(jqXmlnode: any): void;
        newSprite(key: any, params?: object): void;
        initClip(sprite: Phaser.Sprite, params?: object): void;
        private initLiner(sprite);
        showBounds(sprite: Phaser.Sprite, colors?: Array<number>): void;
        clearBounds(sprite: Phaser.Sprite): void;
        getGridPivot(sprite: Phaser.Sprite): any;
        getPivot(sprite: Phaser.Sprite, depth?: number): any;
        getGridGroup(sprite: Phaser.Sprite): any;
        getGroup(sprite: Phaser.Sprite, depth?: number): any;
        private isEmbeded(sprite);
        getGridPivotByName(name: string): any;
        getPivotByName(name: string): any;
        getGridGroupByName(name: string): any;
        getGroupByName(name: string, depth?: number): any;
        text2Json(text: Phaser.Text): {};
        private addDragEvents(sprite);
        setAction(action: LandPlay.EditAction): void;
        locateAllSprites(on: boolean): void;
        isDragable(): boolean;
        isDragableSprite(sprite: Phaser.Sprite): boolean;
        isEditable(): boolean;
        loadConf(): void;
        onConfigLoad(): void;
        update(): void;
        doEmbed(): void;
        private updateEmbed();
        private updateFrames();
        private updateAnchor();
        private clearAnchor();
        drawLine(color: string, xb: number, xe: number, yb: number, ye: number, lineWidth: number): void;
        onStageLoad(): void;
    }
}
declare module LandPlay {
    enum AssetType {
        Image = "image",
        Sound = "sound",
    }
    const EmptyImageAsset = "__EMPTY__";
    class Editor {
        /**
         * @deprecated located directly on json.config.assets
         **/
        private assets;
        private sprites;
        json: {
            "config": {
                "assets": {};
                "setup": {
                    "counter": {};
                    "event": any[];
                };
                "layout": {
                    "sprite": any[];
                };
                "button": any[];
                "retry": {};
            };
        };
        private serverSidePath;
        private frameAtts;
        constructor(args: object);
        getAssets(): {};
        addAsset(args: object, type: AssetType): void;
        updateAsset(args: object, type: AssetType): void;
        setFrames(key: string, frames: object): void;
        getAsset(key: string, type: AssetType): any;
        getAssetPos(key: string, type: AssetType): number;
        getSprite(id: string, sprites?: Array<object>, goDeeper?: boolean): any;
        getSpriteIndex(id: string, sprites?: Array<object>): number;
        removeSprite(id: string, sprites?: Array<object>): void;
        getSpriteParent(id: any): any;
        addChildren(id: string, children: Array<object>): void;
        removeChild(id: string, child: object): void;
        appendForm(formSelector: string): void;
        getSpriteXml(formSelector: string): any;
        getSpriteJson(formSelector: string): {
            "sprite": {};
        };
        addButton(button: object): void;
        getButton(sprite: string, position?: boolean): any;
        deleteButton(sprite: string): void;
        json2xml(json: any): any;
        /**
         * recursive: current config node appending the suffix
         */
        walk(node: object, field: object, ignoreEmpty?: boolean): object;
        parse(json: object, fieldName: string): any;
        toXMLURL(): string;
        toJSON(): {
            "config": {
                "assets": {};
                "setup": {
                    "counter": {};
                    "event": any[];
                };
                "layout": {
                    "sprite": any[];
                };
                "button": any[];
                "retry": {};
            };
        };
        private addAssetAtts(from, to, reverse?);
        fromJSON(json: any): void;
        toXML(indent?: boolean): any;
    }
}
declare module LandPlayEdit {
}
declare module LandPlayEdit {
    /**
     * Sprite that its beeing edited right now (one at a time)
     */
    var sprite: Phaser.Sprite;
    var storageKey: string;
    /**
     * Space to temporary save information about the game config
     * */
    var editor: LandPlay.Editor;
    var plugin: LandPlayEdit.Plugin;
    var buttonActionKeys: any[];
    var textSelects: string[];
    var validationSelects: string[];
    function Start(): void;
    function localize(): void;
    function reInitFoundation(): void;
    /**
    * Interactive phaser game/canvas to graphically deal with sprites
    * */
    var lgEditor: LandPlay.EditorMode;
    /**
     * fields that match between sprite form and sprite object
     * */
    var spriteFields: string[];
    /**
     * inverse from textFields, loaded at Start
     */
    var textAttributes: {};
    var textStyleAttributes: any;
    var reservedSprites: string[];
    function initPlugin(): void;
    function editSprite(): void;
    function changeSpritePosition(name: string, value: string): void;
    function sprite_unnamed(sprite: Phaser.Sprite): void;
    function sprite_click(sprite: Phaser.Sprite, info?: object): void;
    function sprite_drop(sprite: any, info?: object): void;
    function listRecord(destSelector: string, i: number, label: string): void;
    function listOrder(destSelector: string, i: number, label: string, top: boolean, bottom: boolean): void;
    function activateList(prefix: string): void;
    function editRecord(type: string, pos: number): void;
    function deleteRecord(type: string, pos: number): void;
    function upRecord(type: string, pos: number): void;
    function downRecord(type: string, pos: number): void;
    function changeDepth(pos: number, offset: number): void;
    function saveJson(): void;
    function clearActivity(): void;
    function resetSprite(sprite: Phaser.Sprite): void;
    function sprite_framed(sprite: Phaser.Sprite, frames: object): void;
    function stage_load(): void;
    function loadLayout(): void;
    function loadPreview(): void;
    function addAssetRow(asset: any, subtype: any): void;
    function scaleSprite(updated: any): void;
    function updateForms(): boolean;
    function createText(): void;
    /**
     * To sprite JSON, as a new text node
     * */
    function updateText(): void;
    function parseStyleFromSelect(fieldName: string): string;
    function editText(pos: number): void;
    function deleteText(pos: any): void;
    function launchFrameEditor(key: string): void;
    function editFrames(asset: object): void;
    function editButton(): void;
    function editRetry(): void;
    function updateRetry(): void;
    function deleteRetry(): void;
    function validateRetry(): void;
    function editCounter(): void;
    function updateCounter(): void;
    function deleteCounter(): void;
    function validateCounter(name: any): void;
    function openEvents(): void;
    function openEventsDo(events: Array<object>, container?: number, excludedCauses?: Array<string>, excludedEffects?: Array<string>, subcontainer?: number, addAll?: boolean, addThis?: boolean): void;
    function createEvent(): void;
    function editEvent(pos: number): void;
    function updateEvent(): void;
    function deleteEvent(pos: any): void;
    function getEventsCollection(): any[];
    function validateEvent(name: any): void;
    function toggleField(name: string, required: boolean, pattern?: string, errMessage?: string): void;
    function editGrid(): void;
    function updateGrid(): void;
    function deleteGrid(): void;
    function validateGrid(name: any): void;
    function openShapes(): void;
    function createShape(): void;
    function updateShape(): void;
    function editShape(pos: number): void;
    function deleteShape(pos: any): void;
    function validateShape(name: any): void;
    function openDepth(): void;
    function editValidation(): void;
    function updateValidation(): void;
    function deleteValidation(): void;
    function validateValidation(name: any): void;
    function notEmptySelectValidator($el: any, required: any, parent: any): boolean;
    function reloadTextSelect(selector: string, value?: string, force?: boolean): void;
    function reloadSpriteSelect(selector: string, value: string, force?: boolean, extended?: boolean, exceptions?: Array<string>, layers?: boolean, grids?: boolean, addThis?: boolean): void;
    function groupSprites(selector: string, value: string, key: string, contName: string, setOptionAndGetKey: any): void;
    function reloadSoundSelect(selector: any, value?: string, force?: boolean): void;
    function reloadImageSelect(selector: any, value?: string, force?: boolean): void;
    function reloadAssetsSelect(type: LandPlay.AssetType, selector: any, value?: string, force?: boolean): void;
    function behaviour(type: any): void;
    function success(text: any): void;
    function warning(text: any): void;
    function alert(text: any): void;
    function confirm(text: string, yes: any, no?: any, end?: any): boolean;
    function loadStyleEditorTemplate(destination: any, fieldName?: string): void;
    function loadDefaultTextStyle(): void;
    function loadStyleOption(attr: string, value: string): void;
    function addTextStyle(): void;
    function editTextStyle(): void;
    function delTextStyle(): void;
    function addTool(tool: object, group: string): void;
    function changeButtonType(): void;
    function parseFormButton(): {};
    function updateButton(): void;
    function resetButtonForm(): void;
    function updateSprite(): void;
    function getGroup(sprite: Phaser.Sprite): Phaser.Group;
    function spriteToJson(sprite: Phaser.Sprite, oSprite?: object): {};
    /**
     * Running objects (original: from a Phaser.Sprite) don't have persistent reflected properties
     * Copy them from "persistent" to "original"
     * */
    function mergeSprites(original: object, persistent: object): object;
    function xmlReadyJson(jsonSprite: object): any;
    function renderSprite(jsonSprite: object): void;
    function saveData(): void;
    function deleteSprite(): void;
    function openChildren(): void;
    function editChildren(pos: number): void;
    function deleteChildren(pos: any): void;
}
declare module LandPlayEdit {
    class Plugin {
        protected toolBox: {
            behaviour: any[];
        };
        protected effects: any[];
        protected fields: {
            tabs: {
                setup: any[];
                layout: any[];
            };
        };
        static TplPath: string;
        private constraints;
        constructor(params: object);
        completeToolBox(): void;
        private customizeForms();
        private doLoadRemainingSprites(collection, formField, value, layers?, grids?);
        /** Loads the plugin HTML templates to the HTML templates section of the editor page*/
        loadTemplate(): void;
        abstract getTemplateName(): string;
        abstract behaviour(type: any): any;
        /**
         * Preview additional graphic elements
         * Launched from editor when Stage is ready
         * Override in each plugin
         ******* */
        preview(): void;
        validate(): boolean;
        abstract getEventsCollection(container: number, subcontainer: number): any;
        abstract validateEvent(name: any): any;
        abstract editRecord(type: string, pos: number): any;
        abstract deleteRecord(type: string, pos: number): any;
        updateRecord(collection: Array<object>, pos: number, record: object): object;
        modal(template: string, callback: any, params?: object): void;
        appendEffects(selector: string): void;
        protected hasItem(collection: Array<object>, pos: string): boolean;
        protected nextId(collection: Array<object>, field: string): number;
        protected checkSprites(collection: Array<object>, type: string, attrName?: string): void;
        protected checkFrames(sprite: string, length: number): boolean;
        addConstraint(constraint: LandPlayEdit.Constraint): void;
        hasConstraints(): boolean;
        showConstraints(): void;
        resetConstraints(): void;
    }
    class Constraint {
        text: string;
        toolBoxKey: string;
        toolBoxSection: string;
        constructor(toolBoxSection: string, toolBoxKey: string, text: string);
    }
}
declare module LandPlayEdit {
    class PluginArcade extends LandPlayEdit.Plugin {
        private brushSelects;
        constructor();
        getTemplateName(): string;
        behaviour(type: any): void;
        editRecord(type: string, pos: number): void;
        deleteRecord(type: string, pos: number): void;
        private loadRemainingSpritesForCraft(value?);
        private loadRemainingSpritesForEdges(value?);
        private loadRemainingSpritesForLandscapes(value?);
        private loadRemainingSpritesForWhatever(value?);
        private getUsedSpritesInLandscapes();
        private loadEdgeSprites(value?);
        validateEvent(name: any): void;
        validateStage(name: any): void;
        private hasEdge(pos);
        private hasCanvas(pos);
        private hasItem(collection, pos);
        openAsteroids(): void;
        createAsteroid(): void;
        editAsteroid(pos: number): void;
        updateAsteroid(): void;
        deleteAsteroid(pos: number): void;
        openEvents(pos: string): any;
        getEventsCollection(container: number): any;
        private hasPiece(pos);
        openEdges(): void;
        createEdge(): void;
        editEdge(pos: number): void;
        updateEdge(): void;
        deleteEdge(pos: number): void;
        openLandscapes(): void;
        createLandscape(): void;
        editLandscape(pos: number): void;
        updateLandscape(): void;
        deleteLandscape(pos: number): void;
        editStage(): void;
        updateStage(): void;
        validate(): boolean;
    }
}
declare module LandPlayEdit {
    class PluginCatch extends LandPlayEdit.Plugin {
        constructor();
        getTemplateName(): string;
        behaviour(type: any): void;
        editRecord(type: string, pos: number): void;
        deleteRecord(type: string, pos: number): void;
        private loadRemainingSprites(value?);
        openEvents(pos: string): any;
        getEventsCollection(container: number): any;
        validateEvent(name: any): void;
        private hasTarget(pos);
        openTargets(): void;
        createTarget(): void;
        editTarget(pos: number): void;
        deleteTarget(pos: number): void;
        updateTarget(): void;
        openPieces(): void;
        createPiece(): void;
        editPiece(pos: number): void;
        updatePiece(): void;
        deletePiece(pos: number): void;
        editWeapon(): void;
        updateWeapon(): void;
        validate(): boolean;
    }
}
declare module LandPlayEdit {
    class PluginDragDrop extends LandPlayEdit.Plugin {
        constructor();
        getTemplateName(): string;
        behaviour(type: any): void;
        editRecord(type: string, pos: number): void;
        deleteRecord(type: string, pos: number): void;
        private hasAnswer(pos, answers);
        private loadRemainingSpritesForContainers(value?);
        private loadRemainingSpritesForAnswers(value?);
        private getUsedSpritesInContainers(answers?);
        openEvents(pos: string, subPos: string): any;
        getEventsCollection(container: number, subcontainer: number): any;
        openContainers(): void;
        createContainer(): void;
        editContainer(pos: number): void;
        updateContainer(): void;
        deleteContainer(pos: number): void;
        validateContainer(name: any): void;
        validate(): boolean;
    }
}
declare module LandPlayEdit {
    class PluginPaint extends LandPlayEdit.Plugin {
        private brushSelects;
        constructor();
        getTemplateName(): string;
        behaviour(type: any): void;
        editRecord(type: string, pos: number): void;
        deleteRecord(type: string, pos: number): void;
        private loadRemainingSpritesForColors(value?);
        private loadRemainingSpritesForCanvas(value?);
        private loadRemainingSpritesForWhatever(value?);
        private getUsedSpritesInCanvas();
        private loadColorSprites(value?);
        validateEvent(name: any): void;
        validateBrush(name: any): void;
        private hasColor(pos);
        private hasCanvas(pos);
        private hasItem(collection, pos);
        openCanvas(): void;
        createCanvas(): void;
        editCanvas(pos: number): void;
        deleteCanvas(pos: number): void;
        updateCanvas(): void;
        openColors(): void;
        createColor(): void;
        editColor(pos: number): void;
        updateColor(): void;
        deleteColor(pos: number): void;
        editBrush(): void;
        updateBrush(): void;
        validate(): boolean;
    }
}
declare module LandPlayEdit {
    class PluginPuzzle extends LandPlayEdit.Plugin {
        constructor();
        getTemplateName(): string;
        behaviour(type: any): void;
        private loadRemainingSpritesForTable(value?);
        private loadRemainingSpritesForPieces(value?);
        private getUsedSpritesInBoard(answers?);
        createBoard(): void;
        editBoard(): void;
        updateBoard(): void;
        deleteBoard(): void;
        validateBoard(name: any): void;
        validate(): boolean;
    }
}
declare module LandPlayEdit {
    class PluginSimon extends LandPlayEdit.Plugin {
        private padAttrSep;
        constructor();
        getTemplateName(): string;
        behaviour(type: any): void;
        private loadRemainingSpritesForTalk(value?);
        private loadRemainingSpritesForPads(value?);
        private getUsedSpritesInSimon();
        createSimon(): void;
        editSimon(): void;
        updateSimon(): void;
        validateSimon(name: any): void;
        validate(): boolean;
    }
}
declare module LandPlayEdit {
    class PluginType extends LandPlayEdit.Plugin {
        private FREE_TEXT;
        wordsGroup: Phaser.Group;
        constructor();
        getTemplateName(): string;
        behaviour(type: any): void;
        editRecord(type: string, pos: number): void;
        deleteRecord(type: string, pos: number): void;
        /**
         * Preview additional graphic elements
         * Launched from editor when Stage is ready
         * Override in each plugin
         ******* */
        preview(): void;
        private initWordPreview(sprite);
        private loadRemainingSpritesForWords(value?);
        private loadRemainingSpritesForWhatever(value?);
        private getUsedSpritesInWords();
        private loadWordSprites(value?);
        private hasWord(pos);
        private hasCanvas(pos);
        openWords(pos?: number): void;
        createWord(): void;
        editWord(pos: number): void;
        updateWord(): void;
        deleteWord(pos: number): void;
        validateWord(name: any): void;
        validate(): boolean;
    }
}
declare module LandPlayEdit {
    class PluginWordSearch extends LandPlayEdit.Plugin {
        constructor();
        getTemplateName(): string;
        behaviour(type: any): void;
        editRecord(type: string, pos: number): void;
        deleteRecord(type: string, pos: number): void;
        /**
         * Preview additional graphic elements
         * Launched from editor when Stage is ready
         * Override in each plugin
         ******* */
        preview(): void;
        private hasWord(pos);
        editTiles(): void;
        updateTiles(): void;
        private loadColors(csvcolors, fieldname);
        private loadRemainingSpritesForWords(value?);
        private loadRemainingSpritesForWhatever(value?);
        openWords(pos?: number): void;
        createWord(): void;
        editWord(pos: number): void;
        updateWord(): void;
        deleteWord(pos: number): void;
        private clearWords();
        private renderWord(params);
        validateWord(name: any): void;
        validate(): boolean;
    }
}
