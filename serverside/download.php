<?php
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/
$upload = array("status" => "err"); 
if(!empty($_REQUEST['url'])){ 
     
    // File upload configuration 
    $targetDir = "uploads/"; 
    
    // Initialize a file URL to the variable 
	$url = $_REQUEST['url']; 
	  
	// Use basename() function to return the base name of file  
	$file_name = basename($url); 
	   
	// Use file_get_contents() function to get the file 
	// from url and use file_put_contents() function to 
	// save the file by using base name 
	$content = file_get_contents($url);
	if ( $content ) {
		if(file_put_contents( $targetDir . $file_name, $content)) { 
		    $upload["status"] = 'ok';
		    $upload["filename"] = $file_name;
		} else {
			$upload["message"] = "alert.download.write";
		}
	} else {
		$upload["message"] = "alert.download.failed";
	}
} else {
	$upload["message"] = "alert.download.nourl";
}
 
echo json_encode($upload); 
?>
